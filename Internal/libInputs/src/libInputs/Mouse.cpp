#include <libInputs/Mouse.hpp>

namespace libInputs
{
	Mouse::Mouse()
		: m_absoluteMouseCoords(0, 0)
		, m_motion(0, 0)
		, m_wheel(MouseWheel::STALL)
		, m_mouseButtons()
	{
	}

	void Mouse::processEvent(SDL_MouseMotionEvent& e) 
	{
		m_absoluteMouseCoords = glm::ivec2(e.x, e.y);
		m_motion = glm::vec2(e.xrel, -e.yrel);
	}

	void Mouse::processEvent(SDL_MouseButtonEvent& e)
	{
		auto btnState = MouseButtonState::RELEASED;

		if (e.state == SDL_PRESSED)
		{
			btnState = MouseButtonState::PRESSED;
			if (e.clicks == 2)
				btnState = MouseButtonState::DOUBLE_PRESS;
		}
		m_mouseButtons[e.button] = btnState;
	}

	void Mouse::processEvent(SDL_MouseWheelEvent& e)
	{
		if (e.y == 1)
			m_wheel = MouseWheel::MOVEUP;
		
		else 
			m_wheel = MouseWheel::MOVEDOWN;
	}

	void Mouse::cleanMotion()
	{
		m_motion = glm::vec2(0.f, 0.f);
	}

	Mouse::MouseWheel Mouse::getWheel() const
	{
		return m_wheel;
	}

	Mouse::MouseButtonState Mouse::getButtonState(MouseButton btn) const
	{
		int key;
		switch (btn)
		{
			case MouseButton::BUTTON_LEFT: 
			{
				key = SDL_BUTTON_LEFT;
				break;
			}
			case MouseButton::BUTTON_MIDDLE: 
			{
				key = SDL_BUTTON_MIDDLE;
				break;
			}
			case MouseButton::BUTTON_RIGHT:
			{
				key = SDL_BUTTON_RIGHT;
				break;
			}
			case MouseButton::BUTTON_X1:
			{
				key = SDL_BUTTON_X1;
				break;
			}
			case MouseButton::BUTTON_X2:
			{
				key = SDL_BUTTON_X2;
				break;
			}
			default:
				break;
		}
		if (m_mouseButtons.count(key) > 0)
			return m_mouseButtons.at(key);

		return MouseButtonState::RELEASED;
	}

	glm::vec2 Mouse::getMotion() const
	{	
		return m_motion;
	}

	glm::ivec2 Mouse::getMouseCoords() const
	{
		return m_absoluteMouseCoords;
	}
}