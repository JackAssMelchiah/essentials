#include <libInputs/Keyboard.hpp>

namespace libInputs
{
	void Keyboard::processEvent(SDL_KeyboardEvent& e)
	{
		m_keyboard[e.keysym.sym] = bool(e.state);
	}

	bool Keyboard::getKeyState(SDL_Keycode k) const
	{
		if (m_keyboard.find(k) == m_keyboard.end())
			return false;
		return m_keyboard.at(k);
	}
}
