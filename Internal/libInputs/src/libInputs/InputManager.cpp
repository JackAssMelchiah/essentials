//! libInputs
#include <libInputs/InputManager.hpp>
#include <libInputs/Handler.hpp>
//! libUtils
#include <libUtils/Utils.hpp>

namespace libInputs
{
	InputManager::InputManager()
		: m_keyboard(std::make_unique<Keyboard>())
		, m_mouse(std::make_unique<Mouse>())
		, m_currentHandlerStatus(true)
	{
		SDL_GameControllerEventState(SDL_ENABLE);
	}

	bool InputManager::processAllInputs(bool disableHandlers)
	{
		auto event = SDL_Event();
		auto mouseMoved = false;
		auto focusChanged = false;


		if (disableHandlers != m_currentHandlerStatus)
		{
			m_currentHandlerStatus = disableHandlers;
			focusChanged = true;
		}

		while (SDL_PollEvent(&event)) 
		{
			processGUI(event);

			switch (event.type)
			{
				case(SDL_WINDOWEVENT):
					processWindowEvent(event.window);
					break;
				
				case(SDL_KEYDOWN):
					processKeyboardKey(event.key, disableHandlers);
					break;
			
				case(SDL_KEYUP):
					processKeyboardKey(event.key, disableHandlers);
					break;
			
				case(SDL_MOUSEMOTION):
					processMouseMotion(event.motion, disableHandlers);
					mouseMoved = true;
					break;
				
				case(SDL_MOUSEBUTTONDOWN):
					processMouseButton(event.button, disableHandlers);
					break;
				
				case(SDL_MOUSEBUTTONUP):
					processMouseButton(event.button, disableHandlers);
					break;
				
				case(SDL_MOUSEWHEEL):
					processMouseWheel(event.wheel, disableHandlers);
					break;
				
				/*case(SDL_JOYDEVICEADDED):
					Utils::PrintOut("new JOY");

					if (!newJoyDevice(event.jdevice.which))
						Utils::PrintOutWar("Unable to create new gamepad");
					
					break;
				
				case(SDL_JOYDEVICEREMOVED): 
					Utils::PrintOut("removing JOY");

					destroyJoyDevice(event.jdevice.which);
					break;
				*/
				case(SDL_CONTROLLERDEVICEADDED):
					if (!createNewGamePad(event.cdevice.which))
						libUtils::printOutWar("Unable to create new gamepad");
					break;
				
				case(SDL_CONTROLLERDEVICEREMOVED):
					libUtils::printOutWar("Removing gamepad");
					destroyGamePad(event.cdevice.which);
					break;
				
				case(SDL_CONTROLLERDEVICEREMAPPED):
					libUtils::printOutWar("Remmaping gamepad, which is currently not supported");
					break;
				
				case(SDL_CONTROLLERAXISMOTION):
					processGamepadAxisMotion(event.caxis);
					break;
				
				case(SDL_CONTROLLERBUTTONDOWN):
					processGamepadButton(event.cbutton);
					break;
				
				case(SDL_CONTROLLERBUTTONUP):
					processGamepadButton(event.cbutton);
					break;
				
				case(SDL_QUIT):
					return true;
				
				default:
					break;
			}
		}
		if (focusChanged)
		{
			for (auto& handler : m_handlers)
			{
				handler->processFocusChanged(m_currentHandlerStatus);
			}
		}

		// in case that input from the mouse has not been processed, we need to null the acceleration for this frame
		if (!mouseMoved)
			m_mouse->cleanMotion();

		return false;
	}

	void InputManager::addWindowEventCallback(std::function<void(SDL_WindowEvent&)> cb)
	{
		m_windowEventCallbacks.push_back(cb);
	}

	void InputManager::addKeyboardEventCallback(std::function<void(SDL_KeyboardEvent&)> cb)
	{
		m_keyEventCallbacks.push_back(cb);
	}

	void InputManager::addMouseButtonEventCallback(std::function<void(SDL_MouseButtonEvent&)> cb)
	{
		m_mouseBtnEventCallbacks.push_back(cb);
	}

	void InputManager::addMouseMotionCallback(std::function<void(SDL_MouseMotionEvent&)> cb)
	{
		m_mouseMotionEventCallbacks.push_back(cb);
	}

	void InputManager::addMouseWheelEventCallback(std::function<void(SDL_MouseWheelEvent&)> cb)
	{
		m_mouseWheelEventCallbacks.push_back(cb);
	}

	void InputManager::addGamepadAxisMotionEventCallback(std::function<void(SDL_ControllerAxisEvent&)> cb)
	{
		m_gamepadAxisMotion.push_back(cb);
	}

	void InputManager::addGamepadButtonCallback(std::function<void(SDL_ControllerButtonEvent&)> cb)
	{
		m_gamepadButton.push_back(cb);
	}

	void InputManager::setGuiCallback(std::function<void(SDL_Event&)> cb)
	{
		m_guiCallback = cb;
	}

	void InputManager::addHandler(std::shared_ptr<Handler> cameraHandler)
	{
		m_handlers.insert(cameraHandler);
	}

	void InputManager::removeHandler(std::shared_ptr<Handler> cameraHandler)
	{
		if (m_handlers.count(cameraHandler) > 0)
			m_handlers.erase(cameraHandler);
	}
	
	Mouse& InputManager::getMouse()
	{
		return *m_mouse;
	}

	Keyboard& InputManager::getKeyboard()
	{
		return *m_keyboard;
	}

	glm::vec2 InputManager::getNormalizedDeviceCoords(glm::uvec2 const& sdlPoint, glm::uvec2 const& resolution) const
	{ 
		return glm::vec2(((float)sdlPoint.x / (float)resolution.x - 0.5f) * 2.f, ((float)sdlPoint.y / (float)resolution.y - 0.5f) * 2.f);
		/*return glm::vec2((2 * float(sdlPoint.x) / resolution.x) - 1, 1 - (2 * float(sdlPoint.y) / resolution.y));*/
	}

	void InputManager::processWindowEvent(SDL_WindowEvent& e)
	{
		for (auto& cb : m_windowEventCallbacks)
		{
			cb(e);
		}
	}

	void InputManager::processGUI(SDL_Event& e)
	{
		if (m_guiCallback)
			m_guiCallback(e);
	}

	void InputManager::processKeyboardKey(SDL_KeyboardEvent& e, bool disableHandlers)
	{
		m_keyboard->processEvent(e);
		if (disableHandlers)
			return;
		for (auto& handler : m_handlers)
		{
			handler->processKeyboardKey(e, *m_keyboard, *m_mouse);
		}

		for (auto& cb : m_keyEventCallbacks)
		{
			cb(e);
		}
	}

	void InputManager::processMouseMotion(SDL_MouseMotionEvent& e, bool disableHandlers)
	{
		m_mouse->processEvent(e);
		if (disableHandlers)
			return;
		for (auto& handler : m_handlers)
		{
			handler->processMouseMotion(e, *m_keyboard, *m_mouse);
		}

		for (auto& cb : m_mouseMotionEventCallbacks)
		{
			cb(e);
		}
	}

	void InputManager::processMouseButton(SDL_MouseButtonEvent& e, bool disableHandlers)
	{
		m_mouse->processEvent(e);
		if (disableHandlers)
			return;
		for (auto& handler : m_handlers)
		{
			handler->processMouseButton(e, *m_keyboard, *m_mouse);
		}

		for (auto& cb : m_mouseBtnEventCallbacks)
		{
			cb(e);
		}
	}

	void InputManager::processMouseWheel(SDL_MouseWheelEvent& e, bool disableHandlers)
	{
		m_mouse->processEvent(e);
		if (disableHandlers)
			return;
		for (auto& handler : m_handlers)
		{
			handler->processMouseWheel(e, *m_keyboard, *m_mouse);
		}

		for (auto& cb : m_mouseWheelEventCallbacks)
		{
			cb(e);
		}
	}

	void InputManager::processGamepadAxisMotion(SDL_ControllerAxisEvent& e)
	{
		auto& gamepad = m_gamepads.at(e.which);
		gamepad->processEvent(e);
		for (auto& handler : m_handlers)
		{
			handler->processGamepadAxisMotion(e, *gamepad);
		}

		for (auto& cb : m_gamepadAxisMotion)
		{
			cb(e);
		}
	}

	void InputManager::processGamepadButton(SDL_ControllerButtonEvent& e)
	{
		auto& gamepad = m_gamepads.at(e.which);
		gamepad->processEvent(e);
		gamepad->rumble(1.f, 1000); // debug
		for (auto& handler : m_handlers)
		{
			handler->processGamepadButton(e, *gamepad);
		}

		for (auto& cb : m_gamepadButton)
		{
			cb(e);
		}
	}

	bool InputManager::createNewGamePad(int controllerID)
	{
		SDL_GameController* controller = SDL_GameControllerOpen(controllerID);
		if (controller == nullptr)
			return false;
		
		m_gamepads.insert(std::pair<int, std::unique_ptr<Gamepad>>(controllerID, std::make_unique<Gamepad>(controller)));

		auto& gamepad = m_gamepads.at(controllerID);
		gamepad->setDeadZoneAxis(0.0f);
		gamepad->setDeadZoneTrigger(0.0f);

		libUtils::printOut(std::string("CONTROLLER") + std::string{ SDL_GameControllerNameForIndex(controllerID) } + ("ID :") + std::to_string(controllerID));
		return true;
	}

	bool InputManager::destroyGamePad(int controllerID)
	{
		if (m_gamepads.count(controllerID) > 0)
		{
			m_gamepads.erase(controllerID);
			return true;
		}
		return false;
	}

	bool InputManager::newJoyDevice(int joyID) 
	{
		return false;
	}

	bool InputManager::destroyJoyDevice(int joyID)
	{
		return false;
	}
}

