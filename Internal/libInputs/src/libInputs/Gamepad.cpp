#include <libInputs/InputManager.hpp>
#include <algorithm>

namespace libInputs {

	Gamepad::Gamepad(SDL_GameController* controller)
		: m_sdlSelf(controller)
		, m_deadzoneTriggers(0)
		, m_deadzoneAxis(0)
		, m_hapticSelf(nullptr)
		, m_sdlMaxDeadzoneValue(32766.f)
	{
		//try to open it as haptic
		SDL_Haptic* haptic = SDL_HapticOpenFromJoystick(SDL_GameControllerGetJoystick(m_sdlSelf));
		if (SDL_HapticRumbleSupported(haptic) == SDL_TRUE)
		{
			m_hapticSelf = haptic;
			if (SDL_HapticRumbleInit(haptic) == -1)
			{
				SDL_HapticClose(m_hapticSelf);
				m_hapticSelf = nullptr;
			}
		}
	}

	Gamepad::~Gamepad() 
	{
		if (m_hapticSelf != nullptr)
			SDL_HapticClose(m_hapticSelf);
		
		if (m_sdlSelf != nullptr)
			SDL_GameControllerClose(m_sdlSelf);
		
	}

	void Gamepad::rumble(float percent, int duration)
	{
		assert(m_hapticSelf);
		SDL_HapticRumblePlay(m_hapticSelf, percent, duration);
	}

	void Gamepad::processEvent(SDL_ControllerAxisEvent& e) 
	{
		m_axis[(SDL_GameControllerAxis)e.axis] = e.value;
	}

	void Gamepad::processEvent(SDL_ControllerButtonEvent& e)
	{
		m_buttons[e.button] = bool(e.state);
	}

	void Gamepad::setDeadZoneTrigger(float percent) 
	{
		m_deadzoneTriggers = int(m_sdlMaxDeadzoneValue * glm::clamp(percent, 0.f, 1.f));
	}

	float Gamepad::getDeadZoneTrigger() const 
	{
		return m_deadzoneTriggers / m_sdlMaxDeadzoneValue;
	}

	void Gamepad::setDeadZoneAxis(float percent) 
	{
		m_deadzoneAxis = int(m_sdlMaxDeadzoneValue * glm::clamp(percent, 0.f, 1.f));
	}

	float Gamepad::getDeadZoneAxis() const
	{
		return m_deadzoneAxis / m_sdlMaxDeadzoneValue;
	}

	glm::vec3 Gamepad::getAxisValue() const 
	{
		//TODO: test this - seems like oposites (right and left) are allways stored separatelly
		// this code should instead merge oposites into single number, either - or +, representing direction rather than return 6 nums
		auto result = glm::vec3(0.f);
		for (int axis = 0; axis < SDL_GameControllerAxis::SDL_CONTROLLER_AXIS_MAX; ++axis)
		{
			float val = m_axis.at(axis);
				
			if (axis % 2)
			{
				val = std::clamp(val, -(val + m_deadzoneAxis) / (m_sdlMaxDeadzoneValue + m_deadzoneAxis), 0.f);
				result[axis / 2] += val;
			}
			else
			{
				val = std::clamp(val, 0.f, (val - m_deadzoneAxis) / (m_sdlMaxDeadzoneValue - m_deadzoneAxis));
				result[axis / 2] += val;
			}
		}
		return result;
	}
}