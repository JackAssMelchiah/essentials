#pragma once
//! std
#include <map>
//! sdl
#include <SDL.h>

namespace libInputs
{
    class Keyboard 
    {
    public:
        friend class InputManager;
        Keyboard() = default;
        ~Keyboard() = default;
        //! Querries key from keyboard, true for presed, false for released
        bool getKeyState(SDL_Keycode k) const;
    
    protected:
        void processEvent(SDL_KeyboardEvent&);

    protected:
        std::map<SDL_Keycode, bool> m_keyboard;
    };
}