#pragma once
//! sdl
#include <SDL.h>
//! std
#include <memory>
#include <map>
#include <set>
#include <functional>
//! libInputs
#include <libInputs/Gamepad.hpp>
#include <libInputs/Keyboard.hpp>
#include <libInputs/Mouse.hpp>

namespace libInputs
{
	class Handler;
}

namespace libInputs
{
	class InputManager 
	{
	public: 
		enum class InputType {WINDOW, KEYBOARD, MOUSE_MOTION, MOUSE_BUTTON, MOUSE_WHEEL};

	public:
		InputManager();
		~InputManager() = default;

		//! Processes all inputs
		bool processAllInputs(bool disableHandlers);

		//! Callback setters
		void addWindowEventCallback(std::function<void(SDL_WindowEvent&)> cb);
		void addKeyboardEventCallback(std::function<void(SDL_KeyboardEvent&)> cb);
		void addMouseButtonEventCallback(std::function<void(SDL_MouseButtonEvent&)> cb);
		void addMouseMotionCallback(std::function<void(SDL_MouseMotionEvent&)> cb);
		void addMouseWheelEventCallback(std::function<void(SDL_MouseWheelEvent&)> cb);
		void addGamepadAxisMotionEventCallback(std::function<void(SDL_ControllerAxisEvent&)> cb);
		void addGamepadButtonCallback(std::function<void(SDL_ControllerButtonEvent&)> cb);

		//! Callback for gui
		void setGuiCallback(std::function<void(SDL_Event&)> cb);

		//! Adds camera handler, all user event will get processed and update given camera, multiple cameras can be handled by single handler
		void addHandler(std::shared_ptr<Handler> cameraHandler);
		//! Removes camera handler
		void removeHandler(std::shared_ptr<Handler> cameraHandler);

		//! Subsystem getters
		Mouse& getMouse();
		Keyboard& getKeyboard();

		glm::vec2 getNormalizedDeviceCoords(glm::uvec2 const& sdlPoint, glm::uvec2 const& resolution) const;

	protected:
		void processWindowEvent(SDL_WindowEvent&);
		void processGUI(SDL_Event& e);
		void processKeyboardKey(SDL_KeyboardEvent&, bool disableHandlers);
		void processMouseMotion(SDL_MouseMotionEvent&, bool disableHandlers);
		void processMouseButton(SDL_MouseButtonEvent&, bool disableHandlers);
		void processMouseWheel(SDL_MouseWheelEvent&, bool disableHandlers);
		void processGamepadAxisMotion(SDL_ControllerAxisEvent&);
		void processGamepadButton(SDL_ControllerButtonEvent&);

		bool createNewGamePad(int controllerID);
		bool destroyGamePad(int controllerID);
		bool newJoyDevice(int joyID);
		bool destroyJoyDevice(int joyID);

	protected:
		//! Allocated subsystems
		std::map<int, std::unique_ptr<Gamepad>> m_gamepads;
		std::unique_ptr<Mouse> m_mouse;
		std::unique_ptr<Keyboard> m_keyboard;

		//! Handlers
		std::set<std::shared_ptr<Handler>> m_handlers;
		
		//! Stored callbacks
		std::vector<std::function<void(SDL_WindowEvent&)>> m_windowEventCallbacks;
		std::vector<std::function<void(SDL_KeyboardEvent&)>> m_keyEventCallbacks;
		std::vector<std::function<void(SDL_MouseButtonEvent&)>> m_mouseBtnEventCallbacks;
		std::vector<std::function<void(SDL_MouseMotionEvent&)>> m_mouseMotionEventCallbacks;
		std::vector<std::function<void(SDL_MouseWheelEvent&)>> m_mouseWheelEventCallbacks;
		std::vector<std::function<void(SDL_ControllerAxisEvent&)>> m_gamepadAxisMotion;
		std::vector<std::function<void(SDL_ControllerButtonEvent&)>> m_gamepadButton;

		//! Stored callbacks
		std::function<void(SDL_Event&)> m_guiCallback;
		//! Stored handler status
		bool m_currentHandlerStatus;
	};
}