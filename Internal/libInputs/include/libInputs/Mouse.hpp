#pragma once
//! libUtils
#include <libUtils/Time.hpp>
//! sdl
#include <SDL.h>
//! glm
#include <glm/glm.hpp>
//! std
#include <map>

namespace libInputs
{
	class Mouse
	{
		friend class InputManager;
	public:
		enum class MouseWheel { MOVEUP, MOVEDOWN, STALL };
		enum class MouseButton { BUTTON_LEFT, BUTTON_MIDDLE, BUTTON_RIGHT, BUTTON_X1, BUTTON_X2 };
		enum class MouseButtonState { PRESSED, RELEASED, DOUBLE_PRESS };

	public:
		//! Constructs mouse with maximal acceleration 
		Mouse();
		~Mouse() = default;

		//! Mouse wheel getter
		MouseWheel getWheel() const;
		//! Mouse buttons getter
		MouseButtonState getButtonState(MouseButton) const;
		//! Returns acceleration of mouse
		glm::vec2 getMotion() const;
		//! Returns position of the mouse
		glm::ivec2 getMouseCoords() const;
				
	protected:
		void processEvent(SDL_MouseMotionEvent& event);
		void processEvent(SDL_MouseButtonEvent& event);
		void processEvent(SDL_MouseWheelEvent& event);
		//! Sets motion to be 0 in both axies
		void cleanMotion();

	protected:
		//! Captured mouse state so they can be querried at any time 
		std::map<int, MouseButtonState> m_mouseButtons;
		//! Captured wheel state
		MouseWheel m_wheel;
		//! Mouse coordinates within window
		glm::ivec2 m_absoluteMouseCoords;
		//! Stored relative mouse movement
		glm::vec2 m_motion;
	};
}
