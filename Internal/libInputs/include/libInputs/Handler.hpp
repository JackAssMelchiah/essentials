#pragma once
//! sdl
#include <SDL.h>
//! glm
#include <glm/glm.hpp>
//! std
#include <memory>
#include <set>

namespace libInputs
{
	class InputManager;
	class Keyboard;
	class Mouse;
	class Gamepad;
}

namespace libInputs
{
	class Handler
	{
		friend class InputManager;

	public:
		Handler() = default;
		virtual ~Handler() = default;
		//! Returns view matrix which will be used by camera
		virtual void handle(glm::mat4& viewmatrix) = 0;
		//! ViewPort was resized
		virtual void resize(glm::ivec2 const& viewport) = 0;
		//! Allows to control and set parameters to handlers directly
		virtual void set(glm::vec3 position, glm::vec3 viewDirection) = 0;

	protected:
		virtual void processKeyboardKey(SDL_KeyboardEvent const& keyboardEvent, Keyboard const& keyboard, Mouse const& mouse) = 0;
		virtual void processMouseMotion(SDL_MouseMotionEvent const& mouseMotion, Keyboard const& keyboard, Mouse const& mouse) = 0;
		virtual void processMouseButton(SDL_MouseButtonEvent const& mouseButton, Keyboard const& keyboard, Mouse const& mouse) = 0;
		virtual void processMouseWheel(SDL_MouseWheelEvent const& mouseWheel, Keyboard const& keyboard, Mouse const& mouse) = 0;
		virtual void processGamepadAxisMotion(SDL_ControllerAxisEvent const& controllerAxis, Gamepad const& gamepad) = 0;
		virtual void processGamepadButton(SDL_ControllerButtonEvent const& controllerButton, Gamepad const& gamepad) = 0;
		//! Fires when focus of scene is lost (true) or gained (false)
		virtual void processFocusChanged(bool focusLost) = 0;
	};
}