#pragma once
//! std
#include <map>
#include <string>
//! sdl
#include <SDL.h>
//! glm
#include <glm/glm.hpp>

namespace libInputs 
{
	class Gamepad 
	{
	public:
		friend class InputManager;
		Gamepad(SDL_GameController*);
		~Gamepad();

		//! Sets deadzone axis in 0-1 range of the interval
		void setDeadZoneAxis(float percent);
		//! Gets deadzone axis in 0-1 range of the interval
		float getDeadZoneAxis() const;

		//! Sets deadzone of triggers in 0-1 range of the interval
		void setDeadZoneTrigger(float percent);
		//! Gets deadzone of triggers in 0-1 range of the interval
		float getDeadZoneTrigger() const;
		
		//! Returns chosen axis's value in range 0-1
		glm::vec3 getAxisValue() const;

		//! Sets gamepad to rumble for specific duration in ms at %
		void rumble(float, int);

	protected:
		//! Processes axis movement - Axis is defined by number in range <-32766; 32767>
		void processEvent(SDL_ControllerAxisEvent&);
		//! Processes button press
		void processEvent(SDL_ControllerButtonEvent&);
			
	protected:
		SDL_GameController* m_sdlSelf;
		SDL_Haptic* m_hapticSelf;

		int m_deadzoneAxis;
		int m_deadzoneTriggers;

		std::map<int, bool> m_buttons;
		std::map<int, int> m_axis;

		float m_sdlMaxDeadzoneValue;
	};
}