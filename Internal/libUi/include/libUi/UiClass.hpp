#pragma once
//! Std
#include <string>

namespace libUi
{
	class UiClassManager;
}

namespace libUi 
{
	//! Interface for any class that wants to show UI
	class UiClass
	{
		friend class UiClassManager;
	public:
		UiClass() = default;
		~UiClass() = default;
		//! Dialog's title
		virtual std::string getTitle() const = 0;

	protected:
		//! Gets called when class should render its UI 
		virtual void recordUi() = 0;
	};
}