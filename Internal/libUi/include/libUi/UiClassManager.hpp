#pragma once
//! libUi
#include <libUi/UiClass.hpp>
//! libVulkan
#include <libVulkan/VulkanObject.hpp>
//! SDL
#include <SDL_events.h>
//! Std
#include <memory>
#include <string>
#include <vector>
#include <set>

namespace libVulkan
{
	class VulkanInstance;
	class VulkanDevice;
	class VulkanDescriptorPool;
	class VulkanImageAttachment;
	class VulkanCommandPool;
	class VulkanCommandBuffer;
	class VulkanQueue;
	class VulkanFence;
	class VulkanSemaphore;
}

namespace libUi
{	
	class UiClassManager
	{
	public:
		UiClassManager() = default;
		~UiClassManager();

		//! initializes this manager
		bool initialize(std::shared_ptr<libVulkan::VulkanInstance> const& instance, std::shared_ptr<libVulkan::VulkanDevice> const& device, std::shared_ptr<libVulkan::VulkanQueue> const& graphicsQue, SDL_Window& window);
		//! Adds class to manager to be rendered
		bool addUiClass(std::shared_ptr<UiClass> uiDrawable);
		//! Removes class from list, so it wont get rendered
		bool removeUiClass(std::shared_ptr<UiClass> uiDrawable);
		//! Renders the ui to passed framebuffer
		void render(std::shared_ptr<libVulkan::VulkanImageAttachment> const& colorOutput, vk::Extent2D const& resoultion, std::vector<std::reference_wrapper<libVulkan::VulkanSemaphore>> const& waitSemaphores = {});
		//! Semaphore which gets signaled when Ui is done rendering
		libVulkan::VulkanSemaphore& getRenderingFinishedSemaphore();
		//! Check whether mouse is over any ui
		bool mouseHoversOverUi() const;

		void processEvent(SDL_Event& event);

	protected:
		//! Cool style
		void applyStyleExtasy();
		//! Builds descriptor pool
		std::shared_ptr<libVulkan::VulkanDescriptorPool> createDescriptorPool();
		//! Builds command pool
		std::shared_ptr<libVulkan::VulkanCommandPool> createCommandPool();
		//! Submits the recorded buffer to graphical queue
		void submit(libVulkan::VulkanCommandBuffer& commandBuffer, std::vector<std::reference_wrapper<libVulkan::VulkanSemaphore>> const& waitSemaphores = {});
		//! For submiting command buffers which are for initialization purposes (forces que sync)
		void submitOnce(libVulkan::VulkanCommandBuffer& commandBuffer);

	protected:
		//! Stored device
		std::weak_ptr<libVulkan::VulkanDevice> m_device;
		std::weak_ptr<libVulkan::VulkanInstance> m_instance;
		//! Pools
		std::shared_ptr<libVulkan::VulkanDescriptorPool> m_descriptorPool;
		std::shared_ptr<libVulkan::VulkanCommandPool> m_commandPool;
		//! Allocated resources from pools
		std::weak_ptr<libVulkan::VulkanCommandBuffer> m_commandRenderBuffer;
		//! Rendering queue
		std::weak_ptr<libVulkan::VulkanQueue> m_queue;
		//! Fence
		std::shared_ptr<libVulkan::VulkanFence> m_fence;
		//! Semaphore which signals that rendering is done
		std::shared_ptr<libVulkan::VulkanSemaphore> m_semaphoreRenderingDone;
		//! Uis which will be rendered
		std::set<std::shared_ptr<UiClass>> m_uis;
	};
}
