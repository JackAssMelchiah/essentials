//! libUi
#include <libUi/UiClassManager.hpp>
//! libVulkan
#include <libVulkan/VulkanAll.hpp>
#include <libVulkan/UtilFunctions.hpp>
//! Imgui
#include <Imgui/imgui.h>
#include <Imgui/imgui_impl_sdl2.h>
#include <Imgui/imgui_impl_vulkan.h>
//! glm
#include <glm/glm.hpp>

namespace libUi 
{	
	UiClassManager::~UiClassManager()
	{
		ImGui_ImplVulkan_Shutdown();
	}

	//! initializes this manager
	bool UiClassManager::initialize(std::shared_ptr<libVulkan::VulkanInstance> const& instance, std::shared_ptr<libVulkan::VulkanDevice> const& device, std::shared_ptr<libVulkan::VulkanQueue> const& graphicsQue, SDL_Window& window)
	{
		m_device = device;
		m_instance = instance;
		m_queue = graphicsQue;

		m_descriptorPool = createDescriptorPool();
		m_commandPool = createCommandPool();
		m_commandRenderBuffer = m_commandPool->createCommandBuffer(vk::CommandBufferLevel::ePrimary, "Ui primary CB");
		m_fence = std::make_shared<libVulkan::VulkanFence>(instance, device, "uiFence");
		m_semaphoreRenderingDone = std::make_shared<libVulkan::VulkanSemaphore>(instance, device, "Ui rendering Done");

		std::vector<vk::Format> inputs;
		vk::Format depthStencilFormat;
		std::vector<vk::Format> outputs;

		libVulkan::VulkanRenderingInfo::selectSimplePassFormats(*instance, inputs, depthStencilFormat, outputs);

		assert(outputs.size() == 1);
			
		auto renderingCreateInfo = vk::PipelineRenderingCreateInfo();
		{
			renderingCreateInfo.colorAttachmentCount = outputs.size();
			renderingCreateInfo.pColorAttachmentFormats = outputs.data();
		}

		auto imguiCreateInfo = ImGui_ImplVulkan_InitInfo();
		{
			imguiCreateInfo.DescriptorPool = m_descriptorPool->operator()();
			imguiCreateInfo.Device = device->operator()();
			imguiCreateInfo.ImageCount = instance->getDefaults().windowSamples;
			imguiCreateInfo.Instance = instance->operator()();
			imguiCreateInfo.MinImageCount = instance->getDefaults().windowSamples;
			imguiCreateInfo.MSAASamples = VkSampleCountFlagBits::VK_SAMPLE_COUNT_1_BIT;
			imguiCreateInfo.PhysicalDevice = m_instance.lock()->getRawPhysicalDevice();
			//imguiCreateInfo.PipelineCache;
			//imguiCreateInfo.QueueFamily = ;
			imguiCreateInfo.Queue = m_queue.lock()->operator()();
			imguiCreateInfo.UseDynamicRendering = true;
			imguiCreateInfo.PipelineRenderingCreateInfo = renderingCreateInfo.operator const VkPipelineRenderingCreateInfo & ();
			//imguiCreateInfo.DescriptorPool = ;
		}

		//! uses imgui
		// Setup Dear ImGui context
		IMGUI_CHECKVERSION();
		ImGui::CreateContext();
		ImGui_ImplSDL2_InitForVulkan(&window);
		ImGui_ImplVulkan_Init(&imguiCreateInfo);
		
		ImGuiIO& io = ImGui::GetIO(); (void)io;
		//io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;     // Enable Keyboard Controls
		//io.ConfigFlags |= ImGuiConfigFlags_NavEnableGamepad;      // Enable Gamepad Controls

		// Setup Dear ImGui style
		ImGui::StyleColorsDark();
		//ImGui::StyleColorsClassic();

		applyStyleExtasy();

		{//buffer imgui data to GPU
			auto commandBuffer = m_commandRenderBuffer.lock();
			commandBuffer->reset(true);
			commandBuffer->beginRecord(vk::CommandBufferUsageFlagBits::eOneTimeSubmit);
			ImGui_ImplVulkan_CreateFontsTexture();
			commandBuffer->endRecord();
			submitOnce(*commandBuffer);
		}

		//cpu backing of uploaded objects can be destroyed now
		//ImGui_ImplVulkan_DestroyFontUploadObjects();
		return true;
	}

	libVulkan::VulkanSemaphore& UiClassManager::getRenderingFinishedSemaphore()
	{
		return *m_semaphoreRenderingDone;
	}

	bool UiClassManager::mouseHoversOverUi() const
	{
		ImGuiIO& io = ImGui::GetIO();
		return io.WantCaptureMouse;
	}

	void UiClassManager::processEvent(SDL_Event& event)
	{
		ImGui_ImplSDL2_ProcessEvent(&event);
	}

	void UiClassManager::applyStyleExtasy()
	{
		ImGuiIO& io = ImGui::GetIO();
		ImGuiStyle* style = &ImGui::GetStyle();

		style->WindowPadding = ImVec2(15, 15);
		style->WindowRounding = 5.0f;
		style->FramePadding = ImVec2(5, 5);
		style->FrameRounding = 4.0f;
		style->ItemSpacing = ImVec2(12, 8);
		style->ItemInnerSpacing = ImVec2(8, 6);
		style->IndentSpacing = 25.0f;
		style->ScrollbarSize = 15.0f;
		style->ScrollbarRounding = 9.0f;
		style->GrabMinSize = 5.0f;
		style->GrabRounding = 3.0f;

		style->Colors[ImGuiCol_Text] = ImVec4(0.80f, 0.80f, 0.83f, 1.00f);
		style->Colors[ImGuiCol_TextDisabled] = ImVec4(0.24f, 0.23f, 0.29f, 1.00f);
		style->Colors[ImGuiCol_WindowBg] = ImVec4(0.06f, 0.05f, 0.07f, 1.00f);
		//style->Colors[ImGuiCol_ChildWindowBg] = ImVec4(0.07f, 0.07f, 0.09f, 1.00f);
		style->Colors[ImGuiCol_PopupBg] = ImVec4(0.07f, 0.07f, 0.09f, 1.00f);
		style->Colors[ImGuiCol_Border] = ImVec4(0.80f, 0.80f, 0.83f, 0.88f);
		style->Colors[ImGuiCol_BorderShadow] = ImVec4(0.92f, 0.91f, 0.88f, 0.00f);
		style->Colors[ImGuiCol_FrameBg] = ImVec4(0.10f, 0.09f, 0.12f, 1.00f);
		style->Colors[ImGuiCol_FrameBgHovered] = ImVec4(0.24f, 0.23f, 0.29f, 1.00f);
		style->Colors[ImGuiCol_FrameBgActive] = ImVec4(0.56f, 0.56f, 0.58f, 1.00f);
		style->Colors[ImGuiCol_TitleBg] = ImVec4(0.10f, 0.09f, 0.12f, 1.00f);
		style->Colors[ImGuiCol_TitleBgCollapsed] = ImVec4(1.00f, 0.98f, 0.95f, 0.75f);
		style->Colors[ImGuiCol_TitleBgActive] = ImVec4(0.07f, 0.07f, 0.09f, 1.00f);
		style->Colors[ImGuiCol_MenuBarBg] = ImVec4(0.10f, 0.09f, 0.12f, 1.00f);
		style->Colors[ImGuiCol_ScrollbarBg] = ImVec4(0.10f, 0.09f, 0.12f, 1.00f);
		style->Colors[ImGuiCol_ScrollbarGrab] = ImVec4(0.80f, 0.80f, 0.83f, 0.31f);
		style->Colors[ImGuiCol_ScrollbarGrabHovered] = ImVec4(0.56f, 0.56f, 0.58f, 1.00f);
		style->Colors[ImGuiCol_ScrollbarGrabActive] = ImVec4(0.06f, 0.05f, 0.07f, 1.00f);
		//style->Colors[ImGuiCol_ComboBg] = ImVec4(0.19f, 0.18f, 0.21f, 1.00f);
		style->Colors[ImGuiCol_CheckMark] = ImVec4(0.80f, 0.80f, 0.83f, 0.31f);
		style->Colors[ImGuiCol_SliderGrab] = ImVec4(0.80f, 0.80f, 0.83f, 0.31f);
		style->Colors[ImGuiCol_SliderGrabActive] = ImVec4(0.06f, 0.05f, 0.07f, 1.00f);
		style->Colors[ImGuiCol_Button] = ImVec4(0.10f, 0.09f, 0.12f, 1.00f);
		style->Colors[ImGuiCol_ButtonHovered] = ImVec4(0.24f, 0.23f, 0.29f, 1.00f);
		style->Colors[ImGuiCol_ButtonActive] = ImVec4(0.56f, 0.56f, 0.58f, 1.00f);
		style->Colors[ImGuiCol_Header] = ImVec4(0.10f, 0.09f, 0.12f, 1.00f);
		style->Colors[ImGuiCol_HeaderHovered] = ImVec4(0.56f, 0.56f, 0.58f, 1.00f);
		style->Colors[ImGuiCol_HeaderActive] = ImVec4(0.06f, 0.05f, 0.07f, 1.00f);
		//style->Colors[ImGuiCol_Column] = ImVec4(0.56f, 0.56f, 0.58f, 1.00f);
		//style->Colors[ImGuiCol_ColumnHovered] = ImVec4(0.24f, 0.23f, 0.29f, 1.00f);
		//style->Colors[ImGuiCol_ColumnActive] = ImVec4(0.56f, 0.56f, 0.58f, 1.00f);
		style->Colors[ImGuiCol_ResizeGrip] = ImVec4(0.00f, 0.00f, 0.00f, 0.00f);
		style->Colors[ImGuiCol_ResizeGripHovered] = ImVec4(0.56f, 0.56f, 0.58f, 1.00f);
		style->Colors[ImGuiCol_ResizeGripActive] = ImVec4(0.06f, 0.05f, 0.07f, 1.00f);
		//style->Colors[ImGuiCol_CloseButton] = ImVec4(0.40f, 0.39f, 0.38f, 0.16f);
		//style->Colors[ImGuiCol_CloseButtonHovered] = ImVec4(0.40f, 0.39f, 0.38f, 0.39f);
		//style->Colors[ImGuiCol_CloseButtonActive] = ImVec4(0.40f, 0.39f, 0.38f, 1.00f);
		style->Colors[ImGuiCol_PlotLines] = ImVec4(0.40f, 0.39f, 0.38f, 0.63f);
		style->Colors[ImGuiCol_PlotLinesHovered] = ImVec4(0.25f, 1.00f, 0.00f, 1.00f);
		style->Colors[ImGuiCol_PlotHistogram] = ImVec4(0.40f, 0.39f, 0.38f, 0.63f);
		style->Colors[ImGuiCol_PlotHistogramHovered] = ImVec4(0.25f, 1.00f, 0.00f, 1.00f);
		style->Colors[ImGuiCol_TextSelectedBg] = ImVec4(0.25f, 1.00f, 0.00f, 0.43f);

		//for some reason after win update? theese fonts cannot be loaded
		//io.Fonts->AddFontFromFileTTF(("C:/Windows/Fonts/Ruda-Bold.ttf"), 12);
		//io.Fonts->AddFontFromFileTTF(("C:/Windows/Fonts/Ruda-Bold.ttf"), 10);
		//io.Fonts->AddFontFromFileTTF(("C:/Windows/Fonts/Ruda-Bold.ttf"), 14);
		//io.Fonts->AddFontFromFileTTF(("C:/Windows/Fonts/Ruda-Bold.ttf"), 18);
	}

	std::shared_ptr<libVulkan::VulkanDescriptorPool> UiClassManager::createDescriptorPool()
	{
		auto allocations = std::vector<vk::DescriptorPoolSize>{
			{ vk::DescriptorType::eCombinedImageSampler, 100 },
			{ vk::DescriptorType::eInputAttachment, 100 },
			{ vk::DescriptorType::eSampledImage, 100 },
			{ vk::DescriptorType::eSampler, 100 },
			{ vk::DescriptorType::eStorageBuffer, 100 },
			{ vk::DescriptorType::eStorageBufferDynamic, 100},
			{ vk::DescriptorType::eStorageImage, 100 },
			{ vk::DescriptorType::eUniformBuffer, 100 },
			{ vk::DescriptorType::eUniformBufferDynamic, 100 }
		};

		return std::make_shared<libVulkan::VulkanDescriptorPool>(m_instance.lock(), m_device.lock(), allocations, true, "UIDescriptorPool", false);
	}

	std::shared_ptr<libVulkan::VulkanCommandPool> UiClassManager::createCommandPool()
	{
		auto que = m_queue.lock();
		return std::make_shared<libVulkan::VulkanCommandPool>(m_instance.lock(), m_device.lock(), *que, true, true, "UICommandPoolPool");
	}

	void UiClassManager::submit(libVulkan::VulkanCommandBuffer& commandBuffer, std::vector<std::reference_wrapper<libVulkan::VulkanSemaphore>> const& waitSemaphores)
	{
		auto que = m_queue.lock();
		// submit the recorded work to commandBuffer - but waints for image's availability within swapchain as it is rendering destination, and will notify GPU and CPU that rendering is complete-;
		que->submit({ commandBuffer }, { *m_semaphoreRenderingDone }, waitSemaphores, {vk::PipelineStageFlagBits::eBottomOfPipe}, *m_fence);
		que->wait();
	}

	void UiClassManager::submitOnce(libVulkan::VulkanCommandBuffer& commandBuffer)
	{
		auto que = m_queue.lock();
		// submit the recorded work to commandBuffer - but waints for image's availability within swapchain as it is rendering destination, and will notify GPU and CPU that rendering is complete-;
		que->submit({ commandBuffer });
		que->wait();
	}

	//! Adds pointer to class which's ui will get rendered
	bool UiClassManager::addUiClass(std::shared_ptr<UiClass> uiDrawable)
	{
		m_uis.insert(uiDrawable);
		return true;
	}

	//! Removes class'ses ui from rendering
	bool UiClassManager::removeUiClass(std::shared_ptr<UiClass> uiDrawable)
	{
		if (m_uis.count(uiDrawable) < 1)
		{
			m_uis.erase(uiDrawable);
			return true;
		}
		return false;
	}
		
	void UiClassManager::render(std::shared_ptr<libVulkan::VulkanImageAttachment> const& colorOutputAttachment, vk::Extent2D const& resoultion, std::vector<std::reference_wrapper<libVulkan::VulkanSemaphore>> const& waitSemaphores)
	{
		m_fence->wait();

		ImGui_ImplVulkan_NewFrame();
		ImGui_ImplSDL2_NewFrame();
		ImGui::NewFrame();

		//record all uis
		for (auto& uiClass: m_uis)
		{
			ImGui::Begin(uiClass->getTitle().c_str());
			uiClass->recordUi();
			ImGui::End();
		}
		ImGui::EndFrame();
		//now render the whole ui to screen
		ImGui::Render();

		//Ui gets re-recorded every frame since its blackbox
		auto commandBuffer = m_commandRenderBuffer.lock();
		// therefore clean the command buffer just to be safe
		commandBuffer->reset(true);
		commandBuffer->beginRecord(vk::CommandBufferUsageFlagBits::eOneTimeSubmit);
		commandBuffer->beginRendering({ colorOutputAttachment }, nullptr, resoultion, vk::AttachmentLoadOp::eLoad, vk::AttachmentStoreOp::eStore, false, "Ui rendering");
		ImGui_ImplVulkan_RenderDrawData(ImGui::GetDrawData(), commandBuffer->operator()());
		commandBuffer->endRendering();
		commandBuffer->endRecord();
		submit(*commandBuffer, waitSemaphores);
	}
}



