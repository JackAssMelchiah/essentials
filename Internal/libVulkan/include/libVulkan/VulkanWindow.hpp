#pragma once
//! libVulkan
#include <libVulkan/VulkanObject.hpp>
#include <libVulkan/VulkanSurface.hpp>
//! Std
#include <functional>

namespace libVulkan
{
	class VulkanImage;
	class VulkanSemaphore;
	class VulkanImageAttachment;
	class VulkanCommandPool;
	class VulkanCommandBuffer;
	class VulkanQueue;
	class VulkanFence;
}

namespace libVulkan 
{
	class VulkanWindow : public VulkanObject<VulkanWindow>
	{	
	public:
		VulkanWindow(std::shared_ptr<VulkanInstance> const& instance, std::shared_ptr<VulkanDevice> const& device, std::weak_ptr<VulkanQueue> presentationQue, std::unique_ptr<VulkanSurface> surface, vk::Extent2D const& resolution, std::string const& name);
		//! Given pipeline is owned by this, and window will handle resource destruction
		~VulkanWindow();
		//! Presents contents of framebuffer's attachment into window
		void present(std::shared_ptr<libVulkan::VulkanImageAttachment> const& imageToPresent, uint32_t attachmentNumber, std::vector<std::reference_wrapper<VulkanSemaphore>> const& waitSemaphores);
		//! Needs to be set before rendering
		void setResolution(vk::Extent2D const& resolution);
		//! Setups callback which will be invoked every time resize happens - at this point device is synced, so no need to wait
		void setResizeCallback(std::function<void(vk::Extent2D const&)>);
		//! Resolution getter
		vk::Extent2D getExtent() const;
		//! Returns surface of this window
		VulkanSurface const& getSurface() const;
		//! Fence for rendering signalization
		VulkanFence& getRenderingFence();
		//! Returns indice for currently acquired image
		uint32_t currentImageIndex() const;
		//! Should be called when for some reason the surface is invalid - destroys and recreates resources
		void revalidateWindow();
		//! Getter for raw Swapchain object
		vk::SwapchainKHR operator()() const;

		void setName(std::string const& name) override;

	protected:
		struct FrameData
		{
			//! Actual image of the frame
			std::shared_ptr<VulkanImage> image;
			//! Command buffer used to copy data from input fbo to this frame's fbo in present()
			std::weak_ptr<VulkanCommandBuffer> commandBuffer;
		};

	protected:
		//! Initializes all data which does not need to be recreated when resizing, + those which do
		void initialize();
		//! Frees all resources asociated with window in order
		void destroyResources();
		//! Creates all resources
		void recreateResources();

		void recordPresentCommandBuffer(VulkanCommandBuffer& commandBuffer, VulkanImageAttachment& srcImage);

		//! Will create framebuffers to be ready to presend
		void createFrameBuffersForWindow();
		//! Returs next image index to imageview which can be used for framebuffer target - GPU blocking
		uint32_t getNextImage();

		//! Creates swapChain object
		void createSwapChain(uint32_t sampleCount);
		//! Fills up images from swapchiain - memory is backed by swapchain
		void getSwapchainImages(std::vector<std::shared_ptr<VulkanImage>>& imagesOut);
		//! Copies data within src image into current swapchain image - expects source image attachment to be in color attachment optimal 
		void copyToWindow(VulkanCommandBuffer& commandBuffer, VulkanImageAttachment& srcImage);

	protected:
		//! Raw object
		vk::UniqueSwapchainKHR m_rawSwapChain;

		//! Resolution and V-sync
		vk::PresentModeKHR m_presentMode;
		vk::Extent2D m_extent;
		
		//! Surface - owned by this, non-modifiable
		std::unique_ptr<VulkanSurface> m_surface;
		//! Synchronization primitives which gets signalized when image for drawing is available from swapchain
		std::shared_ptr<VulkanSemaphore> m_semaphoreImageAvailable;
		std::shared_ptr<VulkanSemaphore> m_semaphoreCopyFinished;
		std::shared_ptr<VulkanFence> m_fenceCopyFinished;

		//! Stored indice for current image available -> index into FrameData vector
		uint32_t m_currentImageIndex;
		bool m_recreated;

		//! Stored weakPrt to presentation queue, so there is no need to query it allways
		std::weak_ptr<VulkanQueue> m_queue;
		//! Pool for command buffer creation
		std::shared_ptr<VulkanCommandPool> m_commandPool;

		//! Finally, the frame data, count depends on number of window samples 
		std::vector<FrameData> m_frameData;

		//! Resize callback 
		std::function<void(vk::Extent2D const&)> m_resizeCallback;
	};
}