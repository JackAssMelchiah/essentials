#pragma once
//! libVulkan
#include <libVulkan/VulkanPipelineLayout.hpp>
//! std
#include <vector>

namespace libVulkan
{
	class VulkanBuffer;
	class VulkanCommandPool;
	class VulkanImage;
	class VulkanPipeline;
	class VulkanDescriptorSet;
	class VulkanRenderingInfo;
	class VulkanImageAttachment;
}

namespace libVulkan
{
	class VulkanCommandBuffer : public VulkanObject<VulkanCommandBuffer>
	{
	public:
		VulkanCommandBuffer(std::shared_ptr<VulkanInstance> const& instance, std::shared_ptr<VulkanDevice> const& device, vk::UniqueCommandBuffer&& rawCmdBuffer, vk::CommandBufferLevel level, vk::QueueFlags supportedFunctionality, std::string const& name);
		~VulkanCommandBuffer();

		//! Begins recording of secondary command buffer which will be executed in context of the primary, this version with fbo should be faster
		void beginRecord(VulkanRenderingInfo const& info, vk::CommandBufferUsageFlags usage);
		//! All invoked methods of VulkanCommandBuffer must be encapsulated by beginRecord() and endRecord()
		void beginRecord(vk::CommandBufferUsageFlags usage);
		//! All invoked methods of VulkanCommandBuffer must be encapsulated by beginRecord() and endRecord()
		void endRecord();

		//! Starts rendering into attachments
		void beginRendering(std::vector<std::shared_ptr<VulkanImageAttachment>> const& colorAttachments, std::shared_ptr<VulkanImageAttachment> const& depthStencil, vk::Extent2D const& dimms, vk::AttachmentLoadOp loadOp, vk::AttachmentStoreOp storeOp, bool executesSecondaryCommandBuffers, std::string const& description);
		//! Ends rendering
		void endRendering();
		//! Binds the pipeline
		void bindPipeline(VulkanPipeline& pipeline);
		//! Binds vertex buffers to be used with command 
		void bindVertexBuffers(std::vector<std::reference_wrapper<VulkanBuffer>>const& buffers);
		//! Binds index buffers to be used with command 
		void bindIndexBuffer(VulkanBuffer& buffer, vk::IndexType type);
		//! Binds descriptor set to be used with command
		void bindDescriptorSet(VulkanDescriptorSet const& descriptorSet, uint32_t setNumber, VulkanPipelineLayout& pipelineLayout);
		//! Binds descriptor sets to be used with command
		void bindDescriptorSets(std::vector<std::reference_wrapper<VulkanDescriptorSet>> const&, VulkanPipelineLayout& pipelineLayout);
		//! Records into renderPass clear of all framebuffer's attachments
		void recordClearAttachments(std::vector<std::shared_ptr<VulkanImageAttachment>> const& colorAttachments, std::shared_ptr<VulkanImageAttachment> const& depthStencilAttachment, glm::vec4 const& clearColor, glm::vec2 const& depthStencil);

		//! Copies the data among buffers
		void copyBuffer(VulkanBuffer const& sourceBuffer, VulkanBuffer& destinationBuffer, std::vector<vk::BufferCopy> const&);
		//! Copies image to image - just now whole image, and 0 mip level only
		void copyImageToImage(VulkanImage& srcImage, vk::ImageLayout srcImageLayout, VulkanImage& destinationImage, vk::ImageLayout dstImageLayout);
		//! Copies data from buffer to image
		void copyBufferToImage(VulkanBuffer const& sourceBuffer, vk::DeviceSize bufferOffset, VulkanImage const& destinationImage);

		//! Updates pushConstants of max 128 bytes for given pipeline layout, which are accessible in given stages
		template <typename T>
		void updatePushConstants(VulkanPipelineLayout const& pipelineLayout, vk::ShaderStageFlags shaderStages, std::vector<T>const& data)
		{
			assert(data.size() * sizeof(T) <= 128); // spec allows minimum 128 bytes
			m_rawCommandBuffer->pushConstants(pipelineLayout.operator()(), shaderStages, 0, data.size() * sizeof(T), data.data());
		}

		//! Executes all secondary command buffers within context of the this, primary one
		void executeSecondaryBuffers(std::vector<std::reference_wrapper<VulkanCommandBuffer>> const& buffers);

		//! Draw methods
		void draw(int vertexCount, int instanceCount, int firstVertex, int firstInstance);
		void drawIndexed(int indexCount, int instanceCount, int firstVertex, int firstInstance);

		//! Resets commandBuffer (should be used before rendering) and potentionally releases all resources for that buffer
		void reset(bool releaseResources);
		//! Access to raw object
		vk::CommandBuffer operator()();

		void setName(std::string const& name) override;

		bool supportsGraphics() const;

		//! Allows to set debug message with the context of command buffer, visible to external debuggers
		void insertDebugMessage(std::string const& message);
				
	protected:
		//! Allows to mark a region with debug message to external debugger, visible to external debuggers
		void beginDebugRegion(std::string const& message);
		//! Ends the debug region
		void endDebugRegion();

	protected:

		std::string m_renderingDesc;

		//! Raw object
		vk::UniqueCommandBuffer m_rawCommandBuffer;
		//! What functionality is provided by this command buffer
		vk::QueueFlags m_supportedFunctionality;
		//! What kind of command buffer this is
		vk::CommandBufferLevel m_commandBufferLevel;
	};
}