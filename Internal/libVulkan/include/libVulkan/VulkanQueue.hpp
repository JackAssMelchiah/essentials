#pragma once
//! libVulkan
#include <libVulkan/VulkanObject.hpp>
//! Std
#include <functional>

namespace libVulkan
{
	class VulkanCommandBuffer;
	class VulkanSemaphore;
	class VulkanTimelineSemaphore;
	class VulkanWindow;
	class VulkanFence;
}

namespace libVulkan 
{
	class VulkanQueue: public VulkanObject<VulkanQueue>
	{
	public:
		using SemaphorePrimitive = struct SP
		{
			std::shared_ptr<libVulkan::VulkanSemaphore> semaphore;	//semaphore waited or signalized
			vk::PipelineStageFlags2 stage; // stage whiich semaphore will be signalized in
			uint64_t value; //value to which semaphore will signalize or wait for in the stage 
		};

	public:
		VulkanQueue(std::shared_ptr<VulkanInstance> const& instance, std::shared_ptr<VulkanDevice> const& device, vk::Queue rawQue, vk::QueueFlags func, uint32_t queueFamilyIndex, uint32_t indexWithinFamily, std::string const& name);
		~VulkanQueue() = default;

		uint32_t getFamilyIndex() const;
		uint32_t getIndexWithinFamily() const;
		vk::QueueFlags getFunctionality() const;

		//! Submits command buffers to execution
		void submit(std::vector<std::shared_ptr<VulkanCommandBuffer>> const& commandBuffers, std::vector<SemaphorePrimitive> const& signalSemaphores, std::vector<SemaphorePrimitive> const& waitSemaphores, vk::PipelineStageFlags const& waitStage, std::shared_ptr<VulkanFence> const& signalFence);

		//! Submits command buffers to execution - no synchronization
		void submit(std::vector<std::reference_wrapper<VulkanCommandBuffer>> const& commandBuffers);
		//! Submits command buffers to execution - semaphore synchronization
		void submit(std::vector<std::reference_wrapper<VulkanCommandBuffer>> const& commandBuffers, std::vector<std::reference_wrapper<VulkanSemaphore>> const& signalSemaphores, std::vector<std::reference_wrapper<VulkanSemaphore>> const& waitSemaphores, std::vector<vk::PipelineStageFlags> const& waitStage);
		//! Submits command buffers to execution - possible sempahore and fence synchronization
		void submit(std::vector<std::reference_wrapper<VulkanCommandBuffer>> const& commandBuffers, std::vector<std::reference_wrapper<VulkanSemaphore>> const& signalSemaphores, std::vector<std::reference_wrapper<VulkanSemaphore>> const& waitSemaphores, std::vector<vk::PipelineStageFlags> const& waitStage, VulkanFence& signalFence);
		//! If this que has presentation support, it will present onto window
		void present(VulkanWindow& window, std::vector<std::reference_wrapper<VulkanSemaphore>> const& waitSemaphores);
		//! Waits till all work on que has been done
		void wait();

		//! Access to raw object
		vk::Queue operator()() const;

		void setName(std::string const& name) override;

	protected:
		//! Raw object
		vk::Queue m_rawQue;
		//! Info about the que
		uint32_t m_queueFamilyIndex;
		uint32_t m_indexWithinFamily;
	    vk::QueueFlags m_functionality;
	};
}
