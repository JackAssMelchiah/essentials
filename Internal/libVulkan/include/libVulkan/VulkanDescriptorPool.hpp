#pragma once
//! libVulkan
#include <libVulkan/VulkanObject.hpp>
//! std
#include <set>

namespace libVulkan
{
	class VulkanDescriptorSet;
	class VulkanDescriptorSetLayout;
}

namespace libVulkan
{
	class VulkanDescriptorPool: public VulkanObject<VulkanDescriptorPool>
	{
	public:
		friend class VulkanDescriptorSet;
		VulkanDescriptorPool(std::shared_ptr<VulkanInstance> const& instance, std::shared_ptr<VulkanDevice> const& device, std::vector<vk::DescriptorPoolSize> const& allocationDescriptions, bool allowSingleFree, std::string const& name, bool canUpdateAfterBind = false);
		~VulkanDescriptorPool() = default;

		//! Creates new descriptor set
		std::weak_ptr<VulkanDescriptorSet> createDescriptorSet(std::shared_ptr<VulkanDescriptorSetLayout> layout, std::string const& name);
		//! Deallocated single descriptor set
		void destroyDescriptorSet(std::shared_ptr<VulkanDescriptorSet> descriptorSet);
		//! Resets all allocated descriptors
		void reset();
		//! Access to raw object
		vk::DescriptorPool operator()()const;

		void setName(std::string const& name) override;

	protected:
		//! Initializes the pool 
		void initialize(vk::DescriptorPoolCreateFlags flags);
		//! Rebuilds the object
		void recreate();

	protected:
		//! Raw object
		vk::UniqueDescriptorPool m_rawPool;

		//! Stored params
		std::vector<vk::DescriptorPoolSize> m_allocationDescriptions;
		//! Flags for pool
		vk::DescriptorPoolCreateFlags m_flags;

		//! All descriptor sets spawned from this pool
		std::set<std::shared_ptr<VulkanDescriptorSet>> m_descriptorSets;
	};
}