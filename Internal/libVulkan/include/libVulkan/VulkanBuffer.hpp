#pragma once
//! std
#include <span>
//! libVulkan
#include <libVulkan/VulkanObject.hpp>

namespace libVulkan
{
    class VulkanDevice;
}

namespace libVulkan 
{
    class VulkanBuffer: public VulkanObject<VulkanBuffer>
    {
    public:
        VulkanBuffer(std::shared_ptr<VulkanInstance> const& instance, std::shared_ptr<VulkanDevice> const& device, vk::BufferUsageFlags usage, vk::MemoryPropertyFlags flags, uint32_t bytes, std::string const& name);
        ~VulkanBuffer() = default;
        //! How many bytes this buffer takes on CPU|GPU
        uint32_t getByteSize() const;
        //! What kind of buffer this is
        vk::BufferUsageFlags getUsage() const;

        //! Mapping status
        bool mapped() const;
        //! Maps gpu visible memory to Cpu adress space
        void map();
        //! Unmaps gpu mapped memory from cpu adress space
        void unmap();
        //! Flushes memory to be accessible to gpu - currently whole memory: TODO
        void flush();

        //! TODO: use template trait instead of theese two templates
        //! Templated writes to mapped space - currently cannot specify offset: TODO
        template <typename T>
        void bufferData(std::vector<T> const& data)
        {
            assert(m_maped);
            memcpy(m_bufferPointer, data.data(), data.size() * sizeof(T));
        }
        //! Templated writes to mapped space - currently cannot specify offset: TODO
        template <typename T>
        void bufferData(std::span<T> const& data)
        {
            assert(m_maped);
            memcpy(m_bufferPointer, data.data(), data.size() * sizeof(T));
        }
        //! Access to Raw object      
        vk::Buffer operator()() const;

        void setName(std::string const& name) override;

    protected:
        vk::UniqueBuffer createRawBuffer(uint32_t bytes, vk::BufferUsageFlags bufferUsage) const;
        vk::UniqueDeviceMemory backBufferByMemory(vk::Buffer& bufferRawHandle, vk::MemoryPropertyFlags flags) const;

    protected:
        //! What kind of buffer this is
        vk::BufferUsageFlags m_usage;
        //! Byte size allocated to this buffer
        uint32_t m_bytes;
            
        //! Raw objects
        vk::UniqueBuffer m_rawBuffer;
        vk::UniqueDeviceMemory m_rawMemory;

        void* m_bufferPointer;
        //! Whether is mapped or not
        bool m_maped;
    };
}