#pragma once
//! libVulkan
#include <libVulkan/VulkanObject.hpp>

namespace libVulkan
{
	class VulkanShaders;
	class VulkanVertexAttributesDescription;
	class VulkanPipelineLayout;
	class VulkanRenderingInfo;
}

namespace libVulkan
{
	class VulkanPipeline: public VulkanObject<VulkanPipeline>
	{
	public:
		//! Note that for now all dynamic states are set as static!!!!
		VulkanPipeline(std::shared_ptr<VulkanInstance> const& instance, std::shared_ptr<VulkanDevice> const& device, std::shared_ptr<VulkanShaders> const& shaders, std::shared_ptr<VulkanPipelineLayout> const& pipelineLayout, std::shared_ptr<VulkanVertexAttributesDescription> const& vertexAttribsDescription, std::shared_ptr<VulkanRenderingInfo> const& renderingInfo, vk::Extent2D const& resolution, std::string const& name);

		virtual ~VulkanPipeline() = default;

		//! Will trigger pipeline recompilation
		void setShader(std::shared_ptr<VulkanShaders> const& shaders);
		void setVertexDescription(std::shared_ptr<VulkanVertexAttributesDescription> const& vertexAttribsDescription);
		void setLayoutDescription(std::shared_ptr<VulkanPipelineLayout> const& pipelineLayout);
		void setRenderingInfo(std::shared_ptr<VulkanRenderingInfo> const& renderingInfo);

		void setInputAssebly(vk::PrimitiveTopology topology, bool primitiveRestart);
		void setRasterizer(vk::PolygonMode polyMode, vk::CullModeFlagBits cullMode, vk::FrontFace frontFace);
		
		//! Ideally should be swapchain extent size
		void setViewPort(vk::Extent2D const& size);
		//! Disabled for now
		void setDepthTest(bool enable, bool writeFragmentDepth);

		//! Compiles the pipeline - should be called when some pipeline setting change 
		void compilePipeline();

		//! Getters for bound owned objects
		VulkanShaders& getShaders();
		VulkanVertexAttributesDescription& getVertexAttribsDescription();
		VulkanPipelineLayout& getPipelineLayout();
		//! VertexAttributesDescription is optional, so this returs whether pipeline has it or not, then use getter() 
		bool hasVertexAttributesDescription() const;

		//! Access to raw object
		vk::Pipeline operator()();
		
		//! Each time parameter is changed, pipeline is flagged as dirty... it needs to be re-compiled, then it wont be dirty
		bool isDirty() const;

		void setName(std::string const& name) override;

	protected:
		//! Creates pipeline
		void createPipeline(std::shared_ptr<VulkanShaders> const& shaders, std::shared_ptr<VulkanVertexAttributesDescription> const& vertexAttribsDescription, std::shared_ptr<VulkanPipelineLayout> const& pipelineLayout, std::shared_ptr<VulkanRenderingInfo> const& renderingInfo, vk::Extent2D const& resolution);

		//! Disabled for now
		void setStencilTest();
		//! Disabled for now
		void setColorBlending();
		//! Disabled for now, since it requires GPU feature
		void setMultisampling();

		//! Disabled for now, should cover whole viewport for it to be shown - for now dont allow anyone to meddle with it
		void setScizzorTest(glm::ivec2 const& offset, glm::ivec2 const& size);
		//! Builds viewport and scizzor test info - because they are dinamic states
		void buildVPSZInfo();
		//! Builds blend info - because its dinamic state
		void buildBlendInfo();

	protected:
		//! flag for marking the need to recompile pipeline
		bool m_dirty;

		//! dynamic states - requires its own info
		vk::Viewport m_viewPort;
		vk::Rect2D m_scizzor;
		vk::PipelineColorBlendAttachmentState m_blendAttachment;

		//! create infos for static states
		vk::PipelineLayoutCreateInfo m_layoutInfo;
		vk::PipelineViewportStateCreateInfo m_vpszInfo;
		vk::PipelineColorBlendStateCreateInfo m_blendInfo;
		vk::PipelineInputAssemblyStateCreateInfo m_inputAssemblyInfo;
		vk::PipelineRasterizationStateCreateInfo m_rasterizerInfo;
		vk::PipelineMultisampleStateCreateInfo m_multisampleInfo;
		vk::PipelineVertexInputStateCreateInfo m_vertexInputInfo;
		vk::PipelineDepthStencilStateCreateInfo m_depthInfo;

		//! owned by pipeline, since its using them ... till rawPipeline is not created, then can be released.. thats TODO if needed
		std::shared_ptr<VulkanShaders> m_shaders;
		std::shared_ptr<VulkanVertexAttributesDescription> m_vertexAttribsDescription;
		std::shared_ptr<VulkanPipelineLayout> m_pipelineLayout;
		std::shared_ptr<VulkanRenderingInfo> m_renderingInfo;

		//! raw pipeline
		vk::UniquePipeline m_rawPipeline;
	};

}