#pragma once
//! libVulkan
#include <libVulkan/VulkanObject.hpp>
#include <libVulkan/VulkanInstance.hpp>

#include <map>

namespace libVulkan 
{
	class VulkanRenderingInfo
	{
	public:
		VulkanRenderingInfo(std::vector<vk::Format> const& inputAttachments, vk::Format depthStencilAttachment, std::vector<vk::Format> const& outputAttachments, std::map<uint32_t, uint32_t> const& sharedIOAttachments = {});
		~VulkanRenderingInfo() = default;
		//! Returns input formats used
		std::vector<vk::Format> const& getInputFormats() const;
		//! Returns outputs formats used
		std::vector<vk::Format> const& getOutputFormats() const;
		//! Returns optional depthStencil format, if format == undefined, depth is not used
		vk::Format const& getDepthStencilFormat() const;
		//! Getter for indexes for input|output attachments which can be shared - first is inputs, second is output 
		std::map<uint32_t, uint32_t> const& getSharedAttachments() const;
		//! Helper method for looking at compatibility among RenderingInfos
		bool operator ==(VulkanRenderingInfo const& info ) const;

		//! helper method which selects single depth format, and single output attachment which is supported by instance
		static void selectSimplePassFormats(VulkanInstance const& instance, std::vector<vk::Format>& inputFormatOut, vk::Format& depthStencilOut, std::vector<vk::Format>& outputFormatOut);

	protected:
		//! input attachments which are used within pipeline, should be bound in order which is specified
 		std::vector<vk::Format> m_inputAttachments;
		//! output attachments which are used within pipeline, should be bound in order which is specified
		std::vector<vk::Format> m_outputAttachments;
		//! optional depth attachment
		vk::Format m_depthStencil;
		//! Indexes for input and output attachments which should have single memory backing
		std::map<uint32_t, uint32_t> m_sharedIOAttachments;

		//! TODO: when chaining in renderin passes, the barriers should be created among them, automatically, and on last and fist should chain the top of pipe and bot of pipe or something
		//! Synchronization access flags which are used to create barrier within rendering passes
		vk::AccessFlags2 m_inputResourcesDstFlags;
		vk::AccessFlags2 m_outputresourcesSrcFlags;
		//! Synchronization pipeline flags which are used to create barrier within rendering passes
		vk::PipelineStageFlags2 m_inputResourceDstStage;
		vk::PipelineStageFlags2 m_outputResourceSrcStage;
	};
}
