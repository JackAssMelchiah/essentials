#pragma once
//! std
#include <set>
//! libVulkan
#include <libVulkan/UtilFunctions.hpp>

namespace libVulkan
{
	class VulkanInstance;
	class VulkanDevice;
	class VulkanQueue;
}

namespace libVulkan
{
	class VulkanQueues
	{
	public:
		VulkanQueues(std::shared_ptr<VulkanInstance> const& instance, std::shared_ptr<VulkanDevice> const& device, std::vector<vk::QueueFlagBits> const& wantedQues);
		~VulkanQueues() = default;

		void createPresentationQueue(vk::SurfaceKHR const& surface);

		//! Returns Queue minus presentation one, separate method for that one
		std::weak_ptr<VulkanQueue> getQueue(vk::QueueFlagBits bits) const;
		//! Returs presentation queue, allways present if device was created with swapchain support (has window)
		std::weak_ptr<VulkanQueue> getPresentationQueue() const;
		//! Test what is supported for que
		bool supports(vk::QueueFlags) const;

	protected:
		//! Creates all ques, which then can be retrieved from device
		void createAllAvailableQueues(std::vector<vk::QueueFlagBits> const& wantedQues);

	protected:
		std::weak_ptr<libVulkan::VulkanDevice> m_device;
		std::weak_ptr<libVulkan::VulkanInstance> m_instance;

		//! Stored ques, owned by device
		std::set<std::shared_ptr<VulkanQueue>> m_availableQues;

		//! Stored presentation que, does not have an enum therefore separate, also can be the same as graphics
		std::shared_ptr<VulkanQueue> m_presentationQue;

		//! Just for info, with which que support the device was build 
		vk::QueueFlags m_supportedQues;
	};
}