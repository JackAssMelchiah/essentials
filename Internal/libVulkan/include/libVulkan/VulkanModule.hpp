#pragma once
//! libVulkan
#include <libVulkan/VulkanObject.hpp>

namespace libVulkan
{
	class VulkanShaderModule: public VulkanObject<VulkanShaderModule>
	{
	public:
		VulkanShaderModule(std::shared_ptr<VulkanInstance> const& instance, std::shared_ptr<VulkanDevice> const& device, std::string const& name);
		~VulkanShaderModule() = default;

		//! Creates module from spv
		void create(std::vector<uint32_t> const& spv);

		//! Loades spv from disk and creates module
		void loadAndCreate(std::string const& path);

		void setName(std::string const& name) override;

		vk::ShaderModule operator()() const;

	protected:
		//! Raw object
		vk::UniqueShaderModule m_module;
	};
}