#pragma once
//! libVulkan
#include <libVulkan/VulkanObject.hpp>

namespace libVulkan
{
	class VulkanDescriptorSetLayout;
}

namespace libVulkan
{
	class VulkanPipelineLayout: public VulkanObject<VulkanPipelineLayout>
	{
	public:
		//! If push constants stage has eNone, then there will be no support, otherwise for given shader stages - one binding for 128 bytes in layout
		VulkanPipelineLayout(std::shared_ptr<VulkanInstance> const& instance, std::shared_ptr<VulkanDevice> const& device, std::vector<std::shared_ptr<VulkanDescriptorSetLayout>> const& descriptorSetLayouts, vk::ShaderStageFlags usePushConstants, std::string const& name);
		~VulkanPipelineLayout() = default;
		//! Access to raw object
		vk::PipelineLayout operator()() const;
		//! Whether layouts are compatible, e.q. same
		bool operator==(VulkanPipelineLayout const& other) const;
		//! Whether pipeline is compatible with this sets
		bool isCompatible(std::vector<std::shared_ptr<VulkanDescriptorSetLayout>> const& descriptorSets) const;

		void setName(std::string const& name) override;

	protected:
		//! Rebuilds raw object
		void recreate();
		//! Default setup without any description
		void initialize();

	protected:
		//! Raw pipeline layout object
		vk::UniquePipelineLayout m_rawPipelineLayout;
		//! Whether pipeline will support pushConstants - default are 128 bites since that is required minimum
		vk::ShaderStageFlags m_pushConstants;

		//! Stored descriptor layouts used by this pipeline's layout - partially owned by this
		std::vector<std::shared_ptr<VulkanDescriptorSetLayout>> m_descriptorSetLayouts;
	};
}