#pragma once
//! libVulkan
#include <libVulkan/VulkanObject.hpp>

namespace libVulkan
{
	class VulkanDescriptorSetLayout: public VulkanObject<VulkanDescriptorSetLayout>
	{
	public:
		VulkanDescriptorSetLayout(std::shared_ptr<VulkanInstance> const& instance, std::shared_ptr<VulkanDevice> const& device, std::vector<vk::DescriptorSetLayoutBinding> const& setLayoutBindings, std::string const& name);
		~VulkanDescriptorSetLayout() = default;
		//! Getter for specified bindings
		std::vector<vk::DescriptorSetLayoutBinding> const& getLayoutBindings() const;
		//! Access to raw object
		vk::DescriptorSetLayout operator()() const;

		void setName(std::string const& name) override;
	
	protected:
		//! Default setup without any description
		void initialize();

	protected:
		//! Stored bindings so they can be recreated on the fly 
		std::vector<vk::DescriptorSetLayoutBinding> m_setLayoutBindings;

		//! Raw objects
		vk::UniqueDescriptorSetLayout m_rawSetLayout;
	};
}