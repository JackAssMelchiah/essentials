#pragma once
//! libVulkan
#include <libVulkan/VulkanQueues.hpp>
//! Std
#include <set>
#include <functional>

namespace libVulkan
{
	class VulkanInstance;
	class VulkanFence;
	class VulkanTimelineSemaphore;
}

namespace libVulkan
{
	class VulkanDevice
	{
	public:
		VulkanDevice(std::shared_ptr<VulkanInstance> const& instance, vk::UniqueDevice device, std::string const& name);
		~VulkanDevice();
				
		//! Active CPU wait for all given fences
		void waitForFences(std::vector<std::reference_wrapper<VulkanFence>>const&);
		//! Waits for idleness of device, ensures no rendering is in progress
		void waitIdle();

		void waitForSemaphores(std::vector<std::pair<std::shared_ptr<VulkanTimelineSemaphore>, uint64_t>> const& semaphores, bool waitAny);
		void waitForSemaphore(std::shared_ptr<VulkanTimelineSemaphore> const& semaphore, uint64_t value);

		//! Memory allocation for buffer and images
		vk::UniqueDeviceMemory allocateMemory(int32_t bytes, vk::MemoryRequirements const&, vk::MemoryPropertyFlags);

		//! Access to raw object
		vk::Device operator()() const;

		//! Allows to set Object name to be able to see it in debuggers
		void setDebugObjectName(uint64_t objectHandle, vk::ObjectType objectType, std::string const& name);

	protected:
		//! Instance
		std::weak_ptr<VulkanInstance> m_instance;
		//! Raw device
		vk::UniqueDevice m_rawDevice;
	};
}
