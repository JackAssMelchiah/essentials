#pragma once
//! vulkan
#include <vulkan/vulkan.hpp>
//! glm
#include <glm/glm.hpp>
//! std
#include <vector>

namespace libVulkan
{
	class VulkanDefaults
	{
	public:
		VulkanDefaults();
		~VulkanDefaults() = default;
		//! Copy Constructor
		VulkanDefaults(VulkanDefaults const& defaults);

	public:
		//! Application name
		std::string appName;
		//! Application version
		glm::ivec3 appVersion;

		//! Default vulkan extensions
		std::vector<std::string> extensions;
		//! Default validation layers
		std::vector<std::string> validationLayers;

		//! Multisampling - note that if not supported, then will result in no msaa support on machine
		vk::SampleCountFlagBits multisampling;
		//! Que functionality which must be presented
		std::vector<vk::QueueFlagBits> functionality;
		//! Resolution for framebuffer creation
		glm::uvec2 frameBufferResolution;
		//! Samples for window - sigle|double|tripple buffering
		unsigned int windowSamples;
		//! Clear color used for framebuffers and window
		glm::vec4 clearColor;
		////! Clear depth value used for framebuffers and window - 1.0 is depth at far view plane, and 0 for stencil
		glm::vec2 clearDepth;
		////! Type of HKR presentation mode, if not available, will be swithed to Fifo which is guaranteed by spec to allways be supported
		vk::PresentModeKHR presentationMode;
		//! For allowing to set non-standart device features - note, that even if they are part of vulkan major version, some stuff needs to be enabled via feature
		vk::PhysicalDeviceFeatures deviceFeatures;
	};
}