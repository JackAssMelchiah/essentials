#pragma once
//! libVulkan
#include <libVulkan/VulkanObject.hpp>
#include <libVulkan/VulkanSurface.hpp>
//! Std
#include <set>

namespace libVulkan
{
	class VulkanInstance;
	class VulkanDevice;
	class VulkanWindow;
	class VulkanDefaults;
	class VulkanQueues;
}

namespace libVulkan 
{
	class VulkanManager
	{
	public:
		VulkanManager(std::shared_ptr<VulkanDefaults> settings);
		~VulkanManager();
		//! Creates and adds Vulkan Window to manager - only if vulkanManager supports the size
		std::weak_ptr<VulkanWindow> createWindow(vk::SurfaceKHR surface, glm::ivec2 const& initialResolution, std::string const& name);
		VulkanWindow& getWindow();

		std::weak_ptr<VulkanDevice> getDevice() const;
		std::weak_ptr<VulkanInstance> getInstance() const;
		std::weak_ptr<VulkanQueues> getQueues() const;

		bool createInstance();
		bool createDevice(std::shared_ptr<VulkanInstance> const& instance, vk::SurfaceKHR const* surface);
		void createQueues();

	protected:
		std::vector<char const*> filterAvailableInstanceExtensions(std::vector<std::string> const&) const;
		std::vector<char const*> filterAvailableInstanceLayers(std::vector<std::string> const&) const;
		//! libCore needs few mandatory features to be enabled
		void setRequiredFeatures();
		//! Creates surface object
		std::unique_ptr<VulkanSurface> createSurface(vk::SurfaceKHR rawSurface) const;

	protected:
		//! Owned instance by manager
		std::shared_ptr<VulkanInstance> m_instance;
		//! Owned device by the manager
		std::shared_ptr<VulkanDevice> m_device;
		//! Defaults values for initialization
		std::shared_ptr<VulkanDefaults> m_defaults;
		//! Owned windows - all windows wihin libVulkan
		std::set<std::shared_ptr<VulkanWindow>> m_windows;
		//! All ques available for the instance + device combination
		std::shared_ptr<VulkanQueues> m_queues;
	};
}