#pragma once
//! libVulkan
#include <libVulkan/VulkanObject.hpp>

namespace libVulkan
{
	class VulkanSemaphore: public VulkanObject<VulkanSemaphore>
	{
	public:
		VulkanSemaphore(std::shared_ptr<VulkanInstance> const& instance, std::shared_ptr<VulkanDevice> const& device, std::string const& name);
		~VulkanSemaphore() = default;
		//! Access to raw object
		vk::Semaphore operator()() const;

		void setName(std::string const& name) override;

	protected:
		// Recreates raw object
		void recreate();

	protected:
		//! Raw objects
		vk::UniqueSemaphore m_rawObject;
	};
}