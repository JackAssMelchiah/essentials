#pragma once 
#include <libVulkan/VulkanObject.hpp>

//! Vulkan hpp's instance does have utils messenger creation and destruction, but it will cause linker errors, therefore manual creation and deletion is nescessary
namespace libVulkan
{
    vk::Result createDebugUtilsMessengerEXT(vk::Instance& instance, const VkDebugUtilsMessengerCreateInfoEXT* pCreateInfo, const VkAllocationCallbacks* pAllocator, VkDebugUtilsMessengerEXT* pCallback)
	{		
		auto func = (PFN_vkCreateDebugUtilsMessengerEXT)instance.getProcAddr("vkCreateDebugUtilsMessengerEXT");
		if (func) 
		{
			return vk::Result(func(instance, pCreateInfo, pAllocator, pCallback));
		}
		else 
		{
			return vk::Result::eErrorExtensionNotPresent;
		}
	}

	vk::Result destroyDebugUtilsMessengerEXT(vk::Instance& instance, VkDebugUtilsMessengerEXT callback, const VkAllocationCallbacks* pAllocator)
	{
		auto func = (PFN_vkDestroyDebugUtilsMessengerEXT)instance.getProcAddr("vkDestroyDebugUtilsMessengerEXT");
		if (func) 
		{
			func(instance, callback, pAllocator);
			return vk::Result::eSuccess;
		}
		else
		{
			return vk::Result::eErrorExtensionNotPresent;
		}
	}
}
	