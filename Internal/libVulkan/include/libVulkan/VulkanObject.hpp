#pragma once

#include <libVulkan/UtilFunctions.hpp>

//!libVulkan
#include <libVulkan/VulkanInstance.hpp>
#include <libVulkan/VulkanDevice.hpp>

#include <glm/glm.hpp>

namespace libVulkan
{
	//! Class which serves as base for all vulkan objects, allows for shared_from_this()
	template <typename T>
	class VulkanObject
	{
	public:
		VulkanObject(std::shared_ptr<VulkanInstance> const& instance, std::shared_ptr<VulkanDevice> const& device, std::string const& name)
			: m_instance(instance)
			, m_device(device)
			, m_name(name)
		{
			//setName(name); TODO in future, i want to have setName in every object, but that means that VulkanDevice needs to be passed in constructor to this base as well
			// now the device ihnerits from this, and it either should not or there should be some other object in between.. either way want to get rid of enable_shared_from_this which is basically only used in device and it enables bad practices in this projects context
		}

		virtual ~VulkanObject() = default;

		//! Sets the name, objects are supposed to overload the method
		virtual void setName(std::string const& name)
		{
			m_name = name;
		}

		//! Returns instance ptr
		std::weak_ptr<VulkanInstance> getInstance() const
		{
			return m_instance;
		}

		//! Returns device ptr
		std::weak_ptr<VulkanDevice> getDevice() const
		{
			return m_device;
		}

		//! Return name
		std::string getName() const
		{
			return m_name;
		}

	protected:
		//! stored instance
		std::weak_ptr<VulkanInstance> m_instance;
		//! stored device
		std::weak_ptr<VulkanDevice> m_device;
		//! Name of the object
		std::string m_name;
	};

	template <class Parent, class Child>
	class CRTP: public Parent, public std::enable_shared_from_this<Child>
	{
	public:
		template <typename... Args>
		CRTP(Args&&... args): Parent(args...) { }

		virtual ~CRTP() = default;

		template <typename... Args>
		static std::shared_ptr<Child> create(Args&&... args)
		{
			return std::make_shared<Child>(args...);
		}
	};
}