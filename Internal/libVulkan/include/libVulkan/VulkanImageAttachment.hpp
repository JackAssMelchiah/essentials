#pragma once
//!libVulkan
#include <libVulkan/VulkanObject.hpp>
#include <libVulkan/VulkanImage.hpp>

namespace libVulkan
{
	class VulkanBarrier;
}

namespace libVulkan
{
	class VulkanImageAttachment: public VulkanObject<VulkanImageAttachment>
	{
	public:
		//! Creates image attachment, which uses underline Vulkan image 
		VulkanImageAttachment(std::shared_ptr<VulkanInstance> const& instance, std::shared_ptr<VulkanDevice> const& device, VulkanImage::Info const& info, vk::Extent2D const& resolution, std::string const& name);
		//! Creates image attachment, from preexisting vulkan image which is used as memory backing
		VulkanImageAttachment(std::shared_ptr<VulkanInstance> const& instance, std::shared_ptr<VulkanDevice> const& device, std::shared_ptr<VulkanImage>&& image, vk::Extent2D const& resolution, std::string const& name);
		~VulkanImageAttachment() = default;
		//! Generates barrier for transitioning into wanted state
		std::shared_ptr<VulkanBarrier> generateBarrier(vk::ImageLayout oldLayout, vk::ImageLayout targetLayout, vk::AccessFlags2 srcAccessFlags, vk::PipelineStageFlagBits2 srcStage, vk::AccessFlags2 dstAccessFlags, vk::PipelineStageFlagBits2 dstStage);
		//! Resizes the attachment and underlying memory
		void resize(vk::Extent2D const& resolution);
		//! Retuns nonmodifiable underlying image
		VulkanImage const& getImage() const;
		//! Retuns modifiable underlying image
		VulkanImage& getImage();

	protected:
		//! VulkanImage which is used as backing memory
		std::shared_ptr<libVulkan::VulkanImage> m_image;
	};
}