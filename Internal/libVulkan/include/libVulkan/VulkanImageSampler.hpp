#pragma once
//! libVulkan
#include <libVulkan/VulkanObject.hpp>

namespace libVulkan
{
	class VulkanImageSampler: public VulkanObject<VulkanImageSampler>
	{
	public:
		//! Default constructor
		VulkanImageSampler(std::shared_ptr<VulkanInstance> const& instance, std::shared_ptr<VulkanDevice> const& device, std::string const& name);
		~VulkanImageSampler() = default;
		//! Access to raw object
		vk::Sampler operator()() const;

		void setName(std::string const& name) override;

	protected:
		//! Creates the sampler
		void createSampler();
	
	protected:
		vk::UniqueSampler m_rawSampler;
	};
}

