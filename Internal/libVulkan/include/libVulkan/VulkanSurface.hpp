#pragma once
//! libVulkan
#include <libVulkan/VulkanObject.hpp>

namespace libVulkan 
{
	class VulkanSurface: public VulkanObject<VulkanSurface>
	{
	public:
		VulkanSurface(std::shared_ptr<VulkanInstance> const& instance, std::shared_ptr<VulkanDevice> const& device, vk::SurfaceKHR surface, vk::SurfaceCapabilitiesKHR const& capabilites, vk::SurfaceFormatKHR const& format, std::string const& name);
		~VulkanSurface();

		vk::SurfaceCapabilitiesKHR const& getCapabilites() const;
		vk::SurfaceFormatKHR getFormat() const;
		std::vector<vk::PresentModeKHR> getSupportedPresentModes() const;
		vk::ImageUsageFlags getSupportedUsages() const;
		//! Access to raw object
		vk::SurfaceKHR operator()() const;

		void setName(std::string const& name) override;

	protected:
		void printInfo() const;

	protected:
		//! Raw object
		vk::SurfaceKHR m_surface;

		//! Raw structs
		vk::SurfaceCapabilitiesKHR m_capabilities;
		vk::SurfaceFormatKHR m_format;
	};

}
