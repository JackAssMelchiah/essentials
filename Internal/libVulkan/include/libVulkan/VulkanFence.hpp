#pragma once
//! libVulkan
#include <libVulkan/VulkanObject.hpp>

namespace libVulkan
{
	class VulkanFence: public VulkanObject<VulkanFence>
	{
	public:
		VulkanFence(std::shared_ptr<VulkanInstance> const& instance, std::shared_ptr<VulkanDevice> const& device, std::string const& name);
		~VulkanFence() = default;
		//! CPU shall wait untill this fence is signalized from outside
		void wait();
		//! Access to raw object
		vk::Fence operator()() const;

		void setName(std::string const& name) override;

	protected:
		//! Raw objects
		vk::UniqueFence m_rawObject;
	};
}