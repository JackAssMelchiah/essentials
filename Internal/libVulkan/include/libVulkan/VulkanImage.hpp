#pragma once
//! libVulkan
#include <libVulkan/VulkanObject.hpp>

namespace libVulkan
{
	class VulkanImageView;
	class VulkanSurface;
	class VulkanCommandBuffer;
	class VulkanBarrier;
}

namespace libVulkan
{
	class VulkanImage: public VulkanObject<VulkanImage>
	{
	public:
		using Info = struct INFO
		{
			INFO(vk::Format format, vk::ImageAspectFlagBits aspectFlag, vk::ImageUsageFlags usage, vk::MemoryPropertyFlags memoryFlags)
				: format(format)
				, aspectFlag(aspectFlag)
				, usage(usage)
				, memoryFlags(memoryFlags)
			{}
			vk::Format format;
			vk::ImageAspectFlagBits aspectFlag;
			vk::ImageUsageFlags usage;
			vk::MemoryPropertyFlags memoryFlags;
		};

	public:
		//! Normal image
		VulkanImage(std::shared_ptr<VulkanInstance> const& instance, std::shared_ptr<VulkanDevice> const& device, vk::Extent2D const& size, Info const& info, std::string const& name);
		//! Image from Swapchain
		VulkanImage(std::shared_ptr<VulkanInstance> const& instance, std::shared_ptr<VulkanDevice> const& device, VulkanSurface const& surface, vk::Image& image, vk::Extent2D const& size, std::string const& name);
		~VulkanImage();
		vk::Extent2D const& getExtent() const;
		//! Rebuilds the underlying vulkan objects
		void resize(vk::Extent2D const& dims);
		//! Whether this image is an array of images and how many are there
		uint32_t getArrayCount() const;
		//! How much mip maps it supports
		uint32_t getMipMapCount() const;
		//! Getter for bound imageView
		VulkanImageView const& getImageView() const;
		VulkanImageView& getImageView();
		//! Returns info with which class was build
		VulkanImage::Info const& getInfo() const;
		//! Whether image's memory backing is on API 
		bool isSwapChainImage() const;
		//! Access to raw object
		vk::Image operator()() const;
		//! Get info about this image for descriptors to use
		void createDescriptorImageInfo(vk::DescriptorImageInfo& descriptorImageInfo, vk::ImageLayout layout) const;

		//! Barrier generator for correct resource usage
		std::shared_ptr<VulkanBarrier> generateBarrier(vk::ImageLayout oldLayout, vk::ImageLayout targetLayout, vk::AccessFlags2 srcAccessFlags, vk::PipelineStageFlagBits2 srcStage, vk::AccessFlags2 dstAccessFlags, vk::PipelineStageFlagBits2 dstStage);

		void setName(std::string const& name) override;

	protected:
		//! Creates raw image
		vk::UniqueImage createRawImage(vk::Extent3D const& imageSize, vk::ImageType imageDimms, vk::Format imageFormat, uint32_t arrayCount, uint32_t mipMapCount, vk::ImageUsageFlags imageUsage, vk::ImageLayout initialLayout) const;
		//! Creates underlying memory for image
		vk::UniqueDeviceMemory backImageByMemory(vk::Image& rawImage, vk::MemoryPropertyFlags flags);
		//! Initializes class, creates image, backs it by memory and creates view
		void initialize(vk::Extent2D const& dimms, vk::Format format, vk::ImageAspectFlags aspect, uint32_t arrayCount, uint32_t mipMapCount);
		//! Self explanatory
		void createImageAndBackItByMemory(vk::Extent2D const& dimms, uint32_t arrayCount, uint32_t mipMapCount);

	protected:
		//! Info about class
		Info m_info;

		//! Raw image objects
		vk::UniqueImage m_rawImage;
		vk::UniqueDeviceMemory m_rawMemory;
		//! This is because swapchain image is non-owned, it is owned by swapchain
		vk::Image m_swapChainRawImage;

		//! Image's View - owned by this
		std::shared_ptr<VulkanImageView> m_imageView;

		//! Dimms of image
		vk::Extent2D m_extent;	

		bool m_swapchainImage;
	};
}

