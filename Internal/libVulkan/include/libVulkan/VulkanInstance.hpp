#pragma once
#include <vulkan/vulkan.hpp>

namespace libVulkan 
{
    class VulkanDefaults;
    class VulkanManager;
}

namespace libVulkan
{
    class VulkanExtFunctions
    {
    public:
        //! Pointers to functions
        PFN_vkSetDebugUtilsObjectNameEXT vkSetDebugUtilsObjectNameEXT;
        PFN_vkCmdBeginDebugUtilsLabelEXT vkCmdBeginDebugUtilsLabelEXT;
        PFN_vkCmdEndDebugUtilsLabelEXT vkCmdEndDebugUtilsLabelEXT;
        PFN_vkCmdInsertDebugUtilsLabelEXT vkCmdInsertDebugUtilsLabelEXT;
    };

    class VulkanInstance
    {
        static VkBool32 debugMessage(
            VkDebugUtilsMessageSeverityFlagBitsEXT messageSeverity,
            VkDebugUtilsMessageTypeFlagsEXT messageTypes,
            const VkDebugUtilsMessengerCallbackDataEXT* pCallbackData,
            void* pUserData);

        friend class VulkanSurface;
        friend class VulkanManager;

    public:
        VulkanInstance(vk::UniqueInstance instance, std::shared_ptr<VulkanDefaults> defaults, std::string const& name);
        ~VulkanInstance();

        std::string const& getName() const;

        void getPhysicalDeviceLimits(vk::PhysicalDeviceLimits& limitsOut) const;

        uint32_t getMemoryTypeHeap(vk::MemoryRequirements const& memoryRequirements, vk::MemoryPropertyFlags requiredFlags) const;

        bool getSurfaceCapabilites(vk::SurfaceKHR const& surface, vk::SurfaceCapabilitiesKHR& capabilitesOut) const;
        void getSurfaceFormats(vk::SurfaceKHR const& surface, std::vector<vk::SurfaceFormatKHR>& formatsOut) const;
        void getSurfacePresentModes(vk::SurfaceKHR const& surface, std::vector<vk::PresentModeKHR>& presentModesOut) const;

        uint32_t getQueueFamilyIndex(vk::QueueFlagBits) const;
        uint32_t getQueueFamilyCount(uint32_t familyIndex) const;

        uint32_t getPresentationQueueFamilyIndex(vk::SurfaceKHR const& surface) const;
        
        vk::FormatProperties getFormatInfo(vk::Format format) const;

        VulkanDefaults const& getDefaults() const;

        bool formatSupported(vk::Format format, vk::ImageTiling imageTiling, vk::FormatFeatureFlags wantedUsage) const;

        //! Access to raw object
        vk::Instance operator()();
        vk::PhysicalDevice& getRawPhysicalDevice();

        VulkanExtFunctions const& extentions() const;

    protected:
        bool getSurfaceCapabilites(vk::PhysicalDevice const& hwDevice, vk::SurfaceKHR const& surface, vk::SurfaceCapabilitiesKHR& capabilites) const;
        void getSurfaceFormats(vk::PhysicalDevice const& hwDevice, vk::SurfaceKHR const& surface, std::vector<vk::SurfaceFormatKHR>& formatsOut) const;
        void getSurfacePresentModes(vk::PhysicalDevice const& hwDevice, vk::SurfaceKHR const& surface, std::vector<vk::PresentModeKHR>& presentModesOut) const;

        uint32_t getQueueFamilyIndex(vk::PhysicalDevice const& hwDevice, vk::QueueFlagBits) const;
        uint32_t getQueueFamilyCount(vk::PhysicalDevice const& hwDevice, uint32_t familyIndex) const;

        uint32_t getPresentationQueueFamilyIndex(vk::PhysicalDevice const& hwDevice, vk::SurfaceKHR const& surface) const;
        
        bool selectHwDevice(std::vector<vk::QueueFlagBits> const&, vk::PhysicalDeviceType type = vk::PhysicalDeviceType::eDiscreteGpu);

        vk::Result createDebugMessenger();

        std::vector<char const*> filterAvailableDeviceExtensions(std::vector<std::string> const&) const;
        std::vector<char const*> filterAvailableDeviceLayers(std::vector<std::string> const&) const;

        bool pickDevice(std::vector<vk::QueueFlagBits> const& requiredFunc);

        //! Tries to checks as much of stuff requested in Vulkan defaults, and in case of mismatch, will set some valid value, and prints warning 
        void validateDefaults();
        //! Loads the extension function pointers
        void loadExtensionFunctionPointers();

    protected:
        std::string m_name;
        
        //! Raw objects
        vk::UniqueInstance m_rawInstance;
        vk::PhysicalDevice m_hwDevice;

        //! Choosen hw device's hw limits
        vk::PhysicalDeviceLimits m_limits;

        //! Debug output from validation layers
        VkDebugUtilsMessengerEXT m_debugMessenger;
       
        //! Default values to what to initialize vulkan stuff
        std::shared_ptr<VulkanDefaults> m_defaults;

        //extension functions
        VulkanExtFunctions m_extensionFunctions;
    };
}
