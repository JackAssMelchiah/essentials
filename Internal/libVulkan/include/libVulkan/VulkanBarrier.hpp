#pragma once
//! vulkan
#include <vulkan/vulkan.hpp>

namespace libVulkan
{
    class VulkanCommandBuffer;
    class VulkanDevice;
}

namespace libVulkan
{
    class VulkanBarrier
    {
    public:
        VulkanBarrier(vk::ImageMemoryBarrier2 const& imageBarrier, vk::ImageSubresourceRange const& imageRange);
        VulkanBarrier(vk::BufferMemoryBarrier2 const& bufferBarrier);

    protected:
        //! members for image barrier
        vk::ImageMemoryBarrier2 m_imageBarrier;
        vk::ImageSubresourceRange m_imageRange;
        //! members for buffer barrier
        vk::BufferMemoryBarrier2 m_bufferBarrier;
        //! it is expected that this class is either image barrier or buffer barrier, cannot be combination
        bool m_isImageBarrier;

        std::string m_name;
        std::weak_ptr<VulkanDevice> m_device;

    public:
        //! Executes all memory barriers simultaneously
        static void recordBarriers(VulkanCommandBuffer& commandBuffer, std::vector<std::shared_ptr<VulkanBarrier>> const& barriers);
    };


}