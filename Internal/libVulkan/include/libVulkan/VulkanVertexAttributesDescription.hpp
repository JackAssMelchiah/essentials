#pragma once
//! libVulkan
#include <libVulkan/VulkanObject.hpp>

namespace libVulkan 
{
	class VulkanVertexAttributesDescription
	{
	public:
		VulkanVertexAttributesDescription(std::vector<vk::VertexInputAttributeDescription> const& attributes, std::vector<vk::VertexInputBindingDescription> const& bindings);
		~VulkanVertexAttributesDescription() = default;

		std::vector<vk::VertexInputAttributeDescription> const& getDescriptions() const;
		std::vector<vk::VertexInputBindingDescription> const& getBindings() const;

	protected:
		std::vector<vk::VertexInputAttributeDescription> m_attribDescs;
		std::vector<vk::VertexInputBindingDescription> m_bindings;
	};
}