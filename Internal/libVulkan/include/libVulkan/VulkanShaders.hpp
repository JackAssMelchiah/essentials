#pragma once
//! libVulkan
#include <libVulkan/VulkanObject.hpp>
#include <libVulkan/VulkanModule.hpp>
//! Std
#include <map>
#include <memory>

namespace libVulkan
{
	class VulkanShaders
	{
	public:
		enum class ShaderType{ VERTEX, GEOMETRY, TESALATION_CONTROL, TESALATION_EVALUATION, FRAGMENT, COMPUTE};

	public:
		VulkanShaders(std::unique_ptr<VulkanShaderModule>&& computeModule);
		VulkanShaders(std::unique_ptr<VulkanShaderModule>&& vertexModule, std::unique_ptr<VulkanShaderModule>&& fragmentModule);
		VulkanShaders(std::unique_ptr<VulkanShaderModule>&& vertexModule, std::unique_ptr<VulkanShaderModule>&& geometryModule, std::unique_ptr<VulkanShaderModule>&& fragmentModule);
		VulkanShaders(std::unique_ptr<VulkanShaderModule>&& vertexModule, std::unique_ptr<VulkanShaderModule>&& tessalationControllModule, std::unique_ptr<VulkanShaderModule>&& tessalationEvaluationModule, std::unique_ptr<VulkanShaderModule>&& fragmentModule);
		VulkanShaders(std::unique_ptr<VulkanShaderModule>&& vertexModule, std::unique_ptr<VulkanShaderModule>&& geometryModule, std::unique_ptr<VulkanShaderModule>&& tessalationControllModule, std::unique_ptr<VulkanShaderModule>&& tessalationEvaluationModule, std::unique_ptr<VulkanShaderModule>&& fragmentModule);
		~VulkanShaders() = default;
		
		//! Getter for shader module
		void getShadersInfos(std::vector<vk::PipelineShaderStageCreateInfo>& infos);

	protected:
		vk::ShaderStageFlagBits getShaderStageFlag(ShaderType shaderType) const;

	protected:
		std::map<ShaderType, std::unique_ptr<VulkanShaderModule>> m_modules;
	};
}
