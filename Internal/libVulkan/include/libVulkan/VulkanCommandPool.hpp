#pragma once
//! libVulkan
#include <libVulkan/VulkanObject.hpp>
//! Std
#include <set>

namespace libVulkan
{
	class VulkanQueue;
	class VulkanCommandBuffer;
}

namespace libVulkan 
{
	class VulkanCommandPool: public VulkanObject<VulkanCommandPool>
	{
		//! friends
		friend class VulkanCommandBuffer;
	public:
		VulkanCommandPool(std::shared_ptr<VulkanInstance> const& instance, std::shared_ptr<VulkanDevice> const& device, VulkanQueue const& queue, bool allowSingleReset, bool shortlived, std::string const& name);
		~VulkanCommandPool();
		//! Creates new command buffer - intended to be stored somewhere, however, this class owns it!
		std::weak_ptr<VulkanCommandBuffer> createCommandBuffer(vk::CommandBufferLevel level, std::string const& name);
		//! In case that command buffer should no longer be used, can be destroyed 
		void destroyCommandBuffer(std::shared_ptr<VulkanCommandBuffer> commandBuffer);

		//! All command buffers are destroyed
		void freePool();
		//! Whether que which whis was allocated supported graphics operations
		bool supportsGraphics() const;
		//! Access to raw object
		vk::CommandPool operator()() const;
	
		void setName(std::string const& name) override;

	protected:
		//! Creates underlying object
		void createPool(VulkanQueue const& queue, bool allowSingleReset, bool shortLived);
	
	protected:
		//! Owned command buffers (all that has been created by this)
		std::set<std::shared_ptr<VulkanCommandBuffer>> m_commandBuffers;
		//! Raw object
		vk::UniqueCommandPool m_rawCommandPool;

		//! Determines what kind of features are supported by command pool and command buffers
		vk::QueueFlags m_supportedFunctionality;
	};
}