#pragma once
//! libVulkan
#include <libVulkan/VulkanObject.hpp>

namespace libVulkan
{
	class VulkanImage;
}

namespace libVulkan
{
	//TODO: separate to multiple - view image and memory?
	class VulkanImageView: public VulkanObject<VulkanImageView>
	{
	public:
		//! Default constructor
		VulkanImageView(std::shared_ptr<VulkanInstance> const& instance, std::shared_ptr<VulkanDevice> const& device, vk::Image const& image, vk::Format format, vk::ImageAspectFlags aspect, uint32_t arrayCount, uint32_t mipMapCount, vk::ImageViewType imageViewType, std::string const& name);
		~VulkanImageView() = default;
		//! Getter for subresource range - describes how the image is accessed
		vk::ImageSubresourceRange const& getSubresourceRange() const;
		//! Called when resize has occured
		void resize(VulkanImage const& image);
		//! Access to raw object
		vk::ImageView operator()() const;

		void setName(std::string const& name) override;

	protected:
		//! Creates raw image
		void createRawImageView(vk::Image const& rawImage, vk::Format format, vk::ImageViewType imageViewType, vk::ImageAspectFlags imageAspect, uint32_t arrayImageCount, uint32_t mipMapCount);
	
	protected:
		//! Raw object
		vk::UniqueImageView m_rawImageView;
		//! What kind of dimms this is
		vk::ImageViewType m_imageViewType;
		//! Describes how the resource is viewed by view - since technically can get only subset of image feature
		vk::ImageSubresourceRange m_subresourceRange;
	};
}

