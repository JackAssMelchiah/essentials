#pragma once
#include <libVulkan/VulkanObject.hpp>

namespace libVulkan
{
	class VulkanDescriptorSetLayout;
	class VulkanBuffer;
	class VulkanImage;
	class VulkanImageSampler;
}

namespace libVulkan
{
	class VulkanDescriptorSet : public VulkanObject<VulkanDescriptorSet>
	{
	public:
		struct BufferBindingInfo 
		{
			VulkanBuffer& buffer;
			uint32_t bindPointWithinDescriptor;
			uint32_t offsetWithinBuffer;
			uint32_t byteSize;
		};

	public:
		VulkanDescriptorSet(std::shared_ptr<VulkanInstance> const& instance, std::shared_ptr<VulkanDevice> const& device, vk::UniqueDescriptorSet rawSet, std::shared_ptr<VulkanDescriptorSetLayout> descriptorSetLayout, std::string const& name);
		~VulkanDescriptorSet() = default;
		//! Writes contents of the buffers specified in imageInfos into descriptor of same type. The buffer binPoint starts from startingBindPoint, and increments per imageInfo
		void updateImage(uint32_t startingBindPoint, vk::DescriptorType type, std::vector<vk::DescriptorImageInfo> const& imageInfos);
		//! Writes contents of the buffers specified in bufferInfos into descriptor of same type. The buffer binPoint starts from startingBindPoint, and increments per bufferInfo
		void updateBuffer(uint32_t startingBindPoint, vk::DescriptorType type, std::vector<vk::DescriptorBufferInfo> const& bufferInfos);

		//! Writes data from buffer into single descriptor which is described by binding
		void write(uint32_t binding, vk::DescriptorType type, VulkanBuffer const&);
		//! Writes data from Image with sampler support into single descriptor which is described by binding
		void write(uint32_t binding, VulkanImage const&, VulkanImageSampler const&);
		//! Returns layout with which this set has been created
		VulkanDescriptorSetLayout const& getLayout() const;
		//! Access to raw object
		vk::DescriptorSet operator()()const;

		void setName(std::string const& name) override;

	protected:
		//! Generalized write of descriptor set - writes single descriptor which is described by binding
		void write(uint32_t binding, vk::DescriptorType type, vk::BufferView const* texel, vk::DescriptorImageInfo const* image, vk::DescriptorBufferInfo const* buffer);

	protected:
		//! Stored layout description - describes layout of descriptor set - owned by this, just like pipelineLayout is
		std::shared_ptr<VulkanDescriptorSetLayout> m_descriptionSetLayout;

		//! Raw object
		vk::UniqueDescriptorSet m_rawSet;
	};
}