//! libVulkan
#include <libVulkan/VulkanAll.hpp>
//! libUtils
#include <libUtils/Utils.hpp>

namespace libVulkan
{
	VulkanCommandBuffer::VulkanCommandBuffer(std::shared_ptr<VulkanInstance> const& instance, std::shared_ptr<VulkanDevice> const& device, vk::UniqueCommandBuffer&& rawCmdBuffer, vk::CommandBufferLevel level, vk::QueueFlags supportedFunctionality, std::string const& name)
		: VulkanObject(instance, device, name)
		, m_rawCommandBuffer(std::move(rawCmdBuffer))
		, m_commandBufferLevel(level)
		, m_supportedFunctionality(supportedFunctionality)
	{
		setName(getName());
	}

	VulkanCommandBuffer::~VulkanCommandBuffer()
	{
	}

	void VulkanCommandBuffer::beginRecord(VulkanRenderingInfo const& info, vk::CommandBufferUsageFlags usage)
	{
		//TODO: prob assert this->isSecondary
		auto renderingInheritanceInfo = vk::CommandBufferInheritanceRenderingInfo();
		{
			renderingInheritanceInfo.colorAttachmentCount = info.getOutputFormats().size();
			renderingInheritanceInfo.pColorAttachmentFormats = info.getOutputFormats().data();
			renderingInheritanceInfo.depthAttachmentFormat = info.getDepthStencilFormat();
			renderingInheritanceInfo.stencilAttachmentFormat = vk::Format::eUndefined; //TODO
			renderingInheritanceInfo.flags = vk::RenderingFlagBits::eContentsSecondaryCommandBuffers;
			renderingInheritanceInfo.rasterizationSamples = vk::SampleCountFlagBits::e1;
			renderingInheritanceInfo.viewMask = 0;
		}

		auto inheritanceInfo = vk::CommandBufferInheritanceInfo();
		{
			inheritanceInfo.framebuffer = nullptr;
			inheritanceInfo.renderPass = nullptr;
			inheritanceInfo.subpass = 0;
			inheritanceInfo.pNext = &renderingInheritanceInfo;
		}

		auto beginInfo = vk::CommandBufferBeginInfo();
		{
			beginInfo.pInheritanceInfo = &inheritanceInfo; // only for secondary buffers
			beginInfo.flags = usage;
		}
		m_rawCommandBuffer->begin(beginInfo);
	}

	void VulkanCommandBuffer::beginRecord(vk::CommandBufferUsageFlags usage)
	{
		auto beginInfo = vk::CommandBufferBeginInfo();
		{
			beginInfo.pInheritanceInfo = nullptr; // only for secondary buffers
			beginInfo.flags = usage;
		}
		m_rawCommandBuffer->begin(beginInfo);
	}

	void VulkanCommandBuffer::endRecord()
	{
		m_rawCommandBuffer->end();
	}

	void VulkanCommandBuffer::beginDebugRegion(std::string const& message)
	{
		auto label = vk::DebugUtilsLabelEXT();
		{
			label.pLabelName = message.c_str();
			label.color[0] = 0.f;
			label.color[1] = 0.f;
			label.color[2] = 0.f;
			label.color[3] = 1.f;
		}
		getInstance().lock()->extentions().vkCmdBeginDebugUtilsLabelEXT(operator()(), &label.operator const VkDebugUtilsLabelEXT & ());
	}

	void VulkanCommandBuffer::endDebugRegion()
	{
		getInstance().lock()->extentions().vkCmdEndDebugUtilsLabelEXT(operator()());
	}

	void VulkanCommandBuffer::insertDebugMessage(std::string const& message)
	{
		auto label = vk::DebugUtilsLabelEXT();
		{
			label.pLabelName = message.c_str();
			label.color[0] = 0.f;
			label.color[1] = 0.f;
			label.color[2] = 0.f;
			label.color[3] = 1.f;
		}
		getInstance().lock()->extentions().vkCmdInsertDebugUtilsLabelEXT(operator()(), &label.operator const VkDebugUtilsLabelEXT & ());
	}

	void VulkanCommandBuffer::copyBuffer(VulkanBuffer const& sourceBuffer, VulkanBuffer& destinationBuffer, std::vector<vk::BufferCopy> const& regions)
	{
		m_rawCommandBuffer->copyBuffer(sourceBuffer.operator()(), destinationBuffer.operator()(), regions);
	}

	void VulkanCommandBuffer::copyImageToImage(VulkanImage& srcImage, vk::ImageLayout srcImageLayout, VulkanImage& destinationImage, vk::ImageLayout dstImageLayout)
	{
		assert(srcImage.getInfo().aspectFlag == destinationImage.getInfo().aspectFlag);
		assert(srcImage.getExtent() == destinationImage.getExtent());
		
		//!Todo: for each mip
		auto imageRegion = vk::ImageCopy();
		{
			imageRegion.dstOffset = 0;
			imageRegion.dstSubresource.aspectMask = vk::ImageAspectFlagBits::eColor;
			imageRegion.dstSubresource.baseArrayLayer = 0;
			imageRegion.dstSubresource.layerCount = srcImage.getArrayCount();
			imageRegion.dstSubresource.mipLevel = 0;
			
			imageRegion.extent.depth = 1;
			imageRegion.extent.height = srcImage.getExtent().height;
			imageRegion.extent.width = srcImage.getExtent().width;
			imageRegion.srcOffset = 0;
			
			imageRegion.srcSubresource.aspectMask = vk::ImageAspectFlagBits::eColor;
			imageRegion.srcSubresource.baseArrayLayer = 0;
			imageRegion.srcSubresource.layerCount = destinationImage.getArrayCount();
			imageRegion.srcSubresource.mipLevel = 0;
		}
		m_rawCommandBuffer->copyImage(srcImage.operator()(), srcImageLayout, destinationImage.operator()(), dstImageLayout, {imageRegion});

	}

	void VulkanCommandBuffer::copyBufferToImage(VulkanBuffer const& sourceBuffer, vk::DeviceSize bufferOffset, VulkanImage const& destinationImage)
	{
		auto region = vk::BufferImageCopy();
		{
			region.bufferOffset = bufferOffset;
			
			region.bufferImageHeight = 0;// no padding between pixels in memory
			region.bufferRowLength = 0;// no padding between pixels in memory

			//allways copy whole image
			region.imageOffset = vk::Offset3D{0, 0, 0};
			region.imageExtent = vk::Extent3D(destinationImage.getExtent(), 1);
			
			region.imageSubresource.aspectMask = destinationImage.getInfo().aspectFlag;
			region.imageSubresource.baseArrayLayer = 0; //start from 0 array
			region.imageSubresource.layerCount = destinationImage.getArrayCount(); //to count array
			region.imageSubresource.mipLevel = 0; // for now no mipmapping
		}

		m_rawCommandBuffer->copyBufferToImage(sourceBuffer.operator()(), destinationImage.operator()(), vk::ImageLayout::eTransferDstOptimal, { region });
	}

	void VulkanCommandBuffer::executeSecondaryBuffers(std::vector<std::reference_wrapper<VulkanCommandBuffer>> const& buffers)
	{
		//TODO: is slow, that for loop hurts
		std::vector<vk::CommandBuffer> secondaries;
		for (auto& buffer : buffers)
		{
			secondaries.push_back(buffer.operator()());
		}
		m_rawCommandBuffer->executeCommands(secondaries);
	}

	void VulkanCommandBuffer::draw(int vertexCount, int instanceCount, int firstVertex, int firstInstance)
	{
		m_rawCommandBuffer->draw(vertexCount, instanceCount, firstVertex, firstInstance);
	}

	void VulkanCommandBuffer::drawIndexed(int indexCount, int instanceCount, int firstVertex, int firstInstance)
	{
		m_rawCommandBuffer->drawIndexed(indexCount, instanceCount, firstVertex, 0, firstInstance);
	}

	void VulkanCommandBuffer::reset(bool releaseResources)
	{
		if (!releaseResources)
			m_rawCommandBuffer->reset();
		else
			m_rawCommandBuffer->reset({ vk::CommandBufferResetFlagBits::eReleaseResources });
	}

	vk::CommandBuffer VulkanCommandBuffer::operator()()
	{
		return m_rawCommandBuffer.get();
	}

	void VulkanCommandBuffer::setName(std::string const& name)
	{
		VulkanObject::setName(name);
		if (operator()())
			getDevice().lock()->setDebugObjectName(reinterpret_cast<uint64_t>(static_cast<VkCommandBuffer>(operator()())), vk::ObjectType::eCommandBuffer, name);
	}

	void VulkanCommandBuffer::beginRendering(std::vector<std::shared_ptr<VulkanImageAttachment>> const& colorAttachments, std::shared_ptr<VulkanImageAttachment> const& depthStencil, vk::Extent2D const& extent, vk::AttachmentLoadOp loadOp, vk::AttachmentStoreOp storeOp, bool executesSecondaryCommandBuffers, std::string const& description)
	{
		assert((m_commandBufferLevel == vk::CommandBufferLevel::ePrimary) && supportsGraphics());

		m_renderingDesc = description;

		auto& defaults = getInstance().lock()->getDefaults();
		auto& defaultClearColor = defaults.clearColor;
		auto& defaultClearDepth = defaults.clearDepth;

		auto colorClearValue = vk::ClearValue{ std::array<float, 4>{ defaultClearColor.x, defaultClearColor.y, defaultClearColor.z, defaultClearColor.w  } };
		auto depthStencilClearValue = vk::ClearDepthStencilValue{ defaultClearDepth.x, uint32_t(defaultClearDepth.y) };

		auto renderingArea = vk::Rect2D({ 0, 0 }, extent);

		auto attachmentInfos = std::vector<vk::RenderingAttachmentInfo>();

		for (auto i = 0; i < colorAttachments.size(); ++i)
		{
			auto info = vk::RenderingAttachmentInfo();
			{
				info.clearValue = colorClearValue;
				info.imageLayout = vk::ImageLayout::eColorAttachmentOptimal;
				info.imageView = colorAttachments.at(i)->getImage().getImageView()();
				info.loadOp = vk::AttachmentLoadOp::eLoad;
				info.storeOp = vk::AttachmentStoreOp::eStore;
				//resolve - currently no need for that
				//info.resolveImageLayout;
				//info.resolveImageView;
				//info.resolveMode;
			}
			attachmentInfos.push_back(info);
		}

		auto depthInfo = vk::RenderingAttachmentInfo();
		if (depthStencil)
		{
			depthInfo.imageLayout = vk::ImageLayout::eDepthAttachmentOptimal;
			depthInfo.imageView = depthStencil->getImage().getImageView()();
			depthInfo.loadOp = loadOp;
			depthInfo.storeOp = storeOp;
		}

		//TODO: everyng in this function up to this point should be part of the rendering link --- maybe rendrering link should provide
		//this should not be changed but instead of using in command buffer this method, should call something like
		//link->beginLink(cmdBuff, bool secondaries) and that should call this, with all data inferred. 
		//run generate the clear colors, should also clear new attachments, preserve the others (this should be specified in rendering info struct )

		auto renderingInfo = vk::RenderingInfo();
		{
			renderingInfo.colorAttachmentCount = attachmentInfos.size();
			renderingInfo.pColorAttachments = attachmentInfos.data();
			renderingInfo.pDepthAttachment = depthStencil ? &depthInfo : nullptr;
			renderingInfo.pStencilAttachment = nullptr;
			renderingInfo.renderArea = renderingArea;
			renderingInfo.flags = executesSecondaryCommandBuffers ? vk::RenderingFlagBits::eContentsSecondaryCommandBuffers : vk::RenderingFlagBits{};
			renderingInfo.layerCount = 1;
			renderingInfo.viewMask = 0;//no multiview
		}
	
		if (!m_renderingDesc.empty())
			beginDebugRegion(m_renderingDesc);

		m_rawCommandBuffer->beginRendering(renderingInfo);
	}

	void VulkanCommandBuffer::endRendering()
	{
		m_rawCommandBuffer->endRendering();
		if (!m_renderingDesc.empty())
		{
			endDebugRegion();
			m_renderingDesc = "";
		}
	}

	void VulkanCommandBuffer::bindPipeline(VulkanPipeline& pipeline)
	{
		m_rawCommandBuffer->bindPipeline(vk::PipelineBindPoint::eGraphics, pipeline.operator()());
	}

	void VulkanCommandBuffer::bindVertexBuffers(std::vector<std::reference_wrapper<VulkanBuffer>>const& buffers)
	{
		// defaultly binds first buffer to BP 0, there is no support for dynamic offsets yet
		auto rawVertexBuffers = std::vector<vk::Buffer>();
		auto offsets = std::vector<vk::DeviceSize>();
		for (auto& buffer: buffers)
		{
			rawVertexBuffers.push_back(buffer.get().operator()());
			offsets.push_back(0);
		}
		m_rawCommandBuffer->bindVertexBuffers(0, rawVertexBuffers, offsets);
	}

	void VulkanCommandBuffer::bindIndexBuffer(VulkanBuffer& buffer, vk::IndexType type)
	{
		m_rawCommandBuffer->bindIndexBuffer(buffer.operator()(), 0, type);
	}

	void VulkanCommandBuffer::bindDescriptorSet(VulkanDescriptorSet const& descriptorSet, uint32_t setNumber, VulkanPipelineLayout& pipelineLayout)
	{
		auto rawDescriptorSets = std::vector<vk::DescriptorSet>{ descriptorSet.operator()() };
		m_rawCommandBuffer->bindDescriptorSets(vk::PipelineBindPoint::eGraphics, pipelineLayout.operator()(), setNumber, rawDescriptorSets, {});
	}

	void VulkanCommandBuffer::bindDescriptorSets(std::vector<std::reference_wrapper<VulkanDescriptorSet>> const& descriptorSets, VulkanPipelineLayout& pipelineLayout)
	{
		auto rawDescriptorSets = std::vector<vk::DescriptorSet>();
		for(auto& descriptorSet: descriptorSets){
			rawDescriptorSets.push_back(descriptorSet.get().operator()());
		}
		m_rawCommandBuffer->bindDescriptorSets(vk::PipelineBindPoint::eGraphics, pipelineLayout.operator()(), 0, rawDescriptorSets, {});
	}
	
	void VulkanCommandBuffer::recordClearAttachments(std::vector<std::shared_ptr<VulkanImageAttachment>> const& colorAttachments, std::shared_ptr<VulkanImageAttachment> const& depthStencil, glm::vec4 const& clearColor, glm::vec2 const& depthStencilColor)
	{
		auto clearAttachments = std::vector<vk::ClearAttachment>();
		auto clearRectangles = std::vector<vk::ClearRect>();
		for (auto attachmentIndex = 0; attachmentIndex < colorAttachments.size(); ++attachmentIndex)
		{
			auto& image = colorAttachments.at(attachmentIndex)->getImage();
			auto clearValue = vk::ClearValue();
			clearValue.color = std::array<float, 4>{ clearColor.x, clearColor.y, clearColor.z, clearColor.w };

			auto clearColorAttachment = vk::ClearAttachment();
			{
				clearColorAttachment.aspectMask = image.getInfo().aspectFlag;
				clearColorAttachment.clearValue = clearValue;
				clearColorAttachment.colorAttachment = attachmentIndex;
			}
			clearAttachments.push_back(clearColorAttachment);

			auto clearRectangle = vk::ClearRect();
			{
				clearRectangle.baseArrayLayer = 0;
				clearRectangle.layerCount = image.getArrayCount();
				clearRectangle.rect.offset = vk::Offset2D(0, 0);
				clearRectangle.rect.extent = image.getExtent();
			}
			clearRectangles.push_back(clearRectangle);
		}

		if (depthStencil)
		{
			auto& image = depthStencil->getImage();
			auto clearValue = vk::ClearValue();
			clearValue.depthStencil.depth = depthStencilColor.x;
			clearValue.depthStencil.stencil = depthStencilColor.y;

			auto clearColorAttachment = vk::ClearAttachment();
			{
				clearColorAttachment.aspectMask = image.getInfo().aspectFlag;
				clearColorAttachment.clearValue = clearValue;
				clearColorAttachment.colorAttachment = 0;
			}
			clearAttachments.push_back(clearColorAttachment);

			auto clearRectangle = vk::ClearRect();
			{
				clearRectangle.baseArrayLayer = 0;
				clearRectangle.layerCount = image.getArrayCount();
				clearRectangle.rect.offset = vk::Offset2D(0, 0);
				clearRectangle.rect.extent = image.getExtent();
			}
			clearRectangles.push_back(clearRectangle);
		}

		m_rawCommandBuffer->clearAttachments(clearAttachments, clearRectangles);
	}

	bool VulkanCommandBuffer::supportsGraphics() const
	{
		return (m_supportedFunctionality & vk::QueueFlagBits::eGraphics).operator bool();
	}
}