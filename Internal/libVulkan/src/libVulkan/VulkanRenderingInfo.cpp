//! libVulkan
#include <libVulkan/VulkanRenderingInfo.hpp>

namespace libVulkan
{
	VulkanRenderingInfo::VulkanRenderingInfo(std::vector<vk::Format> const& inputAttachments, vk::Format depthStencilAttachment, std::vector<vk::Format> const& outputAttachments, std::map<uint32_t, uint32_t> const& sharedIOAttachments)
		: m_inputAttachments(inputAttachments)
		, m_outputAttachments(outputAttachments)
		, m_depthStencil(depthStencilAttachment)
		, m_sharedIOAttachments(sharedIOAttachments)
	{
	}

	std::vector<vk::Format> const& VulkanRenderingInfo::getInputFormats() const
	{
		return m_inputAttachments;
	}

	std::vector<vk::Format> const& VulkanRenderingInfo::getOutputFormats() const
	{
		return m_outputAttachments;
	}

	vk::Format const& VulkanRenderingInfo::getDepthStencilFormat() const
	{
		return m_depthStencil;
	}

	std::map<uint32_t, uint32_t> const& VulkanRenderingInfo::getSharedAttachments() const
	{
		return m_sharedIOAttachments;
	}

	bool VulkanRenderingInfo::operator ==(VulkanRenderingInfo const& info) const
	{
		bool retval = true;
		if (info.m_inputAttachments.size() == m_inputAttachments.size() && info.m_sharedIOAttachments == m_sharedIOAttachments && info.m_depthStencil == m_depthStencil)
		{
			for (auto i = 0; i < info.m_inputAttachments.size(); ++i)
			{
				if (info.m_inputAttachments.at(i) != m_inputAttachments.at(i))
				{
					retval = false;
					break;
				}
			}
			for (auto i = 0; i < info.m_outputAttachments.size(); ++i)
			{
				if (info.m_outputAttachments.at(i) != m_outputAttachments.at(i))
				{
					retval = false;
					break;
				}
			}

			for (auto i = 0; i < info.m_sharedIOAttachments.size(); ++i)
			{
				if (info.m_sharedIOAttachments.at(i) != m_sharedIOAttachments.at(i))
				{
					retval = false;
					break;
				}
			}
		}
		else
			retval = false;
		return retval;		
	}

	void VulkanRenderingInfo::selectSimplePassFormats(VulkanInstance const& instance, std::vector<vk::Format>& inputFormatOut, vk::Format& depthStencilOut, std::vector<vk::Format>& outputFormatOut)
	{
		inputFormatOut.clear();
		depthStencilOut = vk::Format::eUndefined;
		outputFormatOut.clear();

		//output attachment needs image with 4 channels, and optimal tiling
		auto possibleAttachmentFormats = std::vector<vk::Format>{ vk::Format::eB8G8R8A8Unorm, vk::Format::eB8G8R8A8Unorm, vk::Format::eR8G8B8A8Srgb, vk::Format::eB8G8R8A8Srgb };
		for (auto& format : possibleAttachmentFormats)
		{
			if (instance.formatSupported(format, vk::ImageTiling::eOptimal, vk::FormatFeatureFlagBits::eColorAttachment | vk::FormatFeatureFlagBits::eTransferDst))
			{
				outputFormatOut.push_back(format);
				break;
			}
		}
		//depth attachment is whatever, pick the best - no need for the stencil
		auto possibleDepthFormats = std::vector<vk::Format>{ vk::Format::eD32Sfloat, vk::Format::eD16Unorm, vk::Format::eS8Uint };
		for (auto& format : possibleDepthFormats)
		{
			if (instance.formatSupported(format, vk::ImageTiling::eOptimal, vk::FormatFeatureFlagBits::eColorAttachment | vk::FormatFeatureFlagBits::eTransferDst))
			{
				depthStencilOut = format;
				break;
			}
		}
	}
}