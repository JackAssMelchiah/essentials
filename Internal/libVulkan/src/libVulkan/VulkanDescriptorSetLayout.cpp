//! libVulkan
#include <libVulkan/VulkanDescriptorSetLayout.hpp>

namespace libVulkan
{
	VulkanDescriptorSetLayout::VulkanDescriptorSetLayout(std::shared_ptr<VulkanInstance> const& instance, std::shared_ptr<VulkanDevice> const& device, std::vector<vk::DescriptorSetLayoutBinding> const& setLayoutBindings, std::string const& name)
		: VulkanObject(instance, device, name)
		, m_setLayoutBindings(setLayoutBindings)
	{
		initialize();
		setName(getName());
	}

	vk::DescriptorSetLayout VulkanDescriptorSetLayout::operator()() const
	{
		return m_rawSetLayout.get();
	}

	void VulkanDescriptorSetLayout::setName(std::string const& name)
	{
		VulkanObject::setName(name);
		if (operator()())
			getDevice().lock()->setDebugObjectName(reinterpret_cast<uint64_t>(static_cast<VkDescriptorSetLayout>(operator()())), vk::ObjectType::eDescriptorSetLayout, name);
	}

	std::vector<vk::DescriptorSetLayoutBinding> const& VulkanDescriptorSetLayout::getLayoutBindings() const
	{
		return m_setLayoutBindings;
	}

	void VulkanDescriptorSetLayout::initialize()
	{
		auto setlayoutInfo = vk::DescriptorSetLayoutCreateInfo();
		{
			setlayoutInfo.bindingCount = m_setLayoutBindings.size();
			setlayoutInfo.pBindings = m_setLayoutBindings.data();
		}
		m_rawSetLayout = getDevice().lock()->operator()().createDescriptorSetLayoutUnique(setlayoutInfo);
	}
}