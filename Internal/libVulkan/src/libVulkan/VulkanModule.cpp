#pragma once
//! libVulkan
#include <libVulkan/VulkanModule.hpp>
#include <libVulkan/VulkanAll.hpp>

#include <libUtils/Utils.hpp>

namespace libVulkan
{

    VulkanShaderModule::VulkanShaderModule(std::shared_ptr<VulkanInstance> const& instance, std::shared_ptr<VulkanDevice> const& device, std::string const& name)
        : VulkanObject(instance, device, name)
    {
    }

    void VulkanShaderModule::loadAndCreate(std::string const& path)
    {
        //! default shaders
        std::vector<uint32_t> spvSource;

        auto realPath = path;

        if (!libUtils::exists(realPath))
        {
            realPath = "shaders/" + path;
            if (!libUtils::exists(realPath))
                libUtils::printOutErr("Unable to load shader on path " + path);
        }

        libUtils::readFile(realPath, spvSource);

        create(spvSource);
    }

    void VulkanShaderModule::create(std::vector<uint32_t> const& spv)
    {
        auto lockedDevice = getDevice().lock();

        auto moduleInfo = vk::ShaderModuleCreateInfo();
        {
            moduleInfo.codeSize = spv.size();
            moduleInfo.pCode = spv.data();
        }
        m_module = lockedDevice->operator()().createShaderModuleUnique(moduleInfo);

        setName(getName());
    }

    vk::ShaderModule VulkanShaderModule::operator()() const
    {
        return *m_module;
    }

    void VulkanShaderModule::setName(std::string const& name)
    {
        VulkanObject::setName(name);
        if (operator()())
            getDevice().lock()->setDebugObjectName(reinterpret_cast<uint64_t>(static_cast<VkShaderModule>(operator()())), vk::ObjectType::eShaderModule, name);
    }
}