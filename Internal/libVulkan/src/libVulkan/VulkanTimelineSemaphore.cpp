//! libVulkan
#include <libVulkan/VulkanTimelineSemaphore.hpp>

namespace libVulkan
{
	VulkanTimelineSemaphore::VulkanTimelineSemaphore(std::shared_ptr<VulkanInstance> const& instance, std::shared_ptr<VulkanDevice> const& device, std::string const& name)
		: VulkanObject(instance, device, name)
	{
		recreate();
	}

	vk::Semaphore VulkanTimelineSemaphore::operator()() const
	{
		return m_rawObject.get();
	}

	void VulkanTimelineSemaphore::setName(std::string const& name)
	{
		VulkanObject::setName(name);
		if (operator()())
			getDevice().lock()->setDebugObjectName(reinterpret_cast<uint64_t>(static_cast<VkSemaphore>(operator()())), vk::ObjectType::eSemaphore, name);
	}

	void VulkanTimelineSemaphore::recreate()
	{
		auto timelineCreateInfo = vk::SemaphoreTypeCreateInfo();
		{
			timelineCreateInfo.initialValue = 0;
			timelineCreateInfo.semaphoreType = vk::SemaphoreType::eTimeline;
		}

		auto info = vk::SemaphoreCreateInfo();
		{
			info.pNext = &timelineCreateInfo;
		};

		m_rawObject = getDevice().lock()->operator()().createSemaphoreUnique(info);
		setName(getName());
	}
}