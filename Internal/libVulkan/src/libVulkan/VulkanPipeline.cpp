//! libVulkan
#include <libVulkan/VulkanPipeline.hpp>
#include <libVulkan/VulkanAll.hpp>

namespace libVulkan
{
	VulkanPipeline::VulkanPipeline(std::shared_ptr<VulkanInstance> const& instance, std::shared_ptr<VulkanDevice> const& device, std::shared_ptr<VulkanShaders> const& shaders, std::shared_ptr<VulkanPipelineLayout> const& pipelineLayout, std::shared_ptr<VulkanVertexAttributesDescription> const& vertexAttribsDescription, std::shared_ptr<VulkanRenderingInfo> const& vulkanRenderingInfo, vk::Extent2D const& resolution, std::string const& name)
		: VulkanObject(instance, device, name)
		, m_dirty(true)
	{
		createPipeline(shaders, vertexAttribsDescription, pipelineLayout, vulkanRenderingInfo, resolution);
	}

	void VulkanPipeline::setShader(std::shared_ptr<VulkanShaders> const& shaders)
	{
		m_shaders = shaders;
		m_dirty = true;
	}

	void VulkanPipeline::setVertexDescription(std::shared_ptr<VulkanVertexAttributesDescription> const& vertexAttribsDescription)
	{
		if (!vertexAttribsDescription || vertexAttribsDescription->getBindings().empty() || vertexAttribsDescription->getDescriptions().empty())
			return;

		m_vertexAttribsDescription = vertexAttribsDescription;

		m_vertexInputInfo.vertexAttributeDescriptionCount = m_vertexAttribsDescription->getDescriptions().size();
		m_vertexInputInfo.pVertexAttributeDescriptions = m_vertexAttribsDescription->getDescriptions().data();
		m_vertexInputInfo.vertexBindingDescriptionCount = m_vertexAttribsDescription->getBindings().size();
		m_vertexInputInfo.pVertexBindingDescriptions = m_vertexAttribsDescription->getBindings().data();

		m_dirty = true;
	}

	void VulkanPipeline::setLayoutDescription(std::shared_ptr<VulkanPipelineLayout> const& pipelineLayout)
	{
		if (!pipelineLayout)
			return;

		m_pipelineLayout = pipelineLayout;
		m_dirty = true;
	}

	void VulkanPipeline::setRenderingInfo(std::shared_ptr<VulkanRenderingInfo> const& renderingInfo)
	{
		m_renderingInfo = renderingInfo;
		m_dirty = true;
	}

	void VulkanPipeline::setInputAssebly(vk::PrimitiveTopology t, bool p)
	{
		m_inputAssemblyInfo.topology = t;
		m_inputAssemblyInfo.primitiveRestartEnable = p;

		m_dirty = true;
	}

	void VulkanPipeline::setRasterizer(vk::PolygonMode polyMode, vk::CullModeFlagBits cullMode, vk::FrontFace frontFace)
	{
		m_rasterizerInfo.cullMode = cullMode;
		m_rasterizerInfo.frontFace = frontFace;
		m_rasterizerInfo.polygonMode = polyMode;

		//allways let geometry pass thru rasterizer stage
		m_rasterizerInfo.rasterizerDiscardEnable = VK_FALSE;
		//allways have line width 1.0 - bigger requires wideLines GPU feature
		m_rasterizerInfo.lineWidth = 1.f;
		//allways disable depth bias - good stuff for shadow mapping
		m_rasterizerInfo.depthBiasEnable = VK_FALSE;
		m_rasterizerInfo.depthBiasClamp = 0.f;
		m_rasterizerInfo.depthBiasConstantFactor = 0.f;
		m_rasterizerInfo.depthBiasSlopeFactor = 0.f;
		//allways discard fragments beyond near and far planes - good stuff for shadow mapping
		m_rasterizerInfo.depthClampEnable = false;

		m_dirty = true;
	}

	vk::Pipeline VulkanPipeline::operator()()
	{
		return m_rawPipeline.get();
	}

	bool VulkanPipeline::isDirty() const
	{
		return m_dirty;
	}

	void VulkanPipeline::setName(std::string const& name)
	{
		VulkanObject::setName(name);
		if (operator()())
			getDevice().lock()->setDebugObjectName(reinterpret_cast<uint64_t>(static_cast<VkPipeline>(operator()())), vk::ObjectType::ePipeline, name);
	}

	void VulkanPipeline::createPipeline(std::shared_ptr<VulkanShaders> const& shaders, std::shared_ptr<VulkanVertexAttributesDescription> const& vertexAttribsDescription, std::shared_ptr<VulkanPipelineLayout> const& pipelineLayout, std::shared_ptr<VulkanRenderingInfo> const& renderingInfo, vk::Extent2D const& resolution)
	{
		setShader(shaders);
		setVertexDescription(vertexAttribsDescription);
		setLayoutDescription(pipelineLayout);
		setRenderingInfo(renderingInfo);

		setInputAssebly(vk::PrimitiveTopology::eTriangleList, false);
		setRasterizer(vk::PolygonMode::eFill, vk::CullModeFlagBits::eNone, vk::FrontFace::eClockwise);

		setViewPort(resolution);

		setDepthTest(true, true);

		//disabled stuff now, just to fill up to default values
		setStencilTest();
		setColorBlending();
		setMultisampling();
		//scizzor test will get set up with viewport, no need to call it here
		compilePipeline();
	}

	void VulkanPipeline::compilePipeline()
	{
		// shaders
		auto rawShaderStages = std::vector<vk::PipelineShaderStageCreateInfo>();
		m_shaders->getShadersInfos(rawShaderStages);

		auto renderingPipelineCreateInfo = vk::PipelineRenderingCreateInfo();
		{
			renderingPipelineCreateInfo.colorAttachmentCount = m_renderingInfo->getOutputFormats().size();
			renderingPipelineCreateInfo.pColorAttachmentFormats = m_renderingInfo->getOutputFormats().data();
			renderingPipelineCreateInfo.depthAttachmentFormat = m_renderingInfo->getDepthStencilFormat();
			renderingPipelineCreateInfo.stencilAttachmentFormat = vk::Format::eUndefined;
			renderingPipelineCreateInfo.viewMask = 0;
		}

		//for now does not support depth stencil, dynamic state
		auto pipelineCreateInfo = vk::GraphicsPipelineCreateInfo();
		{
			pipelineCreateInfo.stageCount = rawShaderStages.size();
			pipelineCreateInfo.pStages = rawShaderStages.data();
			pipelineCreateInfo.pVertexInputState = &m_vertexInputInfo;
			pipelineCreateInfo.pInputAssemblyState = &m_inputAssemblyInfo;
			pipelineCreateInfo.pViewportState = &m_vpszInfo;
			pipelineCreateInfo.pRasterizationState = &m_rasterizerInfo;
			pipelineCreateInfo.pMultisampleState = &m_multisampleInfo;
			pipelineCreateInfo.pDepthStencilState = &m_depthInfo;
			pipelineCreateInfo.pColorBlendState = &m_blendInfo;
			pipelineCreateInfo.pDynamicState = nullptr; //not supported now
			pipelineCreateInfo.layout = m_pipelineLayout->operator()();
			pipelineCreateInfo.renderPass = nullptr;
			pipelineCreateInfo.subpass = 0;
			//for reusing old pipeline
			pipelineCreateInfo.basePipelineHandle = VK_NULL_HANDLE;
			pipelineCreateInfo.basePipelineIndex = -1;
			pipelineCreateInfo.pNext = &renderingPipelineCreateInfo;
		}

		m_rawPipeline = getDevice().lock()->operator()().createGraphicsPipelineUnique(VK_NULL_HANDLE, pipelineCreateInfo).value;
		setName(getName());
		m_dirty = false;
	}

	void VulkanPipeline::setDepthTest(bool enable, bool writeFragmentDepth)
	{
		m_depthInfo.depthTestEnable = enable;
		m_depthInfo.depthWriteEnable = writeFragmentDepth;
		m_depthInfo.depthCompareOp = vk::CompareOp::eLess;
		
		//if ever needed to preserve fragments within specific depth range
		m_depthInfo.depthBoundsTestEnable = VK_FALSE;
		m_depthInfo.minDepthBounds = 1.f;
		m_depthInfo.maxDepthBounds = 1.f;

		//stencil test - for now dont use
		m_depthInfo.stencilTestEnable = VK_FALSE;
		m_depthInfo.front = vk::StencilOp::eKeep;
		m_depthInfo.back = vk::StencilOp::eKeep;
	}

	void VulkanPipeline::setStencilTest()
	{
		m_dirty = true;
	}

	void VulkanPipeline::setColorBlending()
	{
		m_blendAttachment.blendEnable = VK_FALSE;

		m_blendAttachment.srcColorBlendFactor = vk::BlendFactor::eOne;
		m_blendAttachment.dstColorBlendFactor = vk::BlendFactor::eOne;
		m_blendAttachment.colorBlendOp = vk::BlendOp::eAdd;

		m_blendAttachment.srcAlphaBlendFactor = vk::BlendFactor::eOne;
		m_blendAttachment.dstAlphaBlendFactor = vk::BlendFactor::eOne;
		m_blendAttachment.alphaBlendOp = vk::BlendOp::eAdd;

		//allways set write mask for all
		m_blendAttachment.colorWriteMask = vk::ColorComponentFlagBits::eR | vk::ColorComponentFlagBits::eG | vk::ColorComponentFlagBits::eB | vk::ColorComponentFlagBits::eA;
	
		buildBlendInfo();
	}

	void VulkanPipeline::setMultisampling()
	{
		m_multisampleInfo.sampleShadingEnable = VK_FALSE;
		m_multisampleInfo.rasterizationSamples = vk::SampleCountFlagBits::e1;
		m_multisampleInfo.minSampleShading = 1.0f; 
		m_multisampleInfo.pSampleMask = nullptr;
		m_multisampleInfo.alphaToCoverageEnable = VK_FALSE;
		m_multisampleInfo.alphaToOneEnable = VK_FALSE;

		m_dirty = true;
	}

	void VulkanPipeline::setViewPort(vk::Extent2D const& vp)
	{
		// Fixme: should flipp height here 
		m_viewPort.x = 0.0f;
		m_viewPort.y = 0.0f;
		m_viewPort.width = float(vp.width);
		m_viewPort.height = float(vp.height);
		m_viewPort.minDepth = 0.0f;
		m_viewPort.maxDepth = 1.0f;

		setScizzorTest({ m_viewPort.x , m_viewPort.y }, {vp.width, vp.height});

		buildVPSZInfo();
	}

	VulkanShaders& VulkanPipeline::getShaders()
	{
		return *m_shaders;
	}

	VulkanVertexAttributesDescription& VulkanPipeline::getVertexAttribsDescription()
	{
		assert(m_vertexAttribsDescription);
		return *m_vertexAttribsDescription;
	}

	VulkanPipelineLayout& VulkanPipeline::getPipelineLayout()
	{
		return *m_pipelineLayout;
	}

	bool VulkanPipeline::hasVertexAttributesDescription() const
	{
		return m_vertexAttribsDescription != nullptr;
	}

	void VulkanPipeline::setScizzorTest(glm::ivec2 const& offset, glm::ivec2 const& size)
	{
		m_scizzor.offset.x = (float)offset.x;
		m_scizzor.offset.y = (float)offset.y;

		m_scizzor.extent.width = (float)size.x;
		m_scizzor.extent.height = (float)size.y;
	}

	void VulkanPipeline::buildVPSZInfo()
	{
		//its per framebuffer
		m_vpszInfo.viewportCount = 1;
		m_vpszInfo.pViewports = &m_viewPort;
		m_vpszInfo.scissorCount = 1;
		m_vpszInfo.pScissors = &m_scizzor;

		m_dirty = true;
	}

	void VulkanPipeline::buildBlendInfo()
	{
		//its per framebuffer
		m_blendInfo.attachmentCount = 1;
		m_blendInfo.pAttachments = &m_blendAttachment;

		//dont use blend constants
		m_blendInfo.blendConstants[0] = 0.0f; // Optional
		m_blendInfo.blendConstants[1] = 0.0f; // Optional
		m_blendInfo.blendConstants[2] = 0.0f; // Optional
		m_blendInfo.blendConstants[3] = 0.0f; // Optional

		//dont use bitwise logic blend operations
		m_blendInfo.logicOpEnable = VK_FALSE;
		m_blendInfo.logicOp = vk::LogicOp::eNoOp; // Optional

		m_dirty = true;
	}
}