//! libVulkan
#include <libVulkan/VulkanImageView.hpp>
#include <libVulkan/VulkanImage.hpp>

namespace libVulkan
{
	VulkanImageView::VulkanImageView(std::shared_ptr<VulkanInstance> const& instance, std::shared_ptr<VulkanDevice> const& device, vk::Image const& image, vk::Format format, vk::ImageAspectFlags aspect, uint32_t arrayCount, uint32_t mipMapCount, vk::ImageViewType imageViewType, std::string const& name)
        : VulkanObject(instance, device, name)
        , m_imageViewType(imageViewType)
	{
        createRawImageView(image, format, m_imageViewType, aspect, arrayCount, mipMapCount);
        setName(getName());
	}

    vk::ImageSubresourceRange const& VulkanImageView::getSubresourceRange() const
    {
        return m_subresourceRange;
    }

	vk::ImageView VulkanImageView::operator()() const
	{
		return m_rawImageView.get();
	}

    void VulkanImageView::setName(std::string const& name)
    {
        VulkanObject::setName(name);
        if (operator()())
            getDevice().lock()->setDebugObjectName(reinterpret_cast<uint64_t>(static_cast<VkImageView>(operator()())), vk::ObjectType::eImageView, name);
    }

	void VulkanImageView::resize(VulkanImage const& image)
	{
       createRawImageView(image.operator()(), image.getInfo().format, m_imageViewType, m_subresourceRange.aspectMask, m_subresourceRange.layerCount, m_subresourceRange.levelCount);
	}

	void VulkanImageView::createRawImageView(vk::Image const& rawImage, vk::Format format, vk::ImageViewType imageviewType, vk::ImageAspectFlags imageAspect, uint32_t arrayImageCount, uint32_t mipMapCount)
	{
        //create view for image - all arrays and mipmaps
        {
            m_subresourceRange.aspectMask = imageAspect;
            m_subresourceRange.baseArrayLayer = 0;
            m_subresourceRange.layerCount = arrayImageCount;
            m_subresourceRange.baseMipLevel = 0;
            m_subresourceRange.levelCount = mipMapCount;
        }

        auto componentMapping = vk::ComponentMapping();
        {
            componentMapping.r = vk::ComponentSwizzle::eIdentity;
            componentMapping.g = vk::ComponentSwizzle::eIdentity;
            componentMapping.b = vk::ComponentSwizzle::eIdentity;
            componentMapping.a = vk::ComponentSwizzle::eIdentity;
        }

        auto imageViewInfo = vk::ImageViewCreateInfo();
        {
            imageViewInfo.format = format;
            imageViewInfo.image = rawImage;
            imageViewInfo.viewType = imageviewType;
            imageViewInfo.subresourceRange = m_subresourceRange;
            imageViewInfo.components = componentMapping;
        }

        m_rawImageView = getDevice().lock()->operator()().createImageViewUnique(imageViewInfo);
	}
}