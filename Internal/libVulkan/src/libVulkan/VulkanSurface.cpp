//! libVulkan
#include <libVulkan/VulkanSurface.hpp>
#include <libVulkan/VulkanInstance.hpp>
//! libUtils
#include <libUtils/Utils.hpp>

namespace libVulkan
{
	VulkanSurface::VulkanSurface(std::shared_ptr<VulkanInstance> const& instance, std::shared_ptr<VulkanDevice> const& device, vk::SurfaceKHR surface, vk::SurfaceCapabilitiesKHR const& capabilites, vk::SurfaceFormatKHR const& format, std::string const& name)
		: VulkanObject(instance, device, name)
		, m_surface(surface)
		, m_capabilities(capabilites)
		, m_format(format)
	{
		setName(getName());
	}

	VulkanSurface::~VulkanSurface()
	{
		getInstance().lock()->operator()().destroySurfaceKHR(m_surface);
	}

	vk::SurfaceCapabilitiesKHR const& VulkanSurface::getCapabilites() const
	{
		return m_capabilities;
	}

	vk::SurfaceFormatKHR VulkanSurface::getFormat() const
	{
		return m_format;
	}

	std::vector<vk::PresentModeKHR> VulkanSurface::getSupportedPresentModes() const
	{
		auto retval = std::vector<vk::PresentModeKHR>();
		getInstance().lock()->getSurfacePresentModes(m_surface, retval);
		return retval;
	}

	vk::ImageUsageFlags VulkanSurface::getSupportedUsages() const
	{
		return getCapabilites().supportedUsageFlags;
	}

	vk::SurfaceKHR VulkanSurface::operator()() const
	{
		return m_surface;
	}

	void VulkanSurface::setName(std::string const& name)
	{
		VulkanObject::setName(name);
		if (operator()())
			getDevice().lock()->setDebugObjectName(reinterpret_cast<uint64_t>(static_cast<VkSurfaceKHR>(operator()())), vk::ObjectType::eSurfaceKHR, name);
		//getDevice().lock()->setDebugObjectName(reinterpret_cast<uint64_t>(static_cast<VkSurfaceKHR>(m_surface->operator()()), vk::ObjectType::eSurfaceKHR, name);
	}
	
	void VulkanSurface::printInfo() const
	{
		auto& caps = getCapabilites();
		auto str = std::string("Capabilites of the surface:\n");
		for (uint32_t flag = 1; flag < 2048; flag = flag << 1)
		{
			if ((flag & uint32_t(caps.supportedUsageFlags)) != 0)
				str += (to_string(vk::ImageUsageFlagBits(flag)) + "\n");
		}
		libUtils::printOut(str);
	}
}