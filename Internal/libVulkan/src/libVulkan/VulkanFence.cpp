//! libVulkan
#include <libVulkan/VulkanFence.hpp>

namespace libVulkan
{
	VulkanFence::VulkanFence(std::shared_ptr<VulkanInstance> const& instance, std::shared_ptr<VulkanDevice> const& device, std::string const& name)
		: VulkanObject(instance, device, name)
	{
		auto fenceInfo = vk::FenceCreateInfo{};
		{
			fenceInfo.flags = vk::FenceCreateFlagBits::eSignaled;
		}
		m_rawObject = getDevice().lock()->operator()().createFenceUnique(fenceInfo);
		setName(getName());
	}

	void VulkanFence::wait()
	{
		(void)getDevice().lock()->operator()().waitForFences(m_rawObject.get(), VK_TRUE, UINT64_MAX);
		getDevice().lock()->operator()().resetFences(m_rawObject.get());
	}

	vk::Fence VulkanFence::operator()() const
	{
		return m_rawObject.get();
	}

	void VulkanFence::setName(std::string const& name)
	{
		VulkanObject::setName(name);
		if (operator()())
			getDevice().lock()->setDebugObjectName(reinterpret_cast<uint64_t>(static_cast<VkFence>(operator()())), vk::ObjectType::eFence, name);
	}
}