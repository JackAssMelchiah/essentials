//! libVulkan
#include <libVulkan/VulkanImage.hpp>
#include <libVulkan/VulkanAll.hpp>

namespace libVulkan
{
    VulkanImage::VulkanImage(std::shared_ptr<VulkanInstance> const& instance, std::shared_ptr<VulkanDevice> const& device, vk::Extent2D const& size, Info const& info, std::string const& name)
        : VulkanObject(instance, device, name)
        , m_extent(vk::Extent2D{ uint32_t(size.width), uint32_t(size.height) })
        , m_info(info)
        , m_swapchainImage(false)
    {
        auto arrayCount = uint32_t(1);
        auto mipMapCount = uint32_t(1);
        initialize(size, m_info.format, m_info.aspectFlag, arrayCount, mipMapCount);
        setName(getName());
    }

    VulkanImage::VulkanImage(std::shared_ptr<VulkanInstance> const& instance, std::shared_ptr<VulkanDevice> const& device, VulkanSurface const& surface, vk::Image& image, vk::Extent2D const& size, std::string const& name)
        : VulkanObject(instance, device, name)
        , m_extent(vk::Extent2D{ uint32_t(size.width), uint32_t(size.height) })
        , m_info(surface.getFormat().format, vk::ImageAspectFlagBits::eColor, vk::ImageUsageFlagBits{ vk::ImageUsageFlagBits::eColorAttachment }, vk::MemoryPropertyFlagBits{ vk::MemoryPropertyFlagBits::eDeviceLocal })
        , m_swapchainImage(true)
        , m_swapChainRawImage(image)
    {
        auto arraysImageCount = 1;
        auto mipMapCount = 1;
        m_imageView = std::make_shared<VulkanImageView>(instance, device, operator()(), m_info.format, m_info.aspectFlag, arraysImageCount, mipMapCount, vk::ImageViewType::e2D, name);
    }

    VulkanImage::~VulkanImage()
    {
        m_imageView.reset();
        m_rawMemory.reset();
        m_rawImage.reset();
    }

    void VulkanImage::createDescriptorImageInfo(vk::DescriptorImageInfo& descriptorImageInfo, vk::ImageLayout layout) const
    {
        auto info = vk::DescriptorImageInfo();
        {
            info.imageLayout = layout;
            info.imageView = m_imageView->operator()();
        }
        descriptorImageInfo = info;
    }

    std::shared_ptr<VulkanBarrier> VulkanImage::generateBarrier(vk::ImageLayout oldLayout, vk::ImageLayout targetLayout, vk::AccessFlags2 srcAccessFlags, vk::PipelineStageFlagBits2 srcStage, vk::AccessFlags2 dstAccessFlags, vk::PipelineStageFlagBits2 dstStage)
    {
        assert(targetLayout != oldLayout);
        auto subresourceRange = vk::ImageSubresourceRange();
        {
            subresourceRange.aspectMask = getInfo().aspectFlag;
            subresourceRange.baseArrayLayer = 0;
            subresourceRange.baseMipLevel = 0;
            subresourceRange.layerCount = 1;
            subresourceRange.levelCount = 1;
        }
        auto imageBarrier = vk::ImageMemoryBarrier2();
        {
            imageBarrier.image =  m_swapchainImage? m_swapChainRawImage: *m_rawImage;
            imageBarrier.subresourceRange = subresourceRange;
            imageBarrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
            imageBarrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
            imageBarrier.oldLayout = oldLayout;
            imageBarrier.newLayout = targetLayout;
            imageBarrier.srcAccessMask = srcAccessFlags;
            imageBarrier.dstAccessMask = dstAccessFlags;
            imageBarrier.srcStageMask = srcStage;
            imageBarrier.dstStageMask = dstStage;
        }

       return std::make_shared<VulkanBarrier>(imageBarrier, subresourceRange);
    }

    void VulkanImage::setName(std::string const& name)
    {
        VulkanObject::setName(name);
        if (operator()())
            getDevice().lock()->setDebugObjectName(reinterpret_cast<uint64_t>(static_cast<VkImage>(operator()())), vk::ObjectType::eImage, name);
    }

    vk::Extent2D const& VulkanImage::getExtent() const
    {
        return m_extent;
    }

    vk::Image VulkanImage::operator()() const
    {
        if (isSwapChainImage())
            return m_swapChainRawImage;
        return m_rawImage.get();
    }

    void VulkanImage::resize(vk::Extent2D const& dims)
    {
        if (m_swapchainImage)
        {
            assert(!"Needs to be destroyed and recreated from within window");
            return;
        }
        createImageAndBackItByMemory(dims, getArrayCount(), getMipMapCount());
        m_imageView->resize(*this);
    }

    uint32_t VulkanImage::getArrayCount() const
    {
        return getImageView().getSubresourceRange().layerCount;
    }

    uint32_t VulkanImage::getMipMapCount() const
    {
        return getImageView().getSubresourceRange().levelCount;
    }

    VulkanImageView const& VulkanImage::getImageView() const
    {
        return *m_imageView;
    }

    VulkanImageView& VulkanImage::getImageView()
    {
        return *m_imageView;
    }

    VulkanImage::Info const& VulkanImage::getInfo() const
    {
        return m_info;
    }

    bool VulkanImage::isSwapChainImage() const
    {
        return m_swapchainImage;
    }

    vk::UniqueImage VulkanImage::createRawImage(vk::Extent3D const& imageSize, vk::ImageType imageDimms, vk::Format imageFormat, uint32_t arrayCount, uint32_t mipMapCount, vk::ImageUsageFlags imageUsage, vk::ImageLayout initialLayout) const
    {
        auto updatedExtent = imageSize;
        if (imageDimms == vk::ImageType::e1D)
        {
            updatedExtent.height = 1;
            updatedExtent.depth = 1;
        }
        else if (imageDimms == vk::ImageType::e2D)
        {
            updatedExtent.depth = 1;
        }

        auto imageInfo = vk::ImageCreateInfo(); 
        {
            imageInfo.imageType = imageDimms;
            imageInfo.format = imageFormat;
            imageInfo.extent = updatedExtent;
            imageInfo.arrayLayers = arrayCount; //how many images there are - example: cubeMap must have 6 and extend dimms must be equal 
            imageInfo.mipLevels = mipMapCount; //for now turn off, later use some that have about 6-8 levels?
            imageInfo.samples = vk::SampleCountFlagBits::e1; //sample count per texel - no multisampling now
            imageInfo.tiling = vk::ImageTiling::eOptimal; //how data are layed out to vulkan... if manipulated by CPU, then eLinear should be chosen
            imageInfo.usage = imageUsage;
            imageInfo.sharingMode = vk::SharingMode::eExclusive; // used only by one que, the one that supports graphics and thus transfers
            imageInfo.initialLayout = initialLayout;
        }
        return getDevice().lock()->operator()().createImageUnique(imageInfo);
    }

    vk::UniqueDeviceMemory VulkanImage::backImageByMemory(vk::Image& rawImage, vk::MemoryPropertyFlags flags)
    {
        auto device = getDevice().lock();
        auto instance = getInstance().lock();
        auto imageRequirements = device->operator()().getImageMemoryRequirements(rawImage);
        auto memory = device->allocateMemory(imageRequirements.size, imageRequirements, flags); //memory will be equal to of that object, for now not bigger -- so bind that at first byte
        auto memoryOffset = vk::DeviceSize(0);
        //bringing it all together
        device->operator()().bindImageMemory(rawImage, memory.get(), memoryOffset);
        return memory;
    }

    void VulkanImage::initialize(vk::Extent2D const& size, vk::Format format, vk::ImageAspectFlags aspect, uint32_t arrayCount, uint32_t mipMapCount)
    {
        createImageAndBackItByMemory(size, arrayCount, mipMapCount);
        m_imageView = std::make_shared<VulkanImageView>(getInstance().lock(), getDevice().lock(), operator()(), format, aspect, arrayCount, mipMapCount, vk::ImageViewType::e2D, getName());
    }

    void VulkanImage::createImageAndBackItByMemory(vk::Extent2D const& dimms, uint32_t arrayCount, uint32_t mipMapCount)
    {
        m_extent = dimms;

        //creates single 2d image texture without mipmap sampler 
        auto imageDimms = vk::ImageType::e2D;
        auto imageSize = vk::Extent3D{ dimms.width, dimms.height, 1 };
        auto initialLayout = vk::ImageLayout::eUndefined; //Dont care about preserving contents from before

        //raw image
        m_rawImage = createRawImage(imageSize, imageDimms, m_info.format, arrayCount, mipMapCount, m_info.usage, initialLayout);
        //back image by memory
        m_rawMemory = backImageByMemory(m_rawImage.get(), m_info.memoryFlags);
    }
}