//! libVulkan
#include <libVulkan/VulkanPipelineLayout.hpp>
#include <libVulkan/VulkanDevice.hpp>
#include <libVulkan/VulkanDescriptorSetLayout.hpp>

namespace libVulkan
{
	VulkanPipelineLayout::VulkanPipelineLayout(std::shared_ptr<VulkanInstance> const& instance, std::shared_ptr<VulkanDevice> const& device, std::vector<std::shared_ptr<VulkanDescriptorSetLayout>>const& descriptorSetLayouts, vk::ShaderStageFlags pushConstantsStages, std::string const& name)
		: VulkanObject(instance, device, name)
		, m_descriptorSetLayouts(descriptorSetLayouts)
		, m_pushConstants(pushConstantsStages)
	{
		initialize();
	}

	vk::PipelineLayout VulkanPipelineLayout::operator()() const
	{
		return m_rawPipelineLayout.get();
	}

	bool VulkanPipelineLayout::operator==(VulkanPipelineLayout const& other) const
	{
		return (m_pushConstants == other.m_pushConstants) && isCompatible(other.m_descriptorSetLayouts);
	}

	bool VulkanPipelineLayout::isCompatible(std::vector<std::shared_ptr<VulkanDescriptorSetLayout>> const& descriptorSets) const
	{
		// fixme: implement
		return true;
	}

	void VulkanPipelineLayout::setName(std::string const& name)
	{
		VulkanObject::setName(name);
		if (operator()())
			getDevice().lock()->setDebugObjectName(reinterpret_cast<uint64_t>(static_cast<VkPipelineLayout>(operator()())), vk::ObjectType::ePipelineLayout, name);
	}

	void VulkanPipelineLayout::recreate()
	{
		initialize();
	}

	void VulkanPipelineLayout::initialize()
	{
		auto rawDescriptorLayouts = std::vector<vk::DescriptorSetLayout>();
		for (auto& descriptorSetLayout: m_descriptorSetLayouts)
		{
			rawDescriptorLayouts.push_back(descriptorSetLayout->operator()());
		}

		auto pushConstantRange = std::vector<vk::PushConstantRange>();
		if (m_pushConstants)
		{
			auto range = vk::PushConstantRange();
			{
				range.offset = 0;
				range.size = 128;
				range.stageFlags = m_pushConstants;
			}
			pushConstantRange.push_back(range);
		}

		auto layoutInfo = vk::PipelineLayoutCreateInfo(); 
		{
			layoutInfo.pushConstantRangeCount = pushConstantRange.size();
			layoutInfo.pPushConstantRanges = pushConstantRange.data();
			layoutInfo.setLayoutCount = rawDescriptorLayouts.size();
			layoutInfo.pSetLayouts = rawDescriptorLayouts.data();
		}
		m_rawPipelineLayout = getDevice().lock()->operator()().createPipelineLayoutUnique(layoutInfo);

		setName(getName());
	}
}