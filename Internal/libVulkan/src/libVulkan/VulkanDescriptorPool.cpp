//! libVulkan
#include <libVulkan/VulkanDescriptorPool.hpp>
#include <libVulkan/VulkanAll.hpp>

namespace libVulkan
{
	VulkanDescriptorPool::VulkanDescriptorPool(std::shared_ptr<VulkanInstance> const& instance, std::shared_ptr<VulkanDevice> const& device, std::vector<vk::DescriptorPoolSize> const& allocationDescriptions, bool allowSingleFree, std::string const& name, bool canUpdateAfterBind)
		: VulkanObject(instance, device, name)
		, m_allocationDescriptions(allocationDescriptions)
	{
		auto flags = vk::DescriptorPoolCreateFlags();
		if (allowSingleFree)
			flags |= vk::DescriptorPoolCreateFlagBits::eFreeDescriptorSet;
		if (canUpdateAfterBind)
			flags |= vk::DescriptorPoolCreateFlagBits::eUpdateAfterBind;

		initialize(flags);
		setName(getName());
	}

	std::weak_ptr<VulkanDescriptorSet> VulkanDescriptorPool::createDescriptorSet(std::shared_ptr<VulkanDescriptorSetLayout> layout, std::string const& name)
	{
		auto rawDescriptions = std::vector<vk::DescriptorSetLayout>();
		{
			rawDescriptions.push_back(layout->operator()());
		}

		auto info = vk::DescriptorSetAllocateInfo();
		{
			info.descriptorPool = operator()();
			info.descriptorSetCount = rawDescriptions.size();
			info.pSetLayouts = rawDescriptions.data();
		}

		auto descriptorSet = std::make_shared<VulkanDescriptorSet>(m_instance.lock(), m_device.lock(), std::move(getDevice().lock()->operator()().allocateDescriptorSetsUnique(info).front()), layout, name);
		m_descriptorSets.insert(descriptorSet);
		return descriptorSet;
	}

	void VulkanDescriptorPool::destroyDescriptorSet(std::shared_ptr<VulkanDescriptorSet> descriptorSet)
	{
		if (m_descriptorSets.count(descriptorSet) < 0)
			m_descriptorSets.erase(descriptorSet);
	}

	void VulkanDescriptorPool::reset()
	{
		m_descriptorSets.clear();
		getDevice().lock()->operator()().resetDescriptorPool(*m_rawPool);
	}

	vk::DescriptorPool VulkanDescriptorPool::operator()() const
	{
		return m_rawPool.get();
	}

	void VulkanDescriptorPool::initialize(vk::DescriptorPoolCreateFlags flags)
	{
		m_flags = flags;

		auto maxSets = 0; //max number of descriptors that will be allocated match the sum of individual allocations
		for (auto& item : m_allocationDescriptions)
		{
			maxSets += item.descriptorCount;
		}

		auto info = vk::DescriptorPoolCreateInfo();
		{
			info.maxSets = maxSets;
			info.poolSizeCount = m_allocationDescriptions.size();
			info.pPoolSizes = m_allocationDescriptions.data();
			info.flags = flags;
		}
		m_rawPool = getDevice().lock()->operator()().createDescriptorPoolUnique(info);
	}

	void VulkanDescriptorPool::recreate()
	{
		initialize(m_flags);
	}

	void VulkanDescriptorPool::setName(std::string const& name)
	{
		VulkanObject::setName(name);
		if (operator()())
			getDevice().lock()->setDebugObjectName(reinterpret_cast<uint64_t>(static_cast<VkDescriptorPool>(operator()())), vk::ObjectType::eDescriptorPool, name);
	}
}