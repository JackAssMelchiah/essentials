//! libVulkan
#include <libVulkan/VulkanCommandPool.hpp>
#include <libVulkan/VulkanAll.hpp>

namespace libVulkan 
{
	VulkanCommandPool::VulkanCommandPool(std::shared_ptr<VulkanInstance> const& instance, std::shared_ptr<VulkanDevice> const& device, VulkanQueue const& queue, bool allowSingleReset, bool shortlived, std::string const& name)
		: VulkanObject(instance, device, name)
		, m_supportedFunctionality(queue.getFunctionality())
	{
		createPool(queue, allowSingleReset, shortlived);
		setName(getName());
	}

	VulkanCommandPool::~VulkanCommandPool()
	{
		m_commandBuffers.clear();
	}

	std::weak_ptr<VulkanCommandBuffer> VulkanCommandPool::createCommandBuffer(vk::CommandBufferLevel level, std::string const& name)
	{
		auto device = getDevice().lock();
		auto instance = getInstance().lock();

		auto allocateCommandBufferInfo = vk::CommandBufferAllocateInfo();
		{
			allocateCommandBufferInfo.commandPool = operator()();
			allocateCommandBufferInfo.commandBufferCount = 1;
			allocateCommandBufferInfo.level = level;
		}

		auto uniqueCommandBuffers = getDevice().lock()->operator()().allocateCommandBuffersUnique(allocateCommandBufferInfo);
		auto commandBuffer = std::make_shared<VulkanCommandBuffer>(instance, device, std::move(uniqueCommandBuffers.front()), level, m_supportedFunctionality, name);

		m_commandBuffers.insert(commandBuffer);
		return commandBuffer;
	}

	void VulkanCommandPool::destroyCommandBuffer(std::shared_ptr<VulkanCommandBuffer> commandBuffer)
	{
		if (m_commandBuffers.count(commandBuffer))
			m_commandBuffers.erase(commandBuffer);
	}

	void VulkanCommandPool::freePool()
	{
		m_commandBuffers.clear();
		getDevice().lock()->operator()().resetCommandPool(*m_rawCommandPool);
	}

	bool VulkanCommandPool::supportsGraphics() const
	{
		return (m_supportedFunctionality & vk::QueueFlagBits::eGraphics).operator bool();
	}

	vk::CommandPool VulkanCommandPool::operator()() const
	{
		return m_rawCommandPool.get();
	}

	void VulkanCommandPool::createPool(VulkanQueue const& queue, bool allowSingleReset, bool shortLived)
	{
		auto flags = vk::CommandPoolCreateFlags();
		if (allowSingleReset)
			flags |= vk::CommandPoolCreateFlagBits::eResetCommandBuffer;
		if (shortLived)
			flags |= vk::CommandPoolCreateFlagBits::eTransient;
		//! for now dont specify any flags for command buffers - todo: look at Transient and Reset
		auto commandPoolInfo = vk::CommandPoolCreateInfo();
		{
			commandPoolInfo.queueFamilyIndex = queue.getFamilyIndex();
			commandPoolInfo.flags = flags;
		}
		m_rawCommandPool = getDevice().lock()->operator()().createCommandPoolUnique(commandPoolInfo);
	}

	void VulkanCommandPool::setName(std::string const& name)
	{
		VulkanObject::setName(name);
		if (operator()())
			getDevice().lock()->setDebugObjectName(reinterpret_cast<uint64_t>(static_cast<VkCommandPool>(operator()())), vk::ObjectType::eCommandPool, name);
	}

}