//! libVulkan
#include <libVulkan/VulkanInstance.hpp>
#include <libVulkan/VulkanExtensions.hpp>
#include <libVulkan/VulkanDefaults.hpp>
//! libUtils
#include <libUtils/Utils.hpp>

namespace libVulkan
{
	VkBool32 VulkanInstance::debugMessage(VkDebugUtilsMessageSeverityFlagBitsEXT messageSeverity, VkDebugUtilsMessageTypeFlagsEXT messageTypes, const VkDebugUtilsMessengerCallbackDataEXT* pCallbackData, void* pUserData)
	{
		if (messageSeverity & VkDebugUtilsMessageSeverityFlagBitsEXT::VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT)
		{
			libUtils::printOutErr(pCallbackData->pMessage);
		}
		else if (messageSeverity & VkDebugUtilsMessageSeverityFlagBitsEXT::VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT)
		{
			libUtils::printOutWar(pCallbackData->pMessage);
		}
		else
		{
			libUtils::printOutGL(pCallbackData->pMessage);
		}
		return VK_FALSE;
	}

	VulkanInstance::VulkanInstance(vk::UniqueInstance instance, std::shared_ptr<VulkanDefaults> defaults, std::string const& name)
		: m_rawInstance(std::move(instance))
		, m_name(name)
		, m_defaults(defaults)
		, m_debugMessenger(nullptr)
    {
		if (!m_defaults->validationLayers.empty())
		{
			if (createDebugMessenger() != vk::Result::eSuccess)
			{
				libUtils::printOutErr("Cannot create debug messenger");
			}
		}
	}

	VulkanInstance::~VulkanInstance()
	{
		if (m_debugMessenger)
		{
			if (destroyDebugUtilsMessengerEXT(*m_rawInstance, m_debugMessenger, nullptr)!= vk::Result::eSuccess)
			{
				libUtils::printOutErr("Cannot destroy debug messenger");
			}
		}
	}

	std::string const& VulkanInstance::getName() const
	{
		return m_name;
	}

	void VulkanInstance::getPhysicalDeviceLimits(vk::PhysicalDeviceLimits& limitsOut) const
	{
		limitsOut = m_hwDevice.getProperties().limits;
	}

	bool VulkanInstance::selectHwDevice(std::vector<vk::QueueFlagBits> const& requiredFunc, vk::PhysicalDeviceType type)
	{
		auto availableDevices = m_rawInstance->enumeratePhysicalDevices();
		auto matches = std::vector<bool>(requiredFunc.size(), false);

		for (auto& device : availableDevices) 
		{
			auto properties = device.getProperties();
			if (properties.deviceType == type)
			{
				for (auto i = 0; i < device.getQueueFamilyProperties().size(); ++i)
				{
					for (auto j = 0; j < requiredFunc.size(); ++j) 
					{
						if ((device.getQueueFamilyProperties().at(i).queueFlags & requiredFunc.at(j)) == requiredFunc.at(j))
						{
							matches[j] = true;
							break;
						}
					}
				}
				if (std::find(matches.begin(), matches.end(), true) != matches.end())
				{
					m_hwDevice = device;

					libUtils::printOut(std::string(properties.deviceName.data()) + std::string(", Api:") + std::to_string(properties.apiVersion));
					return true;
				}
			}
		}
		return false;
	}

	std::vector<char const*> VulkanInstance::filterAvailableDeviceExtensions(std::vector<std::string> const& wantedExtensions) const
	{
		auto availableExtensions = m_hwDevice.enumerateDeviceExtensionProperties();
		auto filtered = std::vector<char const*>();
		for (auto& wantedExtension : wantedExtensions)
		{
			for (auto& availableExtension : availableExtensions)
			{
				if (wantedExtension == availableExtension.extensionName) 
				{
					filtered.push_back(wantedExtension.c_str());
				}
			}
		}
		return filtered;
	}

	std::vector<char const*> VulkanInstance::filterAvailableDeviceLayers(std::vector<std::string> const& wantedLayers) const
	{
		auto availableLayers = m_hwDevice.enumerateDeviceLayerProperties();
		auto filtered = std::vector<char const*>();
		for (auto& wantedLayer : wantedLayers) 
		{
			for (auto& availableLayer : availableLayers)
			{
				if (wantedLayer == availableLayer.layerName) 
				{
					filtered.push_back(wantedLayer.c_str());
				}
			}
		}
		return filtered;
	}

	bool VulkanInstance::pickDevice(std::vector<vk::QueueFlagBits> const& requiredFunc)
	{	
		if (!selectHwDevice(requiredFunc, vk::PhysicalDeviceType::eDiscreteGpu))
		{
			if (!selectHwDevice(requiredFunc, vk::PhysicalDeviceType::eIntegratedGpu))
			{
				libUtils::printOutErr("Cannot pick suitable hw device");
				return false;
			}
			libUtils::printOutWar("Cannot pick dedicated GPU, picking integrated GPU");
		}

		//load the function pointers
		loadExtensionFunctionPointers();
		return true;
	}

	void VulkanInstance::validateDefaults()
	{
		auto properties = m_hwDevice.getProperties();
		auto features = m_hwDevice.getFeatures();

		// multisampling, defaultly turn off
		if (!(properties.limits.framebufferColorSampleCounts & properties.limits.framebufferDepthSampleCounts & m_defaults->multisampling))
		{
			libUtils::printOutWar("Selected multisampling count not supported, turning off");
			m_defaults->multisampling = vk::SampleCountFlagBits::e1;
		}
		//queue functionality - according to this, device is picked - it is mandatory, so no validation
		
		//resoultion and window samples - cannot be handled here, since no vk surface exists ... its handled in swapchain creation later

		//presentation mode - again needs vkSurface, which is created after getDevice().. so no validation

	}
	
	uint32_t VulkanInstance::getPresentationQueueFamilyIndex(vk::PhysicalDevice const& hwDevice, vk::SurfaceKHR const& surface) const
	{
		for (auto i = 0; i < m_hwDevice.getQueueFamilyProperties().size(); ++i)
		{
			if (m_hwDevice.getSurfaceSupportKHR(i, surface))
			{
				return i;
			}
		}
		return UINT_MAX;
	}

	VulkanDefaults const& VulkanInstance::getDefaults() const
	{
		return *m_defaults.get();
	}

	bool VulkanInstance::formatSupported(vk::Format format, vk::ImageTiling imageTiling, vk::FormatFeatureFlags wantedUsage) const
	{
		auto formatInfo = getFormatInfo(format);
		if (imageTiling == vk::ImageTiling::eLinear)
		{
			if (formatInfo.linearTilingFeatures & wantedUsage)
				return true;
		}
		if (imageTiling == vk::ImageTiling::eOptimal)
		{
			if (formatInfo.optimalTilingFeatures & wantedUsage)
				return true;
		}
		return false;
	}


	vk::Instance VulkanInstance::operator()()
	{
		return m_rawInstance.get();
	}

	vk::PhysicalDevice& VulkanInstance::getRawPhysicalDevice()
	{
		return m_hwDevice;
	}

	bool VulkanInstance::getSurfaceCapabilites(vk::PhysicalDevice const& hwDevice, vk::SurfaceKHR const& surface, vk::SurfaceCapabilitiesKHR& capabilitesOut) const
	{
		capabilitesOut = m_hwDevice.getSurfaceCapabilitiesKHR(surface);
		return true;
	}

	void VulkanInstance::getSurfaceFormats(vk::PhysicalDevice const& hwDevice, vk::SurfaceKHR const& surface, std::vector<vk::SurfaceFormatKHR>& formatsOut) const
	{
		formatsOut = m_hwDevice.getSurfaceFormatsKHR(surface);
	}

	void VulkanInstance::getSurfacePresentModes(vk::PhysicalDevice const& hwDevice, vk::SurfaceKHR const& surface, std::vector<vk::PresentModeKHR>& presentModesOut) const
	{
		presentModesOut = m_hwDevice.getSurfacePresentModesKHR(surface);
	}

	uint32_t VulkanInstance::getQueueFamilyIndex(vk::PhysicalDevice const& hwDevice, vk::QueueFlagBits queType) const
	{
		for (auto i = 0; i < m_hwDevice.getQueueFamilyProperties().size(); ++i)
		{
			if (m_hwDevice.getQueueFamilyProperties().at(i).queueFlags & queType)
			{
				return i;
			}
		}
		return UINT_MAX;
	}

	uint32_t VulkanInstance::getQueueFamilyCount(vk::PhysicalDevice const& hwDevice, uint32_t familyIndex) const
	{
		assert(familyIndex < m_hwDevice.getQueueFamilyProperties().size());
		return m_hwDevice.getQueueFamilyProperties().at(familyIndex).queueCount;
	}

	uint32_t VulkanInstance::getMemoryTypeHeap(vk::MemoryRequirements const& memoryRequirements, vk::MemoryPropertyFlags requiredFlags) const
	{
		auto deviceMemoryProperties = m_hwDevice.getMemoryProperties();
		uint32_t selectedType = ~0u;
		uint32_t memoryType;
		for (memoryType = 0; memoryType < VK_MAX_MEMORY_TYPES; ++memoryType) 
		{
			if (memoryRequirements.memoryTypeBits & (1 << memoryType)) 
			{
				auto& type = deviceMemoryProperties.memoryTypes[memoryType];
				// If it exactly matches preferred properties, grab it.
				if ((type.propertyFlags & requiredFlags) == requiredFlags)
				{
					selectedType = memoryType;
					break;
				}
			}
		}
		return selectedType;
	}

	bool VulkanInstance::getSurfaceCapabilites(vk::SurfaceKHR const& surface, vk::SurfaceCapabilitiesKHR& capabilites) const
	{
		return getSurfaceCapabilites(m_hwDevice, surface, capabilites);
	}

	void VulkanInstance::getSurfaceFormats(vk::SurfaceKHR const& surface, std::vector<vk::SurfaceFormatKHR>& formatsOut) const
	{
		getSurfaceFormats(m_hwDevice, surface, formatsOut);
	}

	void VulkanInstance::getSurfacePresentModes(vk::SurfaceKHR const& surface, std::vector<vk::PresentModeKHR>& presentModesOut) const
	{
		getSurfacePresentModes(m_hwDevice, surface, presentModesOut);
	}

	uint32_t VulkanInstance::getQueueFamilyIndex(vk::QueueFlagBits flags) const
	{
		return getQueueFamilyIndex(m_hwDevice, flags);
	}

	uint32_t VulkanInstance::getQueueFamilyCount(uint32_t familyIndex) const
	{
		return getQueueFamilyCount(m_hwDevice, familyIndex);
	}

	uint32_t VulkanInstance::getPresentationQueueFamilyIndex(vk::SurfaceKHR const& surface) const
	{
		return getPresentationQueueFamilyIndex(m_hwDevice, surface);
	}

	vk::FormatProperties VulkanInstance::getFormatInfo(vk::Format format) const
	{
		return m_hwDevice.getFormatProperties(format);
	}

	vk::Result VulkanInstance::createDebugMessenger()
	{
		vk::DebugUtilsMessengerCreateInfoEXT debugInfo; {
			debugInfo.messageSeverity = (vk::DebugUtilsMessageSeverityFlagBitsEXT::eError | /*vk::DebugUtilsMessageSeverityFlagBitsEXT::eInfo | */ vk::DebugUtilsMessageSeverityFlagBitsEXT::eWarning);
			debugInfo.messageType = vk::DebugUtilsMessageTypeFlagBitsEXT::eValidation | vk::DebugUtilsMessageTypeFlagBitsEXT::eGeneral;
			debugInfo.pfnUserCallback = debugMessage;
		}
		return createDebugUtilsMessengerEXT(*m_rawInstance, reinterpret_cast<const VkDebugUtilsMessengerCreateInfoEXT*>(&debugInfo), nullptr, &m_debugMessenger);
	}

	libVulkan::VulkanExtFunctions const& VulkanInstance::extentions() const
	{
		return m_extensionFunctions;
	}

	void VulkanInstance::loadExtensionFunctionPointers()
	{
		m_extensionFunctions.vkSetDebugUtilsObjectNameEXT = reinterpret_cast<PFN_vkSetDebugUtilsObjectNameEXT>(operator()().getProcAddr("vkSetDebugUtilsObjectNameEXT"));
		m_extensionFunctions.vkCmdBeginDebugUtilsLabelEXT = reinterpret_cast<PFN_vkCmdBeginDebugUtilsLabelEXT>(operator()().getProcAddr("vkCmdBeginDebugUtilsLabelEXT"));
		m_extensionFunctions.vkCmdEndDebugUtilsLabelEXT = reinterpret_cast<PFN_vkCmdEndDebugUtilsLabelEXT>(operator()().getProcAddr("vkCmdEndDebugUtilsLabelEXT"));
		m_extensionFunctions.vkCmdInsertDebugUtilsLabelEXT = reinterpret_cast<PFN_vkCmdInsertDebugUtilsLabelEXT>(operator()().getProcAddr("vkCmdInsertDebugUtilsLabelEXT"));
	}
}