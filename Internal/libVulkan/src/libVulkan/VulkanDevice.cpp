//! libVulkan
#include <libVulkan/VulkanDevice.hpp>
#include <libVulkan/VulkanInstance.hpp>
#include <libVulkan/VulkanFence.hpp>
#include <libVulkan/VulkanTimelineSemaphore.hpp>

namespace libVulkan 
{
    VulkanDevice::VulkanDevice(std::shared_ptr<VulkanInstance> const& instance, vk::UniqueDevice device, std::string const& name)
        : m_instance(instance)
        , m_rawDevice(std::move(device))
    {        
        //for now commented out as renderdoc crashes with this enabled
        //if (instance->operator()()) 
        //    setDebugObjectName(reinterpret_cast<uint64_t>(static_cast<VkInstance>(instance->operator()())), vk::ObjectType::eInstance, instance->getName());
        //if (operator()())
        //    setDebugObjectName(reinterpret_cast<uint64_t>(static_cast<VkDevice>(operator()())), vk::ObjectType::eDevice, name);

    }

    VulkanDevice::~VulkanDevice()
    {
        m_rawDevice.reset();
    }

    void VulkanDevice::waitForFences(std::vector<std::reference_wrapper<VulkanFence>> const& fences)
    {
        auto rawFences = std::vector<vk::Fence>();
        for (auto& fence : fences)
        {
            rawFences.push_back(fence.get().operator()());
        }
        (void)m_rawDevice->waitForFences({ rawFences }, VK_TRUE, UINT64_MAX);
        m_rawDevice->resetFences({ rawFences });
    }

    void VulkanDevice::waitIdle()
    {
        m_rawDevice->waitIdle();
    }

    void VulkanDevice::waitForSemaphore(std::shared_ptr<VulkanTimelineSemaphore> const& semaphore, uint64_t value)
    {
        waitForSemaphores(std::vector<std::pair<std::shared_ptr<VulkanTimelineSemaphore>, uint64_t>>{ std::make_pair(semaphore, value) }, false);
    }

    void VulkanDevice::waitForSemaphores(std::vector<std::pair<std::shared_ptr<VulkanTimelineSemaphore>, uint64_t>> const& semaphores, bool waitAny)
    {
        auto values = std::vector<uint64_t>();
        auto rawSemaphores = std::vector<vk::Semaphore>();
        auto waitFlags = vk::SemaphoreWaitFlags();
        if (waitAny)
            waitFlags |= vk::SemaphoreWaitFlagBits::eAny;

        for (auto& pair : semaphores)
        {
            values.push_back(pair.second);
            rawSemaphores.push_back(pair.first->operator()());
        }
        
        auto waitSemaphoreInfo = vk::SemaphoreWaitInfo();
        {
            waitSemaphoreInfo.pValues = values.data();
            waitSemaphoreInfo.flags = waitFlags;
            waitSemaphoreInfo.semaphoreCount = values.size();
            waitSemaphoreInfo.pSemaphores = rawSemaphores.data();
        }

        m_rawDevice->waitSemaphores(&waitSemaphoreInfo, UINT64_MAX);
    }

    vk::UniqueDeviceMemory VulkanDevice::allocateMemory(int32_t bytes, vk::MemoryRequirements const& requirements, vk::MemoryPropertyFlags memoryPropertyFlagsReq)
    {
        auto memoryAllocation = vk::MemoryAllocateInfo();
        {
            memoryAllocation.allocationSize = bytes;
            memoryAllocation.memoryTypeIndex = m_instance.lock()->getMemoryTypeHeap(requirements, memoryPropertyFlagsReq);
        }
        return m_rawDevice->allocateMemoryUnique(memoryAllocation);
    }

    vk::Device VulkanDevice::operator()() const
    {
        return m_rawDevice.get();
    }

    void VulkanDevice::setDebugObjectName(uint64_t objectHandle, vk::ObjectType objectType, std::string const& name)
    {
        auto instance = m_instance.lock();
        auto debugNameInfo = vk::DebugUtilsObjectNameInfoEXT();
        {
            debugNameInfo.objectHandle = objectHandle;
            debugNameInfo.objectType = objectType;
            debugNameInfo.pObjectName = name.c_str();
        }
        instance->extentions().vkSetDebugUtilsObjectNameEXT(m_rawDevice.get().operator VkDevice(), &debugNameInfo.operator const VkDebugUtilsObjectNameInfoEXT & ());
    }
}
