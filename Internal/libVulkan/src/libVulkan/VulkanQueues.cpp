#pragma once
//! libVulkan
#include <libVulkan/VulkanQueues.hpp>
#include <libVulkan/VulkanAll.hpp>


namespace libVulkan
{
	VulkanQueues::VulkanQueues(std::shared_ptr<VulkanInstance> const& instance, std::shared_ptr<VulkanDevice> const& device, std::vector<vk::QueueFlagBits> const& wantedQues)
		: m_device(device)
		, m_instance(instance)
		, m_supportedQues()
	{
		for (auto& functionality : wantedQues)
			m_supportedQues |= functionality;
		createAllAvailableQueues(wantedQues);
	}

	void VulkanQueues::createPresentationQueue(vk::SurfaceKHR const& surface)
	{
		auto instance = m_instance.lock();
		auto device = m_device.lock();
		auto renderQueueFamilyIndex = getQueue(vk::QueueFlagBits::eGraphics).lock()->getFamilyIndex();
		auto presentationQueIndex = instance->getPresentationQueueFamilyIndex(surface);

		//it might be the same as graphics
		if (presentationQueIndex == renderQueueFamilyIndex)
		{
			m_presentationQue = getQueue(vk::QueueFlagBits::eGraphics).lock();
		}
		else //differs, create separate queue
		{
			//index within family seems like should be 0
			m_presentationQue = std::make_shared<libVulkan::VulkanQueue>(instance, device, device->operator()().getQueue(presentationQueIndex, 0), vk::QueueFlagBits{}, presentationQueIndex, 0, "Presentation Que");
		}		
	}

	bool VulkanQueues::supports(vk::QueueFlags que) const
	{
		return (m_supportedQues & que) == que;
	}

	std::weak_ptr<VulkanQueue> VulkanQueues::getQueue(vk::QueueFlagBits bits) const
	{
		for (auto& queue : m_availableQues)
		{
			if ((queue->getFunctionality() & bits) == bits)
				return queue;
		}
		return std::weak_ptr<VulkanQueue>();
	}
	
	std::weak_ptr<VulkanQueue> VulkanQueues::getPresentationQueue() const
	{
		return m_presentationQue;
	}

	void VulkanQueues::createAllAvailableQueues(std::vector<vk::QueueFlagBits> const& wantedQues)
	{
		// Hopefully dont care that functionality can be shared among single que - example: 1que can be transfer and present as well
		// so whis code will create 2 VulkanQueues, but with the same raw que inside it
		auto lockedInstance = m_instance.lock();
		auto lockedDevice = m_device.lock();
		for (auto& wantedQueFunc : wantedQues) {
			auto familyIndex = lockedInstance->getQueueFamilyIndex(wantedQueFunc);
			auto queCount = lockedInstance->getQueueFamilyCount(familyIndex);

			for (auto queIndexWithinFamily = 0; queIndexWithinFamily < queCount; ++queIndexWithinFamily)
			{
				m_availableQues.insert(std::make_shared<VulkanQueue>(lockedInstance, lockedDevice, lockedDevice->operator()().getQueue(familyIndex, queIndexWithinFamily), vk::QueueFlagBits(wantedQueFunc), familyIndex, queIndexWithinFamily, "Que[" + std::to_string(familyIndex) + ";"+ std::to_string(queIndexWithinFamily) + "]"));
			}
		}
	}
}