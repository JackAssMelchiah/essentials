//! libVulkan
#include <libVulkan/VulkanWindow.hpp>
#include <libVulkan/VulkanAll.hpp>
#include <libVulkan/UtilFunctions.hpp>
//! libUtils
#include <libUtils/Utils.hpp>

#ifdef _WIN32 //solving swapchain creation error for Windows atleast - when steam app is running
	#include <cstdlib>
#endif

namespace libVulkan
{
	VulkanWindow::VulkanWindow(std::shared_ptr<VulkanInstance> const& instance, std::shared_ptr<VulkanDevice> const& device, std::weak_ptr<VulkanQueue> presentationQue, std::unique_ptr<VulkanSurface> surface, vk::Extent2D const& resolution, std::string const& name)
		: VulkanObject(instance, device, name)
		, m_queue(presentationQue)
		, m_surface(std::move(surface))
		, m_extent(resolution)
		, m_presentMode(instance->getDefaults().presentationMode)
		, m_currentImageIndex(uint32_t(-1))
		, m_recreated(false)
	{
		//solving swapchain creation error for Windows atleast - when steam app is running
#ifdef _WIN32
		(void)_putenv("DISABLE_VK_LAYER_VALVE_steam_overlay_1 = 1");
#endif // _WIN32

		// try to init presentation mode with preffered one, unfortunatelly it does not have to be supported
		auto wantedPresentationMode = instance->getDefaults().presentationMode;
		auto supportedPresentaionModes = m_surface->getSupportedPresentModes();
		if (std::find(supportedPresentaionModes.begin(), supportedPresentaionModes.end(), wantedPresentationMode) != supportedPresentaionModes.end())
			m_presentMode = wantedPresentationMode;
		else
			libUtils::printOutWar("Preffered presentation mode not available, switchng to Fifo instead");

		initialize();
	}

	VulkanWindow::~VulkanWindow()
	{
		getDevice().lock()->waitIdle();
		destroyResources();
	}

	void VulkanWindow::initialize()
	{
		auto instance = getInstance().lock();
		auto device = getDevice().lock();
		auto sampleCount = instance->getDefaults().windowSamples;

		auto queue = m_queue.lock();
		m_commandPool = std::make_shared<VulkanCommandPool>(instance, device, *queue, true, false, "Window pool");
		m_frameData = std::vector<FrameData>(sampleCount);
		{// synchronization stuff
			m_semaphoreImageAvailable = std::make_shared<VulkanSemaphore>(instance, device, "SwapchainImageAvailable");
			m_semaphoreCopyFinished = std::make_shared<VulkanSemaphore>(instance, device, "WindowCopyDone");
			m_fenceCopyFinished = std::make_shared<VulkanFence>(instance, device, "WindowCopyDone");
		}

		int i = 0;
		for (auto& frameData : m_frameData)
		{
			frameData.commandBuffer = m_commandPool->createCommandBuffer(vk::CommandBufferLevel::ePrimary, "Window's swapImage " + std::to_string(i));
			++i;
		}
		// init all non-persistent data
		recreateResources();
	}

	void VulkanWindow::present(std::shared_ptr<VulkanImageAttachment> const& imageAttachment, uint32_t attachment, std::vector<std::reference_wrapper<VulkanSemaphore>> const& waitSemaphores)
	{
		// it is expected, that before calling present, code was synchornized via m_fenceCopyFinished fence!
		if (m_extent.width == 0 || m_extent.height == 0)
			return;
		// querry new image -> this needs synchronization
		m_currentImageIndex = getNextImage();
		// next, every frame re-record the commandBuffer, since fbo can be different
		auto commandBuffer = m_frameData.at(m_currentImageIndex).commandBuffer.lock();
		recordPresentCommandBuffer(*commandBuffer, *imageAttachment);
		auto presentationQueue = m_queue.lock();

		// submission of that copy command is dependent on aquisition of next image from swapchain + must ensure that input fbo is complete as well, therefore append passed semaphores
		auto copySubmitWaitSemaphores = std::vector<std::reference_wrapper<VulkanSemaphore>>(waitSemaphores);
		copySubmitWaitSemaphores.push_back(*m_semaphoreImageAvailable);
		// submit the copy work - signalize another semaphore - on wait semaphores wait for attachment to be written, and in semaphore image, wait for transfer
		presentationQueue->submit({ *commandBuffer }, { *m_semaphoreCopyFinished }, copySubmitWaitSemaphores, { vk::PipelineStageFlagBits::eColorAttachmentOutput, vk::PipelineStageFlagBits::eTransfer }, *m_fenceCopyFinished);
		// presentation waits on gpu to finish the copy operation first
		presentationQueue->present(*this, { *m_semaphoreCopyFinished });
	}

	void VulkanWindow::recordPresentCommandBuffer(VulkanCommandBuffer& commandBuffer, VulkanImageAttachment& srcImage)
	{
		// record copying from given fbo to window fbo
		commandBuffer.reset(true);
		commandBuffer.beginRecord(vk::CommandBufferUsageFlagBits::eOneTimeSubmit);
		copyToWindow(commandBuffer, srcImage);
		commandBuffer.endRecord();
	}

	void VulkanWindow::setResolution(vk::Extent2D const& resolution)
	{
		m_extent = resolution;
		// In case of minimalization or so dont refresh swapchain and other stuff...
		// presentation queue querries resolution an in case of invalid wont present
		if (m_extent.height == 0 || m_extent.width == 0)
			return;

		// need to destroy and recreate RAW vulkan objects in mandatory order 
		// - encapsulated objects will thus be in invalid state within scope of this method	
		destroyResources();

		// Now recreate all raw object behind theese handles
		recreateResources();

		if (m_resizeCallback)
			m_resizeCallback(resolution);
	}

	void VulkanWindow::setResizeCallback(std::function<void(vk::Extent2D const&)> cb)
	{
		m_resizeCallback = cb;
	}

	vk::Extent2D VulkanWindow::getExtent() const
	{
		return m_extent;
	}

	VulkanSurface const& VulkanWindow::getSurface() const
	{
		return *m_surface;
	}

	void VulkanWindow::revalidateWindow()
	{
		destroyResources();
		recreateResources();
	}

	uint32_t VulkanWindow::getNextImage()
	{
		auto [result, imageIndex] = getDevice().lock()->operator()().acquireNextImageKHR(m_rawSwapChain.get(), UINT64_MAX, m_semaphoreImageAvailable->operator()());

		if (result == vk::Result::eErrorOutOfDateKHR)
		{
			destroyResources();
			recreateResources();
		}
		return imageIndex;
	}

	VulkanFence& VulkanWindow::getRenderingFence()
	{
		return *m_fenceCopyFinished;
	}

	void VulkanWindow::createFrameBuffersForWindow()
	{
		std::vector<std::shared_ptr<VulkanImage>> swapchainImages;
		getSwapchainImages(swapchainImages);
		for (auto i = 0; i < swapchainImages.size(); ++i)
			m_frameData[i].image = std::move(swapchainImages.at(i));
	}

	uint32_t VulkanWindow::currentImageIndex() const
	{
		return m_currentImageIndex;
	}

	vk::SwapchainKHR VulkanWindow::operator()() const
	{
		return m_rawSwapChain.get();
	}

	void VulkanWindow::setName(std::string const& name)
	{	
		VulkanObject::setName(name);
		if (operator()())
			getDevice().lock()->setDebugObjectName(reinterpret_cast<uint64_t>(static_cast<VkSwapchainKHR>(operator()())), vk::ObjectType::eSwapchainKHR, name);
		//getDevice().lock()->setDebugObjectName(reinterpret_cast<uint64_t>(static_cast<VkSurfaceKHR>(m_surface->operator()()), vk::ObjectType::eSurfaceKHR, name);
	}

	void VulkanWindow::destroyResources()
	{
		//wait for all rendering to be done
		getDevice().lock()->waitIdle();

		// 1. Destroy swapchain images
		for (auto& frameData: m_frameData)
			frameData.image.reset();
		
		// 5. Destroy Window's image views && 6. Destroy Window's swapchain
		m_rawSwapChain.reset();
	}

	void VulkanWindow::recreateResources()
	{
		auto instance = getInstance().lock();
		// 1. Recreate Window's swapchain
		createSwapChain(instance->getDefaults().windowSamples);
		// 2. Recreate fbos
		createFrameBuffersForWindow();

		m_recreated = true;

		setName(getName());
	}

	void VulkanWindow::createSwapChain(uint32_t sampleCount)
	{
		auto lockedDevice = getDevice().lock();
		auto swapChainInfo = vk::SwapchainCreateInfoKHR();
		{
			swapChainInfo.surface = m_surface->operator()();
			swapChainInfo.minImageCount = sampleCount;
			swapChainInfo.imageColorSpace = m_surface->getFormat().colorSpace;
			swapChainInfo.imageFormat = m_surface->getFormat().format;
			swapChainInfo.imageExtent = m_extent;
			swapChainInfo.imageArrayLayers = 1;
			swapChainInfo.imageUsage = m_surface->getSupportedUsages();
			swapChainInfo.imageSharingMode = vk::SharingMode::eExclusive;
			//swapChainInfo.pQueueFamilyIndices; -relevant only for vk::SharingMode::eConcurent - when presentation que is different from graphics que
			//swapChainInfo.queueFamilyIndexCount; -relevant only for vk::SharingMode::eConcurent
			swapChainInfo.preTransform = vk::SurfaceTransformFlagBitsKHR::eIdentity;
			swapChainInfo.compositeAlpha = vk::CompositeAlphaFlagBitsKHR::eOpaque;
			swapChainInfo.presentMode = m_presentMode;
			swapChainInfo.clipped = VK_FALSE;
			swapChainInfo.oldSwapchain = VK_NULL_HANDLE;
		}

		m_rawSwapChain = std::move(lockedDevice->operator()().createSwapchainKHRUnique(swapChainInfo));
	}

	void VulkanWindow::getSwapchainImages(std::vector<std::shared_ptr<VulkanImage>>& imagesOut)
	{
		auto instance = getInstance().lock();
		auto device = getDevice().lock();
		imagesOut.clear();

		auto images = device->operator()().getSwapchainImagesKHR(m_rawSwapChain.get());
		auto i = 0;
		for (auto& image : images)
		{
			imagesOut.push_back(std::make_shared<VulkanImage>(instance, device, *m_surface, image, m_extent, "SwapImg " + std::to_string(i)));
			++i;
		}
	}

	void VulkanWindow::copyToWindow(VulkanCommandBuffer& commandBuffer, VulkanImageAttachment& srcImage)
	{
		auto& dstImage = m_frameData.at(m_currentImageIndex).image;

		//Make the source image's memory format to be compatible for reading
		//Note, that src image format must have been created with FORMAT_FEATURE_TRANSFER_SRC_BIT, so it could be copied form
		auto srcBarrier = srcImage.generateBarrier(vk::ImageLayout::eColorAttachmentOptimal, vk::ImageLayout::eTransferSrcOptimal, vk::AccessFlagBits2::eColorAttachmentWrite, vk::PipelineStageFlagBits2::eColorAttachmentOutput, vk::AccessFlagBits2::eTransferRead, vk::PipelineStageFlagBits2::eCopy);

		//Make the destination image's memory format to be compatible for writing, note that there is que ownership transfer
		//Note, that dst image format must have been created with FORMAT_FEATURE_TRANSFER_DST_BIT, so it could be copied to
		auto oldSwapChainLayout = vk::ImageLayout::ePresentSrcKHR;
		if (m_recreated)
			oldSwapChainLayout = vk::ImageLayout::eUndefined;
		auto dstBarrier = dstImage->generateBarrier(oldSwapChainLayout, vk::ImageLayout::eTransferDstOptimal, vk::AccessFlagBits2::eNone, vk::PipelineStageFlagBits2::eTopOfPipe, vk::AccessFlagBits2::eTransferWrite, vk::PipelineStageFlagBits2::eCopy);

		libVulkan::VulkanBarrier::recordBarriers(commandBuffer, { srcBarrier , dstBarrier });

		//perform the copy operation
		commandBuffer.copyImageToImage(srcImage.getImage(), vk::ImageLayout::eTransferSrcOptimal, *dstImage, vk::ImageLayout::eTransferDstOptimal);

		//set the dstImage to be ready to present --not sure about the desrination stage, dont know which is after fragment shader and after copy.... for bottom of pipe access must allways be none... but i would thought that eRead would be ok... maybe this is wrong
		auto presentBarrier = dstImage->generateBarrier(vk::ImageLayout::eTransferDstOptimal, vk::ImageLayout::ePresentSrcKHR, vk::AccessFlagBits2::eTransferWrite, vk::PipelineStageFlagBits2::eCopy, vk::AccessFlagBits2::eNone, vk::PipelineStageFlagBits2::eBottomOfPipe);

		//set back the source to be attachment
		auto oldSrc = srcImage.generateBarrier(vk::ImageLayout::eTransferSrcOptimal, vk::ImageLayout::eColorAttachmentOptimal, vk::AccessFlagBits2::eTransferRead, vk::PipelineStageFlagBits2::eCopy, vk::AccessFlagBits2::eNone, vk::PipelineStageFlagBits2::eBottomOfPipe);


		libVulkan::VulkanBarrier::recordBarriers(commandBuffer, { presentBarrier , oldSrc });

		if ((m_currentImageIndex == (m_frameData.size() - 1)) && m_recreated)
			m_recreated = false;
	}
}