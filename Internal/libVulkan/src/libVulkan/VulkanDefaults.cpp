//! libVulkan
#include <libVulkan/VulkanDefaults.hpp>

namespace libVulkan
{
	VulkanDefaults::VulkanDefaults()
		: appName("Gleam")
		, appVersion(glm::vec3(0, 0, 1))
		, extensions({ VK_KHR_SWAPCHAIN_EXTENSION_NAME, VK_EXT_DEBUG_UTILS_EXTENSION_NAME })
		, validationLayers()
		, multisampling(vk::SampleCountFlagBits::e1)
		, functionality({ vk::QueueFlagBits::eGraphics })
		, frameBufferResolution(glm::vec2(1280, 720))
		, windowSamples(1)
		, clearColor(glm::vec4({ 47 / 255.f, 79 / 255.f, 79 / 255.f, 1.f }))
		, clearDepth(glm::vec2(1.f, 0.f))
		, presentationMode(vk::PresentModeKHR::eFifo)
		, deviceFeatures()
	{
		deviceFeatures.vertexPipelineStoresAndAtomics = true; //arms dont support this generally, otherwise it seems  to be supported except for old integrated intels
	}

	VulkanDefaults::VulkanDefaults(VulkanDefaults const& defaults)
	{
		appName = defaults.appName;
		appVersion = defaults.appVersion;
		extensions = defaults.extensions;
		validationLayers = defaults.validationLayers;
		multisampling = defaults.multisampling;
		functionality = defaults.functionality;
		frameBufferResolution = defaults.frameBufferResolution;
		windowSamples = defaults.windowSamples;
		clearColor = defaults.clearColor;
		clearDepth = defaults.clearDepth;
		presentationMode = defaults.presentationMode;
		deviceFeatures = defaults.deviceFeatures;
	}
}