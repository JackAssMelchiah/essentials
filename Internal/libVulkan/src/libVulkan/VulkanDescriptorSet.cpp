//! libVulkan
#include <libVulkan/VulkanDescriptorSet.hpp>
#include <libVulkan/VulkanAll.hpp>

namespace libVulkan
{
	VulkanDescriptorSet::VulkanDescriptorSet(std::shared_ptr<VulkanInstance> const& instance, std::shared_ptr<VulkanDevice> const& device, vk::UniqueDescriptorSet rawSet, std::shared_ptr<VulkanDescriptorSetLayout> descriptorSetLayout, std::string const& name)
		: VulkanObject(instance, device, name)
		, m_rawSet(std::move(rawSet))
		, m_descriptionSetLayout(descriptorSetLayout)
	{
		setName(getName());
	}

	void VulkanDescriptorSet::updateImage(uint32_t startingBindPoint, vk::DescriptorType type, std::vector<vk::DescriptorImageInfo> const& imageInfos)
	{
		auto writeSet = vk::WriteDescriptorSet();
		{
			//update this descriptor set 
			writeSet.dstSet = *m_rawSet;
			//with as many descriptors as is provided
			writeSet.descriptorCount = imageInfos.size();
			// of the same descriptor type. For multiple types, update() must be called multiple times
			writeSet.descriptorType = type;
			//and first descriptor begins at startingBindPoint 
			writeSet.dstBinding = startingBindPoint;
			//with information about those descriptors - taking them from buffers
			writeSet.pImageInfo = imageInfos.data(); //the imageLayout within this must correspond to descriptorType - type must be compatible with imageInfos
			//suposedly, start with 0 element of bufferBindings ...
			writeSet.dstArrayElement = 0;

			writeSet.pBufferInfo = nullptr;
			writeSet.pTexelBufferView = nullptr;
		}
		getDevice().lock()->operator()().updateDescriptorSets({ writeSet }, {});
	}

	void VulkanDescriptorSet::updateBuffer(uint32_t startingBindPoint, vk::DescriptorType type, std::vector<vk::DescriptorBufferInfo> const& bufferInfos)
	{
		auto writeSet = vk::WriteDescriptorSet();
		{
			//update this descriptor set 
			writeSet.dstSet = *m_rawSet;
			//with as many descriptors as is provided
			writeSet.descriptorCount = bufferInfos.size();
			// of the same descriptor type. For multiple types, update() must be called multiple times
			writeSet.descriptorType = type;
			//and first descriptor begins at startingBindPoint 
			writeSet.dstBinding = startingBindPoint;
			//with information about those descriptors - taking them from buffers
			writeSet.pBufferInfo = bufferInfos.data();
			//suposedly, start with 0 element of bufferBindings ...
			writeSet.dstArrayElement = 0;

			writeSet.pImageInfo = nullptr;
			writeSet.pTexelBufferView = nullptr;
		}
		getDevice().lock()->operator()().updateDescriptorSets({ writeSet }, {});
	}
			
	void VulkanDescriptorSet::write(uint32_t binding, vk::DescriptorType type, VulkanBuffer const& buffer)
	{
		//currently writes the whole buffer - so double or tripple buffering wont work! TODO
		auto info = vk::DescriptorBufferInfo();
		{
			info.buffer = buffer.operator()();
			info.offset = 0;
			info.range = buffer.getByteSize();
		}

		write(binding, type, nullptr, nullptr, &info);
	}

	void VulkanDescriptorSet::write(uint32_t binding, VulkanImage const& image, VulkanImageSampler const& sampler)
	{
		auto info = vk::DescriptorImageInfo();
		{
			info.imageView = image.getImageView().operator()();
			info.imageLayout = vk::ImageLayout::eShaderReadOnlyOptimal;
			info.sampler = sampler.operator()();
		}
		write(binding, vk::DescriptorType::eCombinedImageSampler, nullptr, &info, nullptr);
	}

	VulkanDescriptorSetLayout const& VulkanDescriptorSet::getLayout() const
	{
		return *m_descriptionSetLayout.get();
	}

	vk::DescriptorSet VulkanDescriptorSet::operator()() const
	{
		return m_rawSet.get();
	}

	void VulkanDescriptorSet::setName(std::string const& name)
	{
		VulkanObject::setName(name);
		if (operator()())
			getDevice().lock()->setDebugObjectName(reinterpret_cast<uint64_t>(static_cast<VkDescriptorSet>(operator()())), vk::ObjectType::eDescriptorSet, name);
	}

	void VulkanDescriptorSet::write(uint32_t binding, vk::DescriptorType type, vk::BufferView const* texel, vk::DescriptorImageInfo const* image, vk::DescriptorBufferInfo const* buffer)
	{
		auto copySet = vk::CopyDescriptorSet();
		auto writeSet = vk::WriteDescriptorSet();
		{
			writeSet.pTexelBufferView = texel;
			writeSet.pImageInfo = image;
			writeSet.pBufferInfo = buffer;

			// single descriptor gets updated so offset is 0 and size is 1
			writeSet.dstArrayElement = 0; 
			writeSet.descriptorCount = 1;

			writeSet.descriptorType = type;
			writeSet.dstBinding = binding;
			writeSet.dstSet = m_rawSet.get(); //raw descriptorSet
		}

		getDevice().lock()->operator()().updateDescriptorSets({ writeSet }, {});
	}
}