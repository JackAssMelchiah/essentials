//! libVulkan
#include <libVulkan/VulkanSemaphore.hpp>

namespace libVulkan
{
	VulkanSemaphore::VulkanSemaphore(std::shared_ptr<VulkanInstance> const& instance, std::shared_ptr<VulkanDevice> const& device, std::string const& name)
		: VulkanObject(instance, device, name)
	{
		recreate();
	}

	vk::Semaphore VulkanSemaphore::operator()() const
	{
		return m_rawObject.get();
	}

	void VulkanSemaphore::setName(std::string const& name)
	{
		VulkanObject::setName(name);
		if (operator()())
			getDevice().lock()->setDebugObjectName(reinterpret_cast<uint64_t>(static_cast<VkSemaphore>(operator()())), vk::ObjectType::eSemaphore, name);
	}

	void VulkanSemaphore::recreate()
	{
		m_rawObject = getDevice().lock()->operator()().createSemaphoreUnique(vk::SemaphoreCreateInfo{});
		setName(getName());
	}
}