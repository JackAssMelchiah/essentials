//! libVulkan
#include <libVulkan/VulkanQueue.hpp>
#include <libVulkan/VulkanAll.hpp>

namespace libVulkan
{
	VulkanQueue::VulkanQueue(std::shared_ptr<VulkanInstance> const& instance, std::shared_ptr<VulkanDevice> const& device, vk::Queue rawQue, vk::QueueFlags func, uint32_t queueFamilyIndex, uint32_t indexWithinFamily, std::string const& name)
		: VulkanObject(instance, device, name)
		, m_rawQue(rawQue)
		, m_functionality(func)
		, m_queueFamilyIndex(queueFamilyIndex)
		, m_indexWithinFamily(indexWithinFamily)		
	{
		setName(getName());
	}

	uint32_t VulkanQueue::getFamilyIndex() const
	{
		return m_queueFamilyIndex;
	}

	uint32_t VulkanQueue::getIndexWithinFamily() const
	{
		return m_indexWithinFamily;
	}

	vk::QueueFlags VulkanQueue::getFunctionality() const
	{
		return m_functionality;
	}

	void VulkanQueue::submit(std::vector<std::shared_ptr<VulkanCommandBuffer>> const& commandBuffers, std::vector<SemaphorePrimitive> const& signalSemaphores, std::vector<SemaphorePrimitive> const& waitSemaphores, vk::PipelineStageFlags const& waitStage, std::shared_ptr<VulkanFence> const& signalFence)
	{
		auto rawCmdBuffersInfos = std::vector<vk::CommandBufferSubmitInfo>();
		for (auto& commandBuffer : commandBuffers)
		{
			auto info = vk::CommandBufferSubmitInfo();
			{
				info.commandBuffer = commandBuffer->operator()();
				info.deviceMask = 0;
			}
			rawCmdBuffersInfos.push_back(commandBuffer->operator()());
		}

		auto rawSSemaphoresInfos = std::vector<vk::SemaphoreSubmitInfo>();
		for (auto& rawSSemaphore : signalSemaphores)
		{
			auto info = vk::SemaphoreSubmitInfo();
			{
				info.semaphore = rawSSemaphore.semaphore->operator()();
				info.value = rawSSemaphore.value;
				info.stageMask = rawSSemaphore.stage;
				info.deviceIndex = 0;
			}
			rawSSemaphoresInfos.push_back(info);
		}
		
		auto rawWSemaphoresInfos = std::vector<vk::SemaphoreSubmitInfo>();
		for (auto& rawWSemaphore : waitSemaphores)
		{
			auto info = vk::SemaphoreSubmitInfo();
			{
				info.semaphore = rawWSemaphore.semaphore->operator()();
				info.value = rawWSemaphore.value;
				info.stageMask = rawWSemaphore.stage;
				info.deviceIndex = 0;
			}
			rawWSemaphoresInfos.push_back(info);
		}

		vk::Fence rawFence = {};
		if (signalFence)
			rawFence = signalFence->operator()();

		auto submitInfo = vk::SubmitInfo2();
		{
			submitInfo.pCommandBufferInfos = rawCmdBuffersInfos.data();
			submitInfo.commandBufferInfoCount = rawCmdBuffersInfos.size();
			submitInfo.pSignalSemaphoreInfos = rawSSemaphoresInfos.data();
			submitInfo.signalSemaphoreInfoCount = rawSSemaphoresInfos.size();

			submitInfo.pWaitSemaphoreInfos = rawWSemaphoresInfos.data();
			submitInfo.waitSemaphoreInfoCount = rawWSemaphoresInfos.size();

			submitInfo.flags = vk::SubmitFlagBits();
		}

		m_rawQue.submit2(submitInfo, rawFence);
	}

	void VulkanQueue::submit(std::vector<std::reference_wrapper<VulkanCommandBuffer>> const& commandBuffers)
	{
		auto rawCmdBuffers = std::vector<vk::CommandBuffer>();
		for (auto& commandBuffer : commandBuffers)
		{
			rawCmdBuffers.push_back(commandBuffer.get().operator()());
		}
		
		auto submitInfo = vk::SubmitInfo();
		{
			submitInfo.commandBufferCount = rawCmdBuffers.size();
			submitInfo.pCommandBuffers = rawCmdBuffers.data();
		}
		m_rawQue.submit(submitInfo);
	}

	void VulkanQueue::submit(std::vector<std::reference_wrapper<VulkanCommandBuffer>> const& commandBuffers, std::vector<std::reference_wrapper<VulkanSemaphore>> const& signalSemaphores, std::vector<std::reference_wrapper<VulkanSemaphore>> const& waitSemaphores, std::vector<vk::PipelineStageFlags> const& waitStage)
	{
		auto rawCmdBuffers = std::vector<vk::CommandBuffer>();
		for (auto& commandBuffer : commandBuffers)
		{
			rawCmdBuffers.push_back(commandBuffer.get().operator()());
		}
		auto rawSSemaphores = std::vector<vk::Semaphore>();
		for (auto& rawSSemaphore : signalSemaphores)
		{
			rawSSemaphores.push_back(rawSSemaphore.get().operator()());
		}
		auto rawWSemaphores = std::vector<vk::Semaphore>();
		for (auto& rawWSemaphore : waitSemaphores)
		{
			rawWSemaphores.push_back(rawWSemaphore.get().operator()());
		}

		auto submitInfo = vk::SubmitInfo();
		{
			submitInfo.commandBufferCount = rawCmdBuffers.size();
			submitInfo.pCommandBuffers = rawCmdBuffers.data();
			submitInfo.signalSemaphoreCount = rawSSemaphores.size();
			submitInfo.pSignalSemaphores = rawSSemaphores.data();
			submitInfo.waitSemaphoreCount = rawWSemaphores.size();
			submitInfo.pWaitSemaphores = rawWSemaphores.data();
			submitInfo.pWaitDstStageMask = waitStage.data();
		}
		m_rawQue.submit(submitInfo);
	}

	void VulkanQueue::submit(std::vector<std::reference_wrapper<VulkanCommandBuffer>> const& commandBuffers, std::vector<std::reference_wrapper<VulkanSemaphore>> const& signalSemaphores, std::vector<std::reference_wrapper<VulkanSemaphore>> const& waitSemaphores, std::vector<vk::PipelineStageFlags> const& waitStage, VulkanFence& signalFence)
	{
		auto rawCmdBuffers = std::vector<vk::CommandBuffer>();
		for (auto& commandBuffer : commandBuffers) 
		{
			rawCmdBuffers.push_back(commandBuffer.get().operator()());
		}
		auto rawSSemaphores = std::vector<vk::Semaphore>();
		for (auto& rawSSemaphore : signalSemaphores)
		{
			rawSSemaphores.push_back(rawSSemaphore.get().operator()());
		}
		auto rawWSemaphores = std::vector<vk::Semaphore>();
		for (auto& rawWSemaphore : waitSemaphores)
		{
			rawWSemaphores.push_back(rawWSemaphore.get().operator()());
		}

		auto submitInfo = vk::SubmitInfo();
		{
			submitInfo.commandBufferCount = rawCmdBuffers.size();
			submitInfo.pCommandBuffers = rawCmdBuffers.data();
			submitInfo.signalSemaphoreCount = rawSSemaphores.size();
			submitInfo.pSignalSemaphores = rawSSemaphores.data();
			submitInfo.waitSemaphoreCount = rawWSemaphores.size();
			submitInfo.pWaitSemaphores = rawWSemaphores.data();
			submitInfo.pWaitDstStageMask = waitStage.data();
		}
		m_rawQue.submit(submitInfo, signalFence.operator()());
	}

	void VulkanQueue::present(VulkanWindow& window, std::vector<std::reference_wrapper<VulkanSemaphore>> const& waitSemaphores)
	{
		assert(window.getExtent().height != 0 && window.getExtent().width != 0);

		auto rawWSemaphores = std::vector<vk::Semaphore>();
		for (auto& rawWSemaphore : waitSemaphores)
		{
			rawWSemaphores.push_back(rawWSemaphore.get().operator()());
		}

		auto swapchainIndice = window.currentImageIndex();
		auto swapchains = std::vector<vk::SwapchainKHR>{window.operator()()};

		auto presentationInfo = vk::PresentInfoKHR();
		{
			presentationInfo.swapchainCount = swapchains.size();
			presentationInfo.pSwapchains = swapchains.data();
			presentationInfo.pImageIndices = &swapchainIndice;

			presentationInfo.waitSemaphoreCount = rawWSemaphores.size();
			presentationInfo.pWaitSemaphores = rawWSemaphores.data();
		}

		auto result = m_rawQue.presentKHR(presentationInfo);
		if (result == vk::Result::eErrorOutOfDateKHR)
			window.revalidateWindow();
		
		else if (result != vk::Result::eSuccess)
			assert(!"Cannot present something bad has happened");
		
	}

	vk::Queue VulkanQueue::operator()() const
	{
		return m_rawQue;
	}

	void VulkanQueue::setName(std::string const& name)
	{
		VulkanObject::setName(name);
		if (operator()())
			getDevice().lock()->setDebugObjectName(reinterpret_cast<uint64_t>(static_cast<VkQueue>(operator()())), vk::ObjectType::eQueue, name);
	}
	
	void VulkanQueue::wait()
	{
		m_rawQue.waitIdle();
	}
}