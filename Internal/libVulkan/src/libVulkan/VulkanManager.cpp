//! libVulkan
#include <libVulkan/VulkanManager.hpp>
#include <libVulkan/VulkanAll.hpp>
//! libUtils
#include <libUtils/Utils.hpp>

namespace libVulkan
{
	VulkanManager::VulkanManager(std::shared_ptr<VulkanDefaults> defaults)
		: m_defaults(defaults)
	{	
	}

	VulkanManager::~VulkanManager()
	{
		//window need to be destroyed before device
		m_windows.clear();
		//device needs to be destoyed before instance
		m_device.reset();
		m_instance.reset();
	}

	std::weak_ptr<VulkanWindow> VulkanManager::createWindow(vk::SurfaceKHR surface, glm::ivec2 const& initialResolution, std::string const& name)
	{
		auto vulkanSurface = createSurface(surface);
		auto window = std::make_shared<libVulkan::VulkanWindow>(getInstance().lock(), getDevice().lock(), m_queues->getPresentationQueue(), std::move(vulkanSurface), vk::Extent2D{uint32_t(initialResolution.x), uint32_t(initialResolution.y)}, name);
		m_windows.insert(window);
		return window;
	}

	VulkanWindow& VulkanManager::getWindow()
	{
		return *(*m_windows.begin());
	}

	std::weak_ptr<VulkanDevice> VulkanManager::getDevice() const
	{
		return m_device;
	}

	std::weak_ptr<VulkanInstance> VulkanManager::getInstance() const
	{
		return m_instance;
	}

	std::weak_ptr<VulkanQueues> VulkanManager::getQueues() const
	{
		return m_queues;
	}

	bool VulkanManager::createInstance()
	{
		if (m_instance)
			return false;

		auto ext = filterAvailableInstanceExtensions(m_defaults->extensions);
		auto layers = filterAvailableInstanceLayers(m_defaults->validationLayers);
		setRequiredFeatures();

		auto applicationInfo = vk::ApplicationInfo();
		{
			applicationInfo.apiVersion = VK_API_VERSION_1_3; // minimal expected vulkan version app is needed to run
			applicationInfo.pApplicationName = m_defaults->appName.c_str();
			applicationInfo.applicationVersion = VK_MAKE_VERSION(m_defaults->appVersion.x, m_defaults->appVersion.y, m_defaults->appVersion.z);
			applicationInfo.pEngineName = "GLEAM";
			applicationInfo.engineVersion = VK_MAKE_VERSION(0, 1, 0);
		}

		auto instanceInfo = vk::InstanceCreateInfo(); 
		{
			instanceInfo.enabledLayerCount = layers.size();
			instanceInfo.ppEnabledLayerNames = layers.data();
			instanceInfo.enabledExtensionCount = ext.size();
			instanceInfo.ppEnabledExtensionNames = ext.data();
			instanceInfo.pApplicationInfo = &applicationInfo;
		}
		m_instance = std::make_shared<VulkanInstance>(vk::createInstanceUnique(instanceInfo), m_defaults, "VulkanInstance");
		return true;
	}

	bool VulkanManager::createDevice(std::shared_ptr<VulkanInstance> const& instance, vk::SurfaceKHR const* surface)
	{
		if (m_device)
			return false;
		
		auto& requiredFunc = m_defaults->functionality;
		if (!instance->pickDevice(requiredFunc))
			return false;

		auto priorities = std::vector<std::vector<float>>();
		auto deviceQueInfos = std::vector<vk::DeviceQueueCreateInfo>();
		for (auto& requirement : requiredFunc)
		{
			auto familyIndex = instance->getQueueFamilyIndex(instance->getRawPhysicalDevice(), requirement);
			auto queFamilyCount = instance->getQueueFamilyCount(instance->getRawPhysicalDevice(), familyIndex);
			priorities.push_back(std::vector<float>(queFamilyCount, 1.f / queFamilyCount));

			auto queInfo = vk::DeviceQueueCreateInfo();
			{
				queInfo.queueFamilyIndex = familyIndex;
				queInfo.queueCount = queFamilyCount;
				queInfo.pQueuePriorities = priorities.back().data();
			}
			deviceQueInfos.push_back(queInfo);
		}

		if (surface)
		{// presentation que might be allready added within ques, if it shares compatibility with other ques, but if its not, find it and add it
			auto presentationFamilyIndex = instance->getPresentationQueueFamilyIndex(instance->getRawPhysicalDevice(), *surface);
			auto presenationCapableFound = false;
			for (auto& que : deviceQueInfos)
			{
				if (que.queueFamilyIndex == presentationFamilyIndex)
					presenationCapableFound = true;
			}
			if (!presenationCapableFound)
			{
				auto queFamilyCount = instance->getQueueFamilyCount(instance->getRawPhysicalDevice(), presentationFamilyIndex);
				priorities.push_back(std::vector<float>(queFamilyCount, 1.f / queFamilyCount));
				auto queInfo = vk::DeviceQueueCreateInfo();
				{
					queInfo.queueFamilyIndex = presentationFamilyIndex;
					queInfo.queueCount = queFamilyCount;
					queInfo.pQueuePriorities = priorities.back().data();
				}
				deviceQueInfos.push_back(queInfo);
			}
		}

		auto extensions = instance->filterAvailableDeviceExtensions(m_defaults->extensions);
		auto layers = instance->filterAvailableDeviceLayers(m_defaults->validationLayers);

		auto vk11Features = vk::PhysicalDeviceVulkan11Features();
		{
		}

		auto vk12Features = vk::PhysicalDeviceVulkan12Features();
		{
			vk12Features.timelineSemaphore = true;
		}

		auto vk13Features = vk::PhysicalDeviceVulkan13Features();
		{
			vk13Features.dynamicRendering = true;
			vk13Features.synchronization2 = true;
		}

		vk11Features.pNext = &vk12Features;
		vk12Features.pNext = &vk13Features;

		auto deviceInfo = vk::DeviceCreateInfo();
		{
			deviceInfo.enabledLayerCount = layers.size();
			deviceInfo.ppEnabledLayerNames = layers.data();
			deviceInfo.enabledExtensionCount = extensions.size();
			deviceInfo.ppEnabledExtensionNames = extensions.data();
			deviceInfo.pEnabledFeatures = &m_defaults->deviceFeatures;
			deviceInfo.queueCreateInfoCount = deviceQueInfos.size();
			deviceInfo.pQueueCreateInfos = deviceQueInfos.data();
			deviceInfo.pNext = &vk11Features;
		}

		//validate default parameters before spawning device, so everything is supported
		instance->validateDefaults();

		m_device = std::make_shared<VulkanDevice>(instance, instance->getRawPhysicalDevice().createDeviceUnique(deviceInfo), "MainDevice");
		
		return true;
	}

	void VulkanManager::createQueues()
	{
		m_queues = std::make_unique<VulkanQueues>(getInstance().lock(), getDevice().lock(), m_instance->getDefaults().functionality);
	}

	std::vector<char const*> VulkanManager::filterAvailableInstanceExtensions(std::vector<std::string> const& wantedExtensions) const
	{
		auto availableExtensions = vk::enumerateInstanceExtensionProperties();
		auto filtered = std::vector<char const*>();
		for (auto& wantedExtension: wantedExtensions) 
		{
			for (auto& availableExtension : availableExtensions) 
			{
				if (wantedExtension == availableExtension.extensionName) 
				{
					filtered.push_back(wantedExtension.c_str());
				}
			}
		}
		return filtered;
	}

	std::vector<char const*> VulkanManager::filterAvailableInstanceLayers(std::vector<std::string> const& wantedLayers) const
	{
		auto availableLayers = vk::enumerateInstanceLayerProperties();
		auto filtered = std::vector<char const*>();
		for (auto& wantedLayer : wantedLayers) 
		{
			for (auto& availableLayer : availableLayers)
			{
				if (wantedLayer == availableLayer.layerName) 
				{
					filtered.push_back(wantedLayer.c_str());
				}
			}
		}
		return filtered;
	}

	void VulkanManager::setRequiredFeatures()
	{
		//m_defaults->deviceFeatures.shaderFloat64 = VK_TRUE;
	}

	std::unique_ptr<VulkanSurface> VulkanManager::createSurface(vk::SurfaceKHR rawSurface) const
	{
		auto displayCapabilites = vk::SurfaceCapabilitiesKHR();
		auto result = m_instance->getSurfaceCapabilites(rawSurface, displayCapabilites);
		if (!result)
			return nullptr;

		auto surfaceFormats = std::vector<vk::SurfaceFormatKHR>();
		m_instance->getSurfaceFormats(rawSurface, surfaceFormats);
		if (surfaceFormats.empty())
			return nullptr;

		auto pickedSurfaceFormat = surfaceFormats.front();

		//also when creating surface, add presentation que
		m_queues->createPresentationQueue(rawSurface);

		return std::make_unique<VulkanSurface>(getInstance().lock(), getDevice().lock(), std::move(rawSurface), displayCapabilites, pickedSurfaceFormat, "surface");
	}
}