//! libVulkan
#include <libVulkan/VulkanAll.hpp>

namespace libVulkan
{
	VulkanBarrier::VulkanBarrier(vk::ImageMemoryBarrier2 const& imageBarrier, vk::ImageSubresourceRange const& imageRange)
		: m_imageBarrier(imageBarrier)
		, m_imageRange(imageRange)
		, m_bufferBarrier()
		, m_isImageBarrier(true)
	{
	}

	VulkanBarrier::VulkanBarrier(vk::BufferMemoryBarrier2 const& bufferBarrier)
		: m_imageBarrier()
		, m_imageRange()
		, m_bufferBarrier(bufferBarrier)
		, m_isImageBarrier(false)
	{
	}

	void VulkanBarrier::recordBarriers(VulkanCommandBuffer& commandBuffer, std::vector<std::shared_ptr<VulkanBarrier>> const& barriers)
	{
		auto imageBarriers = std::vector<vk::ImageMemoryBarrier2>();
		auto bufferBarriers = std::vector<vk::BufferMemoryBarrier2>();
		for (auto& barrier : barriers)
		{
			if (barrier->m_isImageBarrier)
				imageBarriers.push_back(barrier->m_imageBarrier);
			else
				bufferBarriers.push_back(barrier->m_bufferBarrier);
		}

		auto dependencyInfo = vk::DependencyInfo();
		{
			dependencyInfo.imageMemoryBarrierCount = imageBarriers.size();
			dependencyInfo.pImageMemoryBarriers = imageBarriers.data();
			dependencyInfo.bufferMemoryBarrierCount = bufferBarriers.size();
			dependencyInfo.pBufferMemoryBarriers = bufferBarriers.data();
			dependencyInfo.dependencyFlags;//optional
		}
		commandBuffer.operator()().pipelineBarrier2(dependencyInfo);
	}
}