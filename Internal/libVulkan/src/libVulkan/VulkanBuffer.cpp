//! libVulkan
#include <libVulkan/VulkanBuffer.hpp>
#include <libVulkan/VulkanAll.hpp>

namespace libVulkan
{
    VulkanBuffer::VulkanBuffer(std::shared_ptr<VulkanInstance> const& instance, std::shared_ptr<VulkanDevice> const& device, vk::BufferUsageFlags usage, vk::MemoryPropertyFlags flags, uint32_t bytes, std::string const& name)
        : VulkanObject(instance, device, name)
        , m_usage(usage)
        , m_bytes(bytes)
        , m_maped(false)
    {
        //raw buffer
        m_rawBuffer = createRawBuffer(bytes, usage);
        setName(getName());
        //back that buffer by memory 
        m_rawMemory = backBufferByMemory(m_rawBuffer.get(), flags);
    }

    uint32_t VulkanBuffer::getByteSize() const
    {
        return m_bytes;
    }

    vk::BufferUsageFlags VulkanBuffer::getUsage() const
    {
        return m_usage;
    }

    bool VulkanBuffer::mapped() const
    {
        return m_maped;
    }

    void VulkanBuffer::map()
    {
        m_maped = true;
        m_bufferPointer = getDevice().lock()->operator()().mapMemory(m_rawMemory.get(), 0, m_bytes);
    }

    void VulkanBuffer::unmap()
    {
        m_maped = false;
        getDevice().lock()->operator()().unmapMemory(m_rawMemory.get());
    }

    void VulkanBuffer::flush()
    {
        getDevice().lock()->operator()().flushMappedMemoryRanges(vk::MappedMemoryRange{ m_rawMemory.get(), 0, VK_WHOLE_SIZE });
    }

    vk::Buffer VulkanBuffer::operator()() const
    {
        return m_rawBuffer.get();
    }

    vk::UniqueBuffer VulkanBuffer::createRawBuffer(uint32_t bytes, vk::BufferUsageFlags bufferUsage) const
    {
        //buffer will operate only on single queue
        auto bufferInfo = vk::BufferCreateInfo(); {
            bufferInfo.size = bytes;
            bufferInfo.usage = bufferUsage;
            bufferInfo.sharingMode = vk::SharingMode::eExclusive;
        }
        //buffer object
        return getDevice().lock()->operator()().createBufferUnique(bufferInfo);
    }

    vk::UniqueDeviceMemory VulkanBuffer::backBufferByMemory(vk::Buffer& bufferRawHandle, vk::MemoryPropertyFlags flags) const
    {
        auto device = getDevice().lock();
        //memory backing
        auto bufferRequirements = device->operator()().getBufferMemoryRequirements(bufferRawHandle);
        auto memory = device->allocateMemory(bufferRequirements.size, bufferRequirements, flags); //memory will be equal to of that object, for now not bigger
        auto memoryOffset = vk::DeviceSize(0);
        //bringing it all together
        device->operator()().bindBufferMemory(bufferRawHandle, memory.get(), memoryOffset);
        return memory;
    }

    void VulkanBuffer::setName(std::string const& name)
    {
        VulkanObject::setName(name);
        if (operator()())
            getDevice().lock()->setDebugObjectName(reinterpret_cast<uint64_t>(static_cast<VkBuffer>(this->operator()())), vk::ObjectType::eBuffer, name);
    }
}
