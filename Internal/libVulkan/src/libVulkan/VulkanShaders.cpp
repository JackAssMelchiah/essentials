//! libVulkan
#include <libVulkan/VulkanShaders.hpp>

#include <libUtils/Utils.hpp>

namespace libVulkan
{
	VulkanShaders::VulkanShaders(std::unique_ptr<VulkanShaderModule>&& computeModule)
	{
		m_modules.insert(std::make_pair(ShaderType::COMPUTE, std::move(computeModule)));
	}

	VulkanShaders::VulkanShaders(std::unique_ptr<VulkanShaderModule>&& vertexModule, std::unique_ptr<VulkanShaderModule>&& fragmentModule)
	{
		m_modules.insert(std::make_pair(ShaderType::VERTEX, std::move(vertexModule)));
		m_modules.insert(std::make_pair(ShaderType::FRAGMENT, std::move(fragmentModule)));
	}

	VulkanShaders::VulkanShaders(std::unique_ptr<VulkanShaderModule>&& vertexModule, std::unique_ptr<VulkanShaderModule>&& geometryModule, std::unique_ptr<VulkanShaderModule>&& fragmentModule)
	{
		m_modules.insert(std::make_pair(ShaderType::VERTEX, std::move(vertexModule)));
		m_modules.insert(std::make_pair(ShaderType::GEOMETRY, std::move(geometryModule)));
		m_modules.insert(std::make_pair(ShaderType::FRAGMENT, std::move(fragmentModule)));
	}

	VulkanShaders::VulkanShaders(std::unique_ptr<VulkanShaderModule>&& vertexModule, std::unique_ptr<VulkanShaderModule>&& tessalationControllModule, std::unique_ptr<VulkanShaderModule>&& tessalationEvaluationModule, std::unique_ptr<VulkanShaderModule>&& fragmentModule)
	{
		m_modules.insert(std::make_pair(ShaderType::VERTEX, std::move(vertexModule)));
		m_modules.insert(std::make_pair(ShaderType::TESALATION_CONTROL, std::move(tessalationControllModule)));
		m_modules.insert(std::make_pair(ShaderType::TESALATION_EVALUATION, std::move(tessalationEvaluationModule)));
		m_modules.insert(std::make_pair(ShaderType::FRAGMENT, std::move(fragmentModule)));
	}

	VulkanShaders::VulkanShaders(std::unique_ptr<VulkanShaderModule>&& vertexModule, std::unique_ptr<VulkanShaderModule>&& geometryModule, std::unique_ptr<VulkanShaderModule>&& tessalationControllModule, std::unique_ptr<VulkanShaderModule>&& tessalationEvaluationModule, std::unique_ptr<VulkanShaderModule>&& fragmentModule)
	{
		m_modules.insert(std::make_pair(ShaderType::VERTEX, std::move(vertexModule)));
		m_modules.insert(std::make_pair(ShaderType::GEOMETRY, std::move(geometryModule)));
		m_modules.insert(std::make_pair(ShaderType::TESALATION_CONTROL, std::move(tessalationControllModule)));
		m_modules.insert(std::make_pair(ShaderType::TESALATION_EVALUATION, std::move(tessalationEvaluationModule)));
		m_modules.insert(std::make_pair(ShaderType::FRAGMENT, std::move(fragmentModule)));
	}

	void VulkanShaders::getShadersInfos(std::vector<vk::PipelineShaderStageCreateInfo>& infos)
	{
		infos.clear();
		for (auto& module : m_modules)
		{
			auto info = vk::PipelineShaderStageCreateInfo();
			{
				info.module = module.second->operator()();
				info.pName = "main";
				info.pSpecializationInfo = nullptr;
				info.stage = getShaderStageFlag(module.first);
			}
			infos.push_back(info);
		}
	}

	vk::ShaderStageFlagBits VulkanShaders::getShaderStageFlag(ShaderType shaderType) const
	{
		auto retval = vk::ShaderStageFlagBits::eAll;
		switch (shaderType)
		{
		case ShaderType::VERTEX:
		{
			retval = vk::ShaderStageFlagBits::eVertex;
			break;
		}
		case ShaderType::COMPUTE:
		{
			retval = vk::ShaderStageFlagBits::eCompute;
			break;
		}
		case ShaderType::GEOMETRY:
		{
			retval = vk::ShaderStageFlagBits::eGeometry;
			break;
		}
		case ShaderType::TESALATION_EVALUATION:
		{
			retval = vk::ShaderStageFlagBits::eTessellationEvaluation;
			break;
		}
		case ShaderType::TESALATION_CONTROL:
		{
			retval = vk::ShaderStageFlagBits::eTessellationControl;
			break;
		}
		case ShaderType::FRAGMENT:
		{
			retval = vk::ShaderStageFlagBits::eFragment;
			break;
		}
		default:
			break;
		}
		return retval;
	}
}