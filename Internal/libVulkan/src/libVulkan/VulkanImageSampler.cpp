//! libVulkan 
#include <libVulkan/VulkanImageSampler.hpp>

namespace libVulkan
{
	VulkanImageSampler::VulkanImageSampler(std::shared_ptr<VulkanInstance> const& instance, std::shared_ptr<VulkanDevice> const& device, std::string const& name)
		: VulkanObject(instance, device, name)
	{
		createSampler();
		setName(getName());
	}

	vk::Sampler VulkanImageSampler::operator()() const
	{
		return m_rawSampler.get();
	}

	void VulkanImageSampler::setName(std::string const& name)
	{
		VulkanObject::setName(name);
		if (operator()())
			getDevice().lock()->setDebugObjectName(reinterpret_cast<uint64_t>(static_cast<VkSampler>(operator()())), vk::ObjectType::eSampler, name);
	}

	void VulkanImageSampler::createSampler()
	{
		auto info = vk::SamplerCreateInfo();
		{
			info.addressModeU = vk::SamplerAddressMode::eRepeat;
			info.addressModeV = vk::SamplerAddressMode::eRepeat;
			info.addressModeW = vk::SamplerAddressMode::eRepeat;

			// for now dont use anisotropic filtering
			info.anisotropyEnable = VK_FALSE;
			info.maxAnisotropy = 0.f;

			// out off bounds will be white - but they wont be since repeat is enabled
			info.borderColor = vk::BorderColor::eIntOpaqueWhite;

			info.unnormalizedCoordinates = VK_FALSE; // coords are normal 0-1 range

			// basically only for shadow-maps - dont use
			info.compareEnable = VK_FALSE;
			info.compareOp = vk::CompareOp::eAlways;

			//mip-mapping - dont enable now
			info.minLod = 0.f;
			info.maxLod = 0.f;
			info.mipmapMode = vk::SamplerMipmapMode::eLinear;
			info.mipLodBias = 0.f;

		}

		m_rawSampler = getDevice().lock()->operator()().createSamplerUnique(info);
	}
}