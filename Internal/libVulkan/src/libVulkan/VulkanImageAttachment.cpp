//! libVulkan
#include <libVulkan/VulkanImageAttachment.hpp>
#include <libVulkan/VulkanAll.hpp>
#include <libUtils/Utils.hpp>

namespace libVulkan
{
    VulkanImageAttachment::VulkanImageAttachment(std::shared_ptr<VulkanInstance> const& instance, std::shared_ptr<VulkanDevice> const& device, VulkanImage::Info const& info, vk::Extent2D const& resolution, std::string const& name)
        : VulkanObject(instance, device, name)
        , m_image(std::make_shared<VulkanImage>(instance, device, resolution, info, name))
    {
	}

    VulkanImageAttachment::VulkanImageAttachment(std::shared_ptr<VulkanInstance> const& instance, std::shared_ptr<VulkanDevice> const& device, std::shared_ptr<VulkanImage>&& image, vk::Extent2D const& resolution, std::string const& name)
        : VulkanObject(instance, device, name)
        , m_image({ std::move(image) })
    {
    }

    void VulkanImageAttachment::resize(vk::Extent2D const& resolution)
    {
        m_image->resize(resolution);
    }

    VulkanImage const& VulkanImageAttachment::getImage() const
    {
        return *m_image;
    }

    VulkanImage& VulkanImageAttachment::getImage()
    {
        return *m_image;
    }

    std::shared_ptr<VulkanBarrier> VulkanImageAttachment::generateBarrier(vk::ImageLayout oldLayout, vk::ImageLayout targetLayout, vk::AccessFlags2 srcAccessFlags, vk::PipelineStageFlagBits2 srcStage, vk::AccessFlags2 dstAccessFlags, vk::PipelineStageFlagBits2 dstStage)
    {
        return m_image->generateBarrier(oldLayout, targetLayout, srcAccessFlags, srcStage, dstAccessFlags, dstStage);
    }
}