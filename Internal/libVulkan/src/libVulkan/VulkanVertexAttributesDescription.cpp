//! libVulkan
#include <libVulkan/VulkanVertexAttributesDescription.hpp>
#include <libVulkan/VulkanBuffer.hpp>
//! libUtils
#include <libUtils/Utils.hpp>

namespace libVulkan
{
	VulkanVertexAttributesDescription::VulkanVertexAttributesDescription(std::vector<vk::VertexInputAttributeDescription>const& attributes, std::vector<vk::VertexInputBindingDescription>const& bindings)
		: m_attribDescs(attributes)
		, m_bindings(bindings)
	{
	}

	std::vector<vk::VertexInputAttributeDescription> const& VulkanVertexAttributesDescription::getDescriptions() const
	{
		return m_attribDescs;
	}

	std::vector<vk::VertexInputBindingDescription> const& VulkanVertexAttributesDescription::getBindings() const
	{
		return m_bindings;
	}
}