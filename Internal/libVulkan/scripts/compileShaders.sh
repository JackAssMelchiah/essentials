#!/bin/bash
# 2 args 1 for input folder, 2 for output folder, nonrecursive
if (( $# != 2)); then
	echo "Bad arg inputs"
	return 1
fi

FILES=$1"/*"
OUTPUT=$2"/"
for f in $FILES
do
	echo "$f"
	glslangValidator.exe -V $f -o $OUTPUT$f.spv
done