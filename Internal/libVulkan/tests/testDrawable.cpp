#include "testDrawable.hpp"
#include <libVulkan/VulkanAll.hpp>


namespace testDrawable 
{
	//Drawable::Drawable(std::weak_ptr<libVulkan::VulkanInstance> instance, std::weak_ptr<libVulkan::VulkanDevice> device, uint32_t vertexCount, glm::ivec2 imageDimms)
	//	:m_device(device)
	//{
	//	// should work like this
	//	// should anyone spawn class directly via this constructor - each and every resource such as VBO and texture is not shared!
	//	// also the command buffer is not shared as well - this means suboptimal performance, but for quick runs of prototyping fun
	//	// Note that this is the only constructor publicly available
	//	// There is also another constructor which is protected, and that uses some manager which directly injects resource memory into drawable
	//	// meaning shared resources, faster access, however memory is not owned in this case by the drawable, but by the manager itself.

	//	//therefore, initialize whole vulkan pipeline for one model .. ouch
	//	auto lockedDevice = device.lock();
	//	//in the future, instance should be nescesarry for vbo and image creations... leaving it there for now if something woluld be needed 
	//	m_buffer = lockedDevice->createVertexBuffer(vertexCount);
	//	m_image = lockedDevice->createImage(imageDimms);
	//	//command pool needs to be created and stored, so commandBuffer can be retrieved
	//	m_commandPool = lockedDevice->createCommandPool(lockedDevice->getQueue(vk::QueueFlagBits::eGraphics));
	//	m_commandBuffer = lockedDevice->createCommandBuffer(m_commandPool, vk::CommandBufferLevel::ePrimary);
	//}

	//Drawable::Drawable(std::weak_ptr<libVulkan::VulkanDevice> device, std::weak_ptr<libVulkan::VulkanBuffer>& buffer, std::weak_ptr<libVulkan::VulkanImage>& image, std::weak_ptr<libVulkan::VulkanCommandBuffer>& commandBuffer)
	//	:m_device(device)
	//	,m_buffer(buffer)
	//	,m_image(image)
	//	,m_commandBuffer(commandBuffer)
	//{
	//}
}