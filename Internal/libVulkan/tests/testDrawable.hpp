#pragma once
#include <memory>
#include <glm/glm.hpp>

namespace libVulkan
{
	class VulkanBuffer;
	class VulkanImage;
	class VulkanCommandBuffer;
	class VulkanCommandPool;
	class VulkanDevice;
	class VulkanInstance;
}

//! Drawable class - basic drawable using libVulkan, for now prob for debug purposes
namespace testDrawable
{
	class Drawable
	{
	public:
		//! Default constructor which does not give buffer and image - thus drawable's buffer wont be managed by any manager (memory-wise), wil own
		Drawable(std::weak_ptr<libVulkan::VulkanInstance> instance, std::weak_ptr<libVulkan::VulkanDevice> device, uint32_t vertexCount, glm::ivec2 imageDimms);
		~Drawable() = default;

	protected:
		//! Constructor which implies buffer is shared among multiple instances of drawables, so buffer is given... remember to querry later this class of size, so it can be given area of buffer where to copy to!
		Drawable(std::weak_ptr<libVulkan::VulkanDevice> device, std::weak_ptr<libVulkan::VulkanBuffer>& buffer, std::weak_ptr<libVulkan::VulkanImage>& image, std::weak_ptr<libVulkan::VulkanCommandBuffer>& commandBuffer);

	protected:
		//! Device - that needs to be allways everywhere probablly
		std::weak_ptr<libVulkan::VulkanDevice> m_device;
		//! VBO ... owned by drawable - in the future might be shared via some manager among multiple drawables with same vertex format, and therefore this might be weakptr later
		std::shared_ptr<libVulkan::VulkanBuffer> m_buffer;
		//! Image for texture... owned by this
		std::shared_ptr<libVulkan::VulkanImage> m_image;
		//! Command pool - generaly not even available, only when using this as nonShared
		std::shared_ptr<libVulkan::VulkanCommandPool> m_commandPool;
		//! Command buffer - not owned by this class, rather given by some manager - since one command buffer is used by multiple instances if drawables...
		std::weak_ptr<libVulkan::VulkanCommandBuffer> m_commandBuffer;
	};
}