////! gtest
//#include <gtest/gtest.h>
////! libVulkan
//#include <libVulkan/VulkanAll.hpp>
////! libUtils
//#include <libUtils/Utils.hpp>
//
////! test header - classes && defs -- according to this many stuff should be implemented into libCore
//#include "testDrawable.hpp"
//
////! Cretes minimal example of rendering with fixed triangle within shaders 
//TEST(LibVulkanTestSuite, FixedTriangle)
//{
//	auto settings = std::make_shared<libVulkan::VulkanDefaults>();
//	{
//		settings->appName = "FixedTriangle Test";
//		settings->appVersion = { 0, 0, 1 };
//		settings->extensions = { VK_EXT_DEBUG_UTILS_EXTENSION_NAME }; //wont be spawning windows so swapchain is not needed
//	}
//	//creates vulkan manager - creates Vulkan instance
//	auto vulkanManager = std::make_unique<libVulkan::VulkanManager>(settings);
//	EXPECT_TRUE(vulkanManager->createInstance());
//	EXPECT_TRUE(vulkanManager->createDevice());
//
//	//create device
//	auto& device = vulkanManager->getDevice();
//
//	//retrieve que from device
//	auto queue = device.getQueue(settings->functionality.front()).lock();
//
//	//load shaders
//	auto fragSVP = std::vector<uint32_t>();
//	auto vertSVP = std::vector<uint32_t>();
//
//	EXPECT_TRUE(libUtils::readFile(std::string{ RESOURCES_PATH } + "shaders/fixedTriangle.vert.spv", vertSVP));
//	EXPECT_TRUE(libUtils::readFile(std::string{ RESOURCES_PATH } + "shaders/fixedTriangle.frag.spv", fragSVP));
//	auto shaders = device.createShader(vertSVP, fragSVP);
//	EXPECT_TRUE(shaders != nullptr);
//
//	//layout description
//	auto layoutDescription = device.createPipelineLayout({}, {});
//	EXPECT_TRUE(layoutDescription != nullptr);
//
//	//render pass
//	auto renderPass = device.createRenderPass();
//	EXPECT_TRUE(renderPass != nullptr);
//
//	//image for color attachment
//	auto colorAttachment = device.createImage(settings->frameBufferResolution, settings->defaultSurfaceFormat, vk::ImageAspectFlagBits::eColor, vk::ImageUsageFlagBits::eColorAttachment, vk::MemoryPropertyFlagBits::eDeviceLocal);
//	EXPECT_TRUE(colorAttachment != nullptr);
//
//	auto depthAttachment = device.createImage(settings->frameBufferResolution, settings->depthFormat, vk::ImageAspectFlagBits::eDepth, vk::ImageUsageFlagBits::eDepthStencilAttachment, vk::MemoryPropertyFlagBits::eDeviceLocal);
//	EXPECT_TRUE(depthAttachment != nullptr);
//
//	//framebuffer
//	auto framebuffer = device.createFramebuffer(*renderPass, *colorAttachment, *depthAttachment);
//
//	//create pipeline
//	auto pipeline = device.createPipeline(shaders, renderPass, layoutDescription);
//	EXPECT_TRUE(pipeline != nullptr);
//
//	//Command pool
//	auto commandPool = device.createCommandPool(*queue);
//	EXPECT_TRUE(commandPool != nullptr);
//
//	//Command buffer
//	auto& commandBuffer = *commandPool->createCommandBuffer(vk::CommandBufferLevel::ePrimary).lock();
//
//	commandBuffer.reset(false);
//	commandBuffer.beginRecord({});
//	commandBuffer.bindPipeline(*pipeline);
//	commandBuffer.beginRenderPass(*renderPass, *framebuffer, vk::Extent2D{ uint32_t(settings->frameBufferResolution.x), uint32_t(settings->frameBufferResolution.y) });
//	commandBuffer.draw(3, 1, 0, 0);
//	commandBuffer.endRenderPasss();
//	commandBuffer.endRecord();
//
//	queue->submit({ commandBuffer });
//
//	//destroy stuff - pointer will reset so it should be automatic
//	device.waitIdle();
//}
//
////! Drawable, a sample model - like class test 
////TEST(LibVulkanTestSuite, Drawable)
////{
////	auto managerInfo = libVulkan::VulkanManager::ManagerInfo(); {
////		managerInfo.appName = "libVulkanTest";
////		managerInfo.appVersion = { 0, 0, 1 };
////		managerInfo.extensions = {};
////		managerInfo.validationLayers = { "VK_LAYER_KHRONOS_validation" };
////		managerInfo.windowless = true;
////	};
////
////	//initializes vulkan 
////	auto vulkanManager = std::make_shared<libVulkan::VulkanManager>(managerInfo);
////	
////	//create instance
////	auto instance = vulkanManager->createInstance();
////	EXPECT_TRUE(instance != nullptr);
////	
////	//wanted functionality
////	auto functionality = vk::QueueFlagBits::eGraphics;
////
////	//create device
////	auto device = instance->createDevice({ functionality });
////	EXPECT_TRUE(device != nullptr);
////
////	auto vetices = std::vector<glm::vec3>{ {0.f, 0.f, 0.f}, {0.5f, 1.0f, 0.f}, {1.f, 0.f, 0.f} };
////	auto imageSize = glm::ivec2{ 800, 600 };
////
////	auto drawable = std::make_shared<testDrawable::Drawable>(instance, device, vetices.size(), imageSize);
////}