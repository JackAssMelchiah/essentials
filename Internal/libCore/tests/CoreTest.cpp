//! googletest header file
#include <gtest/gtest.h>
//! libVukan
#include <libVulkan/VulkanAll.hpp>
//! libCore
#include <libCore/Core.hpp>
#include <libCore/ShaderResources.hpp>
#include <libCore/Renderer.hpp>
#include <libCore/UniformBuffer.hpp>
//! libUtils
#include <libUtils/Utils.hpp>
//! glm
#define GLM_FORCE_DEPTH_ZERO_TO_ONE
#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>
#include <glm/gtx/quaternion.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include <map>

class ShaderResourceTestSuite: public testing::Test
{
protected:
	//! Override setup method, so code can run once per all tests within this suite
	void SetUp() override
	{
		Test::SetUp();

		// initialize gleam
		m_gleam = std::make_shared<libCore::Gleam>();
		auto defaults = std::make_shared<libVulkan::VulkanDefaults>();
		m_gleam->initialize(true, defaults);

		auto instance = GetInstance().lock();
		auto device = GetDevice().lock();

		//create shader resources
		m_shaderResources = std::make_shared<libCore::ShaderResources>(instance, device);

		//create renderer
		m_renderer = std::make_shared<libCore::Renderer>(instance, device, GetQue().lock());

		//create descriptor set 
		auto slb = std::vector<vk::DescriptorSetLayoutBinding>{ {uint32_t(0), vk::DescriptorType::eStorageBuffer, uint32_t(1), vk::ShaderStageFlagBits::eVertex, nullptr } };
		auto dsl = std::make_shared<libVulkan::VulkanDescriptorSetLayout>(instance, device, slb, "");
		m_descriptorSet = m_renderer->createDescriptorSet(dsl, libCore::Renderer::ResourceFrequency::Object, "").lock();
	}

	void TearDown() override
	{
		m_shaderResources.reset();
		m_descriptorSet.reset();
		m_renderer.reset();
		m_gleam.reset();
		Test::TearDown();
	}

	std::weak_ptr<libVulkan::VulkanInstance> GetInstance()
	{
		return m_gleam->getVulkanManager().getInstance();
	}

	std::weak_ptr<libVulkan::VulkanDevice> GetDevice()
	{
		return m_gleam->getVulkanManager().getDevice();
	}

	std::weak_ptr<libVulkan::VulkanQueue> GetQue()
	{
		return m_gleam->getVulkanManager().getQueues().lock()->getQueue(vk::QueueFlagBits::eGraphics);
	}

	libCore::ShaderResources& getResources()
	{
		return *m_shaderResources;
	}

	std::weak_ptr<libVulkan::VulkanDescriptorSet> getDescriptorSet()
	{
		return m_descriptorSet;
	}

protected:
	//! Shared member among all ShaderResourceTests
	std::shared_ptr<libCore::Gleam> m_gleam;
	std::shared_ptr<libCore::Renderer> m_renderer;
	std::weak_ptr<libVulkan::VulkanDescriptorSet> m_descriptorSet;
	std::shared_ptr<libCore::ShaderResources> m_shaderResources;
};

TEST_F(ShaderResourceTestSuite, InstanceWriting)
{

	//defines data per instance, stored as uniform buffer
	struct UniformData
	{
		std::pair<std::string, glm::mat4> matVal;
		std::pair<std::string, int> intVal;
		std::pair<std::string, glm::vec3> vecVal;
		std::pair<std::string, float> floatVal;

		UniformData(glm::mat4 const& matV, int intV, glm::vec3 const& vecV, float floatV )
			: matVal(std::make_pair("MAT", matV))
			, intVal(std::make_pair("INT", intV))
			, vecVal(std::make_pair("VEC", vecV))
			, floatVal(std::make_pair("FLOAT", floatV))
		{}
	};

	auto instanceCount = 5;
	//Fill up input data
	auto dataInput = std::vector<UniformData>();
	for (auto i = 0; i <instanceCount ; ++i)
	{
		auto instanceIndex = 10 * i;
		auto mat = glm::mat4(
			instanceIndex + 0, instanceIndex + 1, instanceIndex + 2, instanceIndex + 3,
			instanceIndex + 4, instanceIndex + 5, instanceIndex + 6, instanceIndex + 7,
			instanceIndex + 8, instanceIndex + 9, instanceIndex + 10, instanceIndex + 11,
			instanceIndex + 12, instanceIndex + 13, instanceIndex + 14, instanceIndex + 15
		);

		auto intV = int(instanceIndex);
		auto vecVal = glm::vec3(instanceIndex + 0, instanceIndex + 1, instanceIndex + 2);
		auto floatVal = float(instanceIndex);
		dataInput.push_back({ mat, intV, vecVal, floatVal });
	}
    
	//fill shader resources with instance data
	auto& resources = getResources();
	
	for (auto i = 0; i < instanceCount; ++i)
	{
		resources.setInstanceResource(dataInput.at(i).matVal.first, dataInput.at(i).matVal.second, libCore::ShaderResources::Instance{ i });
		resources.setInstanceResource(dataInput.at(i).intVal.first, dataInput.at(i).intVal.second, libCore::ShaderResources::Instance{ i });
		resources.setInstanceResource(dataInput.at(i).vecVal.first, dataInput.at(i).vecVal.second, libCore::ShaderResources::Instance{ i });
		resources.setInstanceResource(dataInput.at(i).floatVal.first, dataInput.at(i).floatVal.second, libCore::ShaderResources::Instance{ i });
	}
	//check if all data are set correctly
	for (auto i = 0; i < instanceCount; ++i)
	{
		auto mVal = resources.getInstanceResource<glm::mat4>(dataInput.at(i).matVal.first, libCore::ShaderResources::Instance{ i });
		EXPECT_EQ(mVal, dataInput.at(i).matVal.second);
		auto iVal = resources.getInstanceResource<int>(dataInput.at(i).intVal.first, libCore::ShaderResources::Instance{ i });
		EXPECT_EQ(iVal, dataInput.at(i).intVal.second);
		auto vVal = resources.getInstanceResource<glm::vec3>(dataInput.at(i).vecVal.first, libCore::ShaderResources::Instance{ i });
		EXPECT_EQ(vVal, dataInput.at(i).vecVal.second);
		auto fVal = resources.getInstanceResource<float>(dataInput.at(i).floatVal.first, libCore::ShaderResources::Instance{ i });
		EXPECT_EQ(fVal, dataInput.at(i).floatVal.second);
	}

	//create buffer from all instances, and verify, that data are still correctly there - instances will be interleaved one by one in order
	auto bufferName = std::string("BUFFER");

	//validate that an buffer is not there
	EXPECT_FALSE(resources.bufferExists(bufferName));

	auto resourceVariables = std::vector<std::string>{ "MAT", "INT", "VEC", "FLOAT" };
	//interleaves all instances of specified variables into given buffer
	resources.bufferResources(resourceVariables, bufferName);
	//now buffer should exists
	EXPECT_TRUE(resources.bufferExists(bufferName));
	
	//validate, that resources are correctly written
	auto& resourcePack = resources.m_resourcePacks.at({ bufferName });

	//resource pack needs to use variables in order as specified in bufferResources	
	for (auto i = 0; i < resourceVariables.size(); ++i)
	{		
		EXPECT_EQ(resourceVariables.at(i), resourcePack.resources.at(i));
	}

	//check that the data are correctly packed - M1I1V1F1M2I2V2F2...
	auto offset = uint32_t(0);
	for (auto i = 0; i < instanceCount; ++i)
	{
		EXPECT_EQ(*(reinterpret_cast<glm::mat4*>(resourcePack.data.data() + offset)), dataInput.at(i).matVal.second);
		offset += sizeof(glm::mat4);
		EXPECT_EQ(*(reinterpret_cast<int*>(resourcePack.data.data() + offset)), dataInput.at(i).intVal.second);
		offset += sizeof(int);
		EXPECT_EQ(*(reinterpret_cast<glm::vec3*>(resourcePack.data.data() + offset)), dataInput.at(i).vecVal.second);
		offset += sizeof(glm::vec3);
		EXPECT_EQ(*(reinterpret_cast<float*>(resourcePack.data.data() + offset)), dataInput.at(i).floatVal.second);
		offset += sizeof(float);
	}

	//set a variable to be different at an instance
	auto instance = 1;
	dataInput[instance].intVal.second = 66;
	resources.setInstanceResource(dataInput.at(instance).intVal.first, dataInput.at(instance).intVal.second, libCore::ShaderResources::Instance{ instance });
	//check if all data are set correctly
	for (auto i = 0; i < instanceCount; ++i)
	{
		auto mVal = resources.getInstanceResource<glm::mat4>(dataInput.at(i).matVal.first, libCore::ShaderResources::Instance{ i });
		EXPECT_EQ(mVal, dataInput.at(i).matVal.second);
		auto iVal = resources.getInstanceResource<int>(dataInput.at(i).intVal.first, libCore::ShaderResources::Instance{ i });
		EXPECT_EQ(iVal, dataInput.at(i).intVal.second);
		auto vVal = resources.getInstanceResource<glm::vec3>(dataInput.at(i).vecVal.first, libCore::ShaderResources::Instance{ i });
		EXPECT_EQ(vVal, dataInput.at(i).vecVal.second);
		auto fVal = resources.getInstanceResource<float>(dataInput.at(i).floatVal.first, libCore::ShaderResources::Instance{ i });
		EXPECT_EQ(fVal, dataInput.at(i).floatVal.second);
	}
	//check that the data are correctly packed - M1I1V1F1M2I2V2F2...
	offset = uint32_t(0);
	for (auto i = 0; i < instanceCount; ++i)
	{
		EXPECT_EQ(*(reinterpret_cast<glm::mat4*>(resourcePack.data.data() + offset)), dataInput.at(i).matVal.second);
		offset += sizeof(glm::mat4);
		EXPECT_EQ(*(reinterpret_cast<int*>(resourcePack.data.data() + offset)), dataInput.at(i).intVal.second);
		offset += sizeof(int);
		EXPECT_EQ(*(reinterpret_cast<glm::vec3*>(resourcePack.data.data() + offset)), dataInput.at(i).vecVal.second);
		offset += sizeof(glm::vec3);
		EXPECT_EQ(*(reinterpret_cast<float*>(resourcePack.data.data() + offset)), dataInput.at(i).floatVal.second);
		offset += sizeof(float);
	}


	//

	//try to delete instance

	//add an instance

	//set a variable to be different at an instance

	//clean-up all instances
}

TEST_F(ShaderResourceTestSuite, InstanceRemoval)
{
	//TODO - from max go to empty and back, each time check
}

//TEST_F(ShaderResourceTestSuite, InstanceInterleaving)
//{
//	//create buffer from all instances, and verify, that data are still correctly there - instances will be interleaved one by one in order
//	auto& resources = getResources();
//	auto descriptorSet = getDescriptorSet().lock();
//	auto bufferName = std::string("BUFFER");
//
//	//1st validate that an buffer is not there
//	EXPECT_FALSE(resources.bufferExists(bufferName));
//	resources.useBuffersWithSet({ bufferName }, *descriptorSet);
//	EXPECT_TRUE(resources.bufferExists(bufferName));
//	//note, that buffer cleanup happens automatically, when instance is added or removed - maybe could add explicit removal later
//
//
//}

////! Does a render into ColorAttachment - now that cannot be stored to HDD just yet
////! While some things as fences might not be needed since there is just a single render, for completion and reference they are left here
//TEST(GeneralUsageTestSuite, SimpleCube)
//{
//	//defines 
//	struct Uniforms
//	{
//		glm::mat4 model;
//		glm::mat4 view;
//		glm::mat4 projection;
//	};
//
//	// initialize gleam
//	auto gleam = std::make_shared<libCore::Gleam>();
//	auto defaults = std::make_shared<libVulkan::VulkanDefaults>();
//	EXPECT_TRUE(gleam->initialize(true, defaults));
//	auto& device = gleam->getVulkanManager()->getDevice();
//
//	//que - owned by hw device
//	auto que = device.getQueue(vk::QueueFlagBits::eGraphics);
//	//command pool - alocates resources for command buffers, and from command buffer can be allocated
//	auto commandPool = device.createCommandPool(*que.lock());
//	auto& commandBuffer = *(commandPool->createCommandBuffer(vk::CommandBufferLevel::ePrimary).lock());
//	//descriptor pool - same as command pool, but for descriptor resources, descriptor sets can be allocated from here 
//
//	//but before allocation, descriptor types must be specified - and how much of them will be taken from allocated descriptor pool
//	//as far as descriptors, example uses single uniform buffer and single texture buffer with 2 textures
//	auto uboPoolSize = vk::DescriptorPoolSize();
//	{
//		uboPoolSize.descriptorCount = 1;
//		uboPoolSize.type = vk::DescriptorType::eUniformBuffer;
//	}
//	auto tboPoolSize = vk::DescriptorPoolSize();
//	{
//		tboPoolSize.descriptorCount = 2;
//		tboPoolSize.type = vk::DescriptorType::eCombinedImageSampler;
//	}
//	//finally allocation of descriptor pool
//	auto descriptorPool = device.createDescriptorPool(std::vector<vk::DescriptorPoolSize>{ uboPoolSize, tboPoolSize });
//		
//	//rendering fence - to let cpu know that GPU is done with everything this frame
//	auto renderingFence = device.createFence();
//	//rendering semaphore - to let the gpu know that gpu is done with rendering this frame
//	auto renderingFinishedSemaphore = device.createSemaphore();
//
//	//parameter to draw function
//	auto vertexShaderInvocationCount = uint32_t(0);
//
//	//VBO - create cube and buffer data
//	auto vbo = std::shared_ptr<libCore::VertexBuffer>();
//	{
//		//create sample cube
//		auto vertices = std::vector<libUtils::Shape::Vertex>();
//		libUtils::Shape::cube(vertices);
//
//		//describe how many times vertex shader will be invoked
//		vertexShaderInvocationCount = vertices.size();
//
//		auto verticesBytes = vertices.size() * sizeof(libUtils::Shape::Vertex);
//		//create actual vertex buffer
//		vbo = std::make_shared<libCore::VertexBuffer>(device.getPtr(), commandPool, uint32_t(verticesBytes));
//		//buffer data
//		vbo->bufferData(vertices, 0);
//	}
//
//	//vertex attributes descriptions - describes to renderer how to interpret data that are within VBO on GPU
//	auto verticesDescription = std::shared_ptr<libVulkan::VulkanVertexAttributesDescription>();
//	{
//		//create vertex description
//		auto vertexAttribute = vk::VertexInputAttributeDescription();
//		{
//			vertexAttribute.binding = 0;
//			vertexAttribute.format = vk::Format::eR32G32B32Sfloat;
//			vertexAttribute.location = 0;
//			vertexAttribute.offset = 0;
//		}
//		auto colorAttribute = vk::VertexInputAttributeDescription();
//		{
//			colorAttribute.binding = 0;
//			colorAttribute.format = vk::Format::eR32G32B32Sfloat;
//			colorAttribute.location = 1;
//			colorAttribute.offset = offsetof(libUtils::Shape::Vertex, normal);
//		}
//		auto uvAttribute = vk::VertexInputAttributeDescription();
//		{
//			uvAttribute.binding = 0;
//			uvAttribute.format = vk::Format::eR32G32Sfloat;
//			uvAttribute.location = 2;
//			uvAttribute.offset = offsetof(libUtils::Shape::Vertex, uv);
//		}
//
//		// create binding for them
//		auto binding = vk::VertexInputBindingDescription();
//		{
//			binding.binding = 0;
//			binding.inputRate = vk::VertexInputRate::eVertex;
//			binding.stride = sizeof(libUtils::Shape::Vertex);
//		}
//		verticesDescription = device.createVertexAttributesDescription(std::vector<vk::VertexInputAttributeDescription>{ vertexAttribute, colorAttribute, uvAttribute }, std::vector<vk::VertexInputBindingDescription>{ binding });
//	}
//
//	//now for descriptors - in order for both UBO and TBO to be created, description of these descriptors must be created  
//	auto descriptorSetLayout = std::shared_ptr<libVulkan::VulkanDescriptorSetLayout>();
//	{
//		//Describe first binding to be 3 matrices within UBO, accesible in VertexShader
//		uint32_t bytes = 3 * sizeof(glm::mat4);
//		auto bindingUBO = vk::DescriptorSetLayoutBinding();
//		{
//			bindingUBO.binding = 0;
//			bindingUBO.descriptorCount = 1;
//			bindingUBO.descriptorType = vk::DescriptorType::eUniformBuffer;
//			bindingUBO.stageFlags = vk::ShaderStageFlagBits::eVertex;
//			bindingUBO.pImmutableSamplers = nullptr;
//		}
//		//Describe second binding to be texture which is gonna be used in fragment shader
//		auto bindingImage1 = vk::DescriptorSetLayoutBinding();
//		{
//			bindingImage1.binding = 1;
//			bindingImage1.descriptorCount = 1;
//			bindingImage1.descriptorType = vk::DescriptorType::eCombinedImageSampler;
//			bindingImage1.stageFlags = vk::ShaderStageFlagBits::eFragment;
//			bindingImage1.pImmutableSamplers = nullptr;
//		}
//		auto bindingImage2 = vk::DescriptorSetLayoutBinding();
//		{
//			bindingImage2.binding = 2;
//			bindingImage2.descriptorCount = 1;
//			bindingImage2.descriptorType = vk::DescriptorType::eCombinedImageSampler;
//			bindingImage2.stageFlags = vk::ShaderStageFlagBits::eFragment;
//			bindingImage2.pImmutableSamplers = nullptr;
//		}
//
//		//create set layout
//		descriptorSetLayout = device.createDescriptorSetLayout({ bindingUBO, bindingImage1, bindingImage2 });
//	}
//	//allocate single descriptor set from the pool - to handle descriptor data
//	auto descriptorSet = descriptorPool->createDescriptorSet(descriptorSetLayout);
//	//create uniform buffer
//	auto ubo = std::shared_ptr<libCore::UniformBuffer>();
//	{
//		// size will be of 3 matrices - model view and projection
//		uint32_t bytes = 3 * sizeof(glm::mat4);
//		ubo = std::make_shared<libCore::UniformBuffer>(device.getPtr(), descriptorSet, bytes);
//	}
//	//create texture buffer - and buffer those textures inmidiatelly
//	auto tbo = std::shared_ptr<libCore::TextureBuffer>();
//	{
//		auto imageDescriptions = std::vector<libCore::TextureBuffer::ImageInfo>(2);
//		auto imagesData = std::vector<std::vector<std::byte>>(2);
//
//		//generate image 1
//		auto darkColorImg1 = std::array<std::byte, 4>{std::byte(127), std::byte(127), std::byte(127), std::byte(255)};
//		auto lightColorImg1 = std::array<std::byte, 4>{std::byte(75), std::byte(0), std::byte(0), std::byte(255)};
//		libUtils::Shape::generateSampleImage(glm::ivec2(256, 256), glm::ivec2(8, 8), darkColorImg1, lightColorImg1, imagesData[0]);
//		imageDescriptions[0] = libCore::TextureBuffer::ImageInfo(glm::ivec2(256, 256), vk::Format::eR8G8B8A8Unorm, imagesData.at(0).size() * sizeof(std::byte), 1, true);
//
//		//generate image 2
//		auto darkColorImg2 = std::array<std::byte, 4>{std::byte(127), std::byte(127), std::byte(127), std::byte(255)};
//		auto lightColorImg2 = std::array<std::byte, 4>{std::byte(0), std::byte(0), std::byte(75), std::byte(255)};
//		libUtils::Shape::generateSampleImage(glm::ivec2(256, 256), glm::ivec2(8, 8), darkColorImg2, lightColorImg2, imagesData[1]);
//		imageDescriptions[1] = libCore::TextureBuffer::ImageInfo(glm::ivec2(256, 256), vk::Format::eR8G8B8A8Unorm, imagesData.at(1).size() * sizeof(std::byte), 2, true);
//
//		//create texture buffer
//		tbo = std::make_shared<libCore::TextureBuffer>(device.getPtr(), descriptorSet, commandPool, imageDescriptions);
//
//		// buffer data
//		for (uint32_t currentImageData = 0; currentImageData < imageDescriptions.size(); ++currentImageData)
//		{
//			tbo->bufferData(imagesData.at(currentImageData), currentImageData);
//		}
//	}
//	//create shaders
//	auto shaders = std::shared_ptr<libVulkan::VulkanShader>();
//	{
//		//! default shaders
//		std::vector<uint32_t> fSource;
//		std::vector<uint32_t> vSource;
//
//		libUtils::readFile(std::string{ RESOURCES_PATH } + "shaders/modelDebug2.vert.spv", vSource);
//		libUtils::readFile(std::string{ RESOURCES_PATH } + "shaders/modelDebug2.frag.spv", fSource);
//		shaders = device.createShader(vSource, fSource);
//	}
//
//	//create reder pass - default now goes with RGBA8888Unorm + output color and depth attachement so output attachments need to reflect that
//	auto renderPass = device.createRenderPass();
//	//pipeline layout
//	auto pipelineLayout = device.createPipelineLayout({ descriptorSetLayout }, {});
//	//create pipeline 
//	auto pipeline = device.createPipeline(shaders, renderPass, pipelineLayout, verticesDescription);
//	
//	//create output attachements for dept and colors
//	auto resolution = glm::ivec2(1280, 720);
//	auto colorAttachment = device.createImage(resolution, device.getDefaults().defaultSurfaceFormat, vk::ImageAspectFlagBits::eColor, vk::ImageUsageFlagBits::eColorAttachment, vk::MemoryPropertyFlagBits::eDeviceLocal);
//	auto depthAttachment = device.createImage(resolution, device.getDefaults().depthFormat, vk::ImageAspectFlagBits::eDepth, vk::ImageUsageFlagBits::eDepthStencilAttachment, vk::MemoryPropertyFlagBits::eDeviceLocal);
//
//	//create framebuffer - merges together memory backing for pipeline to use for output attachments 
//	auto framebuffer = device.createFramebuffer(*renderPass, *colorAttachment, *depthAttachment);
//
//	//rendering loop
//	
//	//cpu waits till the que has processed the rendering work, no need to wait for presentaion
//	renderingFence->wait();
//	
//	{//rendering is done, can update uniforms
//		auto aspectRatio = 16.f / 9.f;
//		auto view = glm::lookAt(glm::vec3(2.0f, 2.0f, 2.0f), glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, 0.0f, 1.0f));
//
//		auto q1 = glm::angleAxis(glm::radians(45.f), glm::vec3{ 1.f, 0.f, 0.f });
//		auto q2 = glm::angleAxis(glm::radians(45.f), glm::vec3{ 0.f, 0.f, 1.f });
//		auto q3 = glm::angleAxis(glm::radians(15.f), glm::vec3{ 0.f, 1.f, 0.f });
//
//		auto rotated = q1 * q2 * glm::inverse(q1);
//
//		auto modelMatrix = glm::translate(glm::identity<glm::mat4>(), glm::vec3{ 0.f, 0.f, 10.f }) * glm::toMat4(q3 * rotated * glm::inverse(q3));
//		auto viewMatrix = glm::lookAt(glm::vec3{ 0.f, 0.f, 0.f }, glm::vec3{ 0.f, 0.f, 1.f }, glm::vec3(0.f, 1.f, 0.f));
//		auto projectionMatrix = glm::perspective(75.f, aspectRatio, 0.1f, 1000.f);
//
//		auto uniforms = Uniforms{ modelMatrix, viewMatrix, projectionMatrix };
//
//		//buffers the matrices, and updates ubo descriptors
//		ubo->bufferData(std::vector<Uniforms>{ uniforms });
//		//tbo does not need to buffer images, but descriptors need update per frame
//		tbo->updateDescriptors();
//	}
//
//	//start recording of command buffer - bind the specified resources
//	commandBuffer.reset(false);
//	commandBuffer.beginRecord({});
//	commandBuffer.beginRenderPass(*renderPass, *framebuffer, vk::Extent2D{ uint32_t(resolution.x), uint32_t(resolution.y) });
//	commandBuffer.bindPipeline(*pipeline);
//	commandBuffer.bindVertexBuffers({ vbo->operator()() });
//	commandBuffer.bindDescriptorSets({ *descriptorSet.lock() }, *pipelineLayout);
//	commandBuffer.draw(vertexShaderInvocationCount, 1, 0, 0);
//	commandBuffer.endRenderPasss();
//	commandBuffer.endRecord();
//
//	// submit the recorded work to commandBuffer - notifies that it is done by rendering semaphore and fence;
//	que.lock()->submit({ commandBuffer }, { *renderingFinishedSemaphore }, {}, vk::PipelineStageFlagBits::eTopOfPipe, *renderingFence);
//	
//	//resources will be automatically deallocated - but need to wait for device to finish all things
//	device.waitIdle();
//}
//
////! Same like before, but this one renders frame onto window and then closes it
//TEST(GeneralUsageTestSuite, SimpleCubeWindow)
//{
//	//defines 
//	struct Uniforms
//	{
//		glm::mat4 model;
//		glm::mat4 view;
//		glm::mat4 projection;
//	};
//
//	// initialize gleam
//	auto gleam = std::make_shared<libCore::Gleam>();
//	auto defaults = std::make_shared<libVulkan::VulkanDefaults>();
//	EXPECT_TRUE(gleam->initialize(false, defaults));
//	auto& device = gleam->getVulkanManager()->getDevice();
//
//	//que - owned by hw device
//	auto que = device.getQueue(vk::QueueFlagBits::eGraphics);
//	//command pool - alocates resources for command buffers, and from command buffer can be allocated
//	auto commandPool = device.createCommandPool(*que.lock());
//	auto& commandBuffer = *(commandPool->createCommandBuffer(vk::CommandBufferLevel::ePrimary).lock());
//	//descriptor pool - same as command pool, but for descriptor resources, descriptor sets can be allocated from here 
//
//	//but before allocation, descriptor types must be specified - and how much of them will be taken from allocated descriptor pool
//	//as far as descriptors, example uses single uniform buffer and single texture buffer with 2 textures
//	auto uboPoolSize = vk::DescriptorPoolSize();
//	{
//		uboPoolSize.descriptorCount = 1;
//		uboPoolSize.type = vk::DescriptorType::eUniformBuffer;
//	}
//	auto tboPoolSize = vk::DescriptorPoolSize();
//	{
//		tboPoolSize.descriptorCount = 2;
//		tboPoolSize.type = vk::DescriptorType::eCombinedImageSampler;
//	}
//	//finally allocation of descriptor pool
//	auto descriptorPool = device.createDescriptorPool(std::vector<vk::DescriptorPoolSize>{ uboPoolSize, tboPoolSize });
//
//	//rendering fence - to let cpu know that GPU is done with everything this frame
//	auto renderingFence = device.createFence();
//	//rendering semaphore - to let the gpu know that gpu is done with rendering this frame
//	auto renderingFinishedSemaphore = device.createSemaphore();
//
//	//parameter to draw function
//	auto vertexShaderInvocationCount = uint32_t(0);
//
//	//VBO - create cube and buffer data
//	auto vbo = std::shared_ptr<libCore::VertexBuffer>();
//	{
//		//create sample cube
//		auto vertices = std::vector<libUtils::Shape::Vertex>();
//		libUtils::Shape::cube(vertices);
//
//		//describe how many times vertex shader will be invoked
//		vertexShaderInvocationCount = vertices.size();
//
//		auto verticesBytes = vertices.size() * sizeof(libUtils::Shape::Vertex);
//		//create actual vertex buffer
//		vbo = std::make_shared<libCore::VertexBuffer>(device.getPtr(), commandPool, uint32_t(verticesBytes));
//		//buffer data
//		vbo->bufferData(vertices, 0);
//	}
//
//	//vertex attributes descriptions - describes to renderer how to interpret data that are within VBO on GPU
//	auto verticesDescription = std::shared_ptr<libVulkan::VulkanVertexAttributesDescription>();
//	{
//		//create vertex description
//		auto vertexAttribute = vk::VertexInputAttributeDescription();
//		{
//			vertexAttribute.binding = 0;
//			vertexAttribute.format = vk::Format::eR32G32B32Sfloat;
//			vertexAttribute.location = 0;
//			vertexAttribute.offset = 0;
//		}
//		auto colorAttribute = vk::VertexInputAttributeDescription();
//		{
//			colorAttribute.binding = 0;
//			colorAttribute.format = vk::Format::eR32G32B32Sfloat;
//			colorAttribute.location = 1;
//			colorAttribute.offset = offsetof(libUtils::Shape::Vertex, normal);
//		}
//		auto uvAttribute = vk::VertexInputAttributeDescription();
//		{
//			uvAttribute.binding = 0;
//			uvAttribute.format = vk::Format::eR32G32Sfloat;
//			uvAttribute.location = 2;
//			uvAttribute.offset = offsetof(libUtils::Shape::Vertex, uv);
//		}
//
//		// create binding for them
//		auto binding = vk::VertexInputBindingDescription();
//		{
//			binding.binding = 0;
//			binding.inputRate = vk::VertexInputRate::eVertex;
//			binding.stride = sizeof(libUtils::Shape::Vertex);
//		}
//		verticesDescription = device.createVertexAttributesDescription(std::vector<vk::VertexInputAttributeDescription>{ vertexAttribute, colorAttribute, uvAttribute }, std::vector<vk::VertexInputBindingDescription>{ binding });
//	}
//
//	//now for descriptors - in order for both UBO and TBO to be created, description of these descriptors must be created  
//	auto descriptorSetLayout = std::shared_ptr<libVulkan::VulkanDescriptorSetLayout>();
//	{
//		//Describe first binding to be 3 matrices within UBO, accesible in VertexShader
//		uint32_t bytes = 3 * sizeof(glm::mat4);
//		auto bindingUBO = vk::DescriptorSetLayoutBinding();
//		{
//			bindingUBO.binding = 0;
//			bindingUBO.descriptorCount = 1;
//			bindingUBO.descriptorType = vk::DescriptorType::eUniformBuffer;
//			bindingUBO.stageFlags = vk::ShaderStageFlagBits::eVertex;
//			bindingUBO.pImmutableSamplers = nullptr;
//		}
//		//Describe second binding to be texture which is gonna be used in fragment shader
//		auto bindingImage1 = vk::DescriptorSetLayoutBinding();
//		{
//			bindingImage1.binding = 1;
//			bindingImage1.descriptorCount = 1;
//			bindingImage1.descriptorType = vk::DescriptorType::eCombinedImageSampler;
//			bindingImage1.stageFlags = vk::ShaderStageFlagBits::eFragment;
//			bindingImage1.pImmutableSamplers = nullptr;
//		}
//		auto bindingImage2 = vk::DescriptorSetLayoutBinding();
//		{
//			bindingImage2.binding = 2;
//			bindingImage2.descriptorCount = 1;
//			bindingImage2.descriptorType = vk::DescriptorType::eCombinedImageSampler;
//			bindingImage2.stageFlags = vk::ShaderStageFlagBits::eFragment;
//			bindingImage2.pImmutableSamplers = nullptr;
//		}
//
//		//create set layout
//		descriptorSetLayout = device.createDescriptorSetLayout({ bindingUBO, bindingImage1, bindingImage2 });
//	}
//	//allocate single descriptor set from the pool - to handle descriptor data
//	auto descriptorSet = descriptorPool->createDescriptorSet(descriptorSetLayout);
//	//create uniform buffer
//	auto ubo = std::shared_ptr<libCore::UniformBuffer>();
//	{
//		// size will be of 3 matrices - model view and projection
//		uint32_t bytes = 3 * sizeof(glm::mat4);
//		ubo = std::make_shared<libCore::UniformBuffer>(device.getPtr(), descriptorSet, bytes);
//	}
//	//create texture buffer - and buffer those textures inmidiatelly
//	auto tbo = std::shared_ptr<libCore::TextureBuffer>();
//	{
//		auto imageDescriptions = std::vector<libCore::TextureBuffer::ImageInfo>(2);
//		auto imagesData = std::vector<std::vector<std::byte>>(2);
//
//		//generate image 1
//		auto darkColorImg1 = std::array<std::byte, 4>{std::byte(127), std::byte(127), std::byte(127), std::byte(255)};
//		auto lightColorImg1 = std::array<std::byte, 4>{std::byte(75), std::byte(0), std::byte(0), std::byte(255)};
//		libUtils::Shape::generateSampleImage(glm::ivec2(256, 256), glm::ivec2(8, 8), darkColorImg1, lightColorImg1, imagesData[0]);
//		imageDescriptions[0] = libCore::TextureBuffer::ImageInfo(glm::ivec2(256, 256), vk::Format::eR8G8B8A8Unorm, imagesData.at(0).size() * sizeof(std::byte), 1, true);
//
//		//generate image 2
//		auto darkColorImg2 = std::array<std::byte, 4>{std::byte(127), std::byte(127), std::byte(127), std::byte(255)};
//		auto lightColorImg2 = std::array<std::byte, 4>{std::byte(0), std::byte(0), std::byte(75), std::byte(255)};
//		libUtils::Shape::generateSampleImage(glm::ivec2(256, 256), glm::ivec2(8, 8), darkColorImg2, lightColorImg2, imagesData[1]);
//		imageDescriptions[1] = libCore::TextureBuffer::ImageInfo(glm::ivec2(256, 256), vk::Format::eR8G8B8A8Unorm, imagesData.at(1).size() * sizeof(std::byte), 2, true);
//
//		//create texture buffer
//		tbo = std::make_shared<libCore::TextureBuffer>(device.getPtr(), descriptorSet, commandPool, imageDescriptions);
//
//		// buffer data
//		for (uint32_t currentImageData = 0; currentImageData < imageDescriptions.size(); ++currentImageData)
//		{
//			tbo->bufferData(imagesData.at(currentImageData), currentImageData);
//		}
//	}
//	//create shaders
//	auto shaders = std::shared_ptr<libVulkan::VulkanShader>();
//	{
//		//! default shaders
//		std::vector<uint32_t> fSource;
//		std::vector<uint32_t> vSource;
//
//		libUtils::readFile(std::string{ RESOURCES_PATH } + "shaders/modelDebug2.vert.spv", vSource);
//		libUtils::readFile(std::string{ RESOURCES_PATH } + "shaders/modelDebug2.frag.spv", fSource);
//		shaders = device.createShader(vSource, fSource);
//	}
//
//	//instead of building framebuffer, framebuffer of window is used
//	auto& window = gleam->getVulkanManager()->getWindow();
//
//	//create reder pass - window has all nescessary stuff for it inside
//	auto renderPass = device.createRenderPass(window.getSurface());
//	//pipeline layout
//	auto pipelineLayout = device.createPipelineLayout({ descriptorSetLayout }, {});
//	//create pipeline 
//	auto pipeline = device.createPipeline(shaders, renderPass, pipelineLayout, verticesDescription);
//
//
//	// pipeline must be asigned to window
//	//window.setPresentable(pipeline);
//
//
//	//rendering loop
//
//	//cpu waits till the que has processed the rendering work, no need to wait for presentaion
//	renderingFence->wait();
//
//	{//rendering is done, can update uniforms
//		auto aspectRatio = window.getExtent().width / (float)window.getExtent().height;
//		auto view = glm::lookAt(glm::vec3(2.0f, 2.0f, 2.0f), glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, 0.0f, 1.0f));
//
//		auto q1 = glm::angleAxis(glm::radians(45.f), glm::vec3{ 1.f, 0.f, 0.f });
//		auto q2 = glm::angleAxis(glm::radians(45.f), glm::vec3{ 0.f, 0.f, 1.f });
//		auto q3 = glm::angleAxis(glm::radians(15.f), glm::vec3{ 0.f, 1.f, 0.f });
//
//		auto rotated = q1 * q2 * glm::inverse(q1);
//
//		auto modelMatrix = glm::translate(glm::identity<glm::mat4>(), glm::vec3{ 0.f, 0.f, 10.f }) * glm::toMat4(q3 * rotated * glm::inverse(q3));
//		auto viewMatrix = glm::lookAt(glm::vec3{ 0.f, 0.f, 0.f }, glm::vec3{ 0.f, 0.f, 1.f }, glm::vec3(0.f, 1.f, 0.f));
//		auto projectionMatrix = glm::perspective(75.f, aspectRatio, 0.1f, 1000.f);
//
//		auto uniforms = Uniforms{ modelMatrix, viewMatrix, projectionMatrix };
//
//		//buffers the matrices, and updates ubo descriptors
//		ubo->bufferData(std::vector<Uniforms>{ uniforms });
//		//tbo does not need to buffer images, but descriptors need update per frame
//		tbo->updateDescriptors();
//	}
//
//	// get image swapchain semaphore - the renderer waits till semaphore image is available
//	auto& semaphoreImageAvailable = window.getImageAvailableSemaphore();
//
//	//start recording of command buffer - bind the specified resources
//	commandBuffer.reset(false);
//	commandBuffer.beginRecord({});
//	commandBuffer.beginRenderPass(*renderPass, window);
//	commandBuffer.bindPipeline(*pipeline);
//	commandBuffer.bindVertexBuffers({ vbo->operator()() });
//	commandBuffer.bindDescriptorSets({ *descriptorSet.lock() }, *pipelineLayout);
//	commandBuffer.draw(vertexShaderInvocationCount, 1, 0, 0);
//	commandBuffer.endRenderPasss();
//	commandBuffer.endRecord();
//
//	// submit the recorded work to commandBuffer - waits for image semaphore and later when done, notifies that it is done by rendering semaphore and fence;
//	que.lock()->submit({ commandBuffer }, { *renderingFinishedSemaphore }, { semaphoreImageAvailable }, vk::PipelineStageFlagBits::eTopOfPipe, *renderingFence);
//
//	// presentation waits on gpu to finish rendering first
//	que.lock()->present(window, { *renderingFinishedSemaphore });
//
//	//end render loop
//
//	//now since renrering is asynchronous, need to wait for it to be done
//	renderingFence->wait();
//	//but also, presentation is asynchronous as well so need to wait for it as well - normally not needed when in loop due to window semaphore handling synchronization
//	// and getting framebuffer from window will trigger explicit synchronization, so image gets painted
//	window.getCurrentFrameBuffer();
//
//	//resources will be automatically deallocated - but need to wait for device to finish all things
//	device.waitIdle();
//}
//
