#version 450
//! Notation used by libCore::Renderer
#define GLOBAL_RESOURCES 0
#define PASS_RESOURCES 1
#define MAT_RESOURCES 2
#define OBJECTS_RESOURCES 3

//! inputs
layout (std140, set = PASS_RESOURCES, binding = 0 ) buffer UniformBufferObjectCamera
{
    mat4 viewMatrix;
    mat4 projectionMatrix;
} cameraData;

layout (std430, set = OBJECTS_RESOURCES, binding = 0) buffer UniformBufferObjectPosition
{
    mat4 modelMatrix[];
} positionData;

//! vertex attrib inputs
layout(location = 0) in vec3 vertex;
layout(location = 1) in vec3 normal;
layout(location = 2) in vec2 uv;
layout(location = 3) in vec4 tangent;

//! outputs
layout(location = 0) out LightningData
{
    vec3 fragPosition;
    vec2 fragUv;
    mat3 TBN; 
} lightningData;

mat3 createTBNMatrix(vec3 normal, vec4 tangent);
void main();

void main()
 {
    vec4 vertexWorld = positionData.modelMatrix[gl_InstanceIndex] * vec4(vertex, 1.0);
    gl_Position = cameraData.projectionMatrix * cameraData.viewMatrix * vertexWorld;

    lightningData.fragPosition = vertexWorld.xyz;
    lightningData.fragUv = uv;
    lightningData.TBN = createTBNMatrix(normal, tangent);
}

mat3 createTBNMatrix(vec3 normal, vec4 tangent)
{
    //mat3 normalRotationMatrix = mat3(transpose(inverse(positionData.modelMatrix[gl_InstanceIndex])));
    vec3 vsNormal = normalize((positionData.modelMatrix[gl_InstanceIndex] * vec4(normal, 0.f)).xyz);
    vec3 vsTangent = normalize((positionData.modelMatrix[gl_InstanceIndex] * vec4(tangent.xyz, 0.f)).xyz);
    //compute bitangent - from normal and tangent - tangent xyz is normalized, tangent w indicates handness of the tanget basis
    vec3 vsBitangent = cross(vsNormal, vsTangent) * tangent.w;
    //create conversion matrix from view to word
    return mat3(vsTangent, vsBitangent, vsNormal);
}
