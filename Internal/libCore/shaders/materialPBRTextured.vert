#version 450

//! Notation used by libCore::Renderer
#define GLOBAL_RESOURCES 0
#define PASS_RESOURCES 1
#define MAT_RESOURCES 2
#define OBJECTS_RESOURCES 3

//! inputs
layout (std140, set = PASS_RESOURCES, binding = 0 ) buffer UniformBufferObjectCamera
{
    mat4 viewMatrix;
    mat4 projectionMatrix;
} cameraData;

struct InstanceData
{
    mat4 modelMatrix;
};

layout (std430, set = OBJECTS_RESOURCES, binding = 0) buffer UniformBufferObjectPosition
{
    InstanceData instanceData[];
} objectData;

//! vertex attrib inputs
layout(location = 0) in vec3 vertex;
layout(location = 1) in vec3 normal;
layout(location = 2) in vec2 uv;
layout(location = 3) in vec4 tangent;

//! outputs
layout(location = 0) out flat int ID;
layout(location = 1) out LightningData
{
    vec3 fragPosition;
    vec2 fragUv;
    mat3 TBN; 
} lightningData;

//! Fw decls
mat3 createTBNMatrix(mat4 modelMatrix, vec3 normal, vec4 tangent);
void main();

void main()
 {
    mat4 modelMatrix = objectData.instanceData[gl_InstanceIndex].modelMatrix;
    vec4 vertexWorld = modelMatrix * vec4(vertex, 1.f);
    
    lightningData.fragPosition = vertexWorld.xyz;
    lightningData.fragUv = uv;
    lightningData.TBN = createTBNMatrix(modelMatrix, normal, tangent);
    
    ID = gl_InstanceIndex;

    gl_Position = cameraData.projectionMatrix * cameraData.viewMatrix * vertexWorld;
}

mat3 createTBNMatrix(mat4 modelMatrix, vec3 normal, vec4 tangent)
{
    vec3 vsNormal = normalize((modelMatrix * vec4(normal, 0.f)).xyz);
    vec3 vsTangent = normalize((modelMatrix * vec4(tangent.xyz, 0.f)).xyz);
    //compute bitangent - from normal and tangent - tangent xyz is normalized, tangent w indicates handness of the tanget basis
    vec3 vsBitangent = cross(vsNormal, vsTangent) * tangent.w;
    //create conversion matrix from view to word
    return mat3(vsTangent, vsBitangent, vsNormal);
}