#version 450
#extension GL_EXT_debug_printf : enable

//! Notation used by libCore::Renderer
#define GLOBAL_RESOURCES 0
#define PASS_RESOURCES 1
#define MAT_RESOURCES 2
#define OBJECTS_RESOURCES 3

//! inputs
layout (std140, set = GLOBAL_RESOURCES, binding = 0) buffer UniformBufferObjectGlobal
{
    double time;
    double frameTime;
} globalData;

layout (std140, set = MAT_RESOURCES, binding = 0) buffer UniformBufferObjectPhong
{
    vec4 cameraPosition; // only vec3 is relevant
    vec4 lightPosition;  // only vec3 is relevant
    vec4 lightColor;
} pbrData;

layout(set = OBJECTS_RESOURCES, binding = 1) uniform sampler2D textureColor;
layout(set = OBJECTS_RESOURCES, binding = 2) uniform sampler2D textureNormal;
layout(set = OBJECTS_RESOURCES, binding = 3) uniform sampler2D textureOcclusion;


//! locations inputs
layout(location = 0) in flat int ID;
layout(location = 1) in LightningData
{
    vec3 fragPosition;
    vec2 fragUv;
    mat3 TBN;
} lightningData;

//! location outputs
layout(location = 0) out vec4 outColor;

//! fw decls
void main();
vec3 normalMapping(vec2 uv, sampler2D normalTextureSampler, mat3 TBN);
vec3 toneMap(vec3 color);
vec3 specular(float NdotV, float NdotL, float NdotH, vec3 fresnel, float Rs);
float ggx(float NdotH, float Rs);
float geometrySchlickGGX(float NdotV, float Rs);
float geometrySmith(float NdotV, float NdotL, float Rs);
vec3 fresnelSchlick(float cosTheta, vec3 F0);

#define PI 3.141592653

//! functions
void main()
{
    //uvs
    vec2 uvs = lightningData.fragUv;
    //Per-vertex Data
    vec3 Ac = texture(textureColor, uvs).rgb;
    vec3 parameterSample = texture(textureOcclusion, uvs).rgb;
    float Ao = parameterSample.r;
    float Ms = parameterSample.g;
    float Rs = parameterSample.b;
    
    //light color
    vec3 Lc = pbrData.lightColor.xyz;
    // normal vector
        vec3 Nv = normalMapping(uvs, textureNormal, lightningData.TBN);
    // view direction vector
    vec3 Vv = normalize(pbrData.cameraPosition.xyz - lightningData.fragPosition.xyz);
    float cosThetaV = max(dot(Nv, Vv), 0.f);

    //FO 
    vec3 F0 = vec3(0.04);
    F0 = mix(F0, Ac, Ms);

    int lightCount = 1;
    int lightIndex = 0;
    vec3 LightSum = vec3(0.f);
    for(int i = 0; i < lightCount; ++i) 
    {
        //light direction vector
        vec3 Lv = normalize(pbrData.lightPosition.xyz - lightningData.fragPosition.xyz);
        //half-way vector between view and light vectors
        vec3 Hv = normalize(Vv + Lv);
        float cosThetaL = max(dot(Nv, Lv), 0.f);
        float cosThetaH = max(dot(Nv, Hv), 0.f);

        vec3 fresnel = fresnelSchlick(cosThetaV, F0);

        vec3 kS = fresnel;
        vec3 kD = vec3(1.0) - kS;
        kD *= 1.0 - Ms;	

        LightSum += (kD * Ac/PI + specular(cosThetaV, cosThetaL, cosThetaH, fresnel, Rs)) * cosThetaL;
        lightIndex++;
    }

    vec3 ambient = vec3(0.03) * Ac * Ao;
    vec3 color = ambient + LightSum;
    outColor = vec4(toneMap(color), 1.f);
}

vec3 normalMapping(vec2 uv, sampler2D normalTextureSampler, mat3 TBN)
{
    // map them from -1,1 to 0,1
    vec3 normal = texture(normalTextureSampler, uv).rgb;
    normal = normalize(normal * 2.0 - 1.0);
    return normalize(TBN * normal);
}

vec3 toneMap(vec3 color)
{
    vec3 val = color / (color + vec3(1.f));
    return pow(val, vec3(1.0 / 2.2)); 
}

// specular function
vec3 specular(float NdotV, float NdotL, float NdotH, vec3 fresnel, float Rs)
{
    vec3 DFG = ggx(NdotH, Rs) * geometrySmith(NdotV, NdotL, Rs) * fresnel;
    float scaledByView = NdotV;
    float scaledByLight = NdotL;
    return DFG / (4* scaledByLight * scaledByView + 0.00001); // 0.00001 prevents division by 0
}

//normal distribution function
float ggx(float NdotH, float Rs)
{//alignment of microfacets to the hald way vector - main visual effect of roughness/specular
    float Rs2 = Rs * Rs;
    float NdotH2 = NdotH*NdotH;
	
    float nom = Rs2;
    float denom = NdotH2 * (Rs2 - 1.0) + 1.0;
    denom  = PI * denom * denom;
	
    return nom / denom;
}

//geometry function
float geometrySchlickGGX(float NdotV, float Rs)
{	
    float k = ((1 + Rs) * (1 + Rs) / 8.); //kdir
    return NdotV / (NdotV * (1.0 - k) + k);
}
  
float geometrySmith(float NdotV, float NdotL, float Rs)
{//microfacet shadowing - 1.  none, 0 full, very tame effect
    float ggx1 = geometrySchlickGGX(NdotV, Rs); //from camera source
    float ggx2 = geometrySchlickGGX(NdotL, Rs); //from light source 
    return ggx1 * ggx2; //combine them
}

//describes ratio of light that gets reflected over the light refracted
vec3 fresnelSchlick(float cosTheta, vec3 F0)
{
    return F0 + (1.0 - F0) * pow(clamp(1.0 - cosTheta, 0.0, 1.0), 5.0);
}