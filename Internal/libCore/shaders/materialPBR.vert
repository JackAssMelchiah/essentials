#version 450

//! Notation used by libCore::Renderer
#define GLOBAL_RESOURCES 0
#define PASS_RESOURCES 1
#define MAT_RESOURCES 2
#define OBJECTS_RESOURCES 3

//! inputs
layout (std140, set = PASS_RESOURCES, binding = 0 ) buffer UniformBufferObjectCamera
{
    mat4 viewMatrix;
    mat4 projectionMatrix;
} cameraData;

struct InstanceData
{
    mat4 modelMatrix;
    vec4 albedoColor;
    vec4 aoMetRough;
};

layout (std430, set = OBJECTS_RESOURCES, binding = 0) buffer UniformBufferObjectPosition
{
    InstanceData instanceData[];
} objectData;

//! vertex attrib inputs
layout(location = 0) in vec3 vertex;
layout(location = 1) in vec3 normal;

//! outputs
layout(location = 0) out LightningData
{
    vec3 fragPosition;
    vec3 fragNormal;
} lightningData;
layout(location = 2) out flat int ID;

//! Fw decls
void main();

void main()
 {
    mat4 Mm = objectData.instanceData[gl_InstanceIndex].modelMatrix;
    vec4 vertexWorld = Mm * vec4(vertex, 1.0);
    
    lightningData.fragPosition = vertexWorld.xyz;
    lightningData.fragNormal = mat3(transpose(inverse(Mm))) * normal; //rotate the normal as well - by the "normal matrix" which is used to correctly just rotate normals
    ID = gl_InstanceIndex;

    gl_Position = cameraData.projectionMatrix * cameraData.viewMatrix * vertexWorld;
}