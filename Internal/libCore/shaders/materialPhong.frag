#version 450
//! Notation used by libCore::Renderer
#define GLOBAL_RESOURCES 0
#define PASS_RESOURCES 1
#define MAT_RESOURCES 2
#define OBJECTS_RESOURCES 3

//! inputs
layout (std140, set = GLOBAL_RESOURCES, binding = 0) buffer UniformBufferObjectGlobal
{
    double time;
    double frameTime;
} globalData;

layout (std140, set = MAT_RESOURCES, binding = 0) buffer UniformBufferObjectPhong
{
    vec4 cameraPosition;
    vec4 lightPosition;
} phongData;

layout(set = OBJECTS_RESOURCES, binding = 1) uniform sampler2D textureColor;

//! locations inputs
layout(location = 0) in vec3 fragPosition;
layout(location = 1) in vec3 fragNormal;
layout(location = 2) in vec2 uvs;
//! location outputs
layout(location = 0) out vec4 outColor;

//! fw decls
vec4 getPhong(vec3 fragPosition, vec3 fragNormal, vec2 uv, vec3 cameraPosition, vec3 lightPosition);
float getAmbientAmount();
float getDifuseAmount(vec3 fragPosition, vec3 fragNormal, vec3 directionToLight);
float getSpecularAmount(vec3 fragPosition, vec3 fragNormal, vec3 cameraPosition, vec3 directionToLight);

//! functions
void main()
{
    outColor = getPhong(fragPosition, fragNormal, uvs, phongData.cameraPosition.xyz, phongData.lightPosition.xyz);
}

vec4 getPhong(vec3 fragPosition, vec3 fragNormal, vec2 uv, vec3 cameraPosition, vec3 lightPosition)
{
    vec3 sampledColor = texture(textureColor, uv).rgb;
    vec3 directionToLight = normalize(lightPosition - fragPosition);

    float ambient = getAmbientAmount();
    float diffuse = getDifuseAmount(fragPosition, fragNormal, directionToLight);
    float specular = getSpecularAmount(fragPosition, fragNormal, cameraPosition, directionToLight);
 
    return vec4((ambient + diffuse + specular) * sampledColor, 1.f);
}

float getAmbientAmount()
{
    return 0.1;
}

float getDifuseAmount(vec3 fragPosition, vec3 fragNormal, vec3 directionToLight)
{
    float amount = max(dot(directionToLight, fragNormal), 0.f);
    return amount;
}

float getSpecularAmount(vec3 fragPosition, vec3 fragNormal, vec3 cameraPosition, vec3 directionToLight)
{
    vec3 viewDirection = normalize(cameraPosition - fragPosition);
    vec3 reflectedLightDir = reflect(-directionToLight, fragNormal);

    return pow(max(dot(reflectedLightDir, viewDirection), 0.f), 64);
}