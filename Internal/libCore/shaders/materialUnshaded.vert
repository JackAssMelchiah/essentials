#version 450
//! Notation used by libCore::Renderer
#define GLOBAL_RESOURCES 0
#define PASS_RESOURCES 1
#define MAT_RESOURCES 2
#define OBJECTS_RESOURCES 3

//! inputs
layout (std140, set = PASS_RESOURCES, binding = 0 ) buffer UniformBufferObjectCamera
{
    mat4 viewMatrix;
    mat4 projectionMatrix;
} cameraData;

layout (std430, set = OBJECTS_RESOURCES, binding = 0) buffer UniformBufferObjectPosition
{
    mat4 modelMatrix[];
} positionData;

//! vertex attrib inputs
layout(location = 0) in vec3 vertex;
layout(location = 1) in vec3 color;
//! shader outputs
layout(location = 0) out vec3 colorOut;

void main()
 {
    gl_Position = cameraData.projectionMatrix * cameraData.viewMatrix * positionData.modelMatrix[gl_InstanceIndex] * vec4(vertex, 1.0);
    colorOut = color;
}