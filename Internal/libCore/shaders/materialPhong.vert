#version 450
//! Notation used by libCore::Renderer
#define GLOBAL_RESOURCES 0
#define PASS_RESOURCES 1
#define MAT_RESOURCES 2
#define OBJECTS_RESOURCES 3

//! inputs
layout (std140, set = PASS_RESOURCES, binding = 0 ) buffer UniformBufferObjectCamera
{
    mat4 viewMatrix;
    mat4 projectionMatrix;
} cameraData;

layout (std430, set = OBJECTS_RESOURCES, binding = 0) buffer UniformBufferObjectPosition
{
    mat4 modelMatrix[];
} positionData;


//! vertex attrib inputs
layout(location = 0) in vec3 vertex;
layout(location = 1) in vec3 normal;
layout(location = 2) in vec2 uv;
//! shader outputs
layout(location = 0) out vec3 vertexPositionOut;
layout(location = 1) out vec3 vertexNormalOut;
layout(location = 2) out vec2 uvOut;

void main()
 {
    vec4 vertexWorld = positionData.modelMatrix[gl_InstanceIndex] * vec4(vertex, 1.0);
    gl_Position = cameraData.projectionMatrix * cameraData.viewMatrix * vertexWorld;
    vertexPositionOut = vertexWorld.xyz;
    vertexNormalOut = mat3(transpose(inverse(positionData.modelMatrix[gl_InstanceIndex]))) * normal; //rotate the normal as well - by the "normal matrix" which is used to correctly just rotate normals
    uvOut = uv;
}