#version 460
//! Notation used by libCore::Renderer
#define GLOBAL_RESOURCES 0
#define PASS_RESOURCES 1
#define MAT_RESOURCES 2
#define OBJECTS_RESOURCES 3

//! inputs
layout (std140, set = PASS_RESOURCES, binding = 0 ) buffer UniformBufferObjectCamera
{
    mat4 viewMatrix;
    mat4 projectionMatrix;
} cameraData;

struct BBData
{
    vec4 bbMin;
    vec4 bbMax;
    vec4 color;
};

layout (std430, set = OBJECTS_RESOURCES, binding = 0) buffer UniformBufferObjectPosition
{
    BBData bbdata[];
} objectData;

//! shader outputs
layout(location = 0) out vec3 colorOut;

void main()
 {
    vec3 aabbMin = objectData.bbdata[gl_InstanceIndex].bbMin.xyz;
    vec3 aabbMax = objectData.bbdata[gl_InstanceIndex].bbMax.xyz;
    vec3 color = objectData.bbdata[gl_InstanceIndex].color.rgb;

    // Define the 8 corners of the AABB
    vec3 corners[8];
    corners[0] = vec3(aabbMin.x, aabbMin.y, aabbMin.z);
    corners[1] = vec3(aabbMin.x, aabbMin.y, aabbMax.z);
    corners[2] = vec3(aabbMin.x, aabbMax.y, aabbMin.z);
    corners[3] = vec3(aabbMin.x, aabbMax.y, aabbMax.z);
    corners[4] = vec3(aabbMax.x, aabbMin.y, aabbMin.z);
    corners[5] = vec3(aabbMax.x, aabbMin.y, aabbMax.z);
    corners[6] = vec3(aabbMax.x, aabbMax.y, aabbMin.z);
    corners[7] = vec3(aabbMax.x, aabbMax.y, aabbMax.z);

    // Define the edges of the AABB - expects 16 invocations of VK_PRIMITIVE_TOPOLOGY_LINE_STRIP 
    int edges[16] = {
        0, 4, 5, 1, 0, 2, 6, 4, 6, 7, 5, 7, 3, 1, 3, 2
    };

    vec4 position = vec4(corners[edges[gl_VertexIndex]], 1.f);
    gl_Position = cameraData.projectionMatrix * cameraData.viewMatrix * position;
    colorOut = color;
}