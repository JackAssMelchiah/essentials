#version 450
//! Notation used by libCore::Renderer
#define GLOBAL_RESOURCES 0
#define PASS_RESOURCES 1
#define MAT_RESOURCES 2
#define OBJECTS_RESOURCES 3

//! inputs
layout(set = OBJECTS_RESOURCES, binding = 1) uniform sampler2D textureColor;

//! inputs locations
layout(location = 0) in vec2 uvs;
//! outuputs locations
layout(location = 0) out vec4 outColor;

void main()
{
    outColor = texture(textureColor, uvs).rgba;
}