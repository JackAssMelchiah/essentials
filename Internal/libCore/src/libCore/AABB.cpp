#include <libCore/AABB.hpp>
#include <libCore/Camera.hpp>
#include <libUtils/Utils.hpp>

#include <limits.h>

namespace libCore
{
	AABB::AABB()
		: m_min(glm::vec3(FLT_MAX))
		, m_max(glm::vec3(-FLT_MAX))
	{
	}

	AABB::AABB(glm::vec3 const& min, glm::vec3 const& max)
		: m_min(min)
		, m_max(max)
	{
	}

	AABB::AABB(AABB const& rhs)
		: m_min(rhs.m_min)
		, m_max(rhs.m_max)
	{
	}

	void AABB::expand(AABB const& aabb)
	{
		m_min = glm::min(aabb.m_min, m_min);
		m_max = glm::max(aabb.m_max, m_max);
	}

	glm::vec3 const& AABB::getMin() const
	{
		return m_min;
	}

	glm::vec3 const& AABB::getMax() const
	{
		return m_max;
	}

	glm::vec3 AABB::center() const
	{
		return (m_max - m_min) / 2.f;
	}

	bool AABB::instersects(Ray const& ray, glm::vec3& firstHitOut, glm::vec3& secondHitOut) const
	{
		auto tNear = -std::numeric_limits<float>::infinity();
		auto tFar = std::numeric_limits<float>::infinity();

		for (int i = 0; i < 3; ++i) {
			float invDir = 1.0f / ray.direction()[i];
			float tMin, tMax;

			// Calculate tMin and tMax for the current axis
			if (invDir >= 0) {
				tMin = (m_min[i] - ray.origin()[i]) * invDir;
				tMax = (m_max[i] - ray.origin()[i]) * invDir;
			}
			else {
				tMin = (m_max[i] - ray.origin()[i]) * invDir;
				tMax = (m_min[i] - ray.origin()[i]) * invDir;
			}

			// Update tNear and tFar
			tNear = std::max(tNear, tMin);
			tFar = std::min(tFar, tMax);

			// Check if there is no intersection
			if (tNear > tFar || tFar < 0.0f) {
				return false;
			}
		}
		// There is an intersection
		firstHitOut = ray.origin() * tNear;
		secondHitOut = ray.origin() * tFar;
		return true; 
	}

	std::string AABB::toString() const
	{
		return "Min:" + libUtils::toString(m_min) + ", Max:" + libUtils::toString(m_max);
	}
}