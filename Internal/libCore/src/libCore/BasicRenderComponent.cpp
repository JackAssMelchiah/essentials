#include <libCore/BasicRenderComponent.hpp>
#include <libCore/Renderer.hpp>
#include <libCore/Scene.hpp>
#include <libCore/MaterialUnshadedLayout.hpp>
#include <libCore/ShaderResources.hpp>

namespace libCore
{
	BasicRenderComponent::BasicRenderComponent(std::shared_ptr<Renderer> const& renderer, std::shared_ptr<Scene> const& scene, std::vector<libUtils::Shape::ColoredVertex> const& vertexData, std::string const& name)
		: m_renderer(renderer)
		, m_scene(scene)
		, m_hidden(false)
	{
		auto lockedInstance = renderer->getInstance().lock();
		auto lockedDevice = renderer->getDevice().lock();

		m_entity = scene->createEntity(name);
		m_materialLayout = std::make_shared<MaterialUnshadedLayout>(lockedInstance, lockedDevice);
		m_renderComponent = std::make_shared<RenderComponent>(renderer, m_materialLayout, 1, uint32_t(vertexData.size()));
		m_renderComponent->setName(name);

		auto vertexBuffer = std::make_shared<libCore::VertexBuffer>(renderer, uint32_t(vertexData.size() * sizeof(libUtils::Shape::ColoredVertex)), name);
		vertexBuffer->bufferData(vertexData, 0);
		m_renderComponent->setVertexBuffer(vertexBuffer);

		auto lockedEntity = m_entity.lock();
		scene->addComponentToEntity(*lockedEntity, m_renderComponent);
	}

	BasicRenderComponent::~BasicRenderComponent()
	{
		auto entity = m_entity.lock();
		auto scene = m_scene.lock();

		//need to do check, because the scene might have gotten freed elsewhere 
		//(for example when importing model - loads new scene, and replaces the old one, and if this added wihin that scene, it got freed already)
		if (entity && scene)
			scene->removeEntity(entity);
	}

	void BasicRenderComponent::setMatrix(glm::mat4 const& matrix, uint32_t instance)
	{
		auto inst = m_renderComponent->getResources().getBaseInstance();
		inst.toInstance(instance);
		m_materialLayout->setObjectModelMatrix(matrix, m_renderComponent->getResources(), inst);
	}

	void BasicRenderComponent::addInstance(glm::mat4 const& matrix)
	{
		//due how hide works, it removes component, but that would mean the renderComponent would not get added,
		//so easiest is to unhide addInstance and then hide again
		auto hideAfter = getHidden();
		if (hideAfter)
			setHide(false);

		auto entity = m_entity.lock();
		m_scene.lock()->addInstance(entity);

		auto inst = m_renderComponent->getResources().getBaseInstance();
		inst.toInstance(m_renderComponent->getInstanceCount() - 1);
		m_materialLayout->setObjectModelMatrix(matrix, m_renderComponent->getResources(), inst);
	
		if (hideAfter)
			setHide(true);
	}

	uint32_t BasicRenderComponent::getInstanceCount() const
	{
		return m_renderComponent->getInstanceCount();
	}

	glm::mat4 BasicRenderComponent::getMatrix(uint32_t instance) const
	{
		auto inst = m_renderComponent->getResources().getBaseInstance();
		inst.toInstance(m_renderComponent->getInstanceCount() - 1);
		return m_materialLayout->getObjectModelMatrix(m_renderComponent->getResources(), inst);
	}

	void BasicRenderComponent::setHide(bool hide)
	{
		if (hide != m_hidden)
		{
			auto scene = m_scene.lock();
			auto entity = m_entity.lock();

			if (hide)
			{
				scene->removeComponentFromEntity(*entity, m_renderComponent);
			}
			else
			{
				scene->addComponentToEntity(*entity, m_renderComponent);
			}
			m_hidden = hide;
		}
	}

	bool BasicRenderComponent::getHidden() const
	{
		return m_hidden;
	}

	void BasicRenderComponent::bufferData()
	{
		m_materialLayout->bufferObjectResources(m_renderComponent->getResources());
	}
}