//! libCore
#include <libCore/MaterialUnshadedTexturedLayout.hpp>
#include <libCore/UniformBuffer.hpp>
#include <libCore/VertexBuffer.hpp>
#include <libCore/ShaderResources.hpp>

//! libVulkan
#include <libVulkan/VulkanAll.hpp>
//! glm
#include <glm/gtc/type_ptr.hpp>

namespace libCore
{
	MaterialUnshadedTexturedLayout::MaterialUnshadedTexturedLayout(std::shared_ptr<libVulkan::VulkanInstance> const& instance, std::shared_ptr<libVulkan::VulkanDevice> const& device)
		: MaterialLayout(instance, device, MaterialLayoutType::UNSHADED_TEXTURED)
		, m_renderingInfo(nullptr)
	{
		//basic requirements for rendereing attachments - only depth and output attachment
		std::vector<vk::Format> inputs;
		vk::Format depthStencilFormat;
		std::vector<vk::Format> outputs;

		libVulkan::VulkanRenderingInfo::selectSimplePassFormats(*instance, inputs, depthStencilFormat, outputs);
		m_renderingInfo = std::make_shared<libVulkan::VulkanRenderingInfo>(inputs, depthStencilFormat, outputs);
	}

	std::shared_ptr<libVulkan::VulkanRenderingInfo> const& MaterialUnshadedTexturedLayout::getRenderingInfo() const
	{
		return m_renderingInfo;
	}

	std::shared_ptr<libVulkan::VulkanPipeline> MaterialUnshadedTexturedLayout::createPipeline(std::shared_ptr<libVulkan::VulkanPipelineLayout> pipelineLayout, vk::Extent2D const& resolution) const
	{
		auto device = m_device.lock();
		auto instance = m_instance.lock();

		auto vsShader = std::make_unique<libVulkan::VulkanShaderModule>(instance, device, "materialUnshadedTextured");
		auto fsShader = std::make_unique<libVulkan::VulkanShaderModule>(instance, device, "materialUnshadedTextured");
		vsShader->loadAndCreate("materialUnshadedTextured.vert.spv");
		fsShader->loadAndCreate("materialUnshadedTextured.frag.spv");
		auto shaders = std::make_shared<libVulkan::VulkanShaders>(std::move(vsShader), std::move(fsShader));

		auto vertexAttributeDesc = createObjectVertexAttributesDescription();
		return std::make_shared<libVulkan::VulkanPipeline>(instance, device, shaders, pipelineLayout, vertexAttributeDesc,m_renderingInfo, resolution, "UNSHADED_TEXTURED");
	}

	void MaterialUnshadedTexturedLayout::updateVertexDataFromResources(VertexBuffer& buffer, std::vector<glm::vec3> const& vertices, std::vector<glm::vec3> const& colors) const
	{
		//TODO implement
		assert(false);
		assert(vertices.size() == colors.size());
		//can interleave in paralel 
	}

	void MaterialUnshadedTexturedLayout::setObjectModelMatrix(glm::mat4 const& matrix, ShaderResources& resources, ShaderResources::Instance const& instance) const
	{
		resources.setInstanceResource("MODEL_MATRIX", matrix, instance);
	}

	void MaterialUnshadedTexturedLayout::setObjectAlbedoTexture(std::shared_ptr<TextureBuffer> albedo, ShaderResources& resources) const
	{
		resources.setTextureBuffer(albedo, "ALBEDO_TEXTURE_BUFFER");
	}

	void MaterialUnshadedTexturedLayout::bindObjectUniformData(libVulkan::VulkanDescriptorSet& modelDataDescriptorSet, ShaderResources& resources) const
	{
		bufferObjectResources(resources);

		auto modelMatrixBuffer = std::string("OBJECT_BUFFER");
		assert(resources.bufferExists(modelMatrixBuffer));
		auto albedoTextureBuffer = std::string("ALBEDO_TEXTURE_BUFFER");
		assert(resources.bufferExists(albedoTextureBuffer));

		resources.useBuffersWithSet({ modelMatrixBuffer, albedoTextureBuffer }, modelDataDescriptorSet);
	}

	void MaterialUnshadedTexturedLayout::bindMaterialUniformData(libVulkan::VulkanDescriptorSet& materialDataDescriptorSet, ShaderResources& resources) const
	{
		// does not have material data
	}

	void MaterialUnshadedTexturedLayout::bufferObjectResources(ShaderResources& resources) const
	{
		//specify resources used by object
		auto modelMatrixResource = std::string("MODEL_MATRIX");
		auto modelMatrixBuffer = std::string("OBJECT_BUFFER");

		//texture resources are already buffered and buffer is provided, so nothing to be done here

		//buffer resources
		resources.bufferResources({ modelMatrixResource }, modelMatrixBuffer);
	}

	void MaterialUnshadedTexturedLayout::bufferMaterialResources(ShaderResources& resources) const
	{
		//there are no resources used by material
	}

	std::shared_ptr<libVulkan::VulkanDescriptorSetLayout> MaterialUnshadedTexturedLayout::createMaterialDescriptorSetLayout() const
	{
		//none here
		return std::make_shared<libVulkan::VulkanDescriptorSetLayout>(m_instance.lock(), m_device.lock(), std::vector<vk::DescriptorSetLayoutBinding>{}, "UNSHADED_TEXTURED");
	}

	std::shared_ptr<libVulkan::VulkanDescriptorSetLayout> MaterialUnshadedTexturedLayout::createObjectDescriptorSetLayout() const
	{
		// REQUIREMENTS:
		// UBO for material data
		// IMAGE - ALBEDO

		auto layoutBindings = std::vector<vk::DescriptorSetLayoutBinding>();
		//UBO
		auto bindingUBO = vk::DescriptorSetLayoutBinding();
		{
			bindingUBO.binding = 0;
			bindingUBO.descriptorCount = 1;
			bindingUBO.descriptorType = vk::DescriptorType::eStorageBuffer;
			bindingUBO.stageFlags = vk::ShaderStageFlagBits::eVertex;
			bindingUBO.pImmutableSamplers = nullptr;
		}
		layoutBindings.push_back(bindingUBO);

		// Describe descriptor per image 
		auto bindingTBO = vk::DescriptorSetLayoutBinding();
		{
			bindingTBO.binding = 1;
			bindingTBO.descriptorCount = 1;	//for now this is allways the case
			bindingTBO.descriptorType = vk::DescriptorType::eCombinedImageSampler;
			bindingTBO.stageFlags = vk::ShaderStageFlagBits::eFragment;
			bindingTBO.pImmutableSamplers = nullptr;
		}
		layoutBindings.push_back(bindingTBO);

		//create set layout
		return std::make_shared<libVulkan::VulkanDescriptorSetLayout>(m_instance.lock(), m_device.lock(), layoutBindings, "UNSHADED_TEXTURED");
	}

	std::shared_ptr<libVulkan::VulkanVertexAttributesDescription> MaterialUnshadedTexturedLayout::createObjectVertexAttributesDescription() const
	{
		auto descriptions = std::vector<vk::VertexInputAttributeDescription>();
		auto binding = 0;

		// REQUIREMENTS:
		// layout(0) - position (VEC3)
		// layout(1) - uvs		(VEC2)
		{
			auto attrib = vk::VertexInputAttributeDescription();
			// Position
			{
				attrib.binding = binding;
				attrib.format = vk::Format::eR32G32B32Sfloat;
				attrib.location = 0;
				attrib.offset = 0;
			}
			descriptions.push_back(attrib);
			//Uvs
			{
				attrib.binding = binding;
				attrib.format = vk::Format::eR32G32Sfloat;
				attrib.location = 1;
				attrib.offset = sizeof(glm::vec2);
			}
			descriptions.push_back(attrib);
		}

		auto bindingDescription = vk::VertexInputBindingDescription();
		{
			bindingDescription.inputRate = vk::VertexInputRate::eVertex;
			bindingDescription.binding = binding;
			bindingDescription.stride = sizeof(glm::vec3) + sizeof(glm::vec2);
		}
		return std::make_shared<libVulkan::VulkanVertexAttributesDescription>(descriptions, std::vector<vk::VertexInputBindingDescription>{ bindingDescription });
	}

	void MaterialUnshadedTexturedLayout::allocateObjectData(ShaderResources& resources) const
	{
		resources.setInstanceResource("MODEL_MATRIX", glm::identity<glm::mat4>(), resources.getBaseInstance());
		resources.setTextureBuffer(std::shared_ptr<TextureBuffer>(), "ALBEDO_TEXTURE_BUFFER");
	}

	void MaterialUnshadedTexturedLayout::allocateMaterialData(ShaderResources& resources) const
	{
	}
}