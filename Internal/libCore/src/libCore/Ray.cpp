#include <libCore/Ray.hpp>

namespace libCore
{
	Ray::Ray(glm::vec3 const& origin, glm::vec3 const& direction)
		: m_origin(origin)
		, m_direction(direction)
	{
	}

	Ray::Ray(Ray const& r)
		: m_origin(r.origin())
		, m_direction(r.direction())
	{
	}

	glm::vec3 const& Ray::origin() const
	{
		return m_origin;
	}

	glm::vec3 const& Ray::direction() const
	{
		return m_direction;
	}
}