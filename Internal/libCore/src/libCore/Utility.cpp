//! libCore
#include <libCore/Utility.hpp>
#include <libCore/Core.hpp>
#include <libCore/Scene.hpp>
#include <libCore/Entity.hpp>
#include <libCore/TransformGraphComponent.hpp>
#include <libCore/RenderComponent.hpp>

namespace libCore
{
	void Utility::addRenderInstance(Gleam& gleam, Scene& scene, Entity& entity, std::string const& path, glm::mat4 const& localMatrix)
	{
		//add instances and form a grid from them
		auto entities = std::vector<std::shared_ptr<libCore::Entity>>();
		scene.getEntitiesWithComponents<libCore::RenderComponent, libCore::TransformGraphComponent>(entities);

		auto transformComponent = scene.getComponent<libCore::TransformGraphComponent>(entity).lock();
		auto renderComponent = scene.getComponent<libCore::RenderComponent>(entity).lock();

		transformComponent->addInstance(path, localMatrix);
		renderComponent->addInstance();
	}

	void Utility::removeRenderInstance(Gleam& gleam, Scene& scene, Entity& entity)
	{
	}

	uint32_t Utility::getRenderInstanceCount(Gleam& gleam, Scene& scene, Entity& entity)
	{
		return 0;
	}
}