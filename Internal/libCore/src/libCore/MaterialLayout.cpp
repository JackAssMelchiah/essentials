//!libCore
#include <libCore/MaterialLayout.hpp>
//! libUtils
#include <libUtils/Utils.hpp>

namespace libCore
{
	MaterialLayout::MaterialLayout(std::shared_ptr<libVulkan::VulkanInstance> const& instance, std::shared_ptr<libVulkan::VulkanDevice> const& device, MaterialLayoutType type)
		: m_instance(instance)
		, m_device(device)
		, m_type(type)
	{
	}

	MaterialLayout::~MaterialLayout()
	{
	}

	MaterialLayout::MaterialLayoutType MaterialLayout::getType() const
	{
		return m_type;
	}

	bool MaterialLayout::operator==(MaterialLayout const& r) const
	{
		return m_type == m_type;
	}

	bool MaterialLayout::operator<(MaterialLayout const& r) const
	{
		return uint32_t(m_type) < uint32_t(r.m_type);
	}
}