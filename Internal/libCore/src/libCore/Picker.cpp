//! libCore
#include <libCore/Picker.hpp>
#include <libCore/Window.hpp>
#include <libCore/Camera.hpp>
#include <libCore/Scene.hpp>
#include <libCore/RenderComponent.hpp>
//! libInputs
#include <libInputs/InputManager.hpp>
//! libUtils
#include <libUtils/Shape.hpp>
#include <libUtils/Utils.hpp>

namespace libCore
{
	Picker::Picker(std::shared_ptr<libInputs::InputManager> const& inputManager, std::shared_ptr<libCore::Window> const& window, std::shared_ptr<libCore::Camera> const& camera, std::shared_ptr<libCore::Scene> const& scene)
		: m_inputManager(inputManager)
		, m_window(window)
		, m_camera(camera)
		, m_scene(scene)
		, m_hoveredComponentInstance(uint32_t(-1))
		, m_selectedComponentInstance(uint32_t(-1))
		, m_defaultColor(glm::vec3(1.f, 0.f, 0.f))
		, m_hoverColor(glm::vec3(0.61f, 0.91f, 0.63f))
		, m_selectColor(glm::vec3(0.f, 1.f, 0.f))
		, m_pressedOnComponent(nullptr)
		, m_pressedOnInstance(uint32_t(-1))
	{
		//stores the intersection
		inputManager->addMouseMotionCallback([&](SDL_MouseMotionEvent const& ev)
			{
				auto inputManager = m_inputManager.lock();
				auto camera = m_camera.lock();
				auto window = m_window.lock();
				auto scene = m_scene.lock();

				auto resolution = window->getWindowResolution();
				auto& mouse = inputManager->getMouse();
				auto mouseCoords = mouse.getMouseCoords();
				
				//generate ray from mouse and camera
				auto ndc = inputManager->getNormalizedDeviceCoords(mouseCoords, resolution);
				auto ray = camera->generateWorldRay(ndc);

				//set the rendered backing of ray
				auto data = std::vector<libUtils::Shape::ColoredVertex>{ {.vertex = ray.origin(), .color = glm::vec3(0.f, 0.f, 1.f)}, {.vertex = ray.direction(), .color = glm::vec3(0.f, 0.f, 1.f)} };
				//m_rayDrawable = std::make_shared<libCore::BasicRenderComponent>(m_renderer, m_importedScene, data, "RAY");

				//want to handle just dynamic imported scene -> throw the ray against drawables within scene
				auto drawObjects = std::vector<std::shared_ptr<libCore::RenderComponent>>();
				scene->getAllComponents<libCore::RenderComponent>(drawObjects);

				m_intersections.clear();

				for (auto& drawObject : drawObjects)
				{
					for (uint32_t i = 0; i < drawObject->getInstanceCount(); ++i)
					{
						auto pointNear = glm::vec3();
						auto pointFar = glm::vec3();
						if (!drawObject->getAABB(i))
							continue;

						if (drawObject->getAABB(i)->instersects(ray, pointNear, pointFar))
						{ //should be given in order of nearest to farest
							m_intersections.push_back(Intersection{ .component = drawObject, .instance = i, .point = pointNear });
						}
					}
				}

				//now sort the intersection by nearest to camera
				std::sort(m_intersections.begin(), m_intersections.end(), [cameraPos = ray.origin()](Intersection const& lhs, Intersection const& rhs)
					{
						return glm::distance(cameraPos, lhs.point) > glm::distance(cameraPos, rhs.point);
					}
				);

				//1. clear hovers when no intersection is reported 
				if (m_intersections.empty())
				{
					clearMouseOver();
					return;
				}
				//2. clear hover if the previously hovered object is no longer in intersections
				{
					auto it = std::find_if(m_intersections.begin(), m_intersections.end(), [component = m_hoveredComponent, componentIndex = m_hoveredComponentInstance](Intersection const& rhs)
						{
							return (rhs.component == component) && (rhs.instance == componentIndex);
						}
					);
					//clean the mouse over for the object only if the previous mouseovered was not found
					if (it == m_intersections.end())
					{
						clearMouseOver();
					}
				}
				//3. if there was no mouseover previously or now, set the mouse overed to be nearest to camera
				if (!m_hoveredComponent)
				{
					setMouseOver(m_intersections.begin()->component, m_intersections.begin()->instance);
				}
			}
		);

		// cycles thru the stored intersections
		inputManager->addMouseWheelEventCallback([&](SDL_MouseWheelEvent const& ev)
			{
				auto inputManager = m_inputManager.lock();
				auto& mouse = inputManager->getMouse();

				if (m_intersections.size() < 2)
					return; // for empty and single one we do not cycle

				//find where the selected one is
				auto it = std::find_if(m_intersections.begin(), m_intersections.end(), [hc = m_hoveredComponent, hci = m_hoveredComponentInstance](Picker::Intersection const& rhs)
					{
						return (rhs.component == hc) && (rhs.instance == hci);
					}
				);
				// 1.if we move up and we are at end, lets cycle back to begin
				if (((m_intersections.end() - 1) == it) && (mouse.getWheel() == libInputs::Mouse::MouseWheel::MOVEUP))
				{
					clearMouseOver();
					auto toUse = m_intersections.begin();
					setMouseOver(toUse->component, toUse->instance);
				}
				// 2.if we move down and we are at begin, lets cycle to end
				else if ((m_intersections.begin() == it) && (mouse.getWheel() == libInputs::Mouse::MouseWheel::MOVEDOWN))
				{
					clearMouseOver();
					auto toUse = m_intersections.end() - 1;
					setMouseOver(toUse->component, toUse->instance);
				}
				// 3.otherwise just move up or down
				else if (mouse.getWheel() == libInputs::Mouse::MouseWheel::MOVEUP)
				{
					clearMouseOver();
					auto& toUse = (++it);
					setMouseOver(toUse->component, toUse->instance);
				}
				else if (mouse.getWheel() == libInputs::Mouse::MouseWheel::MOVEDOWN)
				{
					clearMouseOver();
					auto& toUse = (--it);
					setMouseOver(toUse->component, toUse->instance);
				}
			}
		);

		// cycles thru the stored intersections
		inputManager->addMouseButtonEventCallback([&](SDL_MouseButtonEvent const& ev)
			{
				auto inputManager = m_inputManager.lock();
				auto& mouse = inputManager->getMouse();

				switch (mouse.getButtonState(libInputs::Mouse::MouseButton::BUTTON_LEFT))
				{
				case libInputs::Mouse::MouseButtonState::PRESSED:
				{
					auto hovered = getHovered();
					m_pressedOnComponent = hovered.first;
					m_pressedOnInstance = hovered.second;
					break;
				}
				case libInputs::Mouse::MouseButtonState::RELEASED:
				{
					if (isHovered())
					{
						//in case that mouse hovers over object select it, but only if the hovered differs from currently selected
						auto hovered = getHovered();
						//only if on same object was pressed as is released
						if (hovered.first == m_pressedOnComponent && hovered.second == m_pressedOnInstance)
						{
							if (!isSelected(hovered.first, hovered.second))
								setSelect(hovered.first, hovered.second);
						}
					}
					//clear the pressedOn
					m_pressedOnComponent = nullptr;
					m_pressedOnInstance = uint32_t(-1);

					//in case of no mouseover, clean up
					if (!isHovered())
						clearSelect();

					break;
				}
				case libInputs::Mouse::MouseButtonState::DOUBLE_PRESS:
				{
					break;
				}
				default:
					break;
				}

				if (mouse.getButtonState(libInputs::Mouse::MouseButton::BUTTON_RIGHT) == libInputs::Mouse::MouseButtonState::PRESSED)
				{
					//printPickState();
				}
			}
		);
	}

	void Picker::setCamera(std::shared_ptr<libCore::Camera> const& camera)
	{
		m_camera = camera;
	}

	void Picker::setScene(std::shared_ptr<libCore::Scene> const& scene)
	{
		m_scene = scene;
	}

	void Picker::printPickState()
	{
		auto str = std::string("\n---PICK STATE---:\n");
		if (m_selectedComponent)
			str += "Selected: " + m_selectedComponent->getName() + "[" + std::to_string(m_selectedComponentInstance) + "]" "\n";
		if (m_hoveredComponent)
			str += "MouseOver: " + m_hoveredComponent->getName() + "[" + std::to_string(m_hoveredComponentInstance) + "]" "\n";
		for (auto& intersection : m_intersections)
			str += "intersected: " + intersection.component->getName() + "[" + std::to_string(intersection.instance) + "]" "\n";
		str += "----- -----";
		libUtils::printOut(str);
	}

	void Picker::setColorDefault(glm::vec3 const& color)
	{
		m_defaultColor = color;
	}

	void Picker::setColorHover(glm::vec3 const& color)
	{
		m_hoverColor = color;
	}

	void Picker::setColorSelect(glm::vec3 const& color)
	{
		m_selectColor = color;
	}

	glm::vec3 const& Picker::getColorDefault() const
	{
		return m_defaultColor;
	}

	glm::vec3 const& Picker::getColorHover() const
	{
		return m_hoverColor;
	}

	glm::vec3 const& Picker::getColorSelect() const
	{
		return m_selectColor;
	}

	void Picker::setMouseOver(std::shared_ptr<libCore::RenderComponent> const& renderComponent, uint32_t instance)
	{
		m_hoveredComponent = renderComponent;
		m_hoveredComponentInstance = instance;

		//set hovered color only if the component is not already selected
		if (!isSelected(m_hoveredComponent, m_hoveredComponentInstance))
			m_hoveredComponent->setAABBColor(m_hoverColor, instance);
	}

	void Picker::clearMouseOver()
	{
		if (isHovered())
		{//set the color to default if the component is not selected
			if (!isSelected(m_hoveredComponent, m_hoveredComponentInstance))
				m_hoveredComponent->setAABBColor(m_defaultColor, m_hoveredComponentInstance);
		}
		m_hoveredComponent = nullptr;
		m_hoveredComponentInstance = uint32_t(-1);
	}

	void Picker::setSelect(std::shared_ptr<libCore::RenderComponent> const& renderComponent, uint32_t instance)
	{
		//clear the color of the previously selected
		if (isSelected())
		{
			m_selectedComponent->setAABBColor(m_defaultColor, m_selectedComponentInstance);
		}

		m_selectedComponent = renderComponent;
		m_selectedComponentInstance = instance;
		//overloads the color
		m_selectedComponent->setAABBColor(m_selectColor, instance);
	}

	void Picker::clearSelect()
	{
		if (isSelected())
		{	//set color to default but only if not the selected is also hovered
			if (isHovered(m_selectedComponent, m_selectedComponentInstance))
				m_hoveredComponent->setAABBColor(m_hoverColor, m_selectedComponentInstance);
			else
				m_selectedComponent->setAABBColor(m_defaultColor, m_selectedComponentInstance);
		}
		m_selectedComponentInstance = uint32_t(-1);
		m_selectedComponent = nullptr;
	}

	bool Picker::isSelected(std::shared_ptr<libCore::RenderComponent> const& renderComponent, uint32_t instance) const
	{
		return m_selectedComponent == renderComponent && m_selectedComponentInstance == instance;
	}

	bool Picker::isHovered(std::shared_ptr<libCore::RenderComponent> const& renderComponent, uint32_t instance) const
	{
		return m_hoveredComponent == renderComponent && m_hoveredComponentInstance == instance;
	}

	bool Picker::isSelected() const
	{
		return m_selectedComponent != nullptr && m_selectedComponentInstance != uint32_t(-1);
	}

	bool Picker::isHovered() const
	{
		return m_hoveredComponent != nullptr && m_hoveredComponentInstance != uint32_t(-1);
	}

	std::pair<std::shared_ptr<libCore::RenderComponent>, uint32_t> Picker::getHovered() const
	{
		return std::make_pair(m_hoveredComponent, m_hoveredComponentInstance);
	}

	std::pair<std::shared_ptr<libCore::RenderComponent>, uint32_t> Picker::getSelected() const
	{
		return std::make_pair(m_selectedComponent, m_selectedComponentInstance);
	}
}