//! tinygltf macros 
#define TINYGLTF_IMPLEMENTATION
#define STB_IMAGE_IMPLEMENTATION
#define STB_IMAGE_WRITE_IMPLEMENTATION
#define TINYGLTF_NOEXCEPTION
#define JSON_NOEXCEPTION
//! libCore
#include <libCore/Loader.hpp>
#include <libCore/Scene.hpp>
#include <libCore/ImageLoader.hpp>
#include <libCore/Renderer.hpp>
#include <libCore/MaterialPhongLayout.hpp>
#include <libCore/MaterialPhongNormalMapLayout.hpp>
#include <libCore/MaterialUnshadedLayout.hpp>
#include <libCore/MaterialUnshadedTexturedLayout.hpp>
#include <libCore/MaterialPBRLayout.hpp>
#include <libCore/MaterialPBRTexturedLayout.hpp>
#include <libCore/MaterialSkeletalAnimationLayout.hpp>
#include <libCore/IndexBuffer.hpp>
#include <libCore/VertexBuffer.hpp>
#include <libCore/AABB.hpp>
#include <libCore/EntityManager.hpp>
#include <libCore/Skeleton.hpp>

#include <libCore/RenderComponent.hpp>
#include <libCore/RenderManager.hpp>
#include <libCore/TransformGraphComponent.hpp>
#include <libCore/TransformGraphManager.hpp>

//! libVulkan
#include <libVulkan/VulkanAll.hpp>
//! libUtils
#include <libUtils/Utils.hpp>
//! libInputs
#include <libCore/Camera.hpp>
//! glm
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/quaternion.hpp>
//! std
#include <stack>
#include <tuple>

namespace libCore
{
    Loader::Loader(std::shared_ptr<Renderer> const& renderer, std::weak_ptr<libCore::TransformGraphManager> transformationManager)
        : m_renderer(renderer)
        , m_transformGraphManager(transformationManager)
        , m_loader(std::make_shared<tinygltf::TinyGLTF>())
    {
    }

    Loader::~Loader()
    {
        free();
    }

    bool Loader::load(std::string const& path, Material prefferedMaterial, LoadSettings settings)
    {
        if (hasLoadedData())
            free();

        m_path = path;
        m_scene = std::make_shared<Scene>(m_renderer.lock(), "GENERATE_SCENE_NAME");

        if (path.empty())
        {
            libUtils::printOutErr("Empty path");
            return false;
        }

        auto binary = false;
        if (settings == LoadSettings::BINARY || path.substr(path.find_last_of(".")) != ".gltf")
            binary = true;

        auto errString = std::string();
        auto warnString = std::string();
        auto tinyModel = tinygltf::Model();

        auto valid = false;

        if (binary)
            valid = m_loader->LoadBinaryFromFile(&tinyModel, &errString, &warnString, path);
        else
            valid = m_loader->LoadASCIIFromFile(&tinyModel, &errString, &warnString, path);

        if (!errString.empty())
            libUtils::printOutErr(errString);

        if (!warnString.empty())
            libUtils::printOutWar(warnString);

        if (!valid)
            return false;

        debug(tinyModel, path.substr(path.find_last_of("/") + 1));

        parse(tinyModel, *m_scene, prefferedMaterial);

        return true;
    }

    void Loader::free()
    {
        m_scene.reset();
        m_loadedMaterials.clear();
    }

    bool Loader::hasLoadedData() const
    {
        return m_scene.get();
    }

    std::shared_ptr<Scene> Loader::getScene()
    {
        return m_scene;
    }

    std::vector<Loader::Material> const& Loader::getUsedMaterials()
    {
        return m_loadedMaterials;
    }

    void Loader::parse(tinygltf::Model const& tinyModel, libCore::Scene& scene, Material wantedMaterial)
    {
        for (auto& tinyScene : tinyModel.scenes)
        {
            // Performs expansion of the root node graphs, and then expands each child to depth, and each duplicity is skipped, since not sure about the order
            auto rootName = "/" + (tinyScene.name.empty() ? "GENERATE_SCENE_NAME" : tinyScene.name);
            auto rootNode = tinyScene.nodes.begin();
            
            auto processedNodes = std::vector<std::pair<int, std::string>>();

            auto fn = [&](tinygltf::Node const& node, int nodeID, int parentID)->void
                {
                    auto nodeFullPath = rootName;
                    //find the already processed nodes
                    for (auto& processedNode : processedNodes)
                    {
                        if (processedNode.first == parentID)
                        {
                            nodeFullPath = processedNode.second + "/" + (node.name.empty() ? "GENERATE_NODE_NAME" : node.name);
                        }
                    }
                    processedNodes.push_back(std::make_pair(nodeID, nodeFullPath));
                    // actual logic for the current node
                    processNode(tinyModel, tinyScene, node, nodeFullPath, scene, wantedMaterial);
                };
            
            walkTree(tinyModel, tinyScene, fn);

            break;
        }
    }

    void Loader::walkTree(tinygltf::Model const& tinyModel, tinygltf::Scene const& tinyScene, std::function<void(tinygltf::Node const&, int nodeID, int parentID)> const& fn, std::pair<int, int> rootNodeIDWithParent)
    {
        auto toProcess = std::stack<std::pair<int, int>>(); //first is node id, second its parent id
        auto processed = std::set<int>(); //just node

        if (rootNodeIDWithParent.first > -1)
            toProcess.push(rootNodeIDWithParent);

        if (toProcess.empty())
        {
            for (auto rootNode : tinyScene.nodes)
            {
                toProcess.push(std::pair(rootNode, -1));
            }
        }
        while (!toProcess.empty())
        {
            // Depth walk 
            auto currentNode = toProcess.top();
            toProcess.pop();

            auto& node = tinyModel.nodes.at(currentNode.first);

            for (auto& nodeChild : node.children)
            {
                if (processed.count(nodeChild) < 1)
                {
                    toProcess.push(std::pair(nodeChild, currentNode.second));
                }
            }
            fn(node, currentNode.first, currentNode.second);
            processed.insert(currentNode.first);
        }
    }

    void Loader::processNode(tinygltf::Model const& tinyModel, tinygltf::Scene const& tinyScene, tinygltf::Node const& tinyNode, std::string const& nodePath, libCore::Scene& scene, Material prefferedMaterial)
    {
        auto nodeName = tinyNode.name;
        if (nodeName.empty())
        {
            nodeName = "Unknown";
        }
        libUtils::printOut("Processing node:" + nodePath);

        // handle mesh
        if (tinyNode.mesh != -1)
        {
            auto newEntity = scene.createEntity(nodeName).lock();
            // skin
            if (tinyNode.skin != -1)
            {
                libUtils::printOut("SKIN:" + std::to_string(tinyNode.skin));
                auto& skin = tinyModel.skins.at(tinyNode.skin);
                auto skeletonComponent = parseSkin(tinyModel, tinyScene, skin);
                scene.addComponentToEntity(*newEntity, skeletonComponent);
            }

            auto mesh = std::shared_ptr<RenderComponent>();
            auto transformComponent = std::shared_ptr<libCore::TransformGraphComponent>();

            mesh = parseModel(tinyModel, tinyModel.meshes.at(tinyNode.mesh), scene, prefferedMaterial, nodeName);
            scene.addComponentToEntity(*newEntity, mesh);
            mesh->setName(nodeName);


            if (!tinyNode.weights.empty())
            {
                auto& weights = tinyNode.weights;//->morph targets weights
                // TODO: component for morph animation
                //auto morphComponent = parseMorphAnimation();
            }

            //transform component
            {
                transformComponent = std::make_shared<libCore::TransformGraphComponent>(nodePath + "/0", parseMatrix(tinyNode));
                scene.addComponentToEntity(*newEntity, transformComponent);
            }
            
            if (mesh)
            {
                for (auto i = 0; i < mesh->getInstanceCount(); ++i)
                {
                    scene.getAABB()->expand(*mesh->getAABB(i));
                }
            }
        }

        // camera - does not have to exist
        if (tinyNode.camera != -1)
        {
            auto& tinyCamera = tinyModel.cameras.at(tinyNode.camera);
            // camera is in hierarchy, so lets keep it that way
            auto camera = parseCamera(tinyCamera, nodePath + "/" + tinyCamera.name);
            auto cameraEntity = scene.createEntity(nodeName).lock();
            cameraEntity->setDisplayName(tinyCamera.name);
            scene.addComponentToEntity(*cameraEntity, camera);
        }

    }

    std::shared_ptr<libCore::Camera> Loader::parseCamera(tinygltf::Camera const& tinyCamera, std::string const& transformId)
    {
        auto& name = tinyCamera.name;
        auto camera = std::shared_ptr<libCore::Camera>();
        if ("perspective" == tinyCamera.type)
        {
            auto& fov = tinyCamera.perspective.yfov;
            auto nearFar = glm::dvec2(tinyCamera.perspective.znear, tinyCamera.perspective.zfar);
            auto& aspect = tinyCamera.perspective.aspectRatio;
            camera = std::make_shared<libCore::Camera>(fov, aspect, nearFar);
        }
        else
        {
            camera = std::make_shared<libCore::Camera>(glm::vec2{ 0, float(tinyCamera.orthographic.xmag) }, glm::vec2{ 0, tinyCamera.orthographic.ymag }, glm::vec2{ tinyCamera.orthographic.znear , tinyCamera.orthographic.zfar });
        }
        return camera;
    }

    std::shared_ptr<RenderComponent> Loader::parseModel(tinygltf::Model const& tinyModel, tinygltf::Mesh const& tinyMesh, libCore::Scene& scene, Material prefferedMaterial, std::string const& nodeName)
    {
        //lest pretend that mesh consists of single primitive
        for (auto& primitive : tinyMesh.primitives)
        {
            // suggest best fitting material
            auto materialToUse = prefferedMaterial;
            
            if (supportsMaterial(materialToUse, tinyModel, primitive) == Support::NONE)
                materialToUse = findMaterialForModel(tinyModel, primitive);
            //note that technically can fail if has no vertices
            if (materialToUse == Material::COUNT)
            {
                libUtils::printOutErr("Unable to assign material");
                break;
            }

            auto [drawable, materialLayout] = createRenderComponent(materialToUse, tinyModel, primitive);
            bufferAttributes(*drawable.get(), materialToUse, tinyModel, primitive, nodeName);
            initializeUniforms(*drawable.get(), materialToUse, tinyModel, primitive, nodeName);
            markUsedMaterial(materialToUse);
            // fixme: can consist of multiple primitives
            return drawable;
        }
        return std::shared_ptr<RenderComponent>();
    }

    glm::mat4 Loader::parseMatrix(tinygltf::Node const& node)
    {
        // stuff with transformations
        auto matrix = glm::mat4(1.f);
        // load the matrix - but might be empty, then take a look at rot scale and trans 
        if (!node.matrix.empty())
            matrix = glm::make_mat4(node.matrix.data());
        else
        {
            auto rotationMatrix = glm::dmat4(1.f);
            if (!node.rotation.empty())
                rotationMatrix = glm::toMat4(glm::make_quat(node.rotation.data()));
            auto translationMatrix = glm::dmat4(1.f);
            if (!node.translation.empty())
                translationMatrix = glm::translate(translationMatrix, glm::make_vec3(node.translation.data()));
            auto scaleMatrix = glm::dmat4(1.f);
            if (!node.scale.empty())
                scaleMatrix = glm::scale(scaleMatrix, glm::make_vec3(node.scale.data()));

            matrix = translationMatrix * rotationMatrix * scaleMatrix;
        }
        return matrix;
    }

    void Loader::debug(tinygltf::Model const& model, std::string const& file) const
    {
        auto debugStr = std::string("\n***TinyGLTF model debug for " + file + "***\n");
        for (auto& mesh : model.meshes)
        {
            debugStr += "\tMesh: " + mesh.name + ", consists of " + std::to_string(mesh.primitives.size()) + " primitives \n";

            auto primitiveCounter = 0;

            for (auto& primitive : mesh.primitives)
            {
                debugStr += "\tPrimitive " + std::to_string(primitiveCounter) + ":\n";
                if (hasVertices(primitive))
                {
                    auto aabb = getMinMaxBBValue(model, primitive);
                    debugStr += "\t\t  AABB: min-" + libUtils::toString(aabb->getMin()) + ", max-" + libUtils::toString(aabb->getMax()) + "\n";
                    debugStr += "\t\t  (vec3) Position attribute\n";
                }
                if (hasNormals(primitive))
                    debugStr += "\t\t  (vec3) Normal attribute\n";
                if (hasUvs(primitive))
                    debugStr += "\t\t  (vec2) Uvs attribute\n";
                if (hasColors(primitive))
                    debugStr += "\t\t  (vec3) Color attribute\n";
                if (hasTangents(primitive))
                    debugStr += "\t\t  (vec3) Tangents attribute\n";
                if (hasJoints(primitive))
                    debugStr += "\t\t  (vec4) Joints attribute\n";
                if (hasWeights(primitive))
                    debugStr += "\t\t  (vec4) Weights attribute\n";

                if (hasIndices(primitive))
                    debugStr += "\t\t Indices, " + std::to_string(getIBOSize(model, primitive)) + " bytes\n";
                debugStr += "\t\t Byte size for attributes: " + std::to_string(getVBOSize(findMaterialForModel(model, primitive), model, primitive)) + " bytes\n";

                if (!hasMaterial(model, primitive))
                {
                    debugStr += "\t\t Does not have material\n";
                }
                else
                {
                    auto& mat = getMaterial(model, primitive);
                    debugStr += "\t\t Materials (Only one per primitive is supported now - picks allways first)\n";
                    debugStr += printMaterial(mat);
                    debugStr += printMatchedMaterial(model, primitive, mat);
                    debugStr += "\t\t Available textures:\n";
                    if (hasAlbedoTexture(model, primitive))
                        debugStr += "\t\t\t  Albedo:\n" + printImage(getAlbedoImage(model, primitive));
                    if (hasNormalTexture(model, primitive))
                        debugStr += "\t\t\t  Normal:\n" + printImage(getNormalImage(model, primitive));
                    if (hasOcclusionTexture(model, primitive))
                        debugStr += "\t\t\t  Occlusion:\n" + printImage(getOcclusionImage(model, primitive));
                    if (hasEmissiveTexture(model, primitive))
                        debugStr += "\t\t\t  Emissive:\n" + printImage(getEmissiveImage(model, primitive));
                    if (hasMetalicRoughnessTexture(model, primitive))
                        debugStr += "\t\t\t  MetallicRoughness:\n" + printImage(getMetalicRoughnessImage(model, primitive));
                }
                debugStr += "\n";
                primitiveCounter++;
            }
        }
        libUtils::printOut(debugStr);
    }


    std::string Loader::printImage(tinygltf::Image const& image) const
    {
        auto str = std::string();
        str += "\t   name: " + image.name + "\n";
        str += "\t   bytes: " + std::to_string(image.image.size()) + "\n";
        str += "\t   w/h: " + std::to_string(image.width) + "/" + std::to_string(image.height) + "\n";
        return str;
    }

    std::string Loader::printMaterial(tinygltf::Material const& material) const
    {
        auto str = std::string();
        str += "\t  Material name: " + material.name + ", available values:\n";
        for (auto& material : material.values)
        {
            str += "\t   " + std::string(material.first.c_str()) + "\n";
        }
        return str;
    }

    std::string Loader::printMatchedMaterial(tinygltf::Model const& tinyModel, tinygltf::Primitive const& primitive, tinygltf::Material const& material) const
    {
        auto str = std::string("\t   Following material used will be used by renderer: ");
        auto foundMaterial = findMaterialForModel(tinyModel, primitive);
        switch (foundMaterial)
        {
        case Material::COUNT:
            str += "NONE\n";
            break;
        case Material::PHONG:
            str += "PHONG\n";
            break;
        case Material::NORMAL_PHONG:
            str += "PHONG with Normal mapping\n";
            break;
        case Material::UNSHADED:
            str += "UNSHADED\n";
            break;
        case Material::UNSHADED_TEXTURED:
            str += "UNSHADED with basic texture\n";
            break;
        case Material::PBR:
            str += "PBR\n";
            break;
        case Material::PBR_TEXTURED:
            str += "PBR Textured\n";
            break;
        case Material::SKELETAL_ANIMATION:
            str += "Skeletal animation\n";
            break;
        default:
            str += "Unknown material, fill the enum\n";
            break;
        }
        return str;
    }

    std::tuple<std::shared_ptr<RenderComponent>, std::shared_ptr<MaterialLayout>> Loader::createRenderComponent(Material material, tinygltf::Model const& tinyModel, tinygltf::Primitive const& primitive) const
    {
        auto vertexCount = createVertexInvocationCount(tinyModel, primitive);
        auto materialLayout = createMaterialLayout(material);
        return { std::make_shared<libCore::RenderComponent>(m_renderer.lock(), materialLayout, 1, vertexCount), materialLayout};
    }

    void Loader::bufferAttributes(libCore::RenderComponent& drawable, Material material, tinygltf::Model const& tinyModel, tinygltf::Primitive const& primitive, std::string const& nodeName)
    {
        if (hasIndices(primitive))
        {
            auto indexBuffer = std::make_shared<IndexBuffer>(m_renderer.lock(), getIndexType(tinyModel, primitive), getIBOSize(tinyModel, primitive), nodeName);
            bufferIBO(*indexBuffer, tinyModel, primitive);
            drawable.setIndexBuffer(indexBuffer);
        }

        if (hasVertices(primitive))
        {
            auto vertexBuffer = std::make_shared<VertexBuffer>(m_renderer.lock(), getVBOSize(material, tinyModel, primitive), nodeName);
            bufferVBO(*vertexBuffer, material, tinyModel, primitive);
            drawable.setVertexBuffer(vertexBuffer);

            drawable.setAABB(getMinMaxBBValue(tinyModel, primitive));
        }
    }

    void Loader::markUsedMaterial(Material material)
    {
        if (std::find(m_loadedMaterials.begin(), m_loadedMaterials.end(), material) != m_loadedMaterials.end())
            m_loadedMaterials.push_back(material);
    }

    std::shared_ptr<TextureBuffer> Loader::loadAndBufferTexture(tinygltf::Model const& tinyModel, tinygltf::Image const& image, std::string const& nodeName)
    {
        auto info = libCore::TextureBuffer::ImageInfo();
        generateTextureInfo(image, info);
        auto textureBuffer = std::make_shared<TextureBuffer>(m_renderer.lock(), std::vector<libCore::TextureBuffer::ImageInfo>{ info }, true, nodeName);
        bufferTexture(*textureBuffer, tinyModel, image, 0); //just one image per buffer, no more
        return textureBuffer;
    }

    std::shared_ptr<TextureBuffer> Loader::loadAndBufferAlbedoTexture(tinygltf::Model const& tinyModel, tinygltf::Primitive const& primitive, std::string const& nodeName)
    {
        assert(hasAlbedoTexture(tinyModel, primitive));
        auto& image = getAlbedoImage(tinyModel, primitive);
        return loadAndBufferTexture(tinyModel, image, nodeName);
    }

    std::shared_ptr<TextureBuffer> Loader::loadAndBufferNormalTexture(tinygltf::Model const& tinyModel, tinygltf::Primitive const& primitive, std::string const& nodeName)
    {
        assert(hasNormalTexture(tinyModel, primitive));
        auto& image = getNormalImage(tinyModel, primitive);
        return loadAndBufferTexture(tinyModel, image, nodeName);
    }

    std::shared_ptr<TextureBuffer> Loader::loadAndBufferOcclusionTexture(tinygltf::Model const& tinyModel, tinygltf::Primitive const& primitive, std::string const& nodeName)
    {
        assert(hasOcclusionTexture(tinyModel, primitive));
        auto& image = getOcclusionImage(tinyModel, primitive);
        return loadAndBufferTexture(tinyModel, image, nodeName);
    }

    std::shared_ptr<TextureBuffer> Loader::loadAndBufferMetallicRoughnessTexture(tinygltf::Model const& tinyModel, tinygltf::Primitive const& primitive, std::string const& nodeName)
    {
        assert(hasMetalicRoughnessTexture(tinyModel, primitive));
        auto& image = getMetalicRoughnessImage(tinyModel, primitive);
        return loadAndBufferTexture(tinyModel, image, nodeName);
    }

    void Loader::initializeUniforms(libCore::RenderComponent& drawable, Material material, tinygltf::Model const& tinyModel, tinygltf::Primitive const& primitive, std::string const& nodeName)
    {
        switch (material)
        {
        case Material::UNSHADED:
        {
            auto layout = std::static_pointer_cast<MaterialUnshadedLayout>(drawable.getMaterialLayout());
            layout->setObjectModelMatrix(glm::identity<glm::mat4>(), drawable.getResources(), drawable.getResources().getBaseInstance());
            break;
        }
        case Material::UNSHADED_TEXTURED:
        {
            auto layout = std::static_pointer_cast<MaterialUnshadedTexturedLayout>(drawable.getMaterialLayout());
            layout->setObjectModelMatrix(glm::identity<glm::mat4>(), drawable.getResources(), drawable.getResources().getBaseInstance());
            auto albedoTextureBuffer = loadAndBufferAlbedoTexture(tinyModel, primitive, nodeName);
            layout->setObjectAlbedoTexture(albedoTextureBuffer, drawable.getResources());
            break;
        }
        case Material::PHONG:
        {
            auto layout = std::static_pointer_cast<MaterialPhongLayout>(drawable.getMaterialLayout());
            layout->setObjectModelMatrix(glm::identity<glm::mat4>(), drawable.getResources(), drawable.getResources().getBaseInstance());
            auto albedoTextureBuffer = loadAndBufferAlbedoTexture(tinyModel, primitive, nodeName);
            layout->setObjectAlbedoTexture(albedoTextureBuffer, drawable.getResources());
            break;
        }
        case Material::NORMAL_PHONG:
        {
            auto layout = std::static_pointer_cast<MaterialPhongNormalLayout>(drawable.getMaterialLayout());
            layout->setObjectModelMatrix(glm::identity<glm::mat4>(), drawable.getResources(), drawable.getResources().getBaseInstance());
            auto albedoTextureBuffer = loadAndBufferAlbedoTexture(tinyModel, primitive, nodeName);
            auto normalTextureBuffer = loadAndBufferNormalTexture(tinyModel, primitive, nodeName);
            layout->setObjectAlbedoTexture(albedoTextureBuffer, drawable.getResources());
            layout->setObjectNormalTexture(normalTextureBuffer, drawable.getResources());
            break;
        }
        case Material::PBR:
        {
            auto albedoColor = glm::vec4(1.f, 1.f, 1.f, 1.f);
            auto metallicness = 0.f;
            auto roughness = 0.f;
            auto ambientOcclusion = 0.f;
            if (hasMaterial(tinyModel, primitive))
            {
                auto& tinyMaterial = getMaterial(tinyModel, primitive);
                albedoColor = getAlbedoColorValue(tinyMaterial);
                metallicness = getMetallicnessValue(tinyMaterial);
                roughness = getRoughnessValue(tinyMaterial);
                ambientOcclusion = 0.f; //TODO? IS it here?
            }

            auto layout = std::static_pointer_cast<MaterialPBRLayout>(drawable.getMaterialLayout());
            layout->setObjectModelMatrix(glm::identity<glm::mat4>(), drawable.getResources(), drawable.getResources().getBaseInstance());
            layout->setObjectAlbedoColor(albedoColor, drawable.getResources(), drawable.getResources().getBaseInstance());
            layout->setObjectMetallicness(metallicness, drawable.getResources(), drawable.getResources().getBaseInstance());
            layout->setObjectRoughness(roughness, drawable.getResources(), drawable.getResources().getBaseInstance());
            layout->setObjectAmbientOcclusion(0.f, drawable.getResources(), drawable.getResources().getBaseInstance()); // no global ambient occlusion
            //todo: there is emissivity - F0, add it too
            break;
        }
        case Material::PBR_TEXTURED:
        {
            auto& tinyMaterial = getMaterial(tinyModel, primitive);

            auto layout = std::static_pointer_cast<MaterialPBRTexturedLayout>(drawable.getMaterialLayout());
            layout->setObjectModelMatrix(glm::identity<glm::mat4>(), drawable.getResources(), drawable.getResources().getBaseInstance());
            auto albedoTextureBuffer = loadAndBufferAlbedoTexture(tinyModel, primitive, nodeName);
            auto normalTextureBuffer = loadAndBufferNormalTexture(tinyModel, primitive, nodeName);
            auto occlusionTextureBuffer = loadAndBufferOcclusionTexture(tinyModel, primitive, nodeName);
            layout->setObjectAlbedoTexture(albedoTextureBuffer, drawable.getResources());
            layout->setObjectNormalTexture(normalTextureBuffer, drawable.getResources());
            layout->setObjectSurfaceTexture(occlusionTextureBuffer, drawable.getResources());
            break;
        }
        case Material::SKELETAL_ANIMATION:
        {
            auto layout = std::static_pointer_cast<MaterialSkeletalAnimationLayout>(drawable.getMaterialLayout());
            layout->setObjectModelMatrix(glm::identity<glm::mat4>(), drawable.getResources(), drawable.getResources().getBaseInstance());
            auto matrices = std::vector<glm::mat4>();
            //TODO INVERSE BINDS, ...
            //layout->setObjectInverseBindMatrices(, drawable.getResources(), drawable.getResources().getBaseInstance());
            break;
        }
        case Material::COUNT:
            assert(!"unknown");
            break;
        default:
            assert(!"unknown");
            break;
        }
    }

    uint32_t Loader::createVertexInvocationCount(tinygltf::Model const& tinyModel, tinygltf::Primitive const& primitive) const
    {
        //its probably be indexed rendering, so get count
        if (primitive.indices != -1)
        {
            auto indexAccessor = tinyModel.accessors.at(primitive.indices);
            return indexAccessor.count;
        }
        //otherwise look at first non-null per vertex attribute
        for (auto& attribute : primitive.attributes)
        {
            auto attributeAccessor = tinyModel.accessors.at(attribute.second);
            //in case that attribute is index buffer, set it as one, and dont count it towards combined attribute
            return attributeAccessor.count;
        }
        return 0;
    }

    vk::Format Loader::getAttributeFormat(std::pair<std::string const, int> const& attribute) const
    {
        //https://www.khronos.org/registry/glTF/specs/2.0/glTF-2.0.html#geometry
        // GLTF defines only following attributes as valid: 
        // POSITION(VEC3) | NORMAL(VEC3) | TANGENT(VEC4) | TEXCOORD_n(VEC2) | COLOR_n(VEC3|VEC4) | JOINTS_n (VEC4) | WEIGHTS_n (VEC4)
        if (attribute.first.find("TEXCOORD_") != std::string::npos)
            return vk::Format::eR32G32Sfloat;
        else if (attribute.first == "POSITION" || attribute.first == "NORMAL")
            return vk::Format::eR32G32B32Sfloat;
        else if (attribute.first == "TANGENT" || attribute.first.find("COLOR_") != std::string::npos
            || attribute.first.find("JOINTS_") != std::string::npos || attribute.first.find("WEIGHTS_") != std::string::npos)
            return vk::Format::eR32G32B32A32Sfloat;
        assert(!"");
        return vk::Format::eUndefined;
    }

    uint32_t Loader::getComponentSize(int componentType) const
    {
        if (componentType == TINYGLTF_COMPONENT_TYPE_BYTE || componentType == TINYGLTF_COMPONENT_TYPE_UNSIGNED_BYTE)
            return 1;
        else if (componentType == TINYGLTF_COMPONENT_TYPE_SHORT || componentType == TINYGLTF_COMPONENT_TYPE_UNSIGNED_SHORT)
            return sizeof(short);
        else if (componentType == TINYGLTF_COMPONENT_TYPE_INT || componentType == TINYGLTF_COMPONENT_TYPE_UNSIGNED_INT)
            return sizeof(int);
        else if (componentType == TINYGLTF_COMPONENT_TYPE_FLOAT)
            return sizeof(float);
        else if (componentType == TINYGLTF_COMPONENT_TYPE_DOUBLE)
            return sizeof(double);
        assert(!"");
        return 0;
    }

    uint32_t Loader::getTypeSize(int type) const
    {
        if (type == TINYGLTF_TYPE_SCALAR)
            return 1;
        else if (type == TINYGLTF_TYPE_VEC2)
            return 2;
        else if (type == TINYGLTF_TYPE_VEC3)
            return 3;
        else if (type == TINYGLTF_TYPE_VEC4)
            return 4;
        else if (type == TINYGLTF_TYPE_MAT2)
            return 4;
        else if (type == TINYGLTF_TYPE_MAT3)
            return 9;
        else if (type == TINYGLTF_TYPE_MAT4)
            return 16;
        assert(!"");
        return 0;
    }

    uint32_t Loader::getAtributeByteSize(std::pair<std::string const, int> const& attribute, tinygltf::Model const& tinyModel) const
    {
        auto& attributeAccessor = tinyModel.accessors.at(attribute.second);
        return getTypeSize(attributeAccessor.type) * getComponentSize(attributeAccessor.componentType);
    }

    uint32_t Loader::getAtributeCount(std::pair<std::string const, int> const& attribute, tinygltf::Model const& tinyModel) const
    {
        auto& attributeAccessor = tinyModel.accessors.at(attribute.second);
        return attributeAccessor.count;
    }

    uint32_t Loader::getIndexBufferCount(tinygltf::Model const& tinyModel, tinygltf::Primitive const& primitive) const
    {
        if (!hasIndices(primitive))
            return 0;
        auto& indicesAccessor = tinyModel.accessors.at(primitive.indices);
        return indicesAccessor.count;
    }

    vk::IndexType Loader::getIndexType(tinygltf::Model const& tinyModel, tinygltf::Primitive const& primitive) const
    {
        auto sizeOfIndice = getComponentSize(tinyModel.accessors.at(primitive.indices).componentType);
        assert(sizeOfIndice == 0 || sizeOfIndice == 2 || sizeOfIndice == 4);
        return (sizeOfIndice == 2) ? vk::IndexType::eUint16 : vk::IndexType::eUint32;
    }

    uint32_t Loader::getVertexCount(tinygltf::Model const& tinyModel, tinygltf::Primitive const& primitive) const
    {
        auto vertexAttribute = findVertexAttribute(primitive);
        return getAtributeCount(vertexAttribute, tinyModel);
    }

    uint32_t Loader::getVBOSize(Material const& material, tinygltf::Model const& tinyModel, tinygltf::Primitive const& primitive) const
    {
        auto attributeCount = getVertexCount(tinyModel, primitive);

        return attributeCount * getVertexStride(material);
    }

    uint32_t Loader::getIBOSize(tinygltf::Model const& tinyModel, tinygltf::Primitive const& primitive) const
    {
        if (!hasIndices(primitive))
            return 0;
        auto& indicesAccessor = tinyModel.accessors.at(primitive.indices);
        return getTypeSize(indicesAccessor.type) * getComponentSize(indicesAccessor.componentType) * getIndexBufferCount(tinyModel, primitive);
    }

    void Loader::getTBOSize(Material const& material, tinygltf::Model const& tinyModel, tinygltf::Primitive const& primitive, std::vector<TextureBuffer::ImageInfo>& sizesOut) const
    {
        sizesOut.clear();

        switch (material)
        {
            case (Material::UNSHADED):
            {
                break;
            }
            case (Material::UNSHADED_TEXTURED):
            {
                //ALBEDO
                auto albedoInfo = libCore::TextureBuffer::ImageInfo();
                if (hasAlbedoTexture(tinyModel, primitive))
                {
                    generateTextureInfo(getAlbedoImage(tinyModel, primitive), albedoInfo);
                    sizesOut.push_back(albedoInfo);
                }
                break;
            }
            case (Material::PHONG):
            {
                //ALBEDO
                auto albedoInfo = libCore::TextureBuffer::ImageInfo();
                if (hasAlbedoTexture(tinyModel, primitive))
                {
                    generateTextureInfo(getAlbedoImage(tinyModel, primitive), albedoInfo);
                    sizesOut.push_back(albedoInfo);
                }
                break;
            }
            case (Material::NORMAL_PHONG):
            {
                //ALBEDO
                auto albedoInfo = libCore::TextureBuffer::ImageInfo();
                if (hasAlbedoTexture(tinyModel, primitive))
                {
                    generateTextureInfo(getAlbedoImage(tinyModel, primitive), albedoInfo);
                    sizesOut.push_back(albedoInfo);
                }
                //NORMAL
                auto normalInfo = libCore::TextureBuffer::ImageInfo();
                if (hasAlbedoTexture(tinyModel, primitive))
                {
                    generateTextureInfo(getNormalImage(tinyModel, primitive), normalInfo);
                    sizesOut.push_back(normalInfo);
                }
                break;
            }
            case (Material::PBR):
                break;
            case (Material::PBR_TEXTURED):
            {
                //ALBEDO
                auto albedoInfo = libCore::TextureBuffer::ImageInfo();
                if (hasAlbedoTexture(tinyModel, primitive))
                {
                    generateTextureInfo(getAlbedoImage(tinyModel, primitive), albedoInfo);
                    sizesOut.push_back(albedoInfo);
                }
                //NORMAL
                auto normalInfo = libCore::TextureBuffer::ImageInfo();
                if (hasAlbedoTexture(tinyModel, primitive))
                {
                    generateTextureInfo(getNormalImage(tinyModel, primitive), normalInfo);
                    sizesOut.push_back(normalInfo);
                }
                //OCLUSSION
                auto occlusionInfo = libCore::TextureBuffer::ImageInfo();
                if (hasOcclusionTexture(tinyModel, primitive))
                {
                    generateTextureInfo(getOcclusionImage(tinyModel, primitive), normalInfo);
                    sizesOut.push_back(normalInfo);
                }
                break;
            }
            case (Material::SKELETAL_ANIMATION):
                break;
            case Material::COUNT:
                assert(!"unknown");
                break;

            default:
                assert(!"unknown");
                break;
        }
    }

    uint32_t Loader::getVertexStride(Material material) const
    {
        switch (material)
        {
        case Material::UNSHADED:
            return 2 * sizeof(glm::vec3);
            break;
        case Material::UNSHADED_TEXTURED:
            return sizeof(glm::vec3) + sizeof(glm::vec2);
            break;
        case Material::PHONG:
            return 2 * sizeof(glm::vec3) + sizeof(glm::vec2);
            break;
        case Material::NORMAL_PHONG:
            return 2 * sizeof(glm::vec3) + sizeof(glm::vec2) + sizeof(glm::vec4);
            break;
        case Material::PBR:
            return 2 * sizeof(glm::vec3);
            break;
        case Material::PBR_TEXTURED:
            return 2 * sizeof(glm::vec3) + sizeof(glm::vec2) + sizeof(glm::vec4);
            break;
        case Material::SKELETAL_ANIMATION:
            return 2 * sizeof(glm::vec3) + sizeof(glm::ivec2) + sizeof(glm::vec4);
            break;
        case Material::COUNT:
            assert(!"unknown");
            break;
        default:
            break;
        }
        assert(!"Unimplemented");
        return 0;
    }

    std::pair<std::string, int> Loader::findVertexAttribute(tinygltf::Primitive const& primitive) const
    {
        auto key = "POSITION";
        if (primitive.attributes.count(key) != 1)
            return {};
        return *primitive.attributes.find(key);
    }

    std::pair<std::string, int> Loader::findColorAttribute(tinygltf::Primitive const& primitive) const
    {
        return findAttributeSet(0, "COLOR", primitive);
    }

    std::pair<std::string, int> Loader::findNormalAttribute(tinygltf::Primitive const& primitive) const
    {
        auto key = "NORMAL";
        if (primitive.attributes.count(key) != 1)
            return {};
        return *primitive.attributes.find(key);
    }

    std::pair<std::string, int> Loader::findUvAttribute(tinygltf::Primitive const& primitive) const
    {
        return findAttributeSet(0, "TEXCOORD", primitive);
    }

    std::pair<std::string, int> Loader::findTangentAttribute(tinygltf::Primitive const& primitive) const
    {
        auto key = "TANGENT";
        if (primitive.attributes.count(key) != 1)
            return {};
        return *primitive.attributes.find(key);
    }

    std::pair<std::string, int> Loader::findJointAttribute(tinygltf::Primitive const& primitive) const
    {
        return findAttributeSet(0, "JOINTS", primitive);
    }

    std::pair<std::string, int> Loader::findWeightAttribute(tinygltf::Primitive const& primitive) const
    {
        return findAttributeSet(0, "WEIGHTS", primitive);
    }

    std::pair<std::string, int> Loader::findAttributeSet(uint32_t setIndex, std::string const& attributeName, tinygltf::Primitive const& primitive) const
    {
        auto key = attributeName + "_" + std::to_string(setIndex);
        if (primitive.attributes.count(key) != 1)
            return {};
        return *primitive.attributes.find(key);
    }

    void Loader::generateColors(tinygltf::Model const& tinyModel, tinygltf::Primitive const& primitive, VertexAttributeDescription& descriptionOut) const
    {
        auto vertexAttributeCount = getVertexCount(tinyModel, primitive);

        descriptionOut.customAttribute.resize(vertexAttributeCount * sizeof(glm::vec3));
        descriptionOut.accessorByteOffset = 0;
        descriptionOut.attributeByteSize = sizeof(glm::vec3);
        descriptionOut.view = tinygltf::BufferView();

        auto color = glm::vec3(1.f, 0.f, 0.f);
        
        for (int i = 0; i < vertexAttributeCount; i++)
        {
            auto* src = descriptionOut.customAttribute.data() + i * descriptionOut.attributeByteSize;
            memcpy(src, glm::value_ptr(color), descriptionOut.attributeByteSize);
        }
    }

    void Loader::generateNormals(tinygltf::Model const& tinyModel, tinygltf::Primitive const& primitive, VertexAttributeDescription& descriptionOut) const
    {
        auto vertexAttributeCount = getVertexCount(tinyModel, primitive);

        descriptionOut.customAttribute.resize(vertexAttributeCount * sizeof(glm::vec3));
        descriptionOut.accessorByteOffset = 0;
        descriptionOut.attributeByteSize = sizeof(glm::vec3);
        descriptionOut.view = tinygltf::BufferView();

        auto normal = glm::vec3(1.f, 0.f, 0.f);

        for (int i = 0; i < vertexAttributeCount; i++)
        {
            auto* src = descriptionOut.customAttribute.data() + i * descriptionOut.attributeByteSize;
            memcpy(src, glm::value_ptr(normal), descriptionOut.attributeByteSize);
        }
    }

    void Loader::generateTangents(tinygltf::Model const& tinyModel, tinygltf::Primitive const& primitive, VertexAttributeDescription& descriptionOut) const
    {
        auto vertexAttributeCount = getVertexCount(tinyModel, primitive);

        descriptionOut.customAttribute.resize(vertexAttributeCount * sizeof(glm::vec4));
        descriptionOut.accessorByteOffset = 0;
        descriptionOut.attributeByteSize = sizeof(glm::vec4);
        descriptionOut.view = tinygltf::BufferView();

        auto tangent = glm::vec4(1.f, 0.f, 0.f, 1.f);

        for (int i = 0; i < vertexAttributeCount; i++)
        {
            auto* src = descriptionOut.customAttribute.data() + i * descriptionOut.attributeByteSize;
            memcpy(src, glm::value_ptr(tangent), descriptionOut.attributeByteSize);
        }
    }

    bool Loader::hasAttribute(std::pair<std::string, int> const& attribute, std::string const& searchedAttrib) const
    {
        return ((attribute.first.find(searchedAttrib) != std::string::npos) && attribute.second != -1);
    }

    bool Loader::hasIndices(tinygltf::Primitive const& primitive) const
    {
        return primitive.indices > -1;
    }

    bool Loader::hasVertices(tinygltf::Primitive const& primitive) const
    {
        for (auto& attribute: primitive.attributes)
        {
            if (hasAttribute(attribute, "POSITION"))
                return true;
        }
        return false;
    }

    bool Loader::hasNormals(tinygltf::Primitive const& primitive) const
    {
        for (auto& attribute: primitive.attributes)
        {
            if (hasAttribute(attribute, "NORMAL"))
                return true;
        }
        return false;
    }

    bool Loader::hasUvs(tinygltf::Primitive const& primitive) const
    {
        for (auto& attribute: primitive.attributes)
        {
            if (hasAttribute(attribute, "TEXCOORD"))
                return true;
        }
        return false;
    }

    bool Loader::hasTangents(tinygltf::Primitive const& primitive) const
    {
        for (auto& attribute: primitive.attributes)
        {
            if (hasAttribute(attribute, "TANGENT"))
                return true;
        }
        return false;
    }

    bool Loader::hasColors(tinygltf::Primitive const& primitive) const
    {
        for (auto& attribute: primitive.attributes)
        {
            if (hasAttribute(attribute, "COLOR"))
                return true;
        }
        return false;
    }

    bool Loader::hasJoints(tinygltf::Primitive const& primitive) const
    {
        for (auto& attribute: primitive.attributes)
        {
            if (hasAttribute(attribute, "JOINTS"))
                return true;
        }
        return false;
    }

    bool Loader::hasWeights(tinygltf::Primitive const& primitive) const
    {
        for (auto& attribute: primitive.attributes)
        {
            if (hasAttribute(attribute, "WEIGHTS"))
                return true;
        }
        return false;
    }

    Loader::VertexAttributeDescription Loader::getVertexView(tinygltf::Model const& tinyModel, tinygltf::Primitive const& primitive) const
    {
        for (auto& attribute: primitive.attributes)
        {
           if ( hasAttribute(attribute, "POSITION"))
               return { tinyModel.bufferViews.at(tinyModel.accessors.at(attribute.second).bufferView), getAtributeByteSize(attribute, tinyModel), uint32_t(tinyModel.accessors.at(attribute.second).byteOffset) };
        }
        return VertexAttributeDescription();
    }

    Loader::VertexAttributeDescription Loader::getNormalView(tinygltf::Model const& tinyModel, tinygltf::Primitive const& primitive) const
    {
        for (auto& attribute : primitive.attributes)
        {
            if (hasAttribute(attribute, "NORMAL"))
                return { tinyModel.bufferViews.at(tinyModel.accessors.at(attribute.second).bufferView), getAtributeByteSize(attribute, tinyModel), uint32_t(tinyModel.accessors.at(attribute.second).byteOffset) };
        }
        return VertexAttributeDescription();
    }

    Loader::VertexAttributeDescription Loader::getUvView(tinygltf::Model const& tinyModel, tinygltf::Primitive const& primitive) const
    {
        for (auto& attribute : primitive.attributes)
        {
            if (hasAttribute(attribute, "TEXCOORD"))
                return { tinyModel.bufferViews.at(tinyModel.accessors.at(attribute.second).bufferView), getAtributeByteSize(attribute, tinyModel), uint32_t(tinyModel.accessors.at(attribute.second).byteOffset) };
        }
        return VertexAttributeDescription();
    }

    Loader::VertexAttributeDescription Loader::getTangentView(tinygltf::Model const& tinyModel, tinygltf::Primitive const& primitive) const
    {
        for (auto& attribute : primitive.attributes)
        {
            if (hasAttribute(attribute, "TANGENT"))
                return { tinyModel.bufferViews.at(tinyModel.accessors.at(attribute.second).bufferView), getAtributeByteSize(attribute, tinyModel), uint32_t(tinyModel.accessors.at(attribute.second).byteOffset) };
        }
        return VertexAttributeDescription();
    }

    Loader::VertexAttributeDescription Loader::getColorView(tinygltf::Model const& tinyModel, tinygltf::Primitive const& primitive) const
    {
        for (auto& attribute : primitive.attributes)
        {
            if (hasAttribute(attribute, "COLOR"))
                return { tinyModel.bufferViews.at(tinyModel.accessors.at(attribute.second).bufferView), getAtributeByteSize(attribute, tinyModel), uint32_t(tinyModel.accessors.at(attribute.second).byteOffset) };
        }
        return VertexAttributeDescription();
    }

    Loader::VertexAttributeDescription Loader::getJointView(tinygltf::Model const& tinyModel, tinygltf::Primitive const& primitive) const
    {
        for (auto& attribute : primitive.attributes)
        {
            if (hasAttribute(attribute, "JOINTS"))
                return { tinyModel.bufferViews.at(tinyModel.accessors.at(attribute.second).bufferView), getAtributeByteSize(attribute, tinyModel), uint32_t(tinyModel.accessors.at(attribute.second).byteOffset) };
        }
        return VertexAttributeDescription();
    }

    Loader::VertexAttributeDescription Loader::getWeightView(tinygltf::Model const& tinyModel, tinygltf::Primitive const& primitive) const
    {
        for (auto& attribute : primitive.attributes)
        {
            if (hasAttribute(attribute, "WEIGHTS"))
                return { tinyModel.bufferViews.at(tinyModel.accessors.at(attribute.second).bufferView), getAtributeByteSize(attribute, tinyModel), uint32_t(tinyModel.accessors.at(attribute.second).byteOffset) };
        }
        return VertexAttributeDescription();
    }

    std::shared_ptr<libCore::AABB> Loader::getMinMaxBBValue(tinygltf::Model const& tinyModel, tinygltf::Primitive const& primitive) const
    {
        for (auto& attribute : primitive.attributes)
        {
            if (hasAttribute(attribute, "POSITION"))
            {
                auto& acessor = tinyModel.accessors.at(attribute.second);
                if ((acessor.minValues.size() == 3) && (acessor.maxValues.size() == 3))
                {
                    return std::make_shared<AABB>(glm::vec3(float(acessor.minValues.at(0)), float(acessor.minValues.at(1)), float(acessor.minValues.at(2))), glm::vec3(float(acessor.maxValues.at(0)), float(acessor.maxValues.at(1)), float(acessor.maxValues.at(2))));
                }
            }
        }
        return nullptr;
    }

    void Loader::interleaveData(uint32_t vboSize, std::vector<VertexAttributeDescription> const& attributes, tinygltf::Model const& tinyModel, tinygltf::Primitive const& primitive, std::vector<unsigned char>& dataOut) const
    {
        // how much bytes was already written into the VBO
        std::vector<uint32_t> writtenSizes(attributes.size(), 0);
        auto totalWritten = uint32_t(0);

        dataOut.resize(vboSize);
        auto attributesCount = getVertexCount(tinyModel, primitive);

        //Copy all data
        for(auto currentAttribute = uint32_t(0); currentAttribute < attributesCount; ++currentAttribute)
        {
            //attributes are already sorted - write single sizeof each attribute in order
            for (auto attributeIndex = 0; attributeIndex < attributes.size(); ++attributeIndex)
            {
                auto& bufferView = attributes.at(attributeIndex).view;
                auto& globalBufferOffset = bufferView.byteOffset; 
                auto& strideOffset = attributes.at(attributeIndex).accessorByteOffset;
                auto& sizeToCopy = attributes.at(attributeIndex).attributeByteSize;
                auto bufferStride = bufferView.byteStride;
                if (bufferStride == 0) //managed to get a model without stride filled in
                    bufferStride = attributes.at(attributeIndex).attributeByteSize;

                auto destination = dataOut.data() + totalWritten;

                if (!attributes.at(attributeIndex).customAttribute.empty())
                {//in case that the attribute is custom, its allways noninterleaved and whole buffer is the attrib
                    auto source = attributes.at(attributeIndex).customAttribute.data() + bufferStride * currentAttribute;
                    memcpy(destination, source, sizeToCopy);
                }
                else
                {
                    auto currentAttrib = bufferStride * currentAttribute + strideOffset;
                    auto source = tinyModel.buffers.at(bufferView.buffer).data.data() + globalBufferOffset + currentAttrib;
                    memcpy(destination, source, sizeToCopy);
                }
                
                writtenSizes.at(attributeIndex) += sizeToCopy;
                totalWritten += sizeToCopy;
            }
        }
        assert(totalWritten == vboSize);
    }

    void Loader::gatherVertexAttributesForMaterial(Material materialType, tinygltf::Model const& tinyModel, tinygltf::Primitive const& primitive, std::vector<VertexAttributeDescription>& descriptionsOut) const
    {
        descriptionsOut.clear();

        switch (materialType)
        {
        case libCore::Loader::Material::UNSHADED:
        {
            descriptionsOut.push_back(getVertexView(tinyModel, primitive));
            if (hasColors(primitive))
                descriptionsOut.push_back(getColorView(tinyModel, primitive));
            else
            {
                auto attributeDescription = VertexAttributeDescription();
                generateColors(tinyModel, primitive, attributeDescription);
                descriptionsOut.push_back(attributeDescription);
            }
            break;
        }
        case libCore::Loader::Material::UNSHADED_TEXTURED:
        {
            descriptionsOut.push_back(getVertexView(tinyModel, primitive));
            descriptionsOut.push_back(getUvView(tinyModel, primitive));
            break;
        }
        case libCore::Loader::Material::PHONG:
        {
            descriptionsOut.push_back(getVertexView(tinyModel, primitive));
            if (hasNormals(primitive))
                descriptionsOut.push_back(getNormalView(tinyModel, primitive));
            else
            {
                auto attributeDescription = VertexAttributeDescription();
                generateNormals(tinyModel, primitive, attributeDescription);
                descriptionsOut.push_back(attributeDescription);
            }
            descriptionsOut.push_back(getUvView(tinyModel, primitive));
            break;
        }
        case libCore::Loader::Material::NORMAL_PHONG:
        {
            descriptionsOut.push_back(getVertexView(tinyModel, primitive));
            if (hasNormals(primitive))
                descriptionsOut.push_back(getNormalView(tinyModel, primitive));
            else
            {
                auto attributeDescription = VertexAttributeDescription();
                generateNormals(tinyModel, primitive, attributeDescription);
                descriptionsOut.push_back(attributeDescription);
            }
            descriptionsOut.push_back(getUvView(tinyModel, primitive));

            if (hasTangents(primitive))
                descriptionsOut.push_back(getTangentView(tinyModel, primitive));
            else
            {
                auto attributeDescription = VertexAttributeDescription();
                generateTangents(tinyModel, primitive, attributeDescription);
                descriptionsOut.push_back(attributeDescription);
            }
            break;
        }
        case libCore::Loader::Material::PBR:
        {
            descriptionsOut.push_back(getVertexView(tinyModel, primitive));
            if (hasNormals(primitive))
                descriptionsOut.push_back(getNormalView(tinyModel, primitive));
            else
            {
                auto attributeDescription = VertexAttributeDescription();
                generateColors(tinyModel, primitive, attributeDescription);
                descriptionsOut.push_back(attributeDescription);
            }
            break;
        }
        case libCore::Loader::Material::PBR_TEXTURED:
        {
            descriptionsOut.push_back(getVertexView(tinyModel, primitive));
            if (hasNormals(primitive))
                descriptionsOut.push_back(getNormalView(tinyModel, primitive));
            else
            {
                auto attributeDescription = VertexAttributeDescription();
                generateNormals(tinyModel, primitive, attributeDescription);
                descriptionsOut.push_back(attributeDescription);
            }
            descriptionsOut.push_back(getUvView(tinyModel, primitive));

            if (hasTangents(primitive))
                descriptionsOut.push_back(getTangentView(tinyModel, primitive));
            else
            {
                auto attributeDescription = VertexAttributeDescription();
                generateTangents(tinyModel, primitive, attributeDescription);
                descriptionsOut.push_back(attributeDescription);
            }
            break;
        }
        case libCore::Loader::Material::SKELETAL_ANIMATION:
        {
            descriptionsOut.push_back(getVertexView(tinyModel, primitive));
            if (hasColors(primitive))
                descriptionsOut.push_back(getColorView(tinyModel, primitive));
            else
            {
                auto attributeDescription = VertexAttributeDescription();
                generateColors(tinyModel, primitive, attributeDescription);
                descriptionsOut.push_back(attributeDescription);
            }
            //joints - vec of 4, each is short -> will need to adress it in shader
            {
                descriptionsOut.push_back(getJointView(tinyModel, primitive));
            }
            //weights
            {
                descriptionsOut.push_back(getWeightView(tinyModel, primitive));
            }
            
            break;
        }
        case libCore::Loader::Material::COUNT:
            assert(!"unknown");
            break;
        default:
            assert(!"unknown");
            break;
        }
    }

    void Loader::bufferVBO(VertexBuffer& buffer, Material material, tinygltf::Model const& tinyModel, tinygltf::Primitive const& primitive)
    {
        //get how given attributes are stored within buffers
        auto attribs = std::vector<VertexAttributeDescription>();
        gatherVertexAttributesForMaterial(material, tinyModel, primitive, attribs);
        // interleave data - so we are compatible with instanced rendering
        auto vboSize = getVBOSize(material, tinyModel, primitive);
        auto interleavedData = std::vector<unsigned char>();
        interleaveData(vboSize, attribs, tinyModel, primitive, interleavedData);

        buffer.bufferData(interleavedData, uint32_t(0));
    }

    void Loader::bufferTBO(TextureBuffer& buffer, Material material, tinygltf::Model const& tinyModel, tinygltf::Primitive const& primitive)
    {
        auto nextImageIndex = uint32_t(0);

        if (material == Material::UNSHADED)
            return;
        
        if (material == Material::UNSHADED_TEXTURED)
        {
            auto& image = getAlbedoImage(tinyModel, primitive);
            nextImageIndex = bufferTexture(buffer, tinyModel, image, nextImageIndex);
            return;
        }

        if (material == Material::PHONG)
        {
            auto& image = getAlbedoImage(tinyModel, primitive);
            nextImageIndex = bufferTexture(buffer, tinyModel, image, nextImageIndex);
            return;
        }

        if (material == Material::NORMAL_PHONG)
        {
            auto& image = getAlbedoImage(tinyModel, primitive);
            nextImageIndex = bufferTexture(buffer, tinyModel, image, nextImageIndex);

            auto& imageNormal = getNormalImage(tinyModel, primitive);
            nextImageIndex = bufferTexture(buffer, tinyModel, imageNormal, nextImageIndex);
            return;
        }

        if (material == Material::PBR)
            return;

        if (material == Material::PBR_TEXTURED)
        {
            auto& image = getAlbedoImage(tinyModel, primitive);
            nextImageIndex = bufferTexture(buffer, tinyModel, image, nextImageIndex);

            auto& imageNormal = getNormalImage(tinyModel, primitive);
            nextImageIndex = bufferTexture(buffer, tinyModel, imageNormal, nextImageIndex);
            
            auto& imageOcclusion = getOcclusionImage(tinyModel, primitive);
            nextImageIndex = bufferTexture(buffer, tinyModel, imageOcclusion, nextImageIndex);
            return;
        }

        if (material == Material::SKELETAL_ANIMATION)
            return;

        assert(!"unsupported");
    }

    void Loader::bufferIBO(IndexBuffer& buffer, tinygltf::Model const& tinyModel, tinygltf::Primitive const& primitive)
    {
        auto& bufferView = tinyModel.bufferViews.at(tinyModel.accessors.at(primitive.indices).bufferView);
        auto dataSize = getIBOSize(tinyModel, primitive);
        auto data = std::vector<unsigned char>(dataSize);
        memcpy(data.data(), tinyModel.buffers.at(bufferView.buffer).data.data() + bufferView.byteOffset, dataSize);

        buffer.bufferData(data, 0);
    }

    bool Loader::hasMaterial(tinygltf::Model const& tinyModel, tinygltf::Primitive const& primitive) const
    {
        return primitive.material != -1;
    }

    tinygltf::Material const& Loader::getMaterial(tinygltf::Model const& tinyModel, tinygltf::Primitive const& primitive) const
    {
        return tinyModel.materials.at(primitive.material);
    }

    bool Loader::hasAlbedoTexture(tinygltf::Model const& tinyModel, tinygltf::Primitive const& primitive) const
    {
        if (!hasMaterial(tinyModel, primitive))
            return false;
        auto& material = getMaterial(tinyModel, primitive);
        return material.pbrMetallicRoughness.baseColorTexture.index != -1;
    }

    bool Loader::hasNormalTexture(tinygltf::Model const& tinyModel, tinygltf::Primitive const& primitive) const
    {
        if (!hasMaterial(tinyModel, primitive))
            return false;
        auto& material = getMaterial(tinyModel, primitive);
        return material.normalTexture.index != -1;
    }

    bool Loader::hasEmissiveTexture(tinygltf::Model const& tinyModel, tinygltf::Primitive const& primitive) const
    {
        if (!hasMaterial(tinyModel, primitive))
            return false;
        auto& material = getMaterial(tinyModel, primitive);
        return material.emissiveTexture.index != -1;
    }

    bool Loader::hasOcclusionTexture(tinygltf::Model const& tinyModel, tinygltf::Primitive const& primitive) const
    {
        if (!hasMaterial(tinyModel, primitive))
            return false;
        auto& material = getMaterial(tinyModel, primitive);
        return material.occlusionTexture.index != -1;
    }

    bool Loader::hasMetalicRoughnessTexture(tinygltf::Model const& tinyModel, tinygltf::Primitive const& primitive) const
    {
        if (!hasMaterial(tinyModel, primitive))
            return false;
        auto& material = getMaterial(tinyModel, primitive);
        return material.pbrMetallicRoughness.metallicRoughnessTexture.index != -1;
    }

    tinygltf::Image const& Loader::getAlbedoImage(tinygltf::Model const& tinyModel, tinygltf::Primitive const& primitive) const
    {
        auto& material = getMaterial(tinyModel, primitive);
        return tinyModel.images.at(tinyModel.textures.at(material.pbrMetallicRoughness.baseColorTexture.index).source);
    }

    tinygltf::Image const& Loader::getNormalImage(tinygltf::Model const& tinyModel, tinygltf::Primitive const& primitive) const
    {
        auto& material = getMaterial(tinyModel, primitive);
        return tinyModel.images.at(tinyModel.textures.at(material.normalTexture.index).source);
    }

    glm::vec4 Loader::getAlbedoColorValue(tinygltf::Material const& material) const
    {
        auto baseColor = material.pbrMetallicRoughness.baseColorFactor;
        assert(baseColor.size() == 4);
        return glm::vec4(float(baseColor[0]), float(baseColor[1]), float(baseColor[2]), float(baseColor[3]));
    }

    glm::vec3 Loader::getEmissivityValue(tinygltf::Material const& material) const
    {
        auto emissivefactor = material.emissiveFactor;
        assert(emissivefactor.size() == 3);
        return glm::vec3(float(emissivefactor[0]), float(emissivefactor[1]), float(emissivefactor[2]));
    }

    float Loader::getRoughnessValue(tinygltf::Material const& material) const
    {
        return material.pbrMetallicRoughness.roughnessFactor;
    }

    float Loader::getMetallicnessValue(tinygltf::Material const& material) const
    {
        return material.pbrMetallicRoughness.metallicFactor;
    }

    tinygltf::Image const& Loader::getOcclusionImage(tinygltf::Model const& tinyModel, tinygltf::Primitive const& primitive) const
    {
        auto& material = getMaterial(tinyModel, primitive);
        return tinyModel.images.at(tinyModel.textures.at(material.occlusionTexture.index).source);
    }

    tinygltf::Image const& Loader::getEmissiveImage(tinygltf::Model const& tinyModel, tinygltf::Primitive const& primitive) const
    {
        auto& material = getMaterial(tinyModel, primitive);
        return tinyModel.images.at(tinyModel.textures.at(material.emissiveTexture.index).source);
    }

    tinygltf::Image const& Loader::getMetalicRoughnessImage(tinygltf::Model const& tinyModel, tinygltf::Primitive const& primitive) const
    {
        auto& material = getMaterial(tinyModel, primitive);
        return tinyModel.images.at(tinyModel.textures.at(material.pbrMetallicRoughness.metallicRoughnessTexture.index).source);
    }

    void Loader::generateTextureInfo(tinygltf::Image const& image, libCore::TextureBuffer::ImageInfo& infoOut) const
    {
        // supporting by default R8Unorm, R8B8Unorm, R8G8B8Unorm and R8G8B8A8Unorm - in other case, textures will be converted to this format
        infoOut = { glm::ivec2(image.width, image.height), getFormat(image), getImageByteSize(image) };
    }

    vk::Format Loader::getFormat(tinygltf::Image const& image) const
    {
        //FIXME: probably look at pixel_type to determine of signed, or not?
        if (image.component == 1 && image.bits == 8)
        {
            return vk::Format::eR8Unorm;
        }
        else if (image.component == 1 && image.bits == 16)
        {
            return vk::Format::eR16Unorm;
        }
        else if (image.component == 1 && image.bits == 32)
        {
            return vk::Format::eR32Uint;
        }
        else if (image.component == 2 && image.bits == 8)
        {
            return vk::Format::eR8G8Unorm;
        }
        else if (image.component == 2 && image.bits == 16)
        {
            return vk::Format::eR16G16Unorm;
        }
        else if (image.component == 2 && image.bits == 32)
        {
            return vk::Format::eR32G32Uint;
        }
        else if (image.component == 3 && image.bits == 8)
        {
            return vk::Format::eR8G8B8Unorm;
        }
        else if (image.component == 3 && image.bits == 16)
        {
            return vk::Format::eR16G16B16Unorm;
        }
        else if (image.component == 3 && image.bits == 32)
        {
            return vk::Format::eR32G32B32Uint;
        }
        else if (image.component == 4 && image.bits == 8)
        {
            return vk::Format::eR8G8B8A8Unorm;
        }
        else if (image.component == 4 && image.bits == 16)
        {
            return vk::Format::eR16G16B16A16Unorm;
        }
        else if (image.component == 4 && image.bits == 32)
        {
            return vk::Format::eR32G32B32A32Uint;
        }
        assert(!"");
        return vk::Format::eUndefined;
    }

    bool Loader::textureFromBuffer(tinygltf::Image const& image) const
    {
        return image.bufferView > -1;
    }

    uint32_t Loader::getImageByteSize(tinygltf::Image const& image) const
    {
        //return image.bits / 8 * image.width * image.height * image.component;
        // all images will be converted to have byte size components
        return image.width * image.height * image.component * sizeof(std::byte);
    }

    void Loader::getImageData(tinygltf::Model const& tinyModel, tinygltf::Image const& image, std::span<unsigned char>& imageOut) const
    {
        auto& bufferView = tinyModel.bufferViews.at(image.bufferView);
        auto ptr = tinyModel.buffers.at(bufferView.buffer).data.data() + bufferView.byteOffset;
        imageOut = std::span<unsigned char>{ const_cast<unsigned char*>(ptr), image.image.size() };
    }

    uint32_t Loader::bufferTexture(TextureBuffer& buffer, tinygltf::Model const& tinyModel, tinygltf::Image const& image, uint32_t imageIndex)
    {
        auto resolution = glm::ivec2();
        auto pitch = int(0);
        auto data = std::vector<std::byte>();

        if (!textureFromBuffer(image))
        {//in case that there is texture URI specified, load it instead from external file rather then the packed one
           
            //fix spaces within name
            auto imageName = std::string();
            libUtils::replaceWithinString(image.uri, "%20", " ", imageName);
            //get an absolute path for resource
            auto absolutePath = m_path.substr(0, m_path.find_last_of("/") + 1) + imageName;

            //load image
            auto retval = ImageLoader::loadImage(absolutePath, resolution, pitch, data);
            if (!retval)
                assert(!"Unable to open image");

            //buffer it
            buffer.bufferData(data, imageIndex);
        }
        else
        {//load raw encoded image data 

            auto imageData = std::span<unsigned char>();
            getImageData(tinyModel, image, imageData);

            //load image from them
            auto retval = ImageLoader::loadImage(imageData, resolution, pitch, data);
            if (!retval)
                assert(!"Unable to open image");

            //buffer it
            buffer.bufferData(data, imageIndex);
        }
        return ++imageIndex;
    }

    Loader::Material Loader::findMaterialForModel(tinygltf::Model const& tinyModel, tinygltf::Primitive const& primitive) const
    {
        auto selectedMaterial = Material::COUNT;
        auto maxVal = uint32_t(Material::COUNT);
        //figure out which material can be used - use the highest material
        for (auto currentMaterial = 0; currentMaterial < maxVal; ++currentMaterial)
        {
            auto castedMaterial = static_cast<Material>(currentMaterial);
            auto support = supportsMaterial(castedMaterial, tinyModel, primitive);
            if (support == Support::FULL || support == Support::PARTIAL)
                selectedMaterial = castedMaterial;
        }
        return selectedMaterial;
    }

    Loader::Support Loader::supportsMaterial(Material materialType, tinygltf::Model const& tinyModel, tinygltf::Primitive const& primitive) const
    {
        auto vertices = hasVertices(primitive);
        if (!vertices)
            return Support::NONE;

        switch (materialType)
        {
            case Material::UNSHADED:
            {
                // REQUIREMENTS:
                // COLOR            - can be computed
                if (hasColors(primitive)) 
                    return Support::FULL;
                return Support::PARTIAL;
            }

            case Material::UNSHADED_TEXTURED:
            {
                // REQUIREMENTS:
                // COLOR            - can be computed
                // UVS
                // ALBEDO TEXTURE 
                if (hasAlbedoTexture(tinyModel, primitive) && hasUvs(primitive))
                {
                    if (hasColors(primitive))
                        return Support::FULL;
                    return Support::PARTIAL;
                }
                return Support::NONE;
            }

            case Material::PBR:
            {
                if (hasNormals(primitive))
                    return Support::FULL;
                return Support::NONE;
            }

            case Material::PHONG:
            {
                // REQUIREMENTS:
                // UVS
                // NORMALS
                // ALBEDO TEXTURE 
                if (hasAlbedoTexture(tinyModel, primitive) && hasUvs(primitive))
                {
                    if (hasNormals(primitive))
                        return Support::FULL;
                    return Support::PARTIAL;
                }
                return Support::NONE;
            }

            case Material::NORMAL_PHONG:
            {
                // REQUIREMENTS:
                // UVS
                // NORMALS
                // TANGETS
                // ALBEDO TEXTURE 
                // NORMAL TEXTURE   
                if (hasAlbedoTexture(tinyModel, primitive) && hasNormalTexture(tinyModel, primitive) && hasUvs(primitive) && hasTangents(primitive))
                {
                    if (hasNormals(primitive))
                        return Support::FULL;
                    return Support::PARTIAL;
                }
                return Support::NONE;
            }

            case Material::PBR_TEXTURED:
            {
                // REQUIREMENTS:
                // UVS
                // NORMALS
                // TANGETS
                // ALBEDO TEXTURE 
                // NORMAL TEXTURE
                // OCCLUSION TEXTURE
                if (hasAlbedoTexture(tinyModel, primitive) && hasNormalTexture(tinyModel, primitive) && hasOcclusionTexture(tinyModel, primitive) && hasUvs(primitive) && hasTangents(primitive))
                {
                    if (hasNormals(primitive))
                        return Support::FULL;
                    return Support::PARTIAL;
                }
                return Support::NONE;
            }

            case Material::SKELETAL_ANIMATION:
            {
                // REQUIREMENTS:
                // WEIGHTS todo
                // JOINTS
                if (hasJoints(primitive) && hasWeights(primitive))
                    return Support::NONE; //TODO FOR NOW
                return Support::NONE;
            }

            case Material::COUNT:
                assert(!"unknown");
                break;

            default:
                assert(!"unknown");
                break;
        }
        return Support::NONE;
    }

    std::shared_ptr<MaterialLayout> Loader::createMaterialLayout(Material materialType) const
    {
        auto instance = m_renderer.lock()->getInstance().lock();
        auto device = m_renderer.lock()->getDevice().lock();

        switch (materialType)
        {
            case(Material::UNSHADED):
                return std::make_shared<MaterialUnshadedLayout>(instance, device);
            case(Material::UNSHADED_TEXTURED):
                return std::make_shared<MaterialUnshadedTexturedLayout>(instance, device);
            case(Material::PHONG):
                return std::make_shared<MaterialPhongLayout>(instance, device);
            case(Material::NORMAL_PHONG):
                return std::make_shared<MaterialPhongNormalLayout>(instance, device);
            case(Material::PBR):
                return std::make_shared<MaterialPBRLayout>(instance, device);
            case(Material::PBR_TEXTURED):
                return std::make_shared<MaterialPBRTexturedLayout>(instance, device);
            case(Material::SKELETAL_ANIMATION):
                return std::make_shared<MaterialSkeletalAnimationLayout>(instance, device);
            default:
                break;
        }
        assert(!"unknown");
        return std::shared_ptr<MaterialLayout>();
    }

    std::shared_ptr<Skeleton> Loader::parseSkin(tinygltf::Model const& tinyModel, tinygltf::Scene const& tinyScene, tinygltf::Skin const& skin)
    {
        auto bindMatricesAccessor = skin.inverseBindMatrices;
        auto& name = skin.name;
        auto& jointNodeIndices = skin.joints;

        auto matrices = std::vector<glm::mat4>();

        loadBindMatrices(tinyModel, bindMatricesAccessor, matrices);

        return parseJoints(tinyModel, tinyScene, skin, matrices);
    }

    void Loader::loadBindMatrices(tinygltf::Model const& tinyModel, int accessorID, std::vector<glm::mat4>& inverseBinds)
    {
        inverseBinds.clear();
        auto& accessor = tinyModel.accessors.at(accessorID);
        auto& bufferView = tinyModel.bufferViews.at(accessor.bufferView);
        auto& buffer = tinyModel.buffers.at(bufferView.buffer);

        inverseBinds.resize(accessor.count);
        memcpy(inverseBinds.data(), &buffer.data.at(accessor.byteOffset + bufferView.byteOffset), accessor.count * sizeof(glm::mat4));
    }

    std::shared_ptr<Skeleton> Loader::parseJoints(tinygltf::Model const& tinyModel, tinygltf::Scene const& tinyScene, tinygltf::Skin const& skin, std::vector<glm::mat4> const& inverseBinds)
    {
        auto skinName = skin.name;
        if (skinName.empty())
            skinName = "GENERATE_SKIN_NAME";
        auto skeleton = std::make_shared<Skeleton>(m_renderer.lock(), m_scene, m_transformGraphManager.lock(), skinName);

        const auto& jointIndices = skin.joints;
        // Create a mapping of joint indices to their parent-child relationships
        std::vector<int> parentIndices(tinyModel.nodes.size(), -1); // -1 indicates no parent

        // Fill parent indices based on children relationships
        for (size_t i = 0; i < tinyModel.nodes.size(); ++i) {
            const tinygltf::Node& node = tinyModel.nodes.at(i);
            for (int childIndex : node.children) {
                parentIndices.at(childIndex) = i; // Set the parent index for each child
            }
        }

        // Iterate through each joint index
        for (size_t i = 0; i < jointIndices.size(); ++i) {
            int jointIndex = jointIndices.at(i);
            const tinygltf::Node& jointNode = tinyModel.nodes.at(jointIndex);

            Skeleton::Joint joint;
            joint.name = jointNode.name; // Get the joint name

            // Get the inverse bind pose matrix
            assert(skin.inverseBindMatrices != -1 && i < tinyModel.accessors.at(skin.inverseBindMatrices).count);

            const tinygltf::Accessor& accessor = tinyModel.accessors.at(skin.inverseBindMatrices);
            const tinygltf::BufferView& bufferView = tinyModel.bufferViews.at(accessor.bufferView);
            const tinygltf::Buffer& buffer = tinyModel.buffers.at(bufferView.buffer);

            memcpy(&joint.inverseBP, &buffer.data.at(bufferView.byteOffset + accessor.byteOffset + i*sizeof(glm::mat4)), sizeof(glm::mat4));
            // Set parent index (using -1 for no parent)
            joint.parent = parentIndices.at(jointIndex);

            skeleton->addJoint(joint); // Add the joint
        }

        return skeleton;

        //assert(skin.joints.size() == inverseBinds.size());
        //auto skeleton = std::make_shared<Skeleton>();
        //auto skeletonRootNodeIndice = skin.skeleton;

        //auto fn = [&](tinygltf::Node const& node, int nodeID, int parentID)
        //    {
        //        auto joint = Skeleton::Joint{ .transform = parseMatrix(node), .name = node.name, .parent = uint8_t(parentID)};
        //        skeleton->addJoint(joint);
        //    };

        //walkTree(tinyModel, tinyScene, fn, std::pair(skeletonRootNodeIndice, -1));
        //return skeleton;
    }

    uint32_t Loader::getJointAttribSize(tinygltf::Model const& tinyModel, tinygltf::Primitive const& primitive) const
    {
        for (auto& attribute : primitive.attributes)
        {
            if (hasAttribute(attribute, "JOINTS"))
                return getAtributeByteSize(attribute, tinyModel);
        }
        return 0;
    }
}