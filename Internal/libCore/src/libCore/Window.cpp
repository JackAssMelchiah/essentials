#include <libCore/Window.hpp>
//! libVulkan 
#include <libVulkan/VulkanInstance.hpp>
#include <libVulkan/VulkanDevice.hpp>
#include <libVulkan/VulkanCommandPool.hpp>
#include <libVulkan/VulkanCommandBuffer.hpp>
#include <libVulkan/VulkanQueue.hpp>
#include <libVulkan/VulkanFence.hpp>
#include <libVulkan/VulkanSemaphore.hpp>
//! SDL
#include <SDL.h>
#include <SDL_vulkan.h>

namespace libCore
{
	Window::Window(std::string const& name)
		: m_sdlWindow(nullptr)
		, m_name(name)
	{
		auto resolution = glm::ivec2{ 1280, 720 };
		m_sdlWindow = SDL_CreateWindow("GLEAM", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, resolution.x, resolution.y, SDL_WINDOW_VULKAN | SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE);
	}

	Window::~Window()
	{
		SDL_DestroyWindow(m_sdlWindow);
	}

	glm::ivec2 Window::getWindowResolution() 
	{
		auto resolution = glm::ivec2();
		SDL_GetWindowSize(m_sdlWindow, &resolution.x, &resolution.y);
		return resolution;
	}

	void Window::setWindowResolution(glm::ivec2 const& res)
	{
		SDL_SetWindowSize(m_sdlWindow, res.x, res.y);
		if (auto window = m_vulkanWindow.lock(); window)
			window->setResolution(vk::Extent2D{ uint32_t(res.x), uint32_t(res.y) });
	}

	std::vector<std::string> Window::getVulkanInstanceExtensions() const
	{
		unsigned int count;
		SDL_Vulkan_GetInstanceExtensions(m_sdlWindow, &count, nullptr);
		
		auto extensions = std::vector<const char*>(count);
		auto retval = std::vector<std::string>();

		SDL_Vulkan_GetInstanceExtensions(m_sdlWindow, &count, extensions.data());

		for (auto& extension: extensions)
			retval.push_back(extension);

		return retval;
	}

	vk::SurfaceKHR Window::createAndGetVulkanSurface(libVulkan::VulkanInstance& instance) const
	{
		auto surface = VkSurfaceKHR();
		SDL_Vulkan_CreateSurface(m_sdlWindow, instance.operator()(), &surface);
		return vk::SurfaceKHR(surface);
	}

	void Window::setVulkanBackend(std::weak_ptr<libVulkan::VulkanWindow>& window)
	{
		m_vulkanWindow = window;
	}

	std::weak_ptr<libVulkan::VulkanWindow> Window::getVulkanBackend()
	{
		return m_vulkanWindow;
	}

	void Window::moveMouseCursor(glm::vec2 const& percentDimms)
	{
		assert((percentDimms.x > 0.f) && (percentDimms.x < 1.f) && (percentDimms.y > 0.f) && (percentDimms.y < 1.f));
		auto clampedPercent = glm::clamp(percentDimms, 0.f, 1.f);
		auto resolution = getWindowResolution();
		SDL_WarpMouseInWindow(m_sdlWindow, resolution.x * clampedPercent.x, resolution.y * clampedPercent.y);
	}

	void Window::lockMouseCursor(bool on)
	{
		SDL_SetRelativeMouseMode(SDL_bool(on));
	}

	SDL_Window& Window::operator()()
	{
		return *m_sdlWindow;
	}

	std::string Window::getName() const
	{
		return m_name;
	}

	void Window::setMouseCursorVisibility(bool visible)
	{
		SDL_ShowCursor(int(visible));
	}
}