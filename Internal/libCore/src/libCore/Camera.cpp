//! libCore
#include <libCore/Camera.hpp>
#include <libCore/AABB.hpp>

//! libUtils
#include <libUtils/Utils.hpp>

//! glm
#include <glm/gtc/matrix_transform.hpp>


namespace libCore
{
	Camera::Camera(double fov, double aspect, glm::vec2 const& nearFar)
		: m_projectionMatrix(1.f)
		, m_view(1.f)
		, m_nearFar(nearFar)
	{
		setPerspectiveProjection(fov, aspect, nearFar);
	}

	Camera::Camera(glm::vec2 const& leftRight, glm::vec2 const& topBottom, glm::vec2 const& nearFar)
		: m_projectionMatrix(1.f)
		, m_view(1.f)
		, m_nearFar(nearFar)
	{
		setOrthogonalProjection(leftRight, topBottom, nearFar);
	}

	glm::vec3 Camera::NDCtoWorldCoords(glm::vec2 const& ndc) const
	{
		// transform NDC to clipCoords
		auto clip = glm::vec4(ndc, 0.f, 1.f);

		//inverse it by view - project matrix
		auto invVP = glm::inverse(getProjectionMatrix() * getViewMatrix());
		auto world = invVP * clip;
		
		world /= world.w;
		return { world.x, world.y, world.z };
	}

	glm::vec3 Camera::NDCtoWorldDir(glm::vec2 const& ndc) const
	{
		// Set z = -1 for the near plane and w = 1
		auto clip = glm::vec4(ndc.x, ndc.y, -1.f, 1.f);

		// Inverse of the projection matrix
		auto invProj = glm::inverse(getProjectionMatrix());

		// Transform clip coordinates to eye coordinates
		auto dirEye = invProj * clip;

		// Set z to -1 to point towards the scene (this is already done by setting clip.z = -1)
		// Set w = 0 since we want a direction vector
		dirEye.z = -1.f; // This is already set correctly
		dirEye.w = 0.f;  // Direction vector


		//libUtils::printOut("NDC:" + libUtils::toString(ndc));

		// Transform from eye coordinates to world coordinates
		auto dirWorld = glm::inverse(getViewMatrix()) * dirEye;

		// Normalize the resulting direction vector
		return glm::normalize(glm::vec3(dirWorld));
	}

	Ray Camera::generateWorldRay(glm::vec2 const& ndc) const
	{
		auto rayLength = 100.f;
		auto worldDir = NDCtoWorldDir(ndc);
		auto pos = getPosition();

		return Ray(pos, pos + worldDir * rayLength);
	}

	void Camera::setOrthogonalProjection(glm::vec2 const& leftRight, glm::vec2 const& topBottom, glm::vec2 const& nearFar)
	{
		auto projMat = glm::mat4(1.f);
		projMat[0][0] = 2.f / (leftRight.y - leftRight.x);
		projMat[1][1] = 2.f / (topBottom.y - topBottom.x);
		projMat[2][2] = 1.f / (nearFar.y - nearFar.x);
		projMat[3][0] = -(leftRight.y + leftRight.x) / (leftRight.y - leftRight.x);
		projMat[3][1] = -(topBottom.y + topBottom.x) / (topBottom.y - topBottom.x);
		projMat[3][2] = -nearFar.x / (nearFar.y - nearFar.x);
		m_projectionMatrix = projMat;
		m_nearFar = nearFar;
	}

	void Camera::setPerspectiveProjection(float FoV, float aspectRatio, glm::vec2 const& nearFar)
	{
		//auto halfTan = glm::tan(FoV / 2.);
		//auto projMat = glm::mat4(0.f);
		//projMat[0][0] = 1.f / (aspectRatio * halfTan);
		//projMat[1][1] = 1.f / halfTan;
		//projMat[2][2] = nearFar.y / (nearFar.y - nearFar.x);
		//projMat[2][3] = 1.f;
		//projMat[3][2] = -(nearFar.x * nearFar.y) / (nearFar.y - nearFar.x);
		//m_projectionMatrix = projMat;

		m_projectionMatrix = glm::perspectiveRH_ZO(FoV, aspectRatio, nearFar.x, nearFar.y);

		//m_projectionMatrix = glm::perspective(FoV, aspectRatio, nearFar.x, nearFar.y);
		m_projectionMatrix[1][1] *= -1.f;
	}

	glm::mat4 Camera::getProjectionMatrix() const
	{
		return m_projectionMatrix;
	}

	void Camera::setViewMatrix(glm::mat4 viewMatrix)
	{
		m_view = viewMatrix;
	}

	glm::vec3 Camera::getPosition() const
	{
		glm::mat4 inverseView = glm::inverse(m_view);
		glm::vec4 cameraPos = inverseView[3]; // Extract the position
		return glm::vec3(cameraPos);
	}

	glm::vec3 Camera::getForwardDirection() const
	{
		//prob should be just return m_view[2];
		glm::vec3 forward;
		forward.x = -m_view[2][0];
		forward.y = -m_view[2][1];
		forward.z = -m_view[2][2];
		forward = glm::normalize(forward);
		return forward;
	}

	glm::mat4 Camera::getViewMatrix() const
	{
		return m_view;
	}

	glm::mat4& Camera::getViewMatrix()
	{
		return m_view;
	}

	void Camera::view(libCore::AABB const& aabb)
	{
		auto aabbCenter = aabb.center();
		auto cameraPosition = getPosition();

		glm::vec3 viewDir = glm::normalize(aabbCenter - cameraPosition);

		glm::vec3 right = glm::normalize(glm::cross(viewDir, glm::vec3(0.0f, 1.0f, 0.0f)));
		glm::vec3 up = glm::normalize(glm::cross(right, viewDir));
		//Calculate the camera position that centers the AABB in the view frustum
		glm::vec3 viewPos = aabbCenter - viewDir * glm::length(aabb.getMax() - aabb.getMin()) * 0.5f;
		m_view = glm::lookAt(viewPos, viewPos + viewDir, up);
	}

	glm::mat4 Camera::getFitTranform(libCore::AABB const& aabb) const
	{
		//Transform the AABB to camera space
		glm::vec4 aabbMin4 = glm::vec4(aabb.getMin(), 1.0f);
		glm::vec4 aabbMax4 = glm::vec4(aabb.getMax(), 1.0f);
		aabbMin4 = m_view * aabbMin4;
		aabbMax4 = m_view * aabbMax4;
		//Clamp the AABB to the camera's near and far planes
		aabbMin4.z = glm::max(aabbMin4.z, m_nearFar.x);
		aabbMax4.z = glm::min(aabbMax4.z, m_nearFar.y);
		//Calculate the scale factor to fit the AABB
		float scaleX = 2.0f / (aabbMax4.x - aabbMin4.x);
		float scaleY = 2.0f / (aabbMax4.y - aabbMin4.y);
		float scaleZ = 2.0f / (aabbMax4.z - aabbMin4.z);
		float scale = glm::min(scaleX, glm::min(scaleY, scaleZ));
		//Create the model transformation matrix
		glm::mat4 modelMatrix = glm::mat4(1.0f);
		modelMatrix = glm::scale(modelMatrix, glm::vec3(scale));
		modelMatrix = glm::translate(modelMatrix, -aabb.center());
		return modelMatrix;
	}

}