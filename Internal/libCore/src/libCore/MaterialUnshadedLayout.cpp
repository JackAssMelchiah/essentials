//! libCore
#include <libCore/MaterialUnshadedLayout.hpp>
#include <libCore/UniformBuffer.hpp>
#include <libCore/VertexBuffer.hpp>
#include <libCore/ShaderResources.hpp>

//! libVulkan
#include <libVulkan/VulkanAll.hpp>
//! glm
#include <glm/gtc/type_ptr.hpp>

namespace libCore
{
	MaterialUnshadedLayout::MaterialUnshadedLayout(std::shared_ptr<libVulkan::VulkanInstance> const& instance, std::shared_ptr<libVulkan::VulkanDevice> const& device, vk::PrimitiveTopology topology)
		: MaterialLayout(instance, device, MaterialLayoutType::UNSHADED)
		, m_primitiveTopology(topology)
		, m_renderingInfo(nullptr)
	{
		//basic requirements for rendereing attachments - only depth and output attachment
		std::vector<vk::Format> inputs;
		vk::Format depthStencilFormat;
		std::vector<vk::Format> outputs;

		libVulkan::VulkanRenderingInfo::selectSimplePassFormats(*instance, inputs, depthStencilFormat, outputs);
		m_renderingInfo = std::make_shared<libVulkan::VulkanRenderingInfo>(inputs, depthStencilFormat, outputs);
	}

	std::shared_ptr<libVulkan::VulkanRenderingInfo> const& MaterialUnshadedLayout::getRenderingInfo() const
	{
		return m_renderingInfo;
	}

	std::shared_ptr<libVulkan::VulkanPipeline> MaterialUnshadedLayout::createPipeline(std::shared_ptr<libVulkan::VulkanPipelineLayout> pipelineLayout, vk::Extent2D const& resolution) const
	{
		auto device = m_device.lock();
		auto instance = m_instance.lock();

		auto vsShader = std::make_unique<libVulkan::VulkanShaderModule>(instance, device, "materialUnshaded");
		auto fsShader = std::make_unique<libVulkan::VulkanShaderModule>(instance, device, "materialUnshaded");
		vsShader->loadAndCreate("materialUnshaded.vert.spv");
		fsShader->loadAndCreate("materialUnshaded.frag.spv");
		auto shaders = std::make_shared<libVulkan::VulkanShaders>(std::move(vsShader), std::move(fsShader));

		auto vertexAttributeDesc = createObjectVertexAttributesDescription();
		auto pipeline = std::make_shared<libVulkan::VulkanPipeline>(instance, device, shaders, pipelineLayout, vertexAttributeDesc, m_renderingInfo, resolution, "UNSHADED");
		pipeline->setInputAssebly(m_primitiveTopology, false);
		pipeline->compilePipeline();
		return pipeline;
	}

	void MaterialUnshadedLayout::bindObjectUniformData(libVulkan::VulkanDescriptorSet& modelDataDescriptorSet, ShaderResources& resources) const
	{
		bufferObjectResources(resources);

		//descriptor write
		auto modelMatrixBuffer = std::string("OBJECT_BUFFER");
		assert(resources.bufferExists(modelMatrixBuffer));

		resources.useBuffersWithSet({ modelMatrixBuffer }, modelDataDescriptorSet);
	}

	void MaterialUnshadedLayout::bindMaterialUniformData(libVulkan::VulkanDescriptorSet& materialDataDescriptorSet, ShaderResources& resources) const
	{
		// no material resources for this Material
	}

	void MaterialUnshadedLayout::bufferObjectResources(ShaderResources& resources) const
	{
		//specify resources used by object 
		auto modelMatrixResource = std::string("MODEL_MATRIX");
		auto modelMatrixBuffer = std::string("OBJECT_BUFFER");
		
		//buffer resources
		resources.bufferResources({ modelMatrixResource }, modelMatrixBuffer);
	}

	void MaterialUnshadedLayout::bufferMaterialResources(ShaderResources& resources) const
	{
		//no resources used by material
	}

	void MaterialUnshadedLayout::updateVertexDataFromResources(VertexBuffer& buffer, std::vector<glm::vec3> const& vertices, std::vector<glm::vec3> const& colors) const
	{
		//TODO implement
		assert(false);
		assert(vertices.size() == colors.size());
		//can interleave in paralel 
	}

	void MaterialUnshadedLayout::setObjectModelMatrix(glm::mat4 const& matrix, ShaderResources& resources, ShaderResources::Instance const& instance) const
	{
		resources.setInstanceResource("MODEL_MATRIX", matrix, instance);
	}

	glm::mat4 MaterialUnshadedLayout::getObjectModelMatrix(ShaderResources& resources, ShaderResources::Instance const& instance) const
	{
		return resources.getInstanceResource<glm::mat4>("MODEL_MATRIX", instance);
	}

	std::shared_ptr<libVulkan::VulkanDescriptorSetLayout> MaterialUnshadedLayout::createMaterialDescriptorSetLayout() const
	{
		//while not used, it needs to be set-up
		return std::make_shared<libVulkan::VulkanDescriptorSetLayout>(m_instance.lock(), m_device.lock(), std::vector<vk::DescriptorSetLayoutBinding>{}, "UNSHADED");
	}

	std::shared_ptr<libVulkan::VulkanDescriptorSetLayout> MaterialUnshadedLayout::createObjectDescriptorSetLayout() const
	{
		auto layoutBindings = std::vector<vk::DescriptorSetLayoutBinding>();
		//UBO
		auto bindingUBO = vk::DescriptorSetLayoutBinding();
		{
			bindingUBO.binding = 0;
			bindingUBO.descriptorCount = 1;
			bindingUBO.descriptorType = vk::DescriptorType::eStorageBuffer;
			bindingUBO.stageFlags = vk::ShaderStageFlagBits::eVertex;
			bindingUBO.pImmutableSamplers = nullptr;
		}
		layoutBindings.push_back(bindingUBO);

		//create set layout
		return std::make_shared<libVulkan::VulkanDescriptorSetLayout>(m_instance.lock(), m_device.lock(), layoutBindings, "UNSHADED");
	}

	std::shared_ptr<libVulkan::VulkanVertexAttributesDescription> MaterialUnshadedLayout::createObjectVertexAttributesDescription() const
	{
		auto descriptions = std::vector<vk::VertexInputAttributeDescription>();
		auto binding = 0;

		// REQUIREMENTS:
		// layout(0) - position (VEC3)
		// layout(1) - color	(VEC3)
		{
			auto attrib = vk::VertexInputAttributeDescription();
			// Position
			{
				attrib.binding = binding;
				attrib.format = vk::Format::eR32G32B32Sfloat;
				attrib.location = 0;
				attrib.offset = 0;
			}
			descriptions.push_back(attrib);
			// Color
			{
				attrib.binding = binding;
				attrib.format = vk::Format::eR32G32B32Sfloat;
				attrib.location = 1;
				attrib.offset = sizeof(glm::vec3);
			}
			descriptions.push_back(attrib);
		}

		auto bindingDescription = vk::VertexInputBindingDescription();
		{
			bindingDescription.inputRate = vk::VertexInputRate::eVertex;
			bindingDescription.binding = binding;
			bindingDescription.stride = 2 * sizeof(glm::vec3);
		}
		return std::make_shared<libVulkan::VulkanVertexAttributesDescription>(descriptions, std::vector<vk::VertexInputBindingDescription>{ bindingDescription });
	}

	void MaterialUnshadedLayout::allocateObjectData(ShaderResources& resources) const
	{
		resources.setInstanceResource("MODEL_MATRIX", glm::identity<glm::mat4>(), resources.getBaseInstance());
	}

	void MaterialUnshadedLayout::allocateMaterialData(ShaderResources& resources) const
	{
	}
}
