//! libCore
#include <libCore/FirstPersonHandler.hpp>
//! libInputs
#include <libInputs/Keyboard.hpp>
#include <libInputs/Mouse.hpp>
#include <libInputs/Gamepad.hpp>
//! libUtils
#include <libUtils/Utils.hpp>
//! std
#include <set>
//! glm
#include <glm/ext/matrix_transform.hpp>

namespace libCore
{
	FirstPersonHandler::FirstPersonHandler(glm::vec3 const& position, glm::vec3 const& direction)
		: Handler()
		, m_direction(glm::normalize(direction))
		, m_position(position)
		, m_pitch(0.f)
		, m_yaw(0.f)
		, m_handleMouseMovement(false)
	{
		auto keys = KeyMappings{
			std::make_pair(KeyAction::FORWARD , SDL_Keycode(SDL_KeyCode::SDLK_w)),
			std::make_pair(KeyAction::BACKWARD , SDL_Keycode(SDL_KeyCode::SDLK_s)),
			std::make_pair(KeyAction::DOWN , SDL_Keycode(SDL_KeyCode::SDLK_LCTRL)),
			std::make_pair(KeyAction::UP , SDL_Keycode(SDL_KeyCode::SDLK_SPACE)),
			std::make_pair(KeyAction::LEFT , SDL_Keycode(SDL_KeyCode::SDLK_a)),
			std::make_pair(KeyAction::RIGHT , SDL_Keycode(SDL_KeyCode::SDLK_d))
		};

		m_yaw = glm::degrees(atan2(direction.x, direction.z)) + 90;
		m_pitch = glm::degrees(asin(direction.y));

		customizeKeyMappings(keys);
	}

	void FirstPersonHandler::handle(Camera& camera)
	{
		for (auto& action: m_keyActions)
		{
			auto& metadata = action.second;
			if (!metadata.timer.stopped())
			{
				handleAction(action.second);
				metadata.timer.stop();
				metadata.timer.start();
			}
		}
		auto viewMatrix = glm::lookAt(m_position, m_direction + m_position, glm::vec3(0, 1, 0));
		camera.setViewMatrix(viewMatrix);
	}

	bool FirstPersonHandler::customizeKeyMappings(KeyMappings const& keys)
	{
		auto newKeys = m_keyActions;
		for (auto& key : keys)
		{
			newKeys[key.second] = {key.first, libUtils::Time()};
		}

		//clean all the timers, and validate that there are no duplicits
		std::set<KeyAction> processedActions;
		for (auto& key : newKeys)
		{
			if (processedActions.count(key.second.action))
			{
				//already exist, there are duplicites
				assert(!"Duplicit keys");
				return false;
			}
			processedActions.insert(key.second.action);
			key.second.timer.stop();
		}
		m_keyActions = newKeys;
		return true;
	}

	void FirstPersonHandler::resize(glm::ivec2 const& viewport)
	{
	}

	void FirstPersonHandler::set(glm::vec3 position, glm::vec3 viewDirection)
	{
		m_position = position;
		m_direction = glm::normalize(viewDirection);

		m_yaw = glm::degrees(atan2(m_direction.x, m_direction.z)) + 90;
		m_pitch = glm::degrees(asin(m_direction.y));
	}
	
	void FirstPersonHandler::processKeyboardKey(SDL_KeyboardEvent const& keyboardEvent, libInputs::Keyboard const& keyboard, libInputs::Mouse const& mouse)
	{
		auto keyCode = keyboardEvent.keysym.sym;
		if ((m_keyActions.count(keyCode) > 0) && keyboardEvent.state == SDL_PRESSED)
		{
			m_keyActions.at(keyCode).timer.start();
		}
		else if ((m_keyActions.count(keyCode) > 0) && keyboardEvent.state == SDL_RELEASED)
		{
			m_keyActions.at(keyCode).timer.stop();
		}
	}

	void FirstPersonHandler::processMouseMotion(SDL_MouseMotionEvent const& mouseMotion, libInputs::Keyboard const& keyboard, libInputs::Mouse const& mouse)
	{
		if (!m_handleMouseMovement)
			return;

		auto frameTime = 0.05f;
		auto mousecoords = mouse.getMotion();

		changeCameraPitch(mousecoords.y * 5 * frameTime);
		changeCameraYaw(mousecoords.x * 5 * frameTime);
	}

	void FirstPersonHandler::processMouseButton(SDL_MouseButtonEvent const& mouseButton, libInputs::Keyboard const& keyboard, libInputs::Mouse const& mouse)
	{
		if (mouse.getButtonState(libInputs::Mouse::MouseButton::BUTTON_LEFT) == libInputs::Mouse::MouseButtonState::PRESSED)
			m_handleMouseMovement = true;
		else
			m_handleMouseMovement = false;
	}

	void FirstPersonHandler::processMouseWheel(SDL_MouseWheelEvent const& mouseWheel, libInputs::Keyboard const& keyboard, libInputs::Mouse const& mouse)
	{
	}

	void FirstPersonHandler::processGamepadAxisMotion(SDL_ControllerAxisEvent const& controllerAxis, libInputs::Gamepad const& gamepad)
	{
	}

	void FirstPersonHandler::processGamepadButton(SDL_ControllerButtonEvent const& controllerButton, libInputs::Gamepad const& gamepad)
	{
	}

	void FirstPersonHandler::processFocusChanged(bool focusLost)
	{
		//clean up key presses so the camera is not going in direction if key was pressed when focus is lost
		if (focusLost)
		{
			for (auto& action : m_keyActions)
			{
				action.second.timer.stop();
			}
		}
	}

	void FirstPersonHandler::handleAction(KeyActionMetadata const& action)
	{
		float amount = action.timer.getTime() * 0.001f * 5.f;
		auto& act = action.action;

		if (act == KeyAction::FORWARD)
		{
			moveForwardBackward(amount);
		}
		if (act == KeyAction::BACKWARD)
		{
			moveForwardBackward(-amount);
		}
		if (act == KeyAction::LEFT)
		{
			strafeLeftRight(-amount);
		}
		if (act == KeyAction::RIGHT)
		{
			strafeLeftRight(amount);
		}
		if (act == KeyAction::UP)
		{
			moveUpDown(amount);
		}
		if (act == KeyAction::DOWN)
		{
			moveUpDown(-amount);
		}
		if (act == KeyAction::SELECT)
		{

		}
		if (act == KeyAction::DESELECT)
		{

		}
	}

	void FirstPersonHandler::moveUpDown(float distance)
	{
		m_position = m_position + distance * glm::normalize(glm::cross(m_direction, glm::normalize(glm::cross(glm::vec3(0.f, 1.f, 0.f), m_direction))));
	}

	void FirstPersonHandler::strafeLeftRight(float distance)
	{
		m_position = m_position + glm::normalize(glm::cross(m_direction, glm::vec3(0.f, 1.f, 0.f))) * distance;
	}
	
	void FirstPersonHandler::moveForwardBackward(float distance) 
	{
		m_position = m_direction * distance + m_position;
	}

	void FirstPersonHandler::changeCameraPitch(float angle) 
	{
		m_pitch = glm::clamp(m_pitch + angle, -89.f, 89.f);
		updateDirection();
	}

	void FirstPersonHandler::changeCameraYaw(float angle)
	{
		m_yaw += angle;
		if (m_yaw < -360.f)
			m_yaw += 360.f;
		if (m_yaw > 360.f)
			m_yaw -= 360.f;
		updateDirection();
	}

	void FirstPersonHandler::updateDirection()
	{
		m_direction.x = cos(glm::radians(m_yaw)) * cos(glm::radians(m_pitch));
		m_direction.y = sin(glm::radians(m_pitch));
		m_direction.z = sin(glm::radians(m_yaw)) * cos(glm::radians(m_pitch));
		m_direction = glm::normalize(m_direction);
	}

}