//! libCore
#include <libCore/Buffer.hpp>
#include <libCore/Renderer.hpp>
//! libVulkan
#include <libVulkan/VulkanBarrier.hpp>

namespace libCore
{
	Buffer::Buffer(std::shared_ptr<libVulkan::VulkanInstance> const& instance, std::shared_ptr<libVulkan::VulkanDevice> const& device, uint32_t bytes, std::string const& name)
		: m_instance(instance)
		, m_device(device)
		, m_size(bytes)
		, m_masterBuffer()
		, m_name(name)
	{
	}

	uint32_t Buffer::getByteSize() const
	{
		return m_size;
	}

	libVulkan::VulkanBuffer& Buffer::operator()()
	{
		return *m_masterBuffer;
	}

	std::shared_ptr<libVulkan::VulkanBarrier> Buffer::generateBarrier(vk::AccessFlags2 srcAccessFlags, vk::AccessFlags2 dstAccessFlags, vk::PipelineStageFlagBits2 srcStage, vk::PipelineStageFlagBits2 dstStage)
	{
		auto b = vk::BufferMemoryBarrier2();
		{
			b.dstAccessMask = dstAccessFlags;
			b.dstStageMask = dstStage;
			b.dstAccessMask = srcAccessFlags;
			b.srcStageMask = srcStage;
			b.offset = 0;
			b.size = getByteSize();
		}
		return std::make_shared<libVulkan::VulkanBarrier>(b);
	}
}