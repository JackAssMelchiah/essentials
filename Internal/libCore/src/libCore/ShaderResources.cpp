//! libCore
#include <libCore/ShaderResources.hpp>
#include <libCore/UniformBuffer.hpp>
#include <libCore/TextureBuffer.hpp>
//! libVulkan
#include <libVulkan/VulkanAll.hpp>

namespace libCore 
{
	ShaderResources::ShaderResources(std::shared_ptr<libVulkan::VulkanInstance> const& instance, std::shared_ptr<libVulkan::VulkanDevice> const& device)
		: m_instance(instance)
		, m_device(device)
		, m_instanceIDGenerator(0)
	{
		//create first base instance
		createFirstInstance();
	}

	ShaderResources::Instance ShaderResources::getBaseInstance() const
	{
		return Instance{0, getInstanceCount()};
	}
		
	bool ShaderResources::resourceExist(std::string const& resourceName) const
	{
		return m_resources.count(resourceName) > 0;
	}

	bool ShaderResources::instanceExist(Instance const& instance) const
	{
		return (m_instancesPointer.count(instance) > 0) && (m_instancesPointer.at(instance).valid());
	}

	uint32_t ShaderResources::getInstanceCount() const
	{
		auto count = uint32_t(0);
		for (auto& instancePointer : m_instancesPointer)
		{
			if (instancePointer.second.valid())
				++count;
		}
		return count;
	}

	void ShaderResources::bufferResources(std::vector<std::string> const& resources, std::string const& buffer)
	{
		//0. Resources need to exist, and also, try to make sure that alignment rules for std430 layout are valid
		auto resourcePackSize = uint32_t(0);
		auto maxResourceSize = uint32_t(0);
		for (auto& resource: resources)
		{
			auto resourceSize = uint32_t(getInternalResource(resource, getBaseInstance()).data.size());
			resourcePackSize += resourceSize;
			maxResourceSize = glm::max(maxResourceSize, resourceSize);
			assert(resourceExist(resource));
		}
		if (maxResourceSize % maxResourceSize != 0)
		{
			assert(!"Warning, storage buffers generaly use std430 layout, which has specific alignment requirements.");
		}

		auto bufferID = BufferID{ buffer };
		//1. If there is no buffer, create it
		if (!bufferExists(buffer))
		{
			//find out how much data is needed
			auto dataSize = uint32_t();
			for (auto& resource : resources)
			{
				dataSize += getResourceByteSize(resource);
			}
			auto bufferSize = getInstanceCount() * dataSize;
			createBuffer(bufferID, bufferSize);
		}

		//2. If there is no pack for given resource vector, create it
		auto bufferId = BufferID{ buffer };
		if (!resourcePackExists(resources, bufferId))
		{
			createResourcePack(resources, bufferId);
		}

		//3. Update the resource pack with data TODO: prob can be only as part of 2. step, since they are updated when setting instance resource
		auto instanceCount = getInstanceCount();
		auto& resourcePack = getResourcePack(bufferId);
		auto bytesWritten = uint32_t(0);
		auto instance = getFirstInstance();
		while(instanceExist(instance))
		{
			for (auto& resource : resources)
			{
				auto& src = getInternalResource(resource, { instance });
				auto resourceInstanceByteSize = getResourceByteSize(resource);
				memcpy(resourcePack.data.data() + bytesWritten, src.data.data(), resourceInstanceByteSize);
				bytesWritten += resourceInstanceByteSize;
			}
			instance = getNextInstance(instance);
		}
		//4. Copy prepared and updated resource pack into gpu buffer
		auto& uniformBuffer = getUniformBuffer(bufferID);
		uniformBuffer.bufferData(resourcePack.data);
	}

	void ShaderResources::useBuffersWithSet(std::vector<std::string> const& buffers, libVulkan::VulkanDescriptorSet& descriptorSet)
	{
		auto bufferCount = buffers.size();
		for (auto bufferIndex = 0; bufferIndex < bufferCount; ++bufferIndex)
		{
			auto bufferID = BufferID{ buffers.at(bufferIndex) };
			assert(bufferExists(bufferID()));

			if (isTextureBuffer(bufferID))
			{
				//texture buffer - here we asume its just shader sampled texture resource - TODO: more usage
				auto descriptorImageInfos = std::vector<vk::DescriptorImageInfo>();
				auto& textureBuffer = getTextureBuffer(bufferID);
				textureBuffer.getDescriptorImageInfos(descriptorImageInfos);
				descriptorSet.updateImage(bufferIndex, vk::DescriptorType::eCombinedImageSampler, descriptorImageInfos);
			} 
			else
			{
				auto& uniformBuffer = getUniformBuffer(bufferID);
				//each buffer contains all interleaved attributes - write it per buffer, so can be used with separate buffer types
				auto info = vk::DescriptorBufferInfo();
				{
					info.buffer = uniformBuffer.operator()().operator()();
					info.offset = 0;
					info.range = uniformBuffer.getByteSize();
				}
				descriptorSet.updateBuffer(bufferIndex, uniformBuffer.getType(), { info });
			}
		}
	}

	void ShaderResources::setTextureBuffer(std::shared_ptr<TextureBuffer> textureBuffer, std::string const& buffer)
	{
		m_textureBuffers[{buffer}] = textureBuffer;
	}

	ShaderResources::InternalResource& ShaderResources::getInternalResource(std::string const& resourceName, Instance const& instance)
	{
		//only allow this method to get valid resources!
		assert(instanceExist(instance) && resourceExist(resourceName));
		return m_resources.at(resourceName).at(m_instancesPointer.at(instance).resourceInstanceIndex);
	}

	ShaderResources::InternalResource ShaderResources::getInternalResourceCopy(std::string const& resourceName, Instance const& instance) const
	{
		//only allow this method to get valid resources!
		assert(instanceExist(instance) && resourceExist(resourceName));
		return m_resources.at(resourceName).at(m_instancesPointer.at(instance).resourceInstanceIndex);
	}

	bool ShaderResources::resourceUsedInPack(std::string const& resourceName) const
	{
		assert(resourceExist(resourceName));
		return m_resourceMetadata.count(resourceName) > 0;
	}

	std::vector<ShaderResources::BufferPtr> const& ShaderResources::getPacksForResource(std::string const& resourceName) const
	{
		assert(resourceExist(resourceName) && resourceUsedInPack(resourceName));
		return m_resourceMetadata.at(resourceName).bufferTargets;
	}

	ShaderResources::ResourcePack& ShaderResources::getResourcePack(BufferID const& id)
	{
		assert(resourcePackExists(id));
		return m_resourcePacks.at(id);
	}

	uint32_t ShaderResources::getIndexForInstance(Instance const& instance) const
	{
		assert(instanceExist(instance));
		return m_instancesPointer.at(instance).resourceInstanceIndex;
	}

	bool ShaderResources::resourcePackExists(BufferID const& buffer) const
	{
		return m_resourcePacks.count(buffer) > 0;
	}

	bool ShaderResources::resourcePackExists(std::vector<std::string> const& resources, BufferID const& buffer) const
	{
		assert(bufferExists(buffer()));
		for (auto& resource : resources)
		{
			assert(resourceExist(resource));
		}

		//Resource pack must exist - will have a buffer, and data are in it in same order -- check is expensive
		if (resourcePackExists(buffer) && (m_resourcePacks.at(buffer).resources == resources))
			return true;
		return false;
	}

	bool ShaderResources::bufferExists(std::string const& bufferID) const
	{
		return (m_uniformBuffers.count({ bufferID }) > 0) || (m_textureBuffers.count({bufferID}) > 0);
	}

	void ShaderResources::createBuffer(BufferID const& bufferID, uint32_t byteSize)
	{
		auto instance = m_instance.lock();
		auto device = m_device.lock();

		m_uniformBuffers[bufferID] = std::make_shared<UniformBuffer>(instance, device, byteSize, bufferID.index);
	}

	void ShaderResources::createResourcePack(std::vector<std::string> const& resources, BufferID const& bufferID)
	{
		//find out how much data is needed
		auto dataSize = uint32_t(0);
		for (auto& resource : resources)
		{
			//add the offset and buffer to resource metadata, so it is known where to buffer - implicitly creates if doesnt exist
			m_resourceMetadata[resource].bufferTargets.push_back(BufferPtr{ bufferID, dataSize });
			dataSize += getResourceByteSize(resource);
		}
		m_resourcePacks[bufferID] = { resources, std::vector<char>(getInstanceCount() * dataSize) };
	}

	uint32_t ShaderResources::getResourceByteSize(std::string const& resourceName) const
	{
		assert(resourceExist(resourceName));
		return m_resources.at(resourceName).at(0).data.size();
	}

	UniformBuffer& ShaderResources::getUniformBuffer(BufferID const& id)
	{
		return *m_uniformBuffers.at(id);
	}

	TextureBuffer& ShaderResources::getTextureBuffer(BufferID const& id)
	{
		return *m_textureBuffers.at(id);
	}

	bool ShaderResources::isTextureBuffer(BufferID const& id) const
	{
		assert(bufferExists(id()));
		return m_textureBuffers.count(id) > 0;
	}

	ShaderResources::Instance ShaderResources::getFirstInstance() const
	{
		return Instance{0};
	}

	ShaderResources::Instance ShaderResources::getNextInstance(Instance instanceIndex) const
	{
		assert(instanceExist(instanceIndex));
		return m_instancesPointer.at(instanceIndex).next;
	}

	ShaderResources::Instance ShaderResources::getLastInstance() const
	{
		auto instance = getFirstInstance();
		while (m_instancesPointer.at(instance).valid() && m_instancesPointer.at(instance).next() > -1)
		{
			instance = m_instancesPointer.at(instance).next;
		}
		return instance;
	}

	void ShaderResources::createFirstInstance()
	{
		assert(getInstanceCount() == 0);
		// its first, so there are no data to copy
		auto newInstance = Instance{ m_instanceIDGenerator };
		// empty instances do not have any resources in them
		m_instancesPointer[newInstance] = InstanceMetadata(0, { -1 }, { -1 });

		// allways create one more which is "empty", for ease of search
		addEmptyInstance();
	}

	void ShaderResources::addEmptyInstance()
	{
		auto newInstance = Instance{ ++m_instanceIDGenerator };
		auto lastInstance = getLastInstance();

		// empty instances do not have any resources in them
		m_instancesPointer[newInstance] = InstanceMetadata(-1, { lastInstance }, { -1 });
	}

	ShaderResources::Instance ShaderResources::createInstance(Instance const& srcInstance)
	{
		assert(instanceExist(srcInstance));
		assert(m_resources.size() > 0); //cannot add instances to resources which does not exist
		//1. Among the resources, find possible empty space in vector - first invalid instance index
		auto newInstanceIndex = getFirstInvalidInstance();
		auto invalidResourceIndex = getFirstInvalidResourceIndex();
		// if empty space in vector was not found then allocate for each resource new place, and use index for that
		if (invalidResourceIndex == -1)
		{
			for (auto& resourceItem: m_resources)
			{
				auto& resource = resourceItem.first;
				auto& resourceData = resourceItem.second;
				resourceData.push_back(InternalResource{ std::vector<char>(getResourceByteSize(resource)) });
				//basically, this is new instance index, and that is kinda what instance count is
				invalidResourceIndex = resourceData.size() - 1;
			}
		}
		else
		{//the record is there, but data are cleared out, therefore just reallocate them - by doing that, nonempty data are not invalid
			for (auto& resourceItem : m_resources)
			{
				auto& resource = resourceItem.first;
				auto& resourceData = resourceItem.second;
				resourceData.at(invalidResourceIndex).data.resize(getResourceByteSize(resource)); 
			}
		}

		//2. Metadata for that index needs to be re-validated and now instance can be searched and getInternalResource() wont fail with it
		{
			revalidateInstance(newInstanceIndex, invalidResourceIndex);
		}
		//3. Do a deep resource copy of src
		for (auto& resourceItem : m_resources)
		{
			auto& resource = resourceItem.first;
			auto& resourceData = getInternalResource(resource, srcInstance);
			auto& dstData = getInternalResource(resource, newInstanceIndex);
			memcpy(dstData.data.data(), resourceData.data.data(), getResourceByteSize(resource));
		}
		//4. Clean-up all packs - they need to get recreated and rebuffered, - that happens lazily in buffer method
		cleanResourcePacks();

		//5. Since it is expected, that there is allways atleast one invalid instance index, in case, that last one was used, create another
		if (getFirstInvalidInstance()() == -1)
			addEmptyInstance();

		return newInstanceIndex;
	}

	void ShaderResources::removeInstance(int32_t instance)
	{
		auto instanceHandle = Instance();
		instanceHandle.index = instance;
		assert(instanceExist(instanceHandle));

		bool isLastInstance = (getLastInstance() == instanceHandle);

		//0. Cannot ever remove first instance
		if (instanceHandle == getBaseInstance())
			return;

		//1. Loop thru every resource, and set that instance data empty (invalidate)
		for (auto& resourceItem : m_resources)
		{
			auto& resource = resourceItem.first;
			auto& resourceData = getInternalResource(resource, instanceHandle);
			resourceData.data.clear();
		}
		//2. Instance should be invalidated too
		{
			//connect previous and next items - previous will allways exist
			auto prevIndex = m_instancesPointer.at(instanceHandle).previous;
			auto nextIndex = isLastInstance ? Instance{-1}: m_instancesPointer.at(instanceHandle).next;
			m_instancesPointer.at(prevIndex).next = nextIndex;
			if (!isLastInstance)
				m_instancesPointer.at(nextIndex).previous = prevIndex;

			m_instancesPointer.at(instanceHandle).next = { -1 };
			m_instancesPointer.at(instanceHandle).previous = { -1 };
			m_instancesPointer.at(instanceHandle).resourceInstanceIndex = { -1 };
		}
		//3. Clean-up all packs - they need to get recreated and rebuffered, - that happens lazily in buffer method
		cleanResourcePacks();
	}

	int32_t ShaderResources::getFirstInvalidResourceIndex() const
	{
		//1. If there is no resources bail
		if (m_resources.empty())
			return -1;

		//pick first resource and look if in array there is no empty space
		auto& resourceData = m_resources.begin()->second;
		for (auto index = 0; index < resourceData.size(); ++index)
		{
			if (resourceData.at(index).data.empty())
				return index;
		}
		return -1;
	}
	
	ShaderResources::Instance ShaderResources::getFirstInvalidInstance() const
	{
		for (auto& instanceItem : m_instancesPointer)
		{
			if (!instanceItem.second.valid())
				return instanceItem.first;
		}
		//could not find
		return { -1, 0 };
	}

	void ShaderResources::cleanResourcePacks()
	{
		//1. Clean all buffers
		m_uniformBuffers.clear();
		//2. Clean all resource packs
		m_resourcePacks.clear();
		//3. Clean resource metadata
		m_resourceMetadata.clear();
	}
	
	void ShaderResources::removeResource(std::string const& resourceName)
	{
		assert(resourceExist(resourceName));
		// 1. Remove resource metadata
		m_resourceMetadata.erase(resourceName);

		//2. Find which resource pack are used
		auto toClean = std::vector<BufferID>();
		for (auto& uniformBufferItem : m_uniformBuffers)
		{
			auto& bufferId = uniformBufferItem.first;
			auto& resourcePack = getResourcePack(bufferId);
			if (std::find(resourcePack.resources.begin(), resourcePack.resources.end(), resourceName) != resourcePack.resources.end())
			{
				toClean.push_back(bufferId);
			}
		}
		//3. Remove the resource packs and the buffers for them
		for (auto& bufferId : toClean)
		{
			m_resourcePacks.erase(bufferId);
			m_uniformBuffers.erase(bufferId);
		}
		//4. Remove the resource
		m_resources.erase(resourceName);
	}
	
	void ShaderResources::revalidateInstance(Instance const& invalidInstance, int32_t payload)
	{
		auto lastInstance = getLastInstance();
		m_instancesPointer.at(lastInstance).next = invalidInstance;

		m_instancesPointer[invalidInstance] = InstanceMetadata(payload, lastInstance, { -1 });
		
		//Create an empty instance - no data, just for easy loop, but only if there is none
		if (getFirstInvalidInstance().index == -1)
		{
			addEmptyInstance();
		}
	}
}