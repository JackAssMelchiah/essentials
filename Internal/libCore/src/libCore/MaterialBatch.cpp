//! libCore
#include <libCore/MaterialBatch.hpp>
#include <libCore/RenderComponent.hpp>
#include <libCore/MaterialLayout.hpp>
#include <libCore/Renderer.hpp>
#include <libCore/RenderingLink.hpp>
//! libVulkan
#include <libVulkan/VulkanPipeline.hpp>
#include <libVulkan/VulkanPipelineLayout.hpp>
#include <libVulkan/VulkanCommandBuffer.hpp>
#include <libVulkan/VulkanDescriptorSet.hpp>
#include <libVulkan/VulkanDescriptorSetLayout.hpp>
#include <libVulkan/VulkanDefaults.hpp>

namespace libCore
{
	MaterialBatch::MaterialBatch(std::shared_ptr<Renderer> const& renderer, std::shared_ptr<libVulkan::VulkanDescriptorSetLayout> const& passDescriptorSetLayout, std::shared_ptr<MaterialLayout> const& materialLayout)
		: m_renderer(renderer)
		, m_materialLayout(materialLayout)
		, m_passDescriptorSetLayout(passDescriptorSetLayout)
	{
	}

	bool MaterialBatch::hasObject(std::shared_ptr<RenderComponent> const& drawable) const
	{
		auto pipelineIndex = findCompatiblePipelineWithRenderComponent(*drawable->getMaterialLayout());
		auto& batchObjects = m_pipelinesWithObjects.at(pipelineIndex);
		for (auto& batchObject : batchObjects)
		{
			if (batchObject.contains(drawable))
				return true;
		}
		return false;
	}

	void MaterialBatch::bindObjectResources()
	{
		for (auto& pipelineWithObjects : m_pipelinesWithObjects)
		{
			for (auto& objectStuct : pipelineWithObjects.second)
			{
				auto objectDescriptorSet = getObjectDescriptorSet(objectStuct).lock();
				m_materialLayout->bindObjectUniformData(*objectDescriptorSet, objectStuct.drawable->getResources());
			}
		}		
	}

	void MaterialBatch::addObject(std::shared_ptr<RenderComponent> drawable)
	{
		//!FIXME: BUG? shouldnt it be findCompatiblePipelineWithRenderComponent(*drawable->getLayout())?
		auto pipelineIndex = findCompatiblePipelineWithRenderComponent(*m_materialLayout);
		auto renderer = m_renderer.lock();
		if (pipelineIndex() == -1)
		{
			pipelineIndex = createAndAddPipeline(*drawable, *m_materialLayout);
		}
		auto modelDescriptorSet = createDescriptorSetForRenderComponent(*drawable, *renderer);
		//store the object
		insertObject(drawable, pipelineIndex, modelDescriptorSet);

		//bind its uniform data to gpu 
		auto lockedSet = modelDescriptorSet.lock();
		m_materialLayout->bindObjectUniformData(*lockedSet, drawable->getResources());
	}

	void MaterialBatch::insertObject(std::shared_ptr<RenderComponent> drawable, PipelineKey const& pipelineKey, std::weak_ptr<libVulkan::VulkanDescriptorSet> modelDataDescriptorSet)
	{
		auto& objectsCollection = m_pipelinesWithObjects[pipelineKey];
		//since its vector, find first empty space, or add it to back
		auto objectIndex = RenderComponentKey{ -1 };
		for (auto i = 0; i < objectsCollection.size(); ++i)
		{
			if (!objectsCollection.at(i).valid())
			{
				objectIndex = { i };
			}
			if (objectIndex() != -1)
				break;
		}
		if (objectIndex() == -1)
		{
			objectsCollection.push_back(Object());
			objectIndex = { int32_t(objectsCollection.size()) - 1 };
		}

		objectsCollection[objectIndex()] = Object(objectIndex, drawable, modelDataDescriptorSet);
	}

	std::weak_ptr<libVulkan::VulkanDescriptorSet> MaterialBatch::getObjectDescriptorSet(Object& object)
	{
		auto descriptorSet = object.objectDescriptorSet.lock();
		if (!descriptorSet)
		{// was freed, request new one
			auto renderer = m_renderer.lock();
			object.objectDescriptorSet = createDescriptorSetForRenderComponent(*object.drawable, *renderer);
		}
		return object.objectDescriptorSet;
	}

	bool MaterialBatch::removeObject(std::shared_ptr<RenderComponent> drawable)
	{
		auto pipelineKey = PipelineKey{ -1 };
		auto drawableKey = RenderComponentKey{ -1 };
		auto validModels = 0;
		// find drawable within collection, and also iterate over rest to clean vector + pipeline as well if this was the last valid drawable
		for (auto& pipelineWithObjects : m_pipelinesWithObjects)
		{
			auto& modelCollection = pipelineWithObjects.second;
			auto modelCollectionSize = modelCollection.size();
			validModels = 0;
			for (auto i = 0; i < modelCollectionSize; ++i)
			{
				if (modelCollection.at(i).valid())
				{
					validModels++;
					if (modelCollection.at(i).contains(drawable))
					{
						drawableKey = RenderComponentKey{ i };
						pipelineKey = pipelineWithObjects.first;
					}
				}
				// can return early if already key found and there are more than 1 object alive
				if (drawableKey() != -1 && validModels > 1)
				{
					break;
				}
			}
			//can early return if drawable was found
			if (drawableKey() != -1)
				break;
		}

		// otherwise if drawable was found and was not last, just remove it
		if (drawableKey() != -1)
		{
			m_pipelinesWithObjects.at(pipelineKey).at(drawableKey()).destroy(m_renderer.lock());
		}
		// in case, that this drawable was the last, erase the whole thing with pipeline
		if (validModels == 1)
		{
			m_pipelinesWithObjects.erase(pipelineKey);
			m_pipelines[pipelineKey()] = std::shared_ptr<libVulkan::VulkanPipeline>();
			//also free the descriptor sets for materials and for objects
			m_renderer.lock()->destroyDescriptorSet(m_materialDescriptorSet, Renderer::ResourceFrequency::Material);
		}
		// otherwise, nothing found so do nothing...
		return m_pipelinesWithObjects.empty();
	}

	void MaterialBatch::recordBuffer(libVulkan::VulkanCommandBuffer& cb)
	{
		//this method is called from scene after material resources have been bound
		for (int32_t i = 0; i < m_pipelines.size(); ++i)
		{
			//binds the pipeline
			auto& pipeline = *m_pipelines.at(i);
			cb.bindPipeline(pipeline);
			auto& pipelineModels = m_pipelinesWithObjects.at(PipelineKey{ i });
			for (auto& pipelineModel : pipelineModels)
			{
				auto modelDescriptorSet = getObjectDescriptorSet(pipelineModel).lock();
				cb.bindDescriptorSet(*modelDescriptorSet, m_renderer.lock()->getDescriptorSetNumber(Renderer::ResourceFrequency::Object), pipeline.getPipelineLayout());
				//draws the model
				pipelineModel.drawable->record(cb);
			}
		}
	}

	void MaterialBatch::recordSingleBuffer(libVulkan::VulkanCommandBuffer& cb, uint32_t which)
	{
		//TODO: maybe dont record single per command buffer, but whole batch per buffer
		auto iterativeModelsCout = uint32_t(0);
		for (auto& objectsWithPipeline : m_pipelinesWithObjects)
		{
			auto modelCount = objectsWithPipeline.second.size();
			if (iterativeModelsCout + modelCount > which)
			{ //command buffer will record this pipeline
				auto& pipeline = *m_pipelines.at(objectsWithPipeline.first());
				auto localModelIndex = which - iterativeModelsCout;
				auto& pipelineModel = objectsWithPipeline.second[localModelIndex];
				auto modelDescriptorSet = getObjectDescriptorSet(pipelineModel).lock();
				cb.bindPipeline(pipeline);
				cb.bindDescriptorSet(*modelDescriptorSet, m_renderer.lock()->getDescriptorSetNumber(Renderer::ResourceFrequency::Object), pipeline.getPipelineLayout());
				pipelineModel.drawable->record(cb);
				break;
			}
			iterativeModelsCout += modelCount;
		}
	}

	uint32_t MaterialBatch::pipelineCount() const
	{
		uint32_t count = 0;
		for (auto& pipeline : m_pipelines)
		{
			if (pipeline)
				++count;
		}
		return count;
	}

	uint32_t MaterialBatch::objectCount() const
	{
		uint32_t count = 0;
		for (auto& pipelineWithObjects : m_pipelinesWithObjects)
		{
			count += pipelineWithObjects.second.size();
		}
		return count;
	}

	std::weak_ptr<libVulkan::VulkanPipeline> MaterialBatch::getMaterialCompatiblePipeline() const
	{
		if (!m_pipelines.empty())
			return m_pipelines.front();
		return std::weak_ptr<libVulkan::VulkanPipeline>();
	}

	void MaterialBatch::resolutionChanged(vk::Extent2D const& resolution)
	{
		for (auto& pipeline : m_pipelines)
		{
			if (pipeline)
			{
				pipeline->setViewPort(resolution);
				pipeline->compilePipeline();
			}				
		}
		bindObjectResources();
	}

	std::weak_ptr<libVulkan::VulkanDescriptorSet> MaterialBatch::getMaterialDescriptorSet()
	{
		if (!m_materialDescriptorSet.lock())
		{// in theory can be freed by pool, so if thats the case, reallocate it
			m_materialDescriptorSet = m_renderer.lock()->createDescriptorSet(m_materialLayout->createMaterialDescriptorSetLayout(), Renderer::ResourceFrequency::Material, "Material DS");
		}
		return m_materialDescriptorSet;
	}

	MaterialLayout& MaterialBatch::getMaterialLayout()
	{
		return *m_materialLayout;
	}

	void MaterialBatch::bufferObjectsData()
	{
		for (auto& pipelineWithObjects : m_pipelinesWithObjects)
		{
			for (auto& objectStuct : pipelineWithObjects.second)
			{
				auto& object = objectStuct.drawable;
				m_materialLayout->bufferObjectResources(object->getResources());
			}
		}
	}

	MaterialBatch::PipelineKey MaterialBatch::createAndAddPipeline(RenderComponent const& drawable, MaterialLayout const& materialLayout)
	{
		auto retval = PipelineKey(-1);
		auto renderer = m_renderer.lock();
		auto instance = renderer->getInstance().lock();
		auto device = renderer->getDevice().lock();

		auto descriptorSetLayouts = std::vector<std::shared_ptr<libVulkan::VulkanDescriptorSetLayout>>();
		descriptorSetLayouts.push_back(renderer->createRendererDescriptorSetLayout());
		descriptorSetLayouts.push_back(m_passDescriptorSetLayout);
		descriptorSetLayouts.push_back(materialLayout.createMaterialDescriptorSetLayout());
		descriptorSetLayouts.push_back(materialLayout.createObjectDescriptorSetLayout());

		//use default resolution
		auto& defaults = instance->getDefaults();

		// do not use push constants
		auto pipelineLayout = std::make_shared<libVulkan::VulkanPipelineLayout>(instance, device, descriptorSetLayouts, vk::ShaderStageFlags{}, drawable.getName());
		auto pipeline = materialLayout.createPipeline(pipelineLayout, { defaults.frameBufferResolution.x, defaults.frameBufferResolution.y });

		//todo: now pipeline can be setted up according to material - once theese infos are in material, then here it should be setup
		{
		}

		// since pipelines are stored in vector, and when removing pipelines, it does not get shrunk, so use the potential free space first
		for (int32_t pipelineIndex = 0; pipelineIndex < m_pipelines.size(); ++pipelineIndex)
		{
			if (!m_pipelines.at(pipelineIndex))
			{
				m_pipelines[pipelineIndex] = pipeline;
				retval = { pipelineIndex };
			}
		}
		if (retval() == -1 )
		{
			m_pipelines.push_back(pipeline);
			retval = { int32_t(m_pipelines.size() - 1) };
		}
		return retval;
	}

	MaterialBatch::PipelineKey MaterialBatch::findCompatiblePipelineWithRenderComponent(MaterialLayout const& materialLayout) const
	{		
		auto descriptorSetLayouts = std::vector<std::shared_ptr<libVulkan::VulkanDescriptorSetLayout>>{ materialLayout.createObjectDescriptorSetLayout() };
		for (int32_t i = 0; i < m_pipelines.size(); ++i)
		{
			if (m_pipelines.at(i)->getPipelineLayout().isCompatible(descriptorSetLayouts))
			{
				return PipelineKey{ i };
			}
		}
		return PipelineKey{ -1 };
	}

	std::weak_ptr<libVulkan::VulkanDescriptorSet> MaterialBatch::createDescriptorSetForRenderComponent(RenderComponent const& drawable, Renderer& renderer)
	{
		return renderer.createDescriptorSet(m_materialLayout->createObjectDescriptorSetLayout(), Renderer::ResourceFrequency::Object, "Object DS");
	}

	//! OBJECT struct member methods

	MaterialBatch::Object::Object()
		: key({ -1 })
	{
	}

	MaterialBatch::Object::Object(RenderComponentKey key, std::shared_ptr<RenderComponent> drawable, std::weak_ptr<libVulkan::VulkanDescriptorSet> objectDescriptorSet)
		: key(key)
		, drawable(drawable)
		, objectDescriptorSet(objectDescriptorSet)
	{
	}

	bool MaterialBatch::Object::contains(std::shared_ptr<RenderComponent> const& r) const
	{
		return drawable == r;
	}

	bool MaterialBatch::Object::valid() const
	{
		return key() != -1;
	}

	void MaterialBatch::Object::destroy(std::shared_ptr<Renderer> const& renderer)
	{
		key = RenderComponentKey{ -1 };
		drawable = std::shared_ptr<RenderComponent>();
		renderer->destroyDescriptorSet(objectDescriptorSet, Renderer::ResourceFrequency::Object);
		objectDescriptorSet = std::shared_ptr<libVulkan::VulkanDescriptorSet>();
	}
}