//! libCore
#include <libCore/Scene.hpp>
#include <libCore/Renderer.hpp>
#include <libCore/MaterialBatch.hpp>
#include <libCore/MaterialPhongNormalMapLayout.hpp>
#include <libCore/MaterialPhongLayout.hpp>
#include <libCore/MaterialUnshadedLayout.hpp>
#include <libCore/MaterialUnshadedTexturedLayout.hpp>
#include <libCore/MaterialPBRLayout.hpp>
#include <libCore/MaterialPBRTexturedLayout.hpp>
#include <libCore/MaterialAABBLayout.hpp>
#include <libCore/TransformGraphComponent.hpp>
#include <libCore/AABB.hpp>
#include <libCore/RenderingLink.hpp>
//! libInputs
#include <libCore/Camera.hpp>
//! libVulkan
#include <libVulkan/VulkanPipeline.hpp>
#include <libVulkan/VulkanDescriptorSet.hpp>
#include <libVulkan/VulkanDescriptorSetLayout.hpp>
#include <libVulkan/VulkanCommandBuffer.hpp>


namespace libCore
{
	Scene::Scene(std::shared_ptr<Renderer> const& renderer, std::string const& name)
		: m_renderer(renderer)
		, m_materialResources()
		, m_name(name)
		, m_aabb(std::make_shared<AABB>())
		, m_renderAABB(false)
	{
		m_materialResources = std::make_shared<ShaderResources>(renderer->getInstance().lock(), renderer->getDevice().lock());

		if (m_name.empty())
			assert(!"Name cannot be empty");
		createAABBRenderComponent();
	}

	void Scene::removeComponentFromEntity(Entity const& entity, std::shared_ptr<RenderComponent> component)
	{
		auto materialType = component->getMaterialType();
		if (m_batches.count(materialType) > 0)
		{//in case that the batch is empty, clean it
			if (m_batches.at(materialType)->removeObject(component))
				m_batches.erase(materialType);
		}	
		EntityManager::removeComponentFromEntity(entity, component);
	}

	void Scene::setName(std::string const& name)
	{
		if (name.empty())
			assert(!"Name cannot be empty");
		m_name = name;
		//update all instances of all entities within graph to reflect name change TODO: problem that if multiple scenes use same graph
	}

	std::string Scene::getName() const
	{
		return m_name;
	}

	void Scene::removeEntity(std::weak_ptr<Entity> entity)
	{
		//for drawable entity this should be removed from the drawbatch
		auto locked = entity.lock();
		if (hasComponent<RenderComponent>(*locked))
		{
			auto drawable = getComponent<RenderComponent>(*locked).lock();
			auto materialType = drawable->getMaterialType();
			if (m_batches.count(materialType) > 0)
			{//in case that the batch is empty, clean it
				if (m_batches.at(materialType)->removeObject(drawable))
					m_batches.erase(materialType);
			}	
		}
		EntityManager::removeEntity(entity);
	}

	void Scene::getCameras(std::vector<std::shared_ptr<libCore::Camera>>& camerasOut)
	{
		camerasOut.clear();
		getAllComponents(camerasOut);
	}

	void Scene::recordAll(libVulkan::VulkanCommandBuffer& commandBuffer, RenderingLink& pass)
	{
		{//add object to drawbatch which arent in drawbatch, they will asign data adn be ready to get recorded
			auto entities = std::vector<std::shared_ptr<Entity>>();
			getEntitiesWithComponents<RenderComponent>(entities);
			for (auto& entity : entities)
			{
				auto drawable = getComponent<RenderComponent>(*entity).lock();
				//basically must exist so no need to check
				if (!inBatch(drawable))
				{
					assignToBatch(drawable, pass);
				}
			}			
		}

		{ //first off, bind renderer and pass sets - and since all pipelines in scene are compatible, any one will do 
			auto pipeline = getArbitraryPipeline().lock();
			if (!pipeline)
				return;
			auto rendererDs = m_renderer.lock()->getRendererDescriptorSet().lock();
			commandBuffer.bindDescriptorSet(*rendererDs, m_renderer.lock()->getDescriptorSetNumber(Renderer::ResourceFrequency::Renderer), pipeline->getPipelineLayout());
			commandBuffer.bindDescriptorSet(pass.getDescriptorSet(), m_renderer.lock()->getDescriptorSetNumber(Renderer::ResourceFrequency::RenderPass), pipeline->getPipelineLayout());
		}
		//now record for all batches
		for (auto& batchPair : m_batches)
		{
			auto& batch = batchPair.second;
			assert(batch->objectCount() > 0);
			//now the pipeline must be compatible with this material - to need to get arbitrary for this material's draw batch
			auto compatiblePipeline = batch->getMaterialCompatiblePipeline().lock();
			auto materialDescriptorSet = batch->getMaterialDescriptorSet().lock();
			commandBuffer.bindDescriptorSet(*materialDescriptorSet, m_renderer.lock()->getDescriptorSetNumber(Renderer::ResourceFrequency::Material), compatiblePipeline->getPipelineLayout());
			batch->recordBuffer(commandBuffer);
		}
	}
	
	void Scene::recordSingle(libVulkan::VulkanCommandBuffer& commandBuffer, uint32_t which, RenderingLink& link)
	{
		{//add object to drawbatch which arent in drawbatch, they will asign data adn be ready to get recorded
			auto entities = std::vector<std::shared_ptr<Entity>>();
			getEntitiesWithComponents<RenderComponent>(entities);
			for (auto& entity : entities)
			{
				auto drawable = getComponent<RenderComponent>(*entity).lock();
				//basically must exist so no need to check
				if (!inBatch(drawable))
				{
					assignToBatch(drawable, link);
				}
			}
		}

		{ //first off, bind renderer and pass sets - and since all pipelines in scene are compatible, any one will do 
			auto pipeline = getArbitraryPipeline().lock();
			if (!pipeline)
				return;
			auto rendererDs = m_renderer.lock()->getRendererDescriptorSet().lock();
			commandBuffer.bindDescriptorSet(*rendererDs, m_renderer.lock()->getDescriptorSetNumber(Renderer::ResourceFrequency::Renderer), pipeline->getPipelineLayout());
			commandBuffer.bindDescriptorSet(link.getDescriptorSet(), m_renderer.lock()->getDescriptorSetNumber(Renderer::ResourceFrequency::RenderPass), pipeline->getPipelineLayout());
		}

		auto iterativeModelsCout = uint32_t(0);
		//now record for all batches - they are sorted by material 
		for (auto& batchPair : m_batches)
		{
			auto& batch = batchPair.second;
			auto batchModelCount = batch->objectCount();
			if (iterativeModelsCout + batchModelCount > which)
			{ //command buffer will record this batch
				//now the pipeline must be compatible with this material - to need to get arbitrary for this material's draw batch
				auto compatiblePipeline = batch->getMaterialCompatiblePipeline().lock();
				auto materialDescriptorSet = batch->getMaterialDescriptorSet().lock();
				commandBuffer.bindDescriptorSet(*materialDescriptorSet, m_renderer.lock()->getDescriptorSetNumber(Renderer::ResourceFrequency::Material), compatiblePipeline->getPipelineLayout());
				batch->recordSingleBuffer(commandBuffer, which - iterativeModelsCout);
				break;
			}
			iterativeModelsCout += batch->objectCount();
		}
	}

	void Scene::resolutionChanged(vk::Extent2D const& resolution)
	{
		auto renderer = m_renderer.lock();
		bindMaterialsResources();
		for (auto& batchPair: m_batches)
		{
			auto& batch = batchPair.second;
			batch->resolutionChanged(resolution);
		}
	}

	std::weak_ptr<libVulkan::VulkanDescriptorSet> Scene::createMaterialDescriptorSet(MaterialLayout const& material) const
	{
		auto renderer = m_renderer.lock();
		return renderer->createDescriptorSet(material.createMaterialDescriptorSetLayout(), Renderer::ResourceFrequency::Material, "Material Frequency");
	}

	void Scene::assignToBatch(std::shared_ptr<RenderComponent> const& renderComponent, RenderingLink& pass)
	{
		//in case, that there is no batch of given type, create material resources
		auto objectMaterialType = renderComponent->getMaterialType();
		if (m_batches.count(objectMaterialType) == 0)
		{
			auto materialLayout = createMaterialLayout(objectMaterialType);
			auto batch = std::make_shared<MaterialBatch>(m_renderer.lock(), pass.getDescriptorSetLayout(), materialLayout);
			auto materialDescriptorSet = batch->getMaterialDescriptorSet().lock();
			materialLayout->allocateMaterialData(*m_materialResources);
			materialLayout->bindMaterialUniformData(*materialDescriptorSet, *m_materialResources);
			m_batches[objectMaterialType] = batch;
		}
		//add the object
		m_batches.at(objectMaterialType)->addObject(renderComponent);
	}

	bool Scene::inBatch(std::shared_ptr<RenderComponent> const& drawable)
	{
		auto materialType = drawable->getMaterialType();
		if (m_batches.count(materialType) > 0)
		{
			return m_batches.at(materialType)->hasObject(drawable);
		}
		return false;
	}

	void Scene::bindMaterialsResources()
	{
		for (auto& batchPair: m_batches)
		{
			auto& batch = batchPair.second;
			auto& materialLayout = batch->getMaterialLayout();
			auto materialDescriptorSet = batch->getMaterialDescriptorSet().lock();
			materialLayout.bindMaterialUniformData(*materialDescriptorSet, *m_materialResources);
		}		
	}

	ShaderResources& Scene::getMaterialResources()
	{
		return *m_materialResources;
	}

	void Scene::getRenderEntities(std::vector<std::shared_ptr<libCore::Entity>>& entities, bool mustHaveGraph) const
	{
		if (mustHaveGraph)
			getEntitiesWithComponents<libCore::RenderComponent, libCore::TransformGraphComponent>(entities);
		else 
			getEntitiesWithComponents<libCore::RenderComponent>(entities);
	}

	std::shared_ptr<libCore::AABB>& Scene::getAABB()
	{
		return m_aabb;
	}

	std::shared_ptr<MaterialLayout> Scene::createMaterialLayout(MaterialLayout::MaterialLayoutType materialTypeIndex) const
	{
		auto renderer = m_renderer.lock();
		auto instance = renderer->getInstance().lock();
		auto device = renderer->getDevice().lock();

		switch (materialTypeIndex)
		{
		case libCore::MaterialLayout::UNSHADED:
			return std::make_shared<MaterialUnshadedLayout>(instance, device);
			break;
		case libCore::MaterialLayout::UNSHADED_TEXTURED:
			return std::make_shared<MaterialUnshadedTexturedLayout>(instance, device);
			break;
		case libCore::MaterialLayout::PHONG:
			return std::make_shared<MaterialPhongLayout>(instance, device);
			break;
		case libCore::MaterialLayout::PHONG_NORMAL_MAP:
			return std::make_shared<MaterialPhongNormalLayout>(instance, device);
			break;
		case libCore::MaterialLayout::PBR:
			return std::make_shared<MaterialPBRLayout>(instance, device);
			break;
		case libCore::MaterialLayout::PBR_TEXTURED:
			return std::make_shared<MaterialPBRTexturedLayout>(instance, device);
			break;
		case libCore::MaterialLayout::AABB:
			return std::make_shared<MaterialAABBLayout>(instance, device);
			break;
		case libCore::MaterialLayout::COUNT:
			break;
		default:
			assert("Update material enum");
			break;
		}
		return std::shared_ptr<MaterialLayout>();
	}

	std::weak_ptr<libVulkan::VulkanPipeline> Scene::getArbitraryPipeline()
	{
		if (m_batches.empty())
		{
			assert(!"Batches are mepty, therefore cannot get any pipeline");
			return std::shared_ptr<libVulkan::VulkanPipeline>();
		}
		return m_batches.begin()->second->getMaterialCompatiblePipeline().lock();
	}
		
	void Scene::bufferMaterialAndObjectsData()
	{
		for (auto& batchPair: m_batches)
		{
			auto& batch = batchPair.second;
			auto& batchMaterialLayout = batch->getMaterialLayout();
			//buffer material resources
			batchMaterialLayout.bufferMaterialResources(*m_materialResources);
			//buffer object resources
			batch->bufferObjectsData();
		}		
	}

	void Scene::bufferMaterialData()
	{
		for (auto& batchPair : m_batches)
		{
			auto& batch = batchPair.second;
			auto& batchMaterialLayout = batch->getMaterialLayout();
			//buffer material resources
			batchMaterialLayout.bufferMaterialResources(*m_materialResources);
		}
	}

	void Scene::bindObjectResources(MaterialLayout::MaterialLayoutType layoutType)
	{
		if (m_batches.count(layoutType) != 0)
			m_batches.at(layoutType)->bindObjectResources();
	}

	uint32_t Scene::getRenderComponentObjectCount() const
	{
		auto drawableEntities = std::vector<std::shared_ptr<Entity>>();
		getEntitiesWithComponents<RenderComponent>(drawableEntities);
		return drawableEntities.size();
	}

	void Scene::createAABBRenderComponent()
	{
		auto renderer = m_renderer.lock();
		auto instance = renderer->getInstance().lock();
		auto device = renderer->getDevice().lock();

		m_aabbEntity = createEntity("AABB").lock();
		auto materialLayout = std::make_shared<MaterialAABBLayout>(instance, device);
		m_aabbRenderComponent = std::make_shared<RenderComponent>(renderer, materialLayout, 1, 16);
		
		auto& resources = m_aabbRenderComponent->getResources();
		materialLayout->setObjectAABBMin(m_aabb->getMin(), resources, resources.getBaseInstance());
		materialLayout->setObjectAABBMax(m_aabb->getMax(), resources, resources.getBaseInstance());
		materialLayout->setObjectColor(glm::vec3(1.f, 0.f, 0.f), resources, resources.getBaseInstance());
	}

	void Scene::setRenderAABB(bool enabled)
	{
		if (enabled)
		{
			auto aabbId = m_aabbEntity.lock();
			addComponentToEntity(*aabbId, m_aabbRenderComponent);
			m_renderAABB = true;
		}
		else
		{
			auto aabbId = m_aabbEntity.lock();
			removeComponentFromEntity(*aabbId, m_aabbRenderComponent);
			m_renderAABB = false;
		}
	}

	bool Scene::getRenderAABB() const
	{
		return m_renderAABB;
	}


	void Scene::setTransform(std::shared_ptr<Entity> const& entity, glm::mat4 const& matrix, uint32_t instance)
	{
		if (hasComponent<TransformGraphComponent>(*entity))
		{
			auto transformComponent = getComponent<TransformGraphComponent>(*entity).lock();
			transformComponent->setLocalMatrix(matrix, instance);
			return;
		}
		if (hasComponent<RenderComponent>(*entity))
		{
			auto renderComponent = getComponent<RenderComponent>(*entity).lock();
			auto instanceHandle = renderComponent->getResources().getBaseInstance();
			instanceHandle.toInstance(instance);
			renderComponent->getResources().setInstanceResource("MODEL_MATRIX", matrix, instanceHandle);
			return;
		}
	}

	glm::mat4 Scene::getTransform(std::shared_ptr<Entity> const& entity, uint32_t instance) const
	{
		if (hasComponent<TransformGraphComponent>(*entity))
		{
			auto transformComponent = getComponent<TransformGraphComponent>(*entity).lock();
			return transformComponent->getLocalMatrix(instance);
		}
		if (hasComponent<RenderComponent>(*entity))
		{
			auto renderComponent = getComponent<RenderComponent>(*entity).lock();
			auto instanceHandle = renderComponent->getResources().getBaseInstance();
			instanceHandle.toInstance(instance);

			return renderComponent->getResources().getInstanceResource<glm::mat4>("MODEL_MATRIX", instanceHandle);
		}
		return glm::mat4(1.f);
	}

	int32_t Scene::addInstance(std::shared_ptr<Entity> const& entity)
	{
		int32_t newInstance = 0;
		if (hasComponent<TransformGraphComponent>(*entity))
		{// within the app, notation is used like so: /scene/OBJ_NAME/instanceNumber so object X with 2 instances will look like:
			// /scene/X/0 and /scene/X/1
			auto transformComponent = getComponent<TransformGraphComponent>(*entity).lock();
			newInstance = transformComponent->getInstanceCount();
			auto sceneName = getName();
			if (sceneName.empty())
				assert(!"Cannot have scene name empty");
			if (entity->getDisplayName().empty())
				assert(!"Cannot have entity name empty");

			std::string path = std::string("/") + sceneName + "/" + entity->getDisplayName() + "/" + std::to_string(newInstance);
			transformComponent->addInstance(path, glm::mat4(1.f));
		}
		if (hasComponent<RenderComponent>(*entity))
		{
			auto renderComponent = getComponent<RenderComponent>(*entity).lock();
			newInstance = renderComponent->addInstance().index;

			m_aabbRenderComponent->addInstance();
		}
		return newInstance;
	}

	void Scene::removeInstance(std::shared_ptr<Entity> const& entity)
	{
		if (hasComponent<TransformGraphComponent>(*entity))
		{
			auto transformComponent = getComponent<TransformGraphComponent>(*entity).lock();
			transformComponent->removeInstance();
		}
		if (hasComponent<RenderComponent>(*entity))
		{
			auto renderComponent = getComponent<RenderComponent>(*entity).lock();
			renderComponent->removeInstance();
			m_aabbRenderComponent->removeInstance();
		}
	}

	void Scene::updateAABBRenderComponent()
	{
		//update all instances of AABB, since instance count matches all the instances of all RenderComponents within this scene,
		//just iterate all over them, take their AABB min and max and Model matrices and buffer that info to each AABB instance
		auto materialLayout = std::dynamic_pointer_cast<MaterialAABBLayout>(m_aabbRenderComponent->getMaterialLayout());
		auto renderComponents = std::vector<std::shared_ptr<RenderComponent>>();
		getAllComponents<RenderComponent>(renderComponents);
		auto& aabbResources = m_aabbRenderComponent->getResources();
		auto aabbInstance = aabbResources.getBaseInstance();
		
		auto sceneAABB = AABB();

		for (auto& renderComponent : renderComponents)
		{
			if (renderComponent == m_aabbRenderComponent)
				continue; //dont handle the main aabb component as that would be recursive and it does not have aabb
			auto& renderComponentResources = renderComponent->getResources();
			auto renderComponentInstance = renderComponentResources.getBaseInstance();
			auto instanceCount = renderComponent->getInstanceCount();
			for (auto i = 0; i < instanceCount; ++i)
			{
				auto instanceAABB = renderComponent->getAABB(i);
				if (!instanceAABB)
					continue;
				materialLayout->setObjectAABBMin(instanceAABB->getMin(), aabbResources, aabbInstance);
				materialLayout->setObjectAABBMax(instanceAABB->getMax(), aabbResources, aabbInstance);
				materialLayout->setObjectColor(renderComponent->getAABBColor(i), aabbResources, aabbInstance);
				renderComponentInstance.nextInstance();
				aabbInstance.nextInstance();

				sceneAABB.expand(*instanceAABB);
			}
		}
		//final resource is the whole scene aabb
		materialLayout->setObjectAABBMin(sceneAABB.getMin(), aabbResources, aabbInstance);
		materialLayout->setObjectAABBMax(sceneAABB.getMax(), aabbResources, aabbInstance);

		bindObjectResources(libCore::MaterialAABBLayout::MaterialLayoutType::AABB);
	}
}