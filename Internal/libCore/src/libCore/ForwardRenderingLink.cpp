//! libCore
#include <libCore/ForwardRenderingLink.hpp>
#include <libCore/Renderer.hpp>
#include <libCore/ShaderResources.hpp>
#include <libCore/Scene.hpp>
//! libInputs
#include <libCore/Camera.hpp>
//! libVulkan
#include <libVulkan/VulkanAll.hpp>
//glm
#include <glm/matrix.hpp>

namespace libCore
{
	ForwardRenderingLink::ForwardRenderingLink(std::shared_ptr<Renderer> const& renderer, std::shared_ptr<libVulkan::VulkanRenderingInfo> const& renderingInfo, std::shared_ptr<libCore::Camera> const& camera)
		: RenderingLink(renderer, renderingInfo)
		, m_resources(std::make_shared<ShaderResources>(renderer->getInstance().lock(), renderer->getDevice().lock()))
		, m_camera(camera)
	{
		setUpResources();
	}

	std::shared_ptr<libVulkan::VulkanDescriptorSetLayout> ForwardRenderingLink::getDescriptorSetLayout() const
	{
		auto renderer = m_renderer.lock();
		auto instance = renderer->getInstance().lock();
		auto device = renderer->getDevice().lock();

		auto setLayoutBinding = vk::DescriptorSetLayoutBinding();
		{
			setLayoutBinding.binding = 0;
			setLayoutBinding.descriptorCount = 1;
			setLayoutBinding.descriptorType = vk::DescriptorType::eStorageBuffer;
			setLayoutBinding.stageFlags = vk::ShaderStageFlagBits::eVertex | vk::ShaderStageFlagBits::eFragment;
			setLayoutBinding.pImmutableSamplers = nullptr;
		}
		return std::make_shared<libVulkan::VulkanDescriptorSetLayout>(instance, device, std::vector<vk::DescriptorSetLayoutBinding>{ setLayoutBinding }, "Forward renrering link");
	}

	void ForwardRenderingLink::bindUniformData()
	{
		auto& passDescriptorSet = getDescriptorSet();
		//just to create resources if not present - they willget updated correctly
		m_resources->bufferResources({ "VIEW_MATRIX", "PROJECTION_MATRIX" }, "PASS_BUFFER");
		m_resources->useBuffersWithSet({ "PASS_BUFFER" }, passDescriptorSet);
	}

	void ForwardRenderingLink::setUpResources()
	{
		auto instance = m_resources->getBaseInstance();
		m_resources->setInstanceResource("VIEW_MATRIX", glm::mat4(1.f), instance);
		m_resources->setInstanceResource("PROJECTION_MATRIX", glm::mat4(1.f), instance);
		bindUniformData();
	}

	void ForwardRenderingLink::bufferResources()
	{
		auto instance = m_resources->getBaseInstance();
		m_resources->setInstanceResource("VIEW_MATRIX", m_camera->getViewMatrix(), instance);
		m_resources->setInstanceResource("PROJECTION_MATRIX", m_camera->getProjectionMatrix(), instance);
		m_resources->bufferResources({ "VIEW_MATRIX", "PROJECTION_MATRIX" }, "PASS_BUFFER");
	}
	
	void ForwardRenderingLink::resolutionChanged(vk::Extent2D const& resolution)
	{
		createAttachments(resolution);
		bindUniformData();
	}

	void ForwardRenderingLink::setCamera(std::shared_ptr<libCore::Camera> const& camera)
	{
		m_camera = camera;
	}

	std::shared_ptr<libCore::Camera> const& ForwardRenderingLink::getCamera() const
	{
		return m_camera;
	}

	void ForwardRenderingLink::createDescriptorSet()
	{
		auto renderer = m_renderer.lock();
		m_descriptorSet = renderer->createDescriptorSet(getDescriptorSetLayout(), Renderer::ResourceFrequency::RenderPass, "Forward rendering DS");
	}

	libVulkan::VulkanDescriptorSet& ForwardRenderingLink::getDescriptorSet()
	{
		auto descriptorSet = m_descriptorSet.lock();
		// could have been freed by renderer, in that case, its needed to ask for new one
		if (!descriptorSet)
		{
			createDescriptorSet();
			descriptorSet = m_descriptorSet.lock();
		}
		return *descriptorSet;
	}


}