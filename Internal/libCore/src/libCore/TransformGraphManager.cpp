//! libCore
#include <libCore/TransformGraphManager.hpp>
#include <libCore/TransformGraphComponent.hpp>
#include <libCore/Scene.hpp>
//! libUtils
#include <libUtils/Utils.hpp>
//! std
#include <algorithm>
#include <stack>

namespace libCore
{
	TransformGraphManager::TransformGraphManager()
		: m_genId(0)
	{
		addNode("/", glm::mat4(1.f), true);
		update(m_tree.begin()->second);
	}

	void TransformGraphManager::processScene(libCore::Scene& scene)
	{
		//get all transform components
		auto transformComponents = std::vector<std::shared_ptr<TransformGraphComponent>>();
		scene.getAllComponents<TransformGraphComponent>(transformComponents);
		for (auto& transformComponent: transformComponents)
		{//process instances
			{//firstly remove the needed stuff
				for (auto& erasePath : transformComponent->m_toErase)
				{
					clear(erasePath);
				}
				transformComponent->m_toErase.clear();
			}

			auto instanceCount = transformComponent->getInstanceCount();
			for (int i = 0; i < instanceCount; ++i)
			{//set the local matrix into graph and get the world path back
				set(transformComponent->getLocalNodePath(i), transformComponent->getLocalMatrix(i));
				transformComponent->setWorldMatrix(wordMatrix(transformComponent->getLocalNodePath(i)), i);
			}
		}
	}

	std::string TransformGraphManager::toString(std::string const& path, bool withChildren)
	{
		auto string = std::string("");
		auto node = getNode(path, ValidSettings::BOTH);
		if (!node)
			return ("Nonexistent path");
		auto stack = std::stack<const Node*>({ node });
		while (!stack.empty())
		{
			//take current node
			auto item = stack.top();
			stack.pop();
			//process current node
			{
				string += "##########################\n";
				string += "Node:" + std::to_string(item->id) + "(" + item->fullPath + ")\n";
				string += "Valid:" + libUtils::toString(item->valid) + "\n";
				string += "LM:" + libUtils::toString(item->localMatrix) + "\n";
				auto parentNode = getParent(*item);
				if (parentNode)
				{
					string += "ParentNode:" + std::to_string(parentNode->id) + "\n";
				}
				string += "##########################\n";
			}
			//expand kids
			if (withChildren)
			{
				auto kids = std::vector<Node*>();
				children(item->fullPath, kids);
				for (auto& kid : kids)
				{
					stack.push(kid);
				}
			}
		}
		return string;
	}

	void TransformGraphManager::set(std::string const& path, glm::mat4 const& localMatrix)
	{
		if (isRoot(path))
			return;

		// in case there is an existing node
		auto node = getNode(path, ValidSettings::BOTH);
		// add it, and each subsequent subspace, update self with world matrix and also update the children children
		if (!node)
			node = &createSubSystems(path);
		// if node was found and is invalid, revalidate it
		if (!node->valid)
			node->valid = true;
		// and also update the local matrix
		node->localMatrix = localMatrix;
		// perform self world matrix update and all subsequent children's
		update(*node);
	}

	void TransformGraphManager::clear(std::string const& path)
	{
		auto node = getNode(path, ValidSettings::VALID);
		if (!node)
			return;
		node->valid = false;
		clean(*node);
	}
	
	glm::mat4 TransformGraphManager::wordMatrix(std::string const& path) const
	{
		auto node = getNode(path, ValidSettings::VALID);
		if (!node)
			return glm::mat4(1.f);
		return node->worldMatrix;
	}
	
	glm::mat4 TransformGraphManager::matrix(std::string const& path) const
	{
		auto node = getNode(path, ValidSettings::VALID);
		if (!node)
			return glm::mat4(1.f);
		return node->localMatrix;
	}
	
	glm::mat4 TransformGraphManager::matrix(std::string const& sourcePath, std::string const& destinationPath) const
	{
		// TODO
		return glm::mat4();
	}
	
	bool TransformGraphManager::pathExists(std::string const& path) const
	{
		return m_tree.count(path) > 0;
	}
	
	TransformGraphManager::Node& TransformGraphManager::addNode(std::string const& path, glm::mat4 const& localMatrix, bool valid)
	{
		m_tree[path] = Node(path, localMatrix, m_genId, valid);
		++m_genId;
		return m_tree.at(path);
	}

	TransformGraphManager::Node& TransformGraphManager::createSubSystems(std::string const& path)
	{
		//here, will create all nodes for subsystems
		auto subsystems = std::vector<std::string>();
		Node* retval = nullptr;
		libUtils::splitString(path, "/", subsystems);
		auto newPath = std::string("/");
		for (auto& subsystem: subsystems)
		{
			newPath += subsystem;
			auto node = getNode(newPath, ValidSettings::BOTH);
			if (!node)
				node = &addNode(newPath, glm::mat4(1.f), false);
			newPath += "/";
			retval = node;
		}
		//note, that createSubSystems allways succedess if not something is wrong
		assert(retval);
		return *retval;
	}

	void TransformGraphManager::children(std::string const& path, std::vector<NODE*>& children)
	{
		children.clear();
		//special handling of the root - all are children
		if (path == "/")
		{
			for (auto& node : m_tree)
			{
				if (node.first == "/")
					continue;
				if (std::count(node.first.begin(), node.first.end(), '/') == 1)
					children.push_back(&node.second);
			}
			return;
		}

		// find all paths which have +1 delimeter than this path, then do a match against, must return substring
		for (auto& node : m_tree)
		{
			auto result = std::vector<std::string>();
			libUtils::splitString(node.first, path, result);
			
			if ((!result.empty()) && std::count_if(result.front().begin(), result.front().end(), [](char c) {return c == '/'; }) == 1)
			{
				children.push_back(&node.second);
			}
		}
	}

	TransformGraphManager::Node* TransformGraphManager::getParent(Node const& node)
	{
		auto& path = node.fullPath;
		if((!pathExists(path)) || isRoot(path))
			return nullptr;
		auto key = path.substr(0, path.find_last_of("/"));
		if (key.empty())
			return nullptr;
		return &m_tree.at(key);
	}

	void TransformGraphManager::update(NODE& node)
	{
		auto stack = std::stack<Node*>({ &node });
		while (!stack.empty())
		{
			//take current node
			auto item = stack.top();
			stack.pop();
			//process current node
			{
				auto parentNode = getParent(*item);
				if (!parentNode)
					item->worldMatrix = item->localMatrix;
				else
					item->worldMatrix = parentNode->worldMatrix * item->localMatrix;
			}
			//expand kids
			auto kids = std::vector<Node*>();
			children(item->fullPath, kids);
			for (auto& kid : kids)
			{
				stack.push(kid);
			}
		}
	}

	void TransformGraphManager::clean(NODE& node)
	{
		// TODO
	}

	TransformGraphManager::Node* TransformGraphManager::getNode(std::string const& path, ValidSettings valid)
	{
		if (m_tree.count(path) <= 0)
			return nullptr;
		auto node = &m_tree.at(path);
		if (valid == ValidSettings::BOTH || (valid == ValidSettings::VALID && node->valid) || (valid == ValidSettings::INVALID && !node->valid))
			return node;
		return nullptr;
	}

	TransformGraphManager::Node const* TransformGraphManager::getNode(std::string const& path, ValidSettings valid) const
	{
		if (m_tree.count(path) <= 0)
			return nullptr;
		auto node = &m_tree.at(path);
		if (valid == ValidSettings::BOTH || (valid == ValidSettings::VALID && node->valid) || (valid == ValidSettings::INVALID && !node->valid))
			return node;
		return nullptr;
	}

	void TransformGraphManager::DFS(Node& node, std::function<void(Node&)> evalFunc)
	{
		auto stack = std::stack<Node*>({ &node });
		while (!stack.empty())
		{
			auto item = stack.top();
			stack.pop();

			auto kids = std::vector<Node*>();
			children(item->fullPath, kids);
			for (auto& kid: kids)
			{
				stack.push(kid);
			}
			if (kids.empty())
			{
				evalFunc(*item);
			}
		}		
	}

	bool TransformGraphManager::isRoot(std::string const& path) const
	{
		return path == "/";
	}
	
	std::string TransformGraphManager::NODE::parentPath() const
	{
		if (fullPath == "/")
			return "";
		return fullPath.substr(0, fullPath.find_last_of("/"));
	}
	
	bool TransformGraphManager::NODE::isRoot() const
	{
		return fullPath == "/";
	}
	
	std::string TransformGraphManager::NODE::localPath() const
	{
		auto str = fullPath;
		auto retval = std::vector<std::string>();
		libUtils::splitString(str, "/", retval);
		if (retval.empty())
			return "/";
		return retval.back();
	}
}