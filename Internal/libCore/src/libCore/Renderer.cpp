//! libCore
#include <libCore/Renderer.hpp>
#include <libCore/ShaderResources.hpp>
//! libVulkan
#include <libVulkan/VulkanAll.hpp>

namespace libCore
{
	Renderer::Renderer(std::shared_ptr<libVulkan::VulkanInstance> const& instance, std::shared_ptr<libVulkan::VulkanDevice> const& device, std::shared_ptr<libVulkan::VulkanQueue> graphicsQue)
		: m_instance(instance)
		, m_device(device)
		, m_graphicsQueue(graphicsQue)
		, m_resources(std::make_shared<ShaderResources>(instance, device))
	{
		{// synchronization primitives
			m_sceneSyncSemaphore = std::make_shared<libVulkan::VulkanTimelineSemaphore>(instance, device, "Renderer timeline semaphore");
		}

		initializeCommandPools();
		initializeDescriptorPools();
		createRendererDescriptorSet();
		setUpResources();

		m_totalTimer.start();
	}

	Renderer::~Renderer()
	{
		for (auto& descriptorPool : m_descriptorPools)
		{
			descriptorPool->reset();
		}
	}

	std::weak_ptr<libVulkan::VulkanDevice> Renderer::getDevice() const
	{
		return m_device;
	}

	std::weak_ptr<libVulkan::VulkanInstance> Renderer::getInstance() const
	{
		return m_instance;
	}

	std::weak_ptr<libVulkan::VulkanCommandBuffer> Renderer::getDynamicCommandBuffer(vk::CommandBufferLevel level)
	{
		return getDynamicCommandPool().createCommandBuffer(level, "Dynamic CB");
	}

	void Renderer::recordAndSubmit(std::function<void(libVulkan::VulkanCommandBuffer& commandBuffer)> fn)
	{
		auto& inmidiatePool = getInmidiateCommandPool();
		auto commandBuffer = inmidiatePool.createCommandBuffer(vk::CommandBufferLevel::ePrimary, "OneUse CB").lock();
		commandBuffer->beginRecord(vk::CommandBufferUsageFlagBits::eOneTimeSubmit);
		fn(*commandBuffer);
		commandBuffer->endRecord();

		m_graphicsQueue.lock()->submit({ *commandBuffer });
		m_graphicsQueue.lock()->wait();
		// no need to reset, beginRecord will reset it implicitly
	}

	std::weak_ptr<libVulkan::VulkanCommandBuffer> Renderer::getPersistentCommandBuffer()
	{
		return m_persistentCommandBuffer;
	}

	void Renderer::releasePersistentCommandBuffer()
	{
		auto& persistentPool = getPersistentCommandPool();
		persistentPool.destroyCommandBuffer(m_persistentCommandBuffer.lock());
		
		m_persistentCommandBuffer = persistentPool.createCommandBuffer(vk::CommandBufferLevel::ePrimary, "Perisistent CB");
	}

	void Renderer::submit(libVulkan::VulkanCommandBuffer& commandBuffer)
	{
		m_graphicsQueue.lock()->submit({ commandBuffer });
		m_graphicsQueue.lock()->wait();
	}

	void Renderer::submit(std::vector<std::reference_wrapper<libVulkan::VulkanCommandBuffer>> const& commandBuffers)
	{
		//TODO: sync
		m_graphicsQueue.lock()->submit(commandBuffers);
		m_graphicsQueue.lock()->wait();
	}

	std::weak_ptr<libVulkan::VulkanDescriptorSet> Renderer::getRendererDescriptorSet()
	{
		auto lockedSet = m_rendererDescriptorSet.lock();
		if (!lockedSet)
		{// might be freed - renderer needed to create more sets than the pool supports, therefore recreate it
			createRendererDescriptorSet();
			lockedSet = m_rendererDescriptorSet.lock();
		}
		return lockedSet;
	}

	std::shared_ptr<libVulkan::VulkanDescriptorSetLayout> Renderer::createRendererDescriptorSetLayout() const
	{
		auto device = getDevice().lock();
		auto instance = getInstance().lock();

		auto setLayout = vk::DescriptorSetLayoutBinding();
		{
			setLayout.descriptorCount = 1;
			setLayout.descriptorType = vk::DescriptorType::eStorageBuffer;
			setLayout.stageFlags = vk::ShaderStageFlagBits::eAll;
			setLayout.binding = getDescriptorSetNumber(ResourceFrequency::Renderer);
		}
		return std::make_shared<libVulkan::VulkanDescriptorSetLayout>(instance, device, std::vector<vk::DescriptorSetLayoutBinding>{ setLayout }, "RendererDescriptorSetLayout");
	}

	void Renderer::finalizeFrame()
	{
		// for now comment it - would mean bind every frame... might be feasible
		//getObjectDescriptorPool().reset();
		//getMaterialDescriptorPool().reset();

		getDynamicCommandPool().freePool();

		//store time and reset timer
		auto ft = m_ftTimer.getTime();
		auto time = m_totalTimer.getTime();

		auto defaultInstance = m_resources->getBaseInstance();
		m_resources->setInstanceResource("TIME", time, defaultInstance);
		m_resources->setInstanceResource("FRAME_TIME", ft, defaultInstance);
		
		m_ftTimer.stop();
		m_ftTimer.start();
	}

	void Renderer::setUpResources()
	{
		auto defaultInstance = m_resources->getBaseInstance();
		m_resources->setInstanceResource("TIME", 0., defaultInstance);
		m_resources->setInstanceResource("FRAME_TIME", 0., defaultInstance);
		bindUniformData();
	}

	void Renderer::bindUniformData()
	{
		bufferResources();
		auto descriptorSet = getRendererDescriptorSet().lock();
		m_resources->useBuffersWithSet({ "RENDERER_BUFFER" }, *descriptorSet);
	}

	void Renderer::bufferResources()
	{
		m_resources->bufferResources({ "TIME", "FRAME_TIME" }, "RENDERER_BUFFER");
	}

	std::weak_ptr<libVulkan::VulkanDescriptorSet> Renderer::createDescriptorSet(std::shared_ptr<libVulkan::VulkanDescriptorSetLayout> setLayout, ResourceFrequency frequency, std::string const& name)
	{
		if (!setLayout)
			return std::shared_ptr<libVulkan::VulkanDescriptorSet>();
		return m_descriptorPools[getDescriptorSetNumber(frequency)]->createDescriptorSet(setLayout, name);
	}

	void Renderer::destroyDescriptorSet(std::weak_ptr<libVulkan::VulkanDescriptorSet> descriptorSet, ResourceFrequency frequency)
	{
		auto lockedSet = descriptorSet.lock();
		m_descriptorPools.at(getDescriptorSetNumber(frequency))->destroyDescriptorSet(lockedSet);
	}

	uint32_t Renderer::getDescriptorSetNumber(ResourceFrequency resourceFrequency) const
	{
		return uint32_t(resourceFrequency);
	}

	void Renderer::resolutionChanged(vk::Extent2D const&)
	{
		bindUniformData();
	}

	double Renderer::getTime() const
	{
		auto defaultInstance = m_resources->getBaseInstance();
		return m_resources->getInstanceResource<double>("TIME", defaultInstance);
	}

	double Renderer::getFrameTime() const
	{
		auto defaultInstance = m_resources->getBaseInstance();
		return m_resources->getInstanceResource<double>("FRAME_TIME", defaultInstance);
	}

	void Renderer::createRendererDescriptorSet()
	{
		auto layout = createRendererDescriptorSetLayout();
		m_rendererDescriptorSet = getRendererDescriptorPool().createDescriptorSet(layout, "Renderer descriptor set");
	}

	void Renderer::initializeCommandPools()
	{
		auto device = getDevice().lock();
		auto instance = getInstance().lock();
		auto queue = m_graphicsQueue.lock();
		//1 persistent cb - long lived, but want to allow just one buffer to be freed
		m_commandPools[0] = std::make_shared<libVulkan::VulkanCommandPool>(instance, device, *queue, true, false, "Persistent CB of Renderer");
		//2 dynamic cb pool - short lived (freed every frame as whole)
		m_commandPools[1] = std::make_shared<libVulkan::VulkanCommandPool>(instance, device, *queue, false, true, "Dynamic CB of Renderer");
		//3 one-time use cb - short lived (freed after job was done, its single)
		m_commandPools[2] = std::make_shared<libVulkan::VulkanCommandPool>(instance, device, *queue, true, true, "OneTime CB of Renderer");

		// get the persistent command buffer
		m_persistentCommandBuffer = getPersistentCommandPool().createCommandBuffer(vk::CommandBufferLevel::ePrimary, "Persistent CB");
	}

	void Renderer::initializeDescriptorPools()
	{
		createEngineDescriptorPool();
		createPassDescriptorPool();
		createMaterialDescriptorPool();
		createModelDescriptorPool();
	}

	libVulkan::VulkanCommandPool& Renderer::getDynamicCommandPool()
	{
		return *m_commandPools[1];
	}

	libVulkan::VulkanCommandPool& Renderer::getInmidiateCommandPool()
	{
		return *m_commandPools[2];
	}

	libVulkan::VulkanCommandPool& Renderer::getPersistentCommandPool()
	{
		return *m_commandPools[0];
	}

	libVulkan::VulkanDescriptorPool& Renderer::getRendererDescriptorPool()
	{
		return *m_descriptorPools[getDescriptorSetNumber(ResourceFrequency::Renderer)];
	}

	libVulkan::VulkanDescriptorPool& Renderer::getPassDescriptorPool()
	{
		return *m_descriptorPools[getDescriptorSetNumber(ResourceFrequency::RenderPass)];
	}

	libVulkan::VulkanDescriptorPool& Renderer::getMaterialDescriptorPool()
	{
		return *m_descriptorPools[getDescriptorSetNumber(ResourceFrequency::Material)];
	}

	libVulkan::VulkanDescriptorPool& Renderer::getObjectDescriptorPool()
	{
		return *m_descriptorPools[getDescriptorSetNumber(ResourceFrequency::Object)];
	}

	void Renderer::createEngineDescriptorPool()
	{
		auto device = getDevice().lock();
		auto instance = getInstance().lock();

		auto resourcesCount = uint32_t(1);
		auto uboPoolSize = vk::DescriptorPoolSize();
		{
			uboPoolSize.descriptorCount = resourcesCount;
			uboPoolSize.type = vk::DescriptorType::eUniformBuffer;
		}
		auto tboPoolSize = vk::DescriptorPoolSize();
		{
			tboPoolSize.descriptorCount = resourcesCount;
			tboPoolSize.type = vk::DescriptorType::eCombinedImageSampler;
		}
		auto ssboPoolSize = vk::DescriptorPoolSize();
		{
			ssboPoolSize.descriptorCount = resourcesCount;
			ssboPoolSize.type = vk::DescriptorType::eStorageBuffer;
		}
		auto inputAttachmentsPoolSize = vk::DescriptorPoolSize();
		{
			inputAttachmentsPoolSize.descriptorCount = resourcesCount;
			inputAttachmentsPoolSize.type = vk::DescriptorType::eInputAttachment;
		}
		//FIXME: must allow single free, otherwise validation layers will complain... even when its not single free, rather pool reset... figurte out what is happening
		m_descriptorPools[getDescriptorSetNumber(ResourceFrequency::Renderer)] = std::make_shared<libVulkan::VulkanDescriptorPool>(instance, device, std::vector<vk::DescriptorPoolSize>{ uboPoolSize }, true, "Frequency: Renderer", false);
	}

	void Renderer::createPassDescriptorPool()
	{
		auto device = getDevice().lock();
		auto instance = getInstance().lock();

		auto resourcesCount = uint32_t(10);
		auto uboPoolSize = vk::DescriptorPoolSize();
		{
			uboPoolSize.descriptorCount = resourcesCount;
			uboPoolSize.type = vk::DescriptorType::eUniformBuffer;
		}
		auto tboPoolSize = vk::DescriptorPoolSize();
		{
			tboPoolSize.descriptorCount = resourcesCount;
			tboPoolSize.type = vk::DescriptorType::eCombinedImageSampler;
		}
		auto ssboPoolSize = vk::DescriptorPoolSize();
		{
			ssboPoolSize.descriptorCount = resourcesCount;
			ssboPoolSize.type = vk::DescriptorType::eStorageBuffer;
		}
		auto inputAttachmentsPoolSize = vk::DescriptorPoolSize();
		{
			inputAttachmentsPoolSize.descriptorCount = resourcesCount;
			inputAttachmentsPoolSize.type = vk::DescriptorType::eInputAttachment;
		}
		//FIXME: must allow single free, otherwise validation layers will complain... even when its not single free, rather pool reset... figurte out what is happening
		m_descriptorPools[getDescriptorSetNumber(ResourceFrequency::RenderPass)] = std::make_shared<libVulkan::VulkanDescriptorPool>(instance, device, std::vector<vk::DescriptorPoolSize>{ uboPoolSize }, true, "Frequency: RenderPass", false);
	}

	void Renderer::createMaterialDescriptorPool()
	{
		auto device = getDevice().lock();
		auto instance = getInstance().lock();

		auto resourcesCount = uint32_t(100);
		auto uboPoolSize = vk::DescriptorPoolSize();
		{
			uboPoolSize.descriptorCount = resourcesCount;
			uboPoolSize.type = vk::DescriptorType::eUniformBuffer;
		}
		auto tboPoolSize = vk::DescriptorPoolSize();
		{
			tboPoolSize.descriptorCount = resourcesCount;
			tboPoolSize.type = vk::DescriptorType::eCombinedImageSampler;
		}
		auto ssboPoolSize = vk::DescriptorPoolSize();
		{
			ssboPoolSize.descriptorCount = resourcesCount;
			ssboPoolSize.type = vk::DescriptorType::eStorageBuffer;
		}
		auto inputAttachmentsPoolSize = vk::DescriptorPoolSize();
		{
			inputAttachmentsPoolSize.descriptorCount = resourcesCount;
			inputAttachmentsPoolSize.type = vk::DescriptorType::eInputAttachment;
		}
		m_descriptorPools[getDescriptorSetNumber(ResourceFrequency::Material)] = std::make_shared<libVulkan::VulkanDescriptorPool>(instance, device, std::vector<vk::DescriptorPoolSize>{ uboPoolSize }, true, "Frequency: Material", false);
	}

	void Renderer::createModelDescriptorPool()
	{
		auto device = getDevice().lock();
		auto instance = getInstance().lock();

		auto resourcesCount = uint32_t(1000);
		auto uboPoolSize = vk::DescriptorPoolSize();
		{
			uboPoolSize.descriptorCount = resourcesCount;
			uboPoolSize.type = vk::DescriptorType::eUniformBuffer;
		}
		auto tboPoolSize = vk::DescriptorPoolSize();
		{
			tboPoolSize.descriptorCount = resourcesCount;
			tboPoolSize.type = vk::DescriptorType::eCombinedImageSampler;
		}
		auto ssboPoolSize = vk::DescriptorPoolSize();
		{
			ssboPoolSize.descriptorCount = resourcesCount;
			ssboPoolSize.type = vk::DescriptorType::eStorageBuffer;
		}
		auto inputAttachmentsPoolSize = vk::DescriptorPoolSize();
		{
			inputAttachmentsPoolSize.descriptorCount = resourcesCount;
			inputAttachmentsPoolSize.type = vk::DescriptorType::eInputAttachment;
		}
		m_descriptorPools[getDescriptorSetNumber(ResourceFrequency::Object)] = std::make_shared<libVulkan::VulkanDescriptorPool>(instance, device, std::vector<vk::DescriptorPoolSize>{ uboPoolSize }, true, "Frequency: Object", false);
	}
}