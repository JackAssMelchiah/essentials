#include <libCore/VertexBuffer.hpp>

namespace libCore
{
	VertexBuffer::VertexBuffer(std::shared_ptr<Renderer> const& renderer, uint32_t bytes, std::string const& name)
		: Buffer(renderer->getInstance().lock(), renderer->getDevice().lock(), bytes, name)
		, m_renderer(renderer)
	{
		initializeBuffer(bytes);
	}

	void VertexBuffer::initializeBuffer(uint32_t bytes)
	{
		auto renderer = m_renderer.lock();
		auto device = renderer->getDevice().lock();
		auto instance = renderer->getInstance().lock();
		// eDeviceLocal - GPU direct fast access, No access CPU, GOOD for one CPU upload, GPU reads|writes frequently
		m_masterBuffer = std::make_shared<libVulkan::VulkanBuffer>(instance, device, vk::BufferUsageFlagBits::eVertexBuffer | vk::BufferUsageFlagBits::eTransferDst, vk::MemoryPropertyFlagBits::eDeviceLocal, bytes, m_name);
	}
}