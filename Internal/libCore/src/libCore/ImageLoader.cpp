//! libCore
#include <libCore/ImageLoader.hpp>

namespace libCore 
{
	bool ImageLoader::initialize()
	{
		auto init = IMG_Init(IMG_INIT_JPG | IMG_INIT_PNG | IMG_INIT_TIF);
		return init;
	}

	bool ImageLoader::loadImage(std::string const& textureName, glm::ivec2& resolutionOut, int& pitchOut, std::vector<std::byte>& dataOut)
	{
		dataOut.clear();
		// load image
		auto surface = IMG_Load(textureName.c_str());
		if (!surface)
			return false;

		if (!loadImage(surface, resolutionOut, pitchOut, dataOut))
			return false;
		return true;
	}

	bool ImageLoader::loadImage(SDL_Surface* surface, glm::ivec2& resolutionOut, int& pitchOut, std::vector<std::byte>& dataOut)
	{
		auto convertedSurface = SDL_ConvertSurfaceFormat(surface, SDL_PIXELFORMAT_ABGR8888, 0);
		SDL_FreeSurface(surface);
		if (!convertedSurface)
			return false;
		// do a copy
		auto dataSize = convertedSurface->format->BytesPerPixel * convertedSurface->w * convertedSurface->h;
		dataOut.resize(dataSize);

		pitchOut = convertedSurface->pitch;
		resolutionOut = glm::ivec2(convertedSurface->w, convertedSurface->h);

		SDL_LockSurface(convertedSurface);
		memcpy(dataOut.data(), convertedSurface->pixels, dataSize);
		SDL_UnlockSurface(convertedSurface);
		SDL_FreeSurface(convertedSurface);

		return true;
	}

	void ImageLoader::quit()
	{
		IMG_Quit();
	}
}

