//! libCore
#include <libCore/MaterialAABBLayout.hpp>
#include <libCore/ShaderResources.hpp>
//! libVulkan
#include <libVulkan/VulkanAll.hpp>
//! glm
#include <glm/gtc/type_ptr.hpp>

namespace libCore
{
	MaterialAABBLayout::MaterialAABBLayout(std::shared_ptr<libVulkan::VulkanInstance> const& instance, std::shared_ptr<libVulkan::VulkanDevice> const& device)
		: MaterialLayout(instance, device, MaterialLayoutType::AABB)
		, m_renderingInfo(nullptr)
	{
		//basic requirements for rendereing attachments - only depth and output attachment
		std::vector<vk::Format> inputs;
		vk::Format depthStencilFormat;
		std::vector<vk::Format> outputs;

		libVulkan::VulkanRenderingInfo::selectSimplePassFormats(*instance, inputs, depthStencilFormat, outputs);
		m_renderingInfo = std::make_shared<libVulkan::VulkanRenderingInfo>(inputs, depthStencilFormat, outputs);
	}

	std::shared_ptr<libVulkan::VulkanRenderingInfo> const& MaterialAABBLayout::getRenderingInfo() const
	{
		return m_renderingInfo;
	}

	std::shared_ptr<libVulkan::VulkanPipeline> MaterialAABBLayout::createPipeline(std::shared_ptr<libVulkan::VulkanPipelineLayout> pipelineLayout, vk::Extent2D const& resolution) const
	{
		auto instance = m_instance.lock();
		auto device = m_device.lock();

		auto vsShader = std::make_unique<libVulkan::VulkanShaderModule>(instance, device, "materialAABB");
		auto fsShader = std::make_unique<libVulkan::VulkanShaderModule>(instance, device, "materialAABB");
		vsShader->loadAndCreate("materialAABB.vert.spv");
		fsShader->loadAndCreate("materialAABB.frag.spv");
		auto shaders = std::make_shared<libVulkan::VulkanShaders>(std::move(vsShader), std::move(fsShader));

		auto vertexAttributeDesc = createObjectVertexAttributesDescription();
		auto pipeline = std::make_shared<libVulkan::VulkanPipeline>(instance, device, shaders, pipelineLayout, vertexAttributeDesc, m_renderingInfo, resolution, "AABB");
		pipeline->setInputAssebly(vk::PrimitiveTopology::eLineStrip, false);
		pipeline->compilePipeline();
		return pipeline;
	}

	void MaterialAABBLayout::setObjectColor(glm::vec3 const& color, ShaderResources& resources, ShaderResources::Instance const& instance) const
	{
		resources.setInstanceResource("AABB_COLOR", glm::vec4(color, 0.f), instance);
	}

	glm::vec3 MaterialAABBLayout::getObjectColor(ShaderResources const& resources, ShaderResources::Instance const& instance) const
	{
		return glm::vec3(resources.getInstanceResource<glm::vec4>("AABB_COLOR", instance));
	}

	void MaterialAABBLayout::setObjectAABBMin(glm::vec3 const& min, ShaderResources& resources, ShaderResources::Instance const& instance) const
	{
		resources.setInstanceResource("AABB_MIN", glm::vec4(min, 0.f), instance);
	}

	glm::vec3 MaterialAABBLayout::getObjectAABBMin(ShaderResources const& resources, ShaderResources::Instance const& instance) const
	{
		return glm::vec3(resources.getInstanceResource<glm::vec4>("AABB_MIN", instance));
	}

	void MaterialAABBLayout::setObjectAABBMax(glm::vec3 const& max, ShaderResources& resources, ShaderResources::Instance const& instance) const
	{
		resources.setInstanceResource("AABB_MAX", glm::vec4(max, 0.f), instance);
	}

	glm::vec3 MaterialAABBLayout::getObjectAABBMax(ShaderResources const& resources, ShaderResources::Instance const& instance) const
	{
		return glm::vec3(resources.getInstanceResource<glm::vec4>("AABB_MAX", instance));
	}

	void MaterialAABBLayout::bindObjectUniformData(libVulkan::VulkanDescriptorSet& modelDataDescriptorSet, ShaderResources& resources) const
	{
		bufferObjectResources(resources);

		auto modelMatrixBuffer = std::string("OBJECT_BUFFER");
		assert(resources.bufferExists(modelMatrixBuffer));

		resources.useBuffersWithSet({ modelMatrixBuffer }, modelDataDescriptorSet);
	}

	void MaterialAABBLayout::bindMaterialUniformData(libVulkan::VulkanDescriptorSet& materialDataDescriptorSet, ShaderResources& resources) const
	{
	}

	void MaterialAABBLayout::bufferObjectResources(ShaderResources& resources) const
	{
		//specify resources used by object
		auto aabbMinResource = std::string("AABB_MIN");
		auto aabbMaxResource = std::string("AABB_MAX");
		auto colorResource = std::string("AABB_COLOR");
		auto objectBuffer = std::string("OBJECT_BUFFER");

		//texture resources are already buffered and buffer is provided, so nothing to be done here

		//buffer resources
		resources.bufferResources({ aabbMinResource, aabbMaxResource, colorResource }, objectBuffer);
	}

	void MaterialAABBLayout::bufferMaterialResources(ShaderResources& resources) const
	{
	}

	std::shared_ptr<libVulkan::VulkanDescriptorSetLayout> MaterialAABBLayout::createMaterialDescriptorSetLayout() const
	{
		//while not used, it needs to be set-up
		return std::make_shared<libVulkan::VulkanDescriptorSetLayout>(m_instance.lock(), m_device.lock(), std::vector<vk::DescriptorSetLayoutBinding>{}, "AABB");
	}

	std::shared_ptr<libVulkan::VulkanDescriptorSetLayout> MaterialAABBLayout::createObjectDescriptorSetLayout() const
	{
		auto layoutBindings = std::vector<vk::DescriptorSetLayoutBinding>();
		// All data are packed in shader storage buffer
		auto bindingUBO = vk::DescriptorSetLayoutBinding();
		{
			bindingUBO.binding = 0;
			bindingUBO.descriptorCount = 1;
			bindingUBO.descriptorType = vk::DescriptorType::eStorageBuffer;
			bindingUBO.stageFlags = vk::ShaderStageFlagBits::eVertex;
			bindingUBO.pImmutableSamplers = nullptr;
		}
		layoutBindings.push_back(bindingUBO);

		//create set layout
		return std::make_shared<libVulkan::VulkanDescriptorSetLayout>(m_instance.lock(), m_device.lock(), layoutBindings, "AABB");
	}

	std::shared_ptr<libVulkan::VulkanVertexAttributesDescription> MaterialAABBLayout::createObjectVertexAttributesDescription() const
	{
		auto descriptions = std::vector<vk::VertexInputAttributeDescription>();
		auto bindingDescription = vk::VertexInputBindingDescription();

		return std::make_shared<libVulkan::VulkanVertexAttributesDescription>(descriptions, std::vector<vk::VertexInputBindingDescription>{ bindingDescription });
	}

	void MaterialAABBLayout::allocateObjectData(ShaderResources& resources) const
	{
		resources.setInstanceResource("AABB_MIN", glm::vec4(0.f), resources.getBaseInstance());
		resources.setInstanceResource("AABB_MAX", glm::vec4(0.f), resources.getBaseInstance());
		resources.setInstanceResource("AABB_COLOR", glm::vec4(0.f), resources.getBaseInstance());
	}

	void MaterialAABBLayout::allocateMaterialData(ShaderResources& resources) const
	{
	}
}