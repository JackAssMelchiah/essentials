//! libCore
#include <libCore/Entity.hpp>
//! libUtils
#include <libUtils/Utils.hpp>

namespace libCore
{
	Entity::Entity(entt::entity entity, std::string const& displayName)
		: m_entity(entity)
		, m_displayName(displayName)
	{
		if (m_displayName.empty())
			assert(!"Name cannot be empty");
	}

	void Entity::setDisplayName(std::string const& name)
	{
		m_displayName = name;
		if (m_displayName.empty())
			assert(!"Name cannot be empty");
		//update all instances within graph to reflect name change TODO: problem that if multiple scenes use same graph
	}

	std::string Entity::getDisplayName() const
	{
		return m_displayName;
	}

	entt::entity Entity::operator()() const
	{
		return m_entity;
	}
}
