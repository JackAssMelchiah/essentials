#include <libCore/IndexBuffer.hpp>
#include <libCore/Renderer.hpp>

namespace libCore
{
	IndexBuffer::IndexBuffer(std::shared_ptr<Renderer> const& renderer, vk::IndexType type, uint32_t bytes, std::string const& name)
		: Buffer(renderer->getInstance().lock(), renderer->getDevice().lock(), bytes, name)
        , m_renderer(renderer)
        , m_type(type)
        
	{
		initialize(bytes);
	}

	void IndexBuffer::bufferData(std::vector<unsigned char> const& data, uint32_t offset)
	{
        auto renderer = m_renderer.lock();
        auto device = renderer->getDevice().lock();
        auto instance = renderer->getInstance().lock();

        auto bytes = uint32_t(sizeof(unsigned char) * data.size());

        //1. creates staging buffer            
        // {eHostVisible|eHostCoherent} - CPU writes, copies to GPU, or CPU Writes, GPU reads directly
        auto stageBuffer = std::make_shared<libVulkan::VulkanBuffer>(instance, device, vk::BufferUsageFlagBits::eTransferSrc, vk::MemoryPropertyFlagBits::eHostVisible | vk::MemoryPropertyFlagBits::eHostCoherent, bytes, "Index Staging Buffer");
        //2. map that buffer to be writtable
        stageBuffer->map();
        //3. copy data to it
        stageBuffer->bufferData(data);
        //4. unmap it since this is not persistent buffer and after copyQue will be deleted
        stageBuffer->unmap();
        //5. record command buffer with data transfer from staging buffer to the vertex buffer
        renderer->recordAndSubmit([=](libVulkan::VulkanCommandBuffer& commandBuffer)->void
            {
                commandBuffer.copyBuffer(*stageBuffer, *m_masterBuffer, { vk::BufferCopy{ 0, offset, bytes } });
            }
        );
	}

    vk::IndexType IndexBuffer::getType() const
    {
        return m_type;
    }

	void IndexBuffer::initialize(uint32_t bytes)
	{
        auto renderer = m_renderer.lock();
        auto device = renderer->getDevice().lock();
        auto instance = renderer->getInstance().lock();

		// eDeviceLocal - GPU direct fast access, No access CPU, GOOD for one CPU upload, GPU reads|writes frequently
		m_masterBuffer = std::make_shared<libVulkan::VulkanBuffer>(instance, device, vk::BufferUsageFlagBits::eIndexBuffer | vk::BufferUsageFlagBits::eTransferDst, vk::MemoryPropertyFlagBits::eDeviceLocal, bytes, "IB" + m_name);
	}
}