//! libCore
#include <libCore/MaterialPhongNormalMapLayout.hpp>
#include <libCore/UniformBuffer.hpp>
#include <libCore/VertexBuffer.hpp>
#include <libCore/ShaderResources.hpp>
//! libVulkan
#include <libVulkan/VulkanAll.hpp>
//! glm
#include <glm/gtc/type_ptr.hpp>

namespace libCore
{
	MaterialPhongNormalLayout::MaterialPhongNormalLayout(std::shared_ptr<libVulkan::VulkanInstance> const& instance, std::shared_ptr<libVulkan::VulkanDevice> const& device)
		: MaterialLayout(instance, device, MaterialLayoutType::PHONG_NORMAL_MAP)
		, m_renderingInfo(nullptr)
	{
		//basic requirements for rendereing attachments - only depth and output attachment
		std::vector<vk::Format> inputs;
		vk::Format depthStencilFormat;
		std::vector<vk::Format> outputs;

		libVulkan::VulkanRenderingInfo::selectSimplePassFormats(*instance, inputs, depthStencilFormat, outputs);
		m_renderingInfo = std::make_shared<libVulkan::VulkanRenderingInfo>(inputs, depthStencilFormat, outputs);
	}

	std::shared_ptr<libVulkan::VulkanRenderingInfo> const& MaterialPhongNormalLayout::getRenderingInfo() const
	{
		return m_renderingInfo;
	}

	std::shared_ptr<libVulkan::VulkanPipeline> MaterialPhongNormalLayout::createPipeline(std::shared_ptr<libVulkan::VulkanPipelineLayout> pipelineLayout, vk::Extent2D const& resolution) const
	{
		auto device = m_device.lock();
		auto instance = m_instance.lock();

		auto vsShader = std::make_unique<libVulkan::VulkanShaderModule>(instance, device, "materialPhongNormalMap");
		auto fsShader = std::make_unique<libVulkan::VulkanShaderModule>(instance, device, "materialPhongNormalMap");
		vsShader->loadAndCreate("materialPhongNormalMap.vert.spv");
		fsShader->loadAndCreate("materialPhongNormalMap.frag.spv");
		auto shaders = std::make_shared<libVulkan::VulkanShaders>(std::move(vsShader), std::move(fsShader));

		auto vertexAttributeDesc = createObjectVertexAttributesDescription();
		return std::make_shared<libVulkan::VulkanPipeline>(instance, device, shaders, pipelineLayout, vertexAttributeDesc, m_renderingInfo, resolution, "PHONG_NORMAL");
	}

	void MaterialPhongNormalLayout::updateVertexDataFromResources(VertexBuffer& buffer, std::vector<glm::vec3> const& vertices, std::vector<glm::vec3> const& colors) const
	{
		//TODO implement
		assert(false);
		assert(vertices.size() == colors.size());
		//can interleave in paralel 
	}

	void MaterialPhongNormalLayout::setObjectModelMatrix(glm::mat4 const& matrix, ShaderResources& resources, ShaderResources::Instance const& instance) const
	{
		resources.setInstanceResource("MODEL_MATRIX", matrix, instance);
	}

	void MaterialPhongNormalLayout::setObjectAlbedoTexture(std::shared_ptr<TextureBuffer> albedo, ShaderResources& resources) const
	{
		resources.setTextureBuffer(albedo, "ALBEDO_TEXTURE_BUFFER");
	}

	void MaterialPhongNormalLayout::setObjectNormalTexture(std::shared_ptr<TextureBuffer> normal, ShaderResources& resources) const
	{
		resources.setTextureBuffer(normal, "NORMAL_TEXTURE_BUFFER");
	}

	void MaterialPhongNormalLayout::setMaterialLightPosition(glm::vec3 const& lightPosition, ShaderResources& resources) const
	{
		resources.setInstanceResource("LIGHT_POSITION", glm::vec4(lightPosition, 0.f), resources.getBaseInstance());
	}

	void MaterialPhongNormalLayout::setMaterialCameraPosition(glm::vec3 const& cameraPosition, ShaderResources& resources) const
	{
		resources.setInstanceResource("CAMERA_POSITION", glm::vec4(cameraPosition, 0.f), resources.getBaseInstance());
	}

	void MaterialPhongNormalLayout::bindObjectUniformData(libVulkan::VulkanDescriptorSet& modelDataDescriptorSet, ShaderResources& resources) const
	{
		bufferObjectResources(resources);

		auto modelMatrixBuffer = std::string("OBJECT_BUFFER");
		assert(resources.bufferExists(modelMatrixBuffer));
		auto albedoTextureBuffer = std::string("ALBEDO_TEXTURE_BUFFER");
		assert(resources.bufferExists(albedoTextureBuffer));
		auto normalTextureBuffer = std::string("NORMAL_TEXTURE_BUFFER");
		assert(resources.bufferExists(normalTextureBuffer));

		resources.useBuffersWithSet({ modelMatrixBuffer, albedoTextureBuffer, normalTextureBuffer }, modelDataDescriptorSet);
	}

	void MaterialPhongNormalLayout::bindMaterialUniformData(libVulkan::VulkanDescriptorSet& materialDataDescriptorSet, ShaderResources& resources) const
	{
		bufferMaterialResources(resources);

		auto phongBuffer = std::string("MATERIAL_PHONG_NORMAL_BUFFER");
		assert(resources.bufferExists(phongBuffer));

		resources.useBuffersWithSet({ phongBuffer }, materialDataDescriptorSet);
	}

	void MaterialPhongNormalLayout::bufferObjectResources(ShaderResources& resources) const
	{
		//specify resources used by object
		auto modelMatrixResource = std::string("MODEL_MATRIX");
		auto modelMatrixBuffer = std::string("OBJECT_BUFFER");

		//texture resources are already buffered and buffer is provided, so nothing to be done here

		//buffer resources
		resources.bufferResources({ modelMatrixResource }, modelMatrixBuffer);
	}

	void MaterialPhongNormalLayout::bufferMaterialResources(ShaderResources& resources) const
	{
		//specify resources used by material
		auto materialCameraPositionResource = std::string("CAMERA_POSITION");
		auto materialLightPositionResource = std::string("LIGHT_POSITION");
		auto phongBuffer = std::string("MATERIAL_PHONG_NORMAL_BUFFER");

		//buffer resources
		resources.bufferResources({ materialCameraPositionResource, materialLightPositionResource }, phongBuffer);
	}

	std::shared_ptr<libVulkan::VulkanDescriptorSetLayout> MaterialPhongNormalLayout::createMaterialDescriptorSetLayout() const
	{
		// Phong uses light position and camera position data
		auto bindingUBO = vk::DescriptorSetLayoutBinding();
		{
			bindingUBO.binding = 0;
			bindingUBO.descriptorCount = 1;
			bindingUBO.descriptorType = vk::DescriptorType::eStorageBuffer;
			bindingUBO.stageFlags = vk::ShaderStageFlagBits::eFragment;
			bindingUBO.pImmutableSamplers = nullptr;
		}
		return std::make_shared<libVulkan::VulkanDescriptorSetLayout>(m_instance.lock(), m_device.lock(), std::vector<vk::DescriptorSetLayoutBinding>{ bindingUBO }, "PHONG_NORMAL");
	}

	std::shared_ptr<libVulkan::VulkanDescriptorSetLayout> MaterialPhongNormalLayout::createObjectDescriptorSetLayout() const
	{
		auto layoutBindings = std::vector<vk::DescriptorSetLayoutBinding>();
		// Model matrix uniform
		auto bindingUBO = vk::DescriptorSetLayoutBinding();
		{
			bindingUBO.binding = 0;
			bindingUBO.descriptorCount = 1;
			bindingUBO.descriptorType = vk::DescriptorType::eStorageBuffer;
			bindingUBO.stageFlags = vk::ShaderStageFlagBits::eVertex;
			bindingUBO.pImmutableSamplers = nullptr;
		}
		layoutBindings.push_back(bindingUBO);

		// Albedo texture
		auto bindingAlbedo = vk::DescriptorSetLayoutBinding();
		{
			bindingAlbedo.binding = 1; //BP of texture in shaders, 0 is reserved for VBO
			bindingAlbedo.descriptorCount = 1; //for now this is allways the case
			bindingAlbedo.descriptorType = vk::DescriptorType::eCombinedImageSampler;
			bindingAlbedo.stageFlags = vk::ShaderStageFlagBits::eFragment;
			bindingAlbedo.pImmutableSamplers = nullptr;
		}
		layoutBindings.push_back(bindingAlbedo);

		// Normal texture
		auto bindingNormal = vk::DescriptorSetLayoutBinding();
		{
			bindingNormal.binding = 2; //BP of texture in shaders, 0 is reserved for VBO
			bindingNormal.descriptorCount = 1; //for now this is allways the case
			bindingNormal.descriptorType = vk::DescriptorType::eCombinedImageSampler;
			bindingNormal.stageFlags = vk::ShaderStageFlagBits::eFragment;
			bindingNormal.pImmutableSamplers = nullptr;
		}
		layoutBindings.push_back(bindingNormal);

		//create set layout
		return std::make_shared<libVulkan::VulkanDescriptorSetLayout>(m_instance.lock(), m_device.lock(), layoutBindings, "PHONG_NORMAL");
	}

	std::shared_ptr<libVulkan::VulkanVertexAttributesDescription> MaterialPhongNormalLayout::createObjectVertexAttributesDescription() const
	{
		auto descriptions = std::vector<vk::VertexInputAttributeDescription>();
		auto binding = 0;

		// REQUIREMENTS:
		// layout(0) - position (VEC3)
		// layout(1) - normal	(VEC3)
		// layout(2) - uvs		(VEC2)
		// layout(3) - tangents (VEC4)
		{
			auto attrib = vk::VertexInputAttributeDescription();
			// Position
			{
				attrib.binding = binding;
				attrib.format = vk::Format::eR32G32B32Sfloat;
				attrib.location = 0;
				attrib.offset = 0;
			}
			descriptions.push_back(attrib);
			//Normal
			{
				attrib.binding = binding;
				attrib.format = vk::Format::eR32G32B32Sfloat;
				attrib.location = 1;
				attrib.offset = sizeof(glm::vec3);
			}
			descriptions.push_back(attrib);
			//Uvs
			{
				attrib.binding = binding;
				attrib.format = vk::Format::eR32G32Sfloat;
				attrib.location = 2;
				attrib.offset = 2 * sizeof(glm::vec3);
			}
			descriptions.push_back(attrib);
			//Tangents
			{
				attrib.binding = binding;
				attrib.format = vk::Format::eR32G32B32A32Sfloat;
				attrib.location = 3;
				attrib.offset = 2 * sizeof(glm::vec3) + sizeof(glm::vec2);
			}
			descriptions.push_back(attrib);
		}

		auto bindingDescription = vk::VertexInputBindingDescription();
		{
			bindingDescription.inputRate = vk::VertexInputRate::eVertex;
			bindingDescription.binding = binding;
			bindingDescription.stride = 2 * sizeof(glm::vec3) + sizeof(glm::vec2) + sizeof(glm::vec4);
		}
		return std::make_shared<libVulkan::VulkanVertexAttributesDescription>(descriptions, std::vector<vk::VertexInputBindingDescription>{ bindingDescription });
	}

	void MaterialPhongNormalLayout::allocateObjectData(ShaderResources& resources) const
	{
		resources.setInstanceResource("MODEL_MATRIX", glm::identity<glm::mat4>(), resources.getBaseInstance());
		resources.setTextureBuffer(std::shared_ptr<TextureBuffer>(), "ALBEDO_TEXTURE_BUFFER");
		resources.setTextureBuffer(std::shared_ptr<TextureBuffer>(), "NORMAL_TEXTURE_BUFFER");
	}

	void MaterialPhongNormalLayout::allocateMaterialData(ShaderResources& resources) const
	{
		resources.setInstanceResource("LIGHT_POSITION", glm::vec4(0.f, 0.f, 0.f, 0.f), resources.getBaseInstance());
		resources.setInstanceResource("CAMERA_POSITION", glm::vec4(0.f, 0.f, 0.f, 0.f), resources.getBaseInstance());
	}
}
