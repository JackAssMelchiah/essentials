//! libCore
#include <libCore/Core.hpp>
#include <libCore/Window.hpp>
#include <libCore/Entity.hpp>
#include <libCore/TransformGraphManager.hpp>
//! libUi
#include <libUi/UiClassManager.hpp>
//! libVulkan
#include <libVulkan/VulkanManager.hpp>
#include <libVulkan/VulkanInstance.hpp>
#include <libVulkan/VulkanDevice.hpp>
#include <libVulkan/VulkanDefaults.hpp>
//! libInputs
#include <libInputs/InputManager.hpp>
//! libUtils
#include <libUtils/Utils.hpp>

namespace libCore 
{
    Gleam::~Gleam()
    {
        m_windows.clear();
        m_uiClassManager.reset();
        m_vulkanManager.reset();
    }

    //! Initialization for whole gleam app
    bool Gleam::initialize(bool windowless, std::shared_ptr<libVulkan::VulkanDefaults> config)
    {
        if (!inititalizeExternalLibs())
        {
            libUtils::printOutErr("Unable to initialize External libraries, exiting!");
            return false;
        }
        //uniquePtr should handle deinit
        if (!initializeGraphicsApi(windowless, config))
        {
            libUtils::printOutErr("Unable to initialize graphics api, exiting!");
            return false;
        }
        if (!initializeSoundApi()) 
        {
            libUtils::printOutErr("Unable to initialize sound api, exiting!");
            return false;
        }
        if (!initializeInputApi()) 
        {
            libUtils::printOutErr("Unable to initialize input api, exiting!");
            return false;
        }
        if (!initializeUiApi()) 
        {
            libUtils::printOutErr("Unable to initialize UI api, exiting!");
            return false;
        }
        if (!initializeCoreApi())
        {
            libUtils::printOutErr("Unable to initialize UI api, exiting!");
            return false;
        }

        //conect the gui to event input callback
        getInputManager().setGuiCallback([&](SDL_Event& event)->void
            {
                getUiManager().processEvent(event);
            }
        );

        return true;
    }

    //! Inits sdl, if it fails, bail
    bool Gleam::initializeSDL()
    {
        // Setup SDL
        if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_TIMER | SDL_INIT_GAMECONTROLLER) != 0) 
        {
            //printf("Error: %s\n", SDL_GetError());
            return false;
        }
        return true;
    }

    bool Gleam::handleWindow(SDL_WindowEvent& e)
    {
        switch (e.event) 
        {
        case SDL_WINDOWEVENT_SHOWN:
            //libUtils::printOut(std::string("Window ") + std::to_string(e.windowID) + " shown");
            break;
        case SDL_WINDOWEVENT_HIDDEN:
            //libUtils::printOut(std::string("Window ") + std::to_string(e.windowID) + " hidden");
            break;
        case SDL_WINDOWEVENT_EXPOSED:
            //libUtils::printOut(std::string("Window ") + std::to_string(e.windowID) + " exposed");
            break;
        case SDL_WINDOWEVENT_MOVED:
            //libUtils::printOut(std::string("Window ") + std::to_string(e.windowID) + " moved to " + std::to_string(e.data1) + ", " + std::to_string(e.data2));
            break;
        case SDL_WINDOWEVENT_RESIZED:
            //libUtils::printOut(std::string("Window ") + std::to_string(e.windowID) + " resized to " + std::to_string(e.data1) + ", " + std::to_string(e.data2));
            getWindow()->setWindowResolution({ e.data1, e.data2 });
            break;
        case SDL_WINDOWEVENT_SIZE_CHANGED:
            //libUtils::printOut(std::string("Window ") + std::to_string(e.windowID) + " size changed to " + std::to_string(e.data1) + ", " + std::to_string(e.data2));
            break;
        case SDL_WINDOWEVENT_MINIMIZED:
            //libUtils::printOut(std::string("Window ") + std::to_string(e.windowID) + " minimised");
            getWindow()->setWindowResolution({0, 0});
            break;
        case SDL_WINDOWEVENT_MAXIMIZED:
            //libUtils::printOut(std::string("Window ") + std::to_string(e.windowID) + " maximized");
            getWindow()->setWindowResolution(getWindow()->getWindowResolution());
            break;
        case SDL_WINDOWEVENT_RESTORED:
            //libUtils::printOut(std::string("Window ") + std::to_string(e.windowID) + " restored");
            break;
        case SDL_WINDOWEVENT_ENTER:
            //libUtils::printOut(std::string("Mouse entered window ") + std::to_string(e.windowID));
            break;
        case SDL_WINDOWEVENT_LEAVE:
            //libUtils::printOut(std::string("Mouse left window ") + std::to_string(e.windowID));
            break;
        case SDL_WINDOWEVENT_FOCUS_GAINED:
            //libUtils::printOut(std::string("Window ") + std::to_string(e.windowID) + " gained focus");
            break;
        case SDL_WINDOWEVENT_FOCUS_LOST:
            //libUtils::printOut(std::string("Window ") + std::to_string(e.windowID) + " lost focus");
            break;
        case SDL_WINDOWEVENT_CLOSE:
            //libUtils::printOut(std::string("Window ") + std::to_string(e.windowID) + " closed");
            return false;
            break;
        default:
            break;
        }

        return true;
    }

    bool Gleam::addWindow(std::shared_ptr<libCore::Window> w)
    {
        m_windows.insert(w);
        return true;
    }

    std::shared_ptr<Window> const& Gleam::getWindow() const
    {
        return *m_windows.begin();
    }

    libVulkan::VulkanManager& Gleam::getVulkanManager()
    {
        return *m_vulkanManager;
    }

    libInputs::InputManager& Gleam::getInputManager()
    {
        return *m_inputManager;
    }

    std::weak_ptr<libInputs::InputManager> Gleam::getInputManagerWeak()
    {
        return m_inputManager;
    }

    std::weak_ptr<TransformGraphManager> libCore::Gleam::getTransformGraphManager()
    {
        return m_TransformGraphManager;
    }
    
    libUi::UiClassManager& libCore::Gleam::getUiManager()
    {
        return *m_uiClassManager;
    }

    bool Gleam::inititalizeExternalLibs()
    {
        bool retval = initializeSDL();
        return retval;
    }

    bool Gleam::initializeGraphicsApi(bool windowless, std::shared_ptr<libVulkan::VulkanDefaults> settings)
    {
        if (windowless) // in this case remove swapchain extension as it wont be needed
        {
            settings->extensions = { VK_EXT_DEBUG_UTILS_EXTENSION_NAME };
        }
        
        if (!windowless)
        {
            //in case of window support, must spawn window first to get it's extensions, so the vulkan part of window can be spawned later
            auto window = std::make_shared<Window>("MainWindow");
            // instance needs to be created with extensions from window, otherwise swapchain func would be unavailable
            auto windowExtensions = window->getVulkanInstanceExtensions();
            // Add the windowing extensions to defauts (FIXME: dont add if already there)
            settings->extensions.insert(settings->extensions.end(), windowExtensions.begin(), windowExtensions.end());

            //create manager
            m_vulkanManager = std::make_shared<libVulkan::VulkanManager>(settings);

            //create instance
            m_vulkanManager->createInstance();

            //vulkan surface can be retrieved
            auto lockedInstance = m_vulkanManager->getInstance().lock();
            auto vulkanSurface = window->createAndGetVulkanSurface(*lockedInstance);
            assert(vulkanSurface);

            //create device
            m_vulkanManager->createDevice(lockedInstance, &vulkanSurface);
            
            //create all available queues
            m_vulkanManager->createQueues();

            //add this window's vulkan backend to vulkan manager
            auto vulkanBackend = m_vulkanManager->createWindow(vulkanSurface, window->getWindowResolution(), window->getName());
           
            //add vulkan backend to window so it can be resized automatically
            window->setVulkanBackend(vulkanBackend);

            //add w it to gleam which will own it
            addWindow(window);
        }
        else
        {
            //create manager
            m_vulkanManager = std::make_shared<libVulkan::VulkanManager>(settings);
            //initialize everything
            m_vulkanManager->createInstance();
            auto lockedInstance = m_vulkanManager->getInstance().lock();
            m_vulkanManager->createDevice(lockedInstance, nullptr);
            m_vulkanManager->createQueues();
        }
        return true;
    }

    bool Gleam::initializeSoundApi()
    {
        return true;
    }

    bool Gleam::initializeInputApi()
    {
        m_inputManager = std::make_shared<libInputs::InputManager>();
        // input manager setup
        m_inputManager->addWindowEventCallback([&](SDL_WindowEvent& e)
            {
                handleWindow(e);
            }
        );
        return true;
    }

    bool Gleam::initializeUiApi()
    {
        if (m_windows.empty())
        {
            libUtils::printOut("Cannot initialize ui - window is required");
            return false;
        }

        auto device = m_vulkanManager->getDevice().lock();
        auto instace = m_vulkanManager->getInstance().lock();
        auto queues = m_vulkanManager->getQueues().lock();

        m_uiClassManager = std::make_shared<libUi::UiClassManager>();
        return m_uiClassManager->initialize(instace, device, queues->getPresentationQueue().lock(), getWindow()->operator()());
    }
    
    bool libCore::Gleam::initializeCoreApi()
    {
        m_TransformGraphManager = std::make_shared<TransformGraphManager>();
        return true;
    }
}