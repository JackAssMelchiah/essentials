//! libCore
#include <libCore/RenderingLink.hpp>
#include <libCore/Renderer.hpp>
#include <libCore/ShaderResources.hpp>
//! libVulkan
#include <libVulkan/VulkanAll.hpp>
#include <libVulkan/UtilFunctions.hpp>

namespace libCore
{
	RenderingLink::RenderingLink(std::shared_ptr<Renderer> const& renderer, std::shared_ptr<libVulkan::VulkanRenderingInfo> const& renderingInfo)
		: m_renderer(renderer)
		, m_renderingInfo(renderingInfo)
	{
	}

	void RenderingLink::createAttachments(vk::Extent2D const& resolution)
	{
		m_depthStencilAttachment = nullptr;
		m_outputAttachments.clear();
		m_inputAttachments.clear();

		auto renderer = m_renderer.lock();
		auto instance = renderer->getInstance().lock();
		auto device = renderer->getDevice().lock();

		//images need to be create and also put into its state from initial state by barriers
		auto barriers = std::vector<std::shared_ptr<libVulkan::VulkanBarrier>>();

		//shared data
		auto& sharedIO = m_renderingInfo->getSharedAttachments();

		//inputs
		auto& inputs = m_renderingInfo->getInputFormats();
		for (auto i = 0; i < inputs.size(); ++i)
		{
			auto usage = vk::ImageUsageFlags(vk::ImageUsageFlagBits::eInputAttachment);
			auto layout = vk::ImageLayout::eColorAttachmentOptimal;
			if (sharedIO.contains(i))
			{//this resource will be shared and used as input and output, so append the ouput flags
				usage |= vk::ImageUsageFlagBits::eColorAttachment;
				layout = vk::ImageLayout::eGeneral;
			}
			auto imageInfo = libVulkan::VulkanImage::Info(inputs.at(i), vk::ImageAspectFlagBits::eColor, usage, vk::MemoryPropertyFlagBits::eDeviceLocal);
			auto attachment = std::make_shared<libVulkan::VulkanImageAttachment>(instance, device, imageInfo, resolution, "Input");
			barriers.push_back(attachment->generateBarrier(vk::ImageLayout::eUndefined, layout, vk::AccessFlagBits2::eNone, vk::PipelineStageFlagBits2::eTopOfPipe, vk::AccessFlagBits2::eNone, vk::PipelineStageFlagBits2::eBottomOfPipe));
			m_inputAttachments.push_back(attachment);
		}
		//ouputs
		auto& outputs = m_renderingInfo->getOutputFormats();
		for (auto i = 0; i < outputs.size(); ++i)
		{
			if (sharedIO.contains(i))
			{//do use the same underlying instance
				m_outputAttachments.push_back(m_inputAttachments.at(i));
			}
			else
			{//spawn normal image attachment
				auto imageInfo = libVulkan::VulkanImage::Info(outputs.at(i), vk::ImageAspectFlagBits::eColor, vk::ImageUsageFlagBits::eColorAttachment | vk::ImageUsageFlagBits::eTransferSrc, vk::MemoryPropertyFlagBits::eDeviceLocal);
				auto attachment = std::make_shared<libVulkan::VulkanImageAttachment>(instance, device, imageInfo, resolution, "Output");
				barriers.push_back(attachment->generateBarrier(vk::ImageLayout::eUndefined, vk::ImageLayout::eColorAttachmentOptimal, vk::AccessFlagBits2::eNone, vk::PipelineStageFlagBits2::eTopOfPipe, vk::AccessFlagBits2::eNone, vk::PipelineStageFlagBits2::eBottomOfPipe));
				m_outputAttachments.push_back(attachment);
			}
		}
		//depthStencil - optional
		auto& depthStencilFormat = m_renderingInfo->getDepthStencilFormat();
		if (depthStencilFormat != vk::Format::eUndefined)
		{
			auto imageInfo = libVulkan::VulkanImage::Info(depthStencilFormat, vk::ImageAspectFlagBits::eDepth, vk::ImageUsageFlagBits::eDepthStencilAttachment, vk::MemoryPropertyFlagBits::eDeviceLocal);
			m_depthStencilAttachment = std::make_shared<libVulkan::VulkanImageAttachment>(instance, device, imageInfo, resolution, "DepthStencil");
			barriers.push_back(m_depthStencilAttachment->generateBarrier(vk::ImageLayout::eUndefined, vk::ImageLayout::eDepthAttachmentOptimal, vk::AccessFlagBits2::eNone, vk::PipelineStageFlagBits2::eTopOfPipe, vk::AccessFlagBits2::eNone, vk::PipelineStageFlagBits2::eBottomOfPipe));
		}

		//transition attachments to correct layout
		m_renderer.lock()->recordAndSubmit([barriers](libVulkan::VulkanCommandBuffer& commandBuffer) -> void
			{
				libVulkan::VulkanBarrier::recordBarriers(commandBuffer, barriers);
			}
		);		
	}

	//! TODO: for now to material which sends the renderingInfo be able to use this object pass, it must be same - in future maybe just subset of the attachments may be enough
	bool RenderingLink::compatible(libVulkan::VulkanRenderingInfo const& renderingInfo)
	{
		return *m_renderingInfo == renderingInfo;
	}


	std::shared_ptr<libVulkan::VulkanRenderingInfo> const& RenderingLink::getRenderingInfo() const
	{
		return m_renderingInfo;
	}

	std::weak_ptr<libVulkan::VulkanImageAttachment> RenderingLink::getOutputAttachment()
	{
		return m_outputAttachments.at(0);
	}

	std::weak_ptr<libVulkan::VulkanImageAttachment> RenderingLink::getOutputDepthStencilAttachment()
	{
		return m_depthStencilAttachment;
	}
}