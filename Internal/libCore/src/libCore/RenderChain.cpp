//! libCore
#include <libCore/RenderChain.hpp>
#include <libCore/Scene.hpp>
#include <libCore/RenderingLink.hpp>
//! libVulkan

namespace libCore
{
	RenderChain::RenderChain()
		: m_idGenerator(1)
	{
	}

	RenderChain::ChainHandle RenderChain::addLink(std::shared_ptr<RenderingLink> const& link, std::vector<std::shared_ptr<Scene>> const& scenes)
	{
		auto handle = ChainHandle(m_idGenerator);
		m_passes[handle] = LinkData(handle, link);
		m_invokationOrder.push_back(handle);
		m_idGenerator++;
		return handle;
	}

	void RenderChain::removeLink(ChainHandle const& handle)
	{
		if (!m_passes.contains(handle))
			return;
		
		auto& record = m_passes.at(handle);
		auto link = record.getLink();
		//erase also from the order
		auto it = std::find(m_invokationOrder.begin(), m_invokationOrder.end(), handle);
		if (it != m_invokationOrder.end())
			m_invokationOrder.erase(it);
		m_passes.erase(handle);
	}

	void RenderChain::addScenes(ChainHandle const& handle, std::vector<std::shared_ptr<Scene>> const& scenes)
	{
		if (!m_passes.contains(handle))
			return;
		m_passes.at(handle).addScenes(scenes);
	}

	void RenderChain::removeScenes(ChainHandle const& handle, std::vector<std::shared_ptr<Scene>> const& scenes)
	{
		if (!m_passes.contains(handle))
			return;
		m_passes.at(handle).removeScenes(scenes);
	}
	
	void RenderChain::addOnSetup(ChainHandle const& handle, std::function<void()> callbackFunc)
	{
		if (!m_passes.contains(handle))
			return;
		m_passes.at(handle).getCallbacks().onSetup = callbackFunc;
	}
	
	void RenderChain::addOnEnter(ChainHandle const& handle, std::function<void()> callbackFunc)
	{
		if (!m_passes.contains(handle))
			return;
		m_passes.at(handle).getCallbacks().onEnter = callbackFunc;
	}

	void RenderChain::addOnLeave(ChainHandle const& handle, std::function<void()> callbackFunc)
	{
		if (!m_passes.contains(handle))
			return;
		m_passes.at(handle).getCallbacks().onLeave = callbackFunc;
	}

	void RenderChain::setFirstScene(ChainHandle const& handle, std::shared_ptr<Scene> const& firstScene)
	{
		if (!m_passes.contains(handle))
			return;
		m_passes.at(handle).setFirstScene(firstScene);
	}

	void RenderChain::setOrderBetweenScenes(ChainHandle const& handle, std::shared_ptr<Scene> const& prevScene, std::shared_ptr<Scene> const& nextScene)
	{
		//TODO
	}

	void RenderChain::setFirstLink(ChainHandle const& handle)
	{
		auto it = std::find(m_invokationOrder.begin(), m_invokationOrder.end(), handle);
		if (it != m_invokationOrder.end())
		{
			m_invokationOrder.erase(it);
			m_invokationOrder.insert(m_invokationOrder.begin(), handle);
		}
	}

	void RenderChain::setOrderBetweenLinks(ChainHandle const& prevLink, ChainHandle const& nextLink)
	{
		//TODO
	}

	void RenderChain::buildChain(vk::Extent2D const& resolution)
	{
		for (auto& handle : m_invokationOrder)
		{
			auto& data = m_passes.at(handle);
			//createAttachments(*data.getLink()->getRenderingInfo(), resolution);
		}

		for (auto& handle : m_invokationOrder)
		{
			auto& data = m_passes.at(handle);
			auto& callbacks = data.getCallbacks();
			callbacks.onSetup();
		}
	}

	void RenderChain::execute()
	{
		for (auto& handle : m_invokationOrder)
		{
			auto& data = m_passes.at(handle);
			auto& callbacks = data.getCallbacks();
			callbacks.onEnter();
			//do submit here

			callbacks.onLeave();
		}
	}
}