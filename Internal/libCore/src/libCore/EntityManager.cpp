//! libCore
#include <libCore/EntityManager.hpp>
//! libUtils
#include <libUtils/Utils.hpp>

namespace libCore
{
	EntityManager::EntityManager()
	{
	}

	EntityManager::~EntityManager()
	{
	}

	std::weak_ptr<Entity> EntityManager::createEntity(std::string const& entityName)
	{
		auto entity = m_registry.create();
		auto coreEntity = std::make_shared<Entity>(entity, entityName);
		m_entities.emplace(coreEntity->operator()(), coreEntity);
		return coreEntity;
	}

	void EntityManager::removeEntity(std::weak_ptr<Entity> entity)
	{
		auto lockedEntity = entity.lock();
		if (!lockedEntity || m_entities.count(lockedEntity->operator()()) <= 0 || !m_registry.valid(lockedEntity->operator()()))
			return;

		m_registry.destroy(lockedEntity->operator()());
		m_entities.erase(lockedEntity->operator()());
	}
}