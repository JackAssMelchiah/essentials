//! libCore
#include <libCore/MaterialPBRLayout.hpp>
#include <libCore/UniformBuffer.hpp>
#include <libCore/VertexBuffer.hpp>
#include <libCore/ShaderResources.hpp>
//! libVulkan
#include <libVulkan/VulkanAll.hpp>
//! glm
#include <glm/gtc/type_ptr.hpp>

namespace libCore
{
	MaterialPBRLayout::MaterialPBRLayout(std::shared_ptr<libVulkan::VulkanInstance> const& instance, std::shared_ptr<libVulkan::VulkanDevice> const& device)
		: MaterialLayout(instance, device, MaterialLayoutType::PBR)
		, m_renderingInfo(nullptr)
	{
		//basic requirements for rendereing attachments - only depth and output attachment
		std::vector<vk::Format> inputs;
		vk::Format depthStencilFormat;
		std::vector<vk::Format> outputs;

		libVulkan::VulkanRenderingInfo::selectSimplePassFormats(*instance, inputs, depthStencilFormat, outputs);
		m_renderingInfo = std::make_shared<libVulkan::VulkanRenderingInfo>(inputs, depthStencilFormat, outputs);
	}

	std::shared_ptr<libVulkan::VulkanRenderingInfo> const& MaterialPBRLayout::getRenderingInfo() const
	{
		return m_renderingInfo;
	}

	std::shared_ptr<libVulkan::VulkanPipeline> MaterialPBRLayout::createPipeline(std::shared_ptr<libVulkan::VulkanPipelineLayout> pipelineLayout, vk::Extent2D const& resolution) const
	{
		auto device = m_device.lock();
		auto instance = m_instance.lock();

		auto vsShader = std::make_unique<libVulkan::VulkanShaderModule>(instance, device, "materialPBR");
		auto fsShader = std::make_unique<libVulkan::VulkanShaderModule>(instance, device, "materialPBR");
		vsShader->loadAndCreate("materialPBR.vert.spv");
		fsShader->loadAndCreate("materialPBR.frag.spv");
		auto shaders = std::make_shared<libVulkan::VulkanShaders>(std::move(vsShader), std::move(fsShader));

		auto vertexAttributeDesc = createObjectVertexAttributesDescription();
		return std::make_shared<libVulkan::VulkanPipeline>(instance, device, shaders, pipelineLayout, vertexAttributeDesc, m_renderingInfo, resolution, "PBR");
	}

	void MaterialPBRLayout::updateVertexDataFromResources(VertexBuffer& buffer, std::vector<glm::vec3> const& vertices, std::vector<glm::vec3> const& colors) const
	{
		//TODO implement
		assert(false);
		assert(vertices.size() == colors.size());
		//can interleave in paralel 
	}

	void MaterialPBRLayout::setObjectModelMatrix(glm::mat4 const& matrix, ShaderResources& resources, ShaderResources::Instance const& instance) const
	{
		resources.setInstanceResource("MODEL_MATRIX", matrix, instance);
	}

	glm::mat4 MaterialPBRLayout::getObjectModelMatrix(ShaderResources& resources, ShaderResources::Instance const& instance) const
	{
		return resources.getInstanceResource<glm::mat4>("MODEL_MATRIX", instance);
	}

	void MaterialPBRLayout::setObjectAlbedoColor(glm::vec3 const& albedo, ShaderResources& resources, ShaderResources::Instance const& instance) const
	{
		resources.setInstanceResource("ALBEDO_COLOR", glm::vec4(albedo, 0.f), instance);
	}

	glm::vec3 MaterialPBRLayout::getObjectAlbedoColor(ShaderResources& resources, ShaderResources::Instance const& instance) const
	{
		auto value = resources.getInstanceResource<glm::vec4>("ALBEDO_COLOR", instance);
		return { value.x, value.y, value.z };
	}

	void MaterialPBRLayout::setObjectAmbientOcclusion(float ambientOcclusion, ShaderResources& resources, ShaderResources::Instance const& instance) const
	{
		auto resource = resources.getInstanceResource<glm::vec4>("AO_MET_ROUGH", instance);
		resource.x = glm::clamp(ambientOcclusion, 0.f, 1.f);
		resources.setInstanceResource("AO_MET_ROUGH", resource, instance);
	}

	float MaterialPBRLayout::getObjectAmbientOcclusion(ShaderResources& resources, ShaderResources::Instance const& instance) const
	{
		auto value = resources.getInstanceResource<glm::vec4>("AO_MET_ROUGH", instance);
		return value.x;
	}

	void MaterialPBRLayout::setObjectMetallicness(float metallicness, ShaderResources& resources, ShaderResources::Instance const& instance) const
	{
		auto resource = resources.getInstanceResource<glm::vec4>("AO_MET_ROUGH", instance);
		resource.y = glm::clamp(metallicness, 0.f, 1.f);
		resources.setInstanceResource("AO_MET_ROUGH", resource, instance);
	}

	float MaterialPBRLayout::getObjectMetallicness(ShaderResources& resources, ShaderResources::Instance const& instance) const
	{
		auto value = resources.getInstanceResource<glm::vec4>("AO_MET_ROUGH", instance);
		return value.y;
	}

	void MaterialPBRLayout::setObjectRoughness(float roughness, ShaderResources& resources, ShaderResources::Instance const& instance) const
	{
		auto resource = resources.getInstanceResource<glm::vec4>("AO_MET_ROUGH", instance);
		resource.z = glm::clamp(roughness, 0.f, 1.f);
		resources.setInstanceResource("AO_MET_ROUGH", resource, instance);
	}

	float MaterialPBRLayout::getObjectRoughness(ShaderResources& resources, ShaderResources::Instance const& instance) const
	{
		auto value = resources.getInstanceResource<glm::vec4>("AO_MET_ROUGH", instance);
		return value.z;
	}

	void MaterialPBRLayout::setMaterialLightPosition(glm::vec3 const& lightPosition, ShaderResources& resources) const
	{
		resources.setInstanceResource("LIGHT_POSITION", glm::vec4(lightPosition, 0.f), resources.getBaseInstance());
	}

	void MaterialPBRLayout::setMaterialCameraPosition(glm::vec3 const& cameraPosition, ShaderResources& resources) const
	{
		resources.setInstanceResource("CAMERA_POSITION", glm::vec4(cameraPosition, 0.f), resources.getBaseInstance());
	}

	void MaterialPBRLayout::setMaterialLightColor(glm::vec3 const& lightColor, ShaderResources& resources) const
	{
		resources.setInstanceResource("LIGHT_COLOR", glm::vec4(lightColor, 0.f), resources.getBaseInstance());
	}

	void MaterialPBRLayout::bindObjectUniformData(libVulkan::VulkanDescriptorSet& modelDataDescriptorSet, ShaderResources& resources) const
	{
		bufferObjectResources(resources);

		auto objectBuffer = std::string("OBJECT_BUFFER");
		assert(resources.bufferExists(objectBuffer));

		resources.useBuffersWithSet({ objectBuffer }, modelDataDescriptorSet);
	}

	void MaterialPBRLayout::bindMaterialUniformData(libVulkan::VulkanDescriptorSet& materialDataDescriptorSet, ShaderResources& resources) const
	{
		bufferMaterialResources(resources);

		auto materialBuffer = std::string("PBR_BUFFER");
		assert(resources.bufferExists(materialBuffer));

		resources.useBuffersWithSet({ materialBuffer }, materialDataDescriptorSet);
	}

	void MaterialPBRLayout::bufferObjectResources(ShaderResources& resources) const
	{
		//specify resources used by object
		auto modelMatrixResource = std::string("MODEL_MATRIX");
		auto albedoColorResource = std::string("ALBEDO_COLOR");
		auto aoMetRoughResource = std::string("AO_MET_ROUGH");

		auto objectBuffer = std::string("OBJECT_BUFFER");

		//buffer resources
		resources.bufferResources({ modelMatrixResource, albedoColorResource, aoMetRoughResource }, objectBuffer);
	}

	void MaterialPBRLayout::bufferMaterialResources(ShaderResources& resources) const
	{
		//specify resources used by material
		auto materialCameraPositionResource = std::string("CAMERA_POSITION");
		auto materialLightPositionResource = std::string("LIGHT_POSITION");
		auto materialLightColorResource = std::string("LIGHT_COLOR");

		auto materialBuffer = std::string("PBR_BUFFER");

		//buffer resources
		resources.bufferResources({ materialCameraPositionResource, materialLightPositionResource, materialLightColorResource }, materialBuffer);
	}

	std::shared_ptr<libVulkan::VulkanDescriptorSetLayout> MaterialPBRLayout::createMaterialDescriptorSetLayout() const
	{
		// Phong uses light position and camera position data
		auto bindingUBO = vk::DescriptorSetLayoutBinding();
		{
			bindingUBO.binding = 0;
			bindingUBO.descriptorCount = 1;
			bindingUBO.descriptorType = vk::DescriptorType::eStorageBuffer;
			bindingUBO.stageFlags = vk::ShaderStageFlagBits::eAll;
			bindingUBO.pImmutableSamplers = nullptr;
		}
		return std::make_shared<libVulkan::VulkanDescriptorSetLayout>(m_instance.lock(), m_device.lock(), std::vector<vk::DescriptorSetLayoutBinding>{ bindingUBO }, "PBR");
	}

	std::shared_ptr<libVulkan::VulkanDescriptorSetLayout> MaterialPBRLayout::createObjectDescriptorSetLayout() const
	{
		auto layoutBindings = std::vector<vk::DescriptorSetLayoutBinding>();
		// Model matrix uniform
		auto bindingUBO = vk::DescriptorSetLayoutBinding();
		{
			bindingUBO.binding = 0;
			bindingUBO.descriptorCount = 1;
			bindingUBO.descriptorType = vk::DescriptorType::eStorageBuffer;
			bindingUBO.stageFlags = vk::ShaderStageFlagBits::eAll;
			bindingUBO.pImmutableSamplers = nullptr;
		}
		layoutBindings.push_back(bindingUBO);

		//create set layout
		return std::make_shared<libVulkan::VulkanDescriptorSetLayout>(m_instance.lock(), m_device.lock(), layoutBindings, "PBR");
	}

	std::shared_ptr<libVulkan::VulkanVertexAttributesDescription> MaterialPBRLayout::createObjectVertexAttributesDescription() const
	{
		auto descriptions = std::vector<vk::VertexInputAttributeDescription>();
		auto binding = 0;

		// REQUIREMENTS:
		// layout(0) - position (VEC3)
		// layout(1) - normal	(VEC3)
		{
			auto attrib = vk::VertexInputAttributeDescription();
			// Position
			{
				attrib.binding = binding;
				attrib.format = vk::Format::eR32G32B32Sfloat;
				attrib.location = 0;
				attrib.offset = 0;
			}
			descriptions.push_back(attrib);
			//Normal
			{
				attrib.binding = binding;
				attrib.format = vk::Format::eR32G32B32Sfloat;
				attrib.location = 1;
				attrib.offset = sizeof(glm::vec3);
			}
			descriptions.push_back(attrib);
		}

		auto bindingDescription = vk::VertexInputBindingDescription();
		{
			bindingDescription.inputRate = vk::VertexInputRate::eVertex;
			bindingDescription.binding = binding;
			bindingDescription.stride = 2 * sizeof(glm::vec3);
		}
		return std::make_shared<libVulkan::VulkanVertexAttributesDescription>(descriptions, std::vector<vk::VertexInputBindingDescription>{ bindingDescription });
	}

	void MaterialPBRLayout::allocateObjectData(ShaderResources& resources) const
	{
		resources.setInstanceResource("MODEL_MATRIX", glm::identity<glm::mat4>(), resources.getBaseInstance());
		resources.setInstanceResource("ALBEDO_COLOR", glm::vec4(1.f, 0.f, 0.f, 0.f), resources.getBaseInstance());
		resources.setInstanceResource("AO_MET_ROUGH", glm::vec4(0.f, 0.f, 0.f, 0.f), resources.getBaseInstance());
	}

	void MaterialPBRLayout::allocateMaterialData(ShaderResources& resources) const
	{
		resources.setInstanceResource("LIGHT_POSITION", glm::vec4(0.f, 0.f, 0.f, 0.f), resources.getBaseInstance());
		resources.setInstanceResource("CAMERA_POSITION", glm::vec4(0.f, 0.f, 0.f, 0.f), resources.getBaseInstance());
		resources.setInstanceResource("LIGHT_COLOR", glm::vec4(1.f, 1.f, 1.f, 0.f), resources.getBaseInstance());
	}
}
