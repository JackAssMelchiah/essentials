#include <libCore/Skeleton.hpp>
#include <libCore/BasicRenderComponent.hpp>
#include <libCore/Scene.hpp>
#include <libCore/Renderer.hpp>
#include <libCore/TransformGraphManager.hpp>
#include <libCore/Loader.hpp>
#include <libUtils/Shape.hpp>

namespace libCore
{
	Skeleton::Skeleton(std::shared_ptr<Renderer> const& renderer, std::shared_ptr<Scene> const& scene, std::shared_ptr<TransformGraphManager> const& trManager, std::string const& name, std::string const& boneRepresentationOverload)
		: m_scene(scene)
		, m_basicRenderComponent(nullptr)
	{
		auto loader = Loader(renderer, trManager);

		//todo custom shape load
		if (!boneRepresentationOverload.empty())
		{
			auto path = std::string("");
			if (!boneRepresentationOverload.empty())
				path = boneRepresentationOverload;
			loader.load(path, libCore::Loader::Material::UNSHADED);
		}

		auto verices = std::vector<libUtils::Shape::ColoredVertex>();
		libUtils::Shape::cube(verices);

		m_basicRenderComponent = std::make_shared<BasicRenderComponent>(renderer, scene, verices, name);
		m_basicRenderComponent->setHide(true);
	}

	void Skeleton::addJoint(Joint const& joint)
	{
		m_joints.push_back(joint);
		if (m_joints.size() == 1) //render component has already single instance
			m_basicRenderComponent->setMatrix(joint.inverseBP, 0);
		else
			m_basicRenderComponent->addInstance(joint.inverseBP);
	}

	void Skeleton::setVisible(bool on)
	{
		m_basicRenderComponent->setHide(!on);
	}

	bool Skeleton::getVisible()
	{
		return !m_basicRenderComponent->getHidden();
	}
}