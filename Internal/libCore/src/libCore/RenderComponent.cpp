//! libCore
#include <libCore/RenderComponent.hpp>
#include <libCore/Renderer.hpp>
#include <libCore/ShaderResources.hpp>

namespace libCore 
{
	RenderComponent::RenderComponent(std::shared_ptr<Renderer> const& renderer, std::shared_ptr<MaterialLayout> materialLayout, uint32_t instances, uint32_t vertexShaderInvocationCount)
		: m_renderer(renderer)
		, m_materialLayout(materialLayout)
		, m_materialType(materialLayout->getType())
		, m_vertexInvocationCount(vertexShaderInvocationCount)
		, m_objectResources(std::make_shared<ShaderResources>(renderer->getInstance().lock(), renderer->getDevice().lock()))
	{
		//allocate resources for material layout - so they can be bound to gpu anytime
		materialLayout->allocateObjectData(*m_objectResources);

		auto instancePtr = m_objectResources->getBaseInstance();
		m_objectResources->setInstanceResource("AABB_COLOR", glm::vec4( 1.f, 0.f, 0.f, 0.f), instancePtr);
	}

	void RenderComponent::setVertexBuffer(std::shared_ptr<VertexBuffer> buffer)
	{
		m_vbo = buffer;
	}

	void RenderComponent::setIndexBuffer(std::shared_ptr<IndexBuffer> buffer)
	{
		m_ibo = buffer;
	}

	ShaderResources::Instance RenderComponent::addInstance()
	{
		auto instance = m_objectResources->createInstance(m_objectResources->getBaseInstance());
		return instance;
	}

	void RenderComponent::removeInstance()
	{
		if (getInstanceCount() == 1)
			return;
		m_objectResources->removeInstance(getInstanceCount() - 1);
	}

	bool RenderComponent::usesIndexBuffer() const
	{
		return m_ibo.get() != nullptr;
	}

	void RenderComponent::record(libVulkan::VulkanCommandBuffer& commandBuffer)
	{
		if (m_vbo)
			commandBuffer.bindVertexBuffers({ m_vbo->operator()() });
		
		if (m_ibo)
			commandBuffer.bindIndexBuffer(m_ibo->operator()(), m_ibo->getType());
				
		if (m_ibo)
			commandBuffer.drawIndexed(m_vertexInvocationCount, getInstanceCount(), 0, 0);
		else
			commandBuffer.draw(m_vertexInvocationCount, getInstanceCount(), 0, 0);
	}

	MaterialLayout::MaterialLayoutType RenderComponent::getMaterialType() const
	{
		return m_materialType;
	}

	std::shared_ptr<MaterialLayout> RenderComponent::getMaterialLayout()
	{
		return m_materialLayout;
	}

	ShaderResources& RenderComponent::getResources()
	{
		return *m_objectResources;
	}

	void RenderComponent::setName(std::string const& name)
	{
		m_name = name;
	}

	std::string const& RenderComponent::getName() const
	{
		return m_name;
	}

	void RenderComponent::setAABB(std::shared_ptr<AABB> const& aabb)
	{
		m_aabb = aabb;
	}

	std::shared_ptr<AABB> RenderComponent::getAABB(uint32_t instance)
	{
		auto retval = std::shared_ptr<AABB>();
		if ((instance < getInstanceCount()) && m_aabb)
		{
			auto instancePtr = m_objectResources->getBaseInstance();
			instancePtr.toInstance(int32_t(instance));

			auto originalMin = glm::vec4(m_aabb->getMin(), 1.f);
			auto originalMax = glm::vec4(m_aabb->getMax(), 1.f);

			auto modelMatrix = m_objectResources->getInstanceResource<glm::mat4>("MODEL_MATRIX", instancePtr);

			//modelMatrix *
			auto corners = std::array<glm::vec4, 8>
			{
				modelMatrix * glm::vec4(originalMin.x, originalMin.y, originalMin.z, 1.0f),
				modelMatrix * glm::vec4(originalMax.x, originalMin.y, originalMin.z, 1.0f),
				modelMatrix * glm::vec4(originalMin.x, originalMax.y, originalMin.z, 1.0f),
				modelMatrix * glm::vec4(originalMax.x, originalMax.y, originalMin.z, 1.0f),
				modelMatrix * glm::vec4(originalMin.x, originalMin.y, originalMax.z, 1.0f),
				modelMatrix * glm::vec4(originalMax.x, originalMin.y, originalMax.z, 1.0f),
				modelMatrix * glm::vec4(originalMin.x, originalMax.y, originalMax.z, 1.0f),
				modelMatrix * glm::vec4(originalMax.x, originalMax.y, originalMax.z, 1.0f)
			};

			auto updatedMin = glm::vec4(FLT_MAX);
			auto updatedMax = glm::vec4(-FLT_MAX);

			for (auto& corner: corners)
			{
				updatedMin = glm::min(updatedMin, corner);
				updatedMax = glm::max(updatedMax, corner);
			}
			retval = std::make_shared<AABB>(updatedMin, updatedMax);
			return retval;
		}
		return retval;
	}

	uint32_t RenderComponent::getInstanceCount() const
	{
		return m_objectResources->getInstanceCount();
	}

	void RenderComponent::setAABBColor(glm::vec3 const& color, uint32_t instance)
	{
		auto instancePtr = m_objectResources->getBaseInstance();
		instancePtr.toInstance(int32_t(instance));
		m_objectResources->setInstanceResource("AABB_COLOR", glm::vec4(color, 0.f), instancePtr);
	}
	
	glm::vec3 RenderComponent::getAABBColor(uint32_t instance) const
	{
		auto instancePtr = m_objectResources->getBaseInstance();
		instancePtr.toInstance(int32_t(instance));
		
		auto color = m_objectResources->getInstanceResource<glm::vec4>("AABB_COLOR", instancePtr);
		return glm::vec3(color);
	}
}