//! libCore
#include <libCore/UniformBuffer.hpp>
#include <libCore/Renderer.hpp>
//! libUtils
#include <libUtils/Utils.hpp>
//! libVulkan
#include <libVulkan/VulkanAll.hpp>

namespace libCore
{
	UniformBuffer::UniformBuffer(std::shared_ptr<libVulkan::VulkanInstance> const& instance, std::shared_ptr<libVulkan::VulkanDevice> const& device, uint32_t bytes, std::string const& name)
		: Buffer(instance, device, bytes, name)
		, m_type(vk::DescriptorType::eStorageBuffer)
	{
		auto deviceLimits = vk::PhysicalDeviceLimits();

		instance->getPhysicalDeviceLimits(deviceLimits);
		
		initializeBuffer(libUtils::align(bytes, uint32_t(deviceLimits.nonCoherentAtomSize)));
	}

	UniformBuffer::~UniformBuffer()
	{
		if (m_masterBuffer->mapped())
			m_masterBuffer->unmap();
	}

	vk::DescriptorType UniformBuffer::getType() const
	{
		return m_type;
	}

	void UniformBuffer::initializeBuffer(uint32_t bytes)
	{
		auto device = m_device.lock();
		auto instance = m_instance.lock();
		// {eHostVisible | eHostCoherent} - CPU writes, copies to GPU, or CPU Writes, GPU reads directly
		m_masterBuffer = std::make_shared<libVulkan::VulkanBuffer>(instance, device, vk::BufferUsageFlagBits::eStorageBuffer, vk::MemoryPropertyFlagBits::eHostVisible | vk::MemoryPropertyFlagBits::eHostCoherent, bytes, m_name);
	}

	//void UniformBuffer::updateDescriptors()
	//{
	//	auto descriptorSet = m_descriptorSet.lock();
	//	auto bindings = descriptorSet->getLayout().getLayoutBindings();
	//	for (auto& binding : bindings)
	//	{
	//		if (binding.descriptorType == vk::DescriptorType::eUniformBuffer) //FIXME: multiple might be here and not belong to this UBO...�prob will need to specify list of binding points beforehand
	//			descriptorSet->write(binding.binding, vk::DescriptorType::eUniformBuffer, *m_masterBuffer);
	//	}
	//}
}