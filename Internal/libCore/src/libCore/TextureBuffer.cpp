#include <libCore/TextureBuffer.hpp>
//! libVulkan
#include <libVulkan/VulkanDescriptorSet.hpp>

namespace libCore
{
	TextureBuffer::TextureBuffer(std::shared_ptr<libCore::Renderer> const& renderer, std::vector<ImageInfo> const& images, bool sampledImages, std::string const& name)
		: m_renderer(renderer)
		, m_imageInfos(images)
		, m_size(0)
		, m_sampled(sampledImages)
		, m_name(name)
	{
		for (auto& imageinfo : m_imageInfos)
		{
			initializeImage(imageinfo, sampledImages);
			m_size += imageinfo.byteSize;
		}

		//initialize images layout to general
		renderer->recordAndSubmit([&](libVulkan::VulkanCommandBuffer& commandBuffer)->void
			{
				for (auto& img : m_images)
				{
					libVulkan::VulkanBarrier::recordBarriers(commandBuffer, { img.first->generateBarrier(vk::ImageLayout::eUndefined, vk::ImageLayout::eGeneral, vk::AccessFlagBits2::eNone, vk::PipelineStageFlagBits2::eTopOfPipe, vk::AccessFlagBits2::eNone, vk::PipelineStageFlagBits2::eBottomOfPipe) });
				}
			}
		);
	}

	std::vector<TextureBuffer::ImageInfo> const& TextureBuffer::getTextureInfos() const
	{
		return m_imageInfos;
	}

	void TextureBuffer::getDescriptorImageInfos(std::vector<vk::DescriptorImageInfo>& infos)
	{
		infos.clear();
		for (auto& imageWithSampler : m_images)
		{
			auto& currentImage = imageWithSampler.first;
			auto currentImageInfo = vk::DescriptorImageInfo();
			currentImage->createDescriptorImageInfo(currentImageInfo, vk::ImageLayout::eGeneral); //lets keep it in general to support most of operations with the image
			if (m_sampled)
			{
				auto& currentSampler = imageWithSampler.second;
				currentImageInfo.sampler = currentSampler->operator()();
			}
			infos.push_back(currentImageInfo);
		}
	}

	void TextureBuffer::initializeImage(ImageInfo const& imageInfo, bool sampledImage)
	{
		auto renderer = m_renderer.lock();
		auto instance = renderer->getInstance().lock();
		auto device = renderer->getDevice().lock();

		auto image = std::make_shared<libVulkan::VulkanImage>(instance, device, vk::Extent2D{ uint32_t(imageInfo.resolution.x), uint32_t(imageInfo.resolution.y)}, libVulkan::VulkanImage::Info(imageInfo.format, vk::ImageAspectFlagBits::eColor, vk::ImageUsageFlagBits::eTransferDst | vk::ImageUsageFlagBits::eSampled, vk::MemoryPropertyFlagBits::eDeviceLocal), m_name);
		auto sampler = std::shared_ptr<libVulkan::VulkanImageSampler>();
		// creates sampler if sampled
		if (sampledImage)
			sampler = std::make_shared<libVulkan::VulkanImageSampler>(instance, device, m_name);
		m_images.push_back(std::make_pair(image, sampler));
	}
}