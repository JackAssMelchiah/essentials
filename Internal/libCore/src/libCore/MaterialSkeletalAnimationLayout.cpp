//! libCore
#include <libCore/MaterialSkeletalAnimationLayout.hpp>
#include <libCore/UniformBuffer.hpp>
#include <libCore/VertexBuffer.hpp>
#include <libCore/ShaderResources.hpp>

//! libVulkan
#include <libVulkan/VulkanAll.hpp>
//! glm
#include <glm/gtc/type_ptr.hpp>

namespace libCore
{
	MaterialSkeletalAnimationLayout::MaterialSkeletalAnimationLayout(std::shared_ptr<libVulkan::VulkanInstance> const& instance, std::shared_ptr<libVulkan::VulkanDevice> const& device, vk::PrimitiveTopology topology)
		: MaterialLayout(instance, device, MaterialLayoutType::SKELETAL_ANIMATION)
		, m_primitiveTopology(topology)
		, m_renderingInfo(nullptr)
	{
		//basic requirements for rendereing attachments - only depth and output attachment
		std::vector<vk::Format> inputs;
		vk::Format depthStencilFormat;
		std::vector<vk::Format> outputs;

		libVulkan::VulkanRenderingInfo::selectSimplePassFormats(*instance, inputs, depthStencilFormat, outputs);
		m_renderingInfo = std::make_shared<libVulkan::VulkanRenderingInfo>(inputs, depthStencilFormat, outputs);
	}

	std::shared_ptr<libVulkan::VulkanRenderingInfo> const& MaterialSkeletalAnimationLayout::getRenderingInfo() const
	{
		return m_renderingInfo;
	}

	std::shared_ptr<libVulkan::VulkanPipeline> MaterialSkeletalAnimationLayout::createPipeline(std::shared_ptr<libVulkan::VulkanPipelineLayout> pipelineLayout, vk::Extent2D const& resolution) const
	{
		auto device = m_device.lock();
		auto instance = m_instance.lock();

		auto vsShader = std::make_unique<libVulkan::VulkanShaderModule>(instance, device, "materialSkeletalAnimation");
		auto fsShader = std::make_unique<libVulkan::VulkanShaderModule>(instance, device, "materialUnshaded");
		vsShader->loadAndCreate("materialSkeletalAnimation.vert.spv");
		fsShader->loadAndCreate("materialSkeletalAnimation.frag.spv");
		auto shaders = std::make_shared<libVulkan::VulkanShaders>(std::move(vsShader), std::move(fsShader));

		auto vertexAttributeDesc = createObjectVertexAttributesDescription();
		auto pipeline = std::make_shared<libVulkan::VulkanPipeline>(instance, device, shaders, pipelineLayout, vertexAttributeDesc, m_renderingInfo, resolution, "SkeletalAnimation");
		pipeline->setInputAssebly(m_primitiveTopology, false);
		pipeline->compilePipeline();
		return pipeline;
	}

	void MaterialSkeletalAnimationLayout::bindObjectUniformData(libVulkan::VulkanDescriptorSet& modelDataDescriptorSet, ShaderResources& resources) const
	{
		bufferObjectResources(resources);

		//descriptor write
		auto modelMatrixBuffer = std::string("OBJECT_MATRIX_BUFFER");
		assert(resources.bufferExists(modelMatrixBuffer));

		auto objectIBPMetaBuffer = std::string("OBJECT_IBP_META_BUFFER");
		assert(resources.bufferExists(objectIBPMetaBuffer));

		auto objectIBPBuffer = std::string("OBJECT_IBP_BUFFER");
		assert(resources.bufferExists(objectIBPBuffer));

		resources.useBuffersWithSet({ modelMatrixBuffer, objectIBPMetaBuffer, objectIBPBuffer }, modelDataDescriptorSet);
	}

	void MaterialSkeletalAnimationLayout::bindMaterialUniformData(libVulkan::VulkanDescriptorSet& materialDataDescriptorSet, ShaderResources& resources) const
	{
		// no material resources for this Material
	}

	void MaterialSkeletalAnimationLayout::bufferObjectResources(ShaderResources& resources) const
	{
		//resources.bufferResources ->this interleaves the data - there is no way to non-interleave this, which would be needed for something like
		//layout(std430, set = OBJECTS_RESOURCES, binding = 0) buffer UniformBufferObject {
		//	mat4 modelMatrix[];
		//	int jointCount[]; // Number of joints for each instance
		//} instanceData;
		//therefore use separate buffers on bind points

		//model matrix unbound array for each instance
		{
			auto modelMatrixResource = std::string("MODEL_MATRIX");
			auto objectMatrixBuffer = std::string("OBJECT_MATRIX_BUFFER");
			resources.bufferResources({ modelMatrixResource }, objectMatrixBuffer);
		}

		//jointCount unbound array for each instance
		{
			auto IBPMetaResource = std::string("IBP_META");
			auto objectIBPMetaBuffer = std::string("OBJECT_IBP_META_BUFFER");
			resources.bufferResources({ IBPMetaResource }, objectIBPMetaBuffer);
		}

		//inverse bind pose matrices - one array for all instances -> esentially 2d array linearized
		{
			auto IBPResource = std::string("IBP_MATRICES");
			auto objectIBPBuffer = std::string("OBJECT_IBP_BUFFER");
			resources.bufferResources({ IBPResource }, objectIBPBuffer);
		}
	}

	void MaterialSkeletalAnimationLayout::bufferMaterialResources(ShaderResources& resources) const
	{
		//no resources used by material
	}

	void MaterialSkeletalAnimationLayout::updateVertexDataFromResources(VertexBuffer& buffer, std::vector<glm::vec3> const& vertices, std::vector<glm::vec3> const& colors) const
	{
		//TODO implement
		assert(false);
		assert(vertices.size() == colors.size());
		//can interleave in paralel 
	}

	void MaterialSkeletalAnimationLayout::setObjectModelMatrix(glm::mat4 const& matrix, ShaderResources& resources, ShaderResources::Instance const& instance) const
	{
		resources.setInstanceResource("MODEL_MATRIX", matrix, instance);
	}

	glm::mat4 MaterialSkeletalAnimationLayout::getObjectModelMatrix(ShaderResources& resources, ShaderResources::Instance const& instance) const
	{
		return resources.getInstanceResource<glm::mat4>("MODEL_MATRIX", instance);
	}

	void MaterialSkeletalAnimationLayout::setObjectInverseBindMatrices(std::vector<glm::mat4> const& matrices, ShaderResources& resources, ShaderResources::Instance const& instance) const
	{
		resources.setInstanceResource("IBP_MATRICES", matrices, instance);
		
		//metadata -> size of that instance's IBPMatrices and the offset on which they are stored 
		auto metadata = glm::ivec2(int(matrices.size()), 0);
		if (instance.index != 0)
		{
			auto inst = instance;
			inst.toInstance(instance.index - 1);
			auto prevMetadata = resources.getInstanceResource<glm::ivec2>("IBP_META", inst);
			metadata.y = prevMetadata.y + prevMetadata.x;
		}
		resources.setInstanceResource("IBP_META", metadata, instance);
	}

	std::shared_ptr<libVulkan::VulkanDescriptorSetLayout> MaterialSkeletalAnimationLayout::createMaterialDescriptorSetLayout() const
	{
		//while not used, it needs to be set-up
		return std::make_shared<libVulkan::VulkanDescriptorSetLayout>(m_instance.lock(), m_device.lock(), std::vector<vk::DescriptorSetLayoutBinding>{}, "SkeletalAnimation");
	}

	std::shared_ptr<libVulkan::VulkanDescriptorSetLayout> MaterialSkeletalAnimationLayout::createObjectDescriptorSetLayout() const
	{
		auto layoutBindings = std::vector<vk::DescriptorSetLayoutBinding>();
		{//model matrices
			auto bindingUBO = vk::DescriptorSetLayoutBinding();
			{
				bindingUBO.binding = 0;
				bindingUBO.descriptorCount = 1;
				bindingUBO.descriptorType = vk::DescriptorType::eStorageBuffer;
				bindingUBO.stageFlags = vk::ShaderStageFlagBits::eVertex;
				bindingUBO.pImmutableSamplers = nullptr;
			}
			layoutBindings.push_back(bindingUBO);
		}
		{//metadata for IBP matrices
			auto bindingUBO = vk::DescriptorSetLayoutBinding();
			{
				bindingUBO.binding = 0;
				bindingUBO.descriptorCount = 1;
				bindingUBO.descriptorType = vk::DescriptorType::eStorageBuffer;
				bindingUBO.stageFlags = vk::ShaderStageFlagBits::eVertex;
				bindingUBO.pImmutableSamplers = nullptr;
			}
			layoutBindings.push_back(bindingUBO);
		}
		{//IBP matrices
			auto bindingUBO = vk::DescriptorSetLayoutBinding();
			{
				bindingUBO.binding = 1;
				bindingUBO.descriptorCount = 1;
				bindingUBO.descriptorType = vk::DescriptorType::eStorageBuffer;
				bindingUBO.stageFlags = vk::ShaderStageFlagBits::eVertex;
				bindingUBO.pImmutableSamplers = nullptr;
			}
			layoutBindings.push_back(bindingUBO);
		}
		
		//create set layout
		return std::make_shared<libVulkan::VulkanDescriptorSetLayout>(m_instance.lock(), m_device.lock(), layoutBindings, "SkeletalAnimation");
	}

	std::shared_ptr<libVulkan::VulkanVertexAttributesDescription> MaterialSkeletalAnimationLayout::createObjectVertexAttributesDescription() const
	{
		auto descriptions = std::vector<vk::VertexInputAttributeDescription>();
		auto binding = 0;

		// REQUIREMENTS:
		// layout(0) - position (VEC3)
		// layout(1) - color	(VEC3)
		// layout(2) - jointIDs (IVEC3)
		{
			auto attrib = vk::VertexInputAttributeDescription();
			// Position
			{
				attrib.binding = binding;
				attrib.format = vk::Format::eR32G32B32Sfloat;
				attrib.location = 0;
				attrib.offset = 0;
			}
			descriptions.push_back(attrib);
			// Color
			{
				attrib.binding = binding;
				attrib.format = vk::Format::eR32G32B32Sfloat;
				attrib.location = 1;
				attrib.offset = sizeof(glm::vec3);
			}
			descriptions.push_back(attrib);
			// JointIDs
			{
				attrib.binding = binding;
				attrib.format = vk::Format::eR32G32B32A32Sint;
				attrib.location = 2;
				attrib.offset = sizeof(glm::ivec3);
			}
			descriptions.push_back(attrib);
		}

		auto bindingDescription = vk::VertexInputBindingDescription();
		{
			bindingDescription.inputRate = vk::VertexInputRate::eVertex;
			bindingDescription.binding = binding;
			bindingDescription.stride = 2 * sizeof(glm::vec3);
		}
		return std::make_shared<libVulkan::VulkanVertexAttributesDescription>(descriptions, std::vector<vk::VertexInputBindingDescription>{ bindingDescription });
	}

	void MaterialSkeletalAnimationLayout::allocateObjectData(ShaderResources& resources) const
	{
		resources.setInstanceResource("MODEL_MATRIX", glm::identity<glm::mat4>(), resources.getBaseInstance());
		resources.setInstanceResource("IBP_META", glm::ivec2(1, 0), resources.getBaseInstance());
		resources.setInstanceResource("IBP_MATRICES", glm::identity<glm::mat4>(), resources.getBaseInstance());
	}

	void MaterialSkeletalAnimationLayout::allocateMaterialData(ShaderResources& resources) const
	{
	}
}
