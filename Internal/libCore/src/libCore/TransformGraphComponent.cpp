//! libCore
#include <libCore/TransformGraphComponent.hpp>

namespace libCore
{
	TransformGraphComponent::TransformGraphComponent(std::string const& nodePath, glm::mat4 const& localMatrix)
		: m_componentData({ nodePath }, { localMatrix }, { glm::mat4(1.f) })
	{
	}

	uint32_t TransformGraphComponent::getInstanceCount() const
	{
		return m_componentData.getInstanceCount();
	}

	void TransformGraphComponent::addInstance(std::string const& localNodePath, glm::mat4 const& localMatrix)
	{
		m_componentData.addInstance(localNodePath, localMatrix);
	}

	void TransformGraphComponent::removeInstance()
	{
		if (m_componentData.localPaths.size() == 1)
			return;
		m_toErase.push_back(m_componentData.localPaths.at(m_componentData.localPaths.size() - 1));
		m_componentData.removeInstance();
	}

	std::string TransformGraphComponent::getLocalNodePath(uint32_t instance) const
	{
		if (m_componentData.validInstance(instance))
			return m_componentData.localPaths.at(instance);
		return "";
	}

	glm::mat4 TransformGraphComponent::getLocalMatrix(uint32_t instance)
	{
		if (m_componentData.validInstance(instance))
			return m_componentData.localMatrices.at(instance);
		return glm::mat4(1.f);
	}

	glm::mat4 TransformGraphComponent::getWorldMatrix(uint32_t instance)
	{
		if (m_componentData.validInstance(instance))
			return m_componentData.worldMatrices.at(instance);
		return glm::mat4(1.f);
	}

	void TransformGraphComponent::setLocalMatrix(glm::mat4 const& matrix, uint32_t instance)
	{
		if (m_componentData.validInstance(instance))
			m_componentData.localMatrices.at(instance) = matrix;
	}

	void TransformGraphComponent::setWorldMatrix(glm::mat4 const& matrix, uint32_t instance)
	{
		if (m_componentData.validInstance(instance))
			m_componentData.worldMatrices.at(instance) = matrix;
	}
}