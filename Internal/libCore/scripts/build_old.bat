del materialUnshaded.vert.spv
del materialUnshaded.frag.spv
glslangValidator -V -gVS materialUnshaded.vert -o materialUnshaded.vert.spv
glslangValidator -V -gVS materialUnshaded.frag -o materialUnshaded.frag.spv

del materialUnshadedTextured.vert.spv
del materialUnshadedTextured.frag.spv
glslangValidator -V -gVS materialUnshadedTextured.vert -o materialUnshadedTextured.vert.spv
glslangValidator -V -gVS materialUnshadedTextured.frag -o materialUnshadedTextured.frag.spv

del materialPhong.vert.spv
del materialPhong.frag.spv
glslangValidator -V -gVS materialPhong.vert -o materialPhong.vert.spv
glslangValidator -V -gVS materialPhong.frag -o materialPhong.frag.spv

del materialPhongNormalMap.vert.spv
del materialPhongNormalMap.frag.spv
glslangValidator -V -gVS materialPhongNormalMap.vert -o materialPhongNormalMap.vert.spv
glslangValidator -V -gVS materialPhongNormalMap.frag -o materialPhongNormalMap.frag.spv

del materialPBR.vert.spv
del materialPBR.frag.spv
glslangValidator -V -gVS materialPBR.vert -o materialPBR.vert.spv
glslangValidator -V -gVS materialPBR.frag -o materialPBR.frag.spv

set /p DUMMY=Hit ENTER to continue...