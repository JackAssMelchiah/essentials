#! usr/bin/bash

params=2;
wdir=$(pwd)/

if (($# < $params))
then
    echo "Bad num of args, fisrt should be input folder, second output"
    exit 1
fi

#also check that src and dst dirs exist
if [ ! -d $1 ]; then echo "Bad inputDir"; exit 1; fi
if [ ! -d $2 ]; then echo "Bad outputDir"; exit 1; fi

cd $1
for FILE in *; do
    if [ -f $FILE ]; then 
        echo $FILE
        #remove previous file if it existed
        if [ -f $2/$FILE.spv ]; then rm $2/$FILE.spv; fi
        #removes directory portion from file - for now not needed
        baseName=$(basename "$FILE")
        #pattern expression # removes shortest match of *. from begining
        extension="$/{basename#*.}"
        glslangValidator.exe -V --target-env vulkan1.3 -gVS $1/$FILE -o $2/$FILE.spv
    fi
done
