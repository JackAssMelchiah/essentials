#pragma once
//! libCore
#include <libCore/Buffer.hpp>
#include <libCore/Renderer.hpp>
//! libVulkan
#include <libVulkan/VulkanCommandPool.hpp>
#include <libVulkan/VulkanCommandBuffer.hpp>

namespace libCore
{
    //! Consists of vertex buffer with memory on device_local, and staging buffer
    //! - should be used for resources which are copied to GPU once and then re-read multiple times
    class VertexBuffer: public Buffer
    {
    public:
        VertexBuffer(std::shared_ptr<Renderer> const& renderer, uint32_t bytes, std::string const& name);
        ~VertexBuffer() = default;
        //! For single time data buffering, buffers content of whole vector into vbo on offset
        //! Transfering to device local memory via stage buffer -  not suitable for streaming data in
        template <typename T>
        void bufferData(std::vector<T>const& data, uint32_t offset)
        {
            auto renderer = m_renderer.lock();
            auto device = renderer->getDevice().lock();
            auto instance = renderer->getInstance().lock();

            auto bytes = uint32_t(sizeof(T) * data.size());
            assert(bytes + offset <= m_size);

            //1. creates staging buffer            
            // {eHostVisible|eHostCoherent} - CPU writes, copies to GPU, or CPU Writes, GPU reads directly
            auto stageBuffer = std::make_shared<libVulkan::VulkanBuffer>(instance, device, vk::BufferUsageFlagBits::eTransferSrc, vk::MemoryPropertyFlagBits::eHostVisible | vk::MemoryPropertyFlagBits::eHostCoherent, bytes, "Vertex staging buffer");
            //2. map that buffer to be writtable
            stageBuffer->map();
            //3. copy data to it
            stageBuffer->bufferData(data);
            //4. unmap it since this is not persistent buffer and after copyQue will be deleted
            stageBuffer->unmap();
            //5. record command buffer with data transfer from staging buffer to the vertex buffer
            renderer->recordAndSubmit([=](libVulkan::VulkanCommandBuffer& commandBuffer)->void
                {
                    commandBuffer.copyBuffer(*stageBuffer, *m_masterBuffer, { vk::BufferCopy{ 0, offset, bytes } });
                }
            );
        }

    protected:
        //! Initializes raw buffer 
        void initializeBuffer(uint32_t bytes);

    protected:
        std::weak_ptr<libCore::Renderer> m_renderer;
    };
}