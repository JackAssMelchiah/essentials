#pragma once

namespace libCore
{
	class RenderManager
	{
	public:
		RenderManager();
		~RenderManager() = default;
	};
}