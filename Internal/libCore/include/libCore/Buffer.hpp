#pragma once
//! libVulkan
#include <libVulkan/VulkanBuffer.hpp>

namespace libVulkan
{
	class VulkanBarrier;
}

namespace libCore
{
	class Buffer
	{
	public:
		Buffer(std::shared_ptr<libVulkan::VulkanInstance> const& instance, std::shared_ptr<libVulkan::VulkanDevice> const& device, uint32_t bytes, std::string const& name);
		virtual ~Buffer() = default;
		//! Retuns size of buffer which has been adjusted
		uint32_t getByteSize() const;

		//! Getter for libVulkan object
		libVulkan::VulkanBuffer& operator()();

		//! Generates barrier for safe buffer usage
		std::shared_ptr<libVulkan::VulkanBarrier> generateBarrier(vk::AccessFlags2 srcAccessFlags, vk::AccessFlags2 dstAccessFlags, vk::PipelineStageFlagBits2 srcStage, vk::PipelineStageFlagBits2 dstStage);

	protected:
		std::weak_ptr<libVulkan::VulkanInstance> m_instance;
		std::weak_ptr<libVulkan::VulkanDevice> m_device;

		uint32_t m_size;
		//! Buffer which is accesed when using command buffers with rendering
		std::shared_ptr<libVulkan::VulkanBuffer> m_masterBuffer;

		std::string m_name;
	};
}