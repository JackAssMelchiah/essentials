#pragma once

#include <libCore/Entity.hpp>

#include <libUtils/Shape.hpp>

#include <memory>
#include <set>


namespace libCore
{
	class Renderer;
	class Scene;
	class MaterialUnshadedLayout;
	class RenderComponent;
}

namespace libCore
{
	class BasicRenderComponent
	{
	public:
		BasicRenderComponent(std::shared_ptr<Renderer> const& renderer, std::shared_ptr<Scene> const& scene, std::vector<libUtils::Shape::ColoredVertex> const& vertexData, std::string const& name);
		~BasicRenderComponent();

		void addInstance(glm::mat4 const& matrix);
		uint32_t getInstanceCount() const;

		void setMatrix(glm::mat4 const& matrix, uint32_t instance);
		glm::mat4 getMatrix(uint32_t instance) const;
		
		void setHide(bool hide);
		bool getHidden() const;

		void bufferData();

	protected:
		std::weak_ptr<Renderer> m_renderer;
		std::weak_ptr<Scene> m_scene;
		std::weak_ptr<Entity> m_entity;
		std::shared_ptr<MaterialUnshadedLayout> m_materialLayout;
		std::shared_ptr<RenderComponent> m_renderComponent;

		bool m_hidden;
	};
}