#pragma once
//! libCore
#include <libCore/Camera.hpp>
//! libInputs
#include <libInputs/Handler.hpp>

namespace libCore
{
	class Handler : public libInputs::Handler
	{
	public:
		Handler() = default;
		virtual ~Handler() = default;
		//! Propagates the change into camera
		virtual void handle(Camera& camera) = 0;

	private: 
		//! Privatize the previous imp of handle
		virtual void handle(glm::mat4& viewMatrix)
		{
		};
	};
}