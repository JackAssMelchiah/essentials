#pragma once
#include <string>
#include <glm/glm.hpp>

namespace libCore
{
	class Scene;
	class Entity;
	class Gleam;
}

namespace libCore
{
	class Utility
	{
	public:
		Utility() = default;
		~Utility() = default;

		//! Adds instance to entity for rendering - Transform and Render instances
		static void addRenderInstance(Gleam& gleam, Scene& scene, Entity& entity, std::string const& path, glm::mat4 const& localMatrix);

		//! Removes instance fron entity for rendering - Transform and Render
		static void removeRenderInstance(Gleam& gleam, Scene& scene, Entity& entity);

		//! Returns how many instances object have with both drawable and transform components
		static uint32_t getRenderInstanceCount(Gleam& gleam, Scene& scene, Entity& entity);
	};
}