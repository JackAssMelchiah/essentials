#pragma once
//! SDL
#include <SDL.h>
//! libCore
#include <libCore/EntityManager.hpp>
//! Std
#include <memory>
#include <set>

namespace libVulkan
{
    class VulkanManager;
    class VulkanDefaults;
}

namespace libUi
{
    class UiClassManager;
}

namespace libInputs
{
    class InputManager;
}

namespace libCore
{
    class TransformGraphManager;
    class Entity;
    class Window;
}

namespace libCore 
{
    class Gleam
    {
    public:
        //! Constructor and destructor
        Gleam() = default;
        ~Gleam();

        //! Initializes all Gleam subsystems
        bool initialize(bool windowless, std::shared_ptr<libVulkan::VulkanDefaults> defaults);

        //! Adds window to gleam TODO: refactor
        bool addWindow(std::shared_ptr<libCore::Window> w);
        std::shared_ptr<Window> const& getWindow() const;

        //! Getter for Vulkan manager's subsystem
        libVulkan::VulkanManager& getVulkanManager();
        //! Getter for Input manager
        libInputs::InputManager& getInputManager();
        std::weak_ptr<libInputs::InputManager> getInputManagerWeak();
        //! Getter for Transform manager
        std::weak_ptr<TransformGraphManager> getTransformGraphManager();
        //! Getter for Ui manager
        libUi::UiClassManager& getUiManager();
        
    protected:
        bool inititalizeExternalLibs();
        bool initializeGraphicsApi(bool windowless, std::shared_ptr<libVulkan::VulkanDefaults> defaults);
        bool initializeSoundApi();
        bool initializeInputApi();
        bool initializeUiApi();
        bool initializeCoreApi();

        //! sdl init
        bool initializeSDL();

        //! Handles events asociated with window
        bool handleWindow(SDL_WindowEvent& event);
            
    protected:
        //! windows
        std::set<std::shared_ptr<Window>> m_windows;

        //! manager for rendering classes uis
        std::shared_ptr<libUi::UiClassManager> m_uiClassManager;
        //! manager for vulkan rendering
        std::shared_ptr<libVulkan::VulkanManager> m_vulkanManager;
        //! manager for inputs
        std::shared_ptr<libInputs::InputManager> m_inputManager;
        //! manager for transformations
        std::shared_ptr<libCore::TransformGraphManager> m_TransformGraphManager;
    };
}