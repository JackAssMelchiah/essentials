#pragma once
//! libCore
#include <libCore/Buffer.hpp>

namespace libCore
{
    class UniformBuffer: public Buffer
    {
    public:
        UniformBuffer(std::shared_ptr<libVulkan::VulkanInstance> const& instance, std::shared_ptr<libVulkan::VulkanDevice> const& device, uint32_t bytes, std::string const& name);
        ~UniformBuffer();

        //! Buffers data into buffer - clasic blocking thing, no synchronization in place
        template <typename T>
        void bufferData(std::vector<T> const& data)
        {
            assert(data.size() * sizeof(T) <= m_size);
            // Do persistent buffer mapping - map once and leave it with explicit flushes
            if (!m_masterBuffer->mapped())
                m_masterBuffer->map();
            // buffer arbitrary data
            m_masterBuffer->bufferData(data);
            // flush the buffer so gpu can read it
            m_masterBuffer->flush();
        }

        //! Returns internal representation of uniform buffer
        vk::DescriptorType getType() const;

    protected:
        //! Creates buffer object
        void initializeBuffer(uint32_t bytes);

    protected:
        //! What type of descriptor should be used to represent this buffer
        vk::DescriptorType m_type;
    };    
}