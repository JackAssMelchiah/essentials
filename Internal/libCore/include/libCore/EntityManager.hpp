#pragma once
//! libCore
#include <libCore/Entity.hpp>
//! entt
#include <entity/registry.hpp>
#include <entity/view.hpp>
//! std
#include <memory>
#include <vector>
#include <map>

namespace libCore
{
	class EntityManager
	{
	public:
		EntityManager();
		virtual ~EntityManager();

        //! Creates entity which is managed by Gleam
        std::weak_ptr<Entity> createEntity(std::string const& entityName);
        virtual void removeEntity(std::weak_ptr<Entity> entity);
        //! Add an component
        template <typename T>
        void addComponentToEntity(Entity const& entity, std::shared_ptr<T> component)
        {
            m_registry.emplace<std::shared_ptr<T>>(entity(), component);
        }
        //! In case entity already has given component and wants it to be replaced 
        template <typename T>
        void updateComponentToEntity(Entity const& entity, std::shared_ptr<T> component)
        {
            m_registry.patch<std::shared_ptr<T>>(entity(), component);
        }
        //! Remove component from existing entity 
        template <typename T>
        void removeComponentFromEntity(Entity const& entity, std::shared_ptr<T> component)
        {
            m_registry.remove<std::shared_ptr<T>>(entity());
        }
        //! Getter for component from entity
        template <typename T>
        std::weak_ptr<T> getComponent(Entity const& entity)
        {
            return m_registry.get<std::shared_ptr<T>>(entity());
        }
        //! Const getter for component from entity
        template <typename T>
        std::weak_ptr<T> const getComponent(Entity const& entity) const
        {
            return m_registry.get<std::shared_ptr<T>>(entity());
        }
        //! Querries whether given entity has T component
        template <typename T>
        bool hasComponent(Entity const& entity) const
        {
            //all_of matches all template args, any_of gives true for atleas one
            return m_registry.all_of<std::shared_ptr<T>>(entity());
        }
        //! Returns all components stored within manager of single type T
        template <typename T>
        void getAllComponents(std::vector<std::shared_ptr<T>>& componentsOut)
        {
            componentsOut.clear();
            //entt::basic_view<entt::type_list<entt::constness_as_t<entt::storage_type_t<std::shared_ptr<T>, entt::entity, std::allocator<std::shared_ptr<T>>>, std::shared_ptr<T>>>, entt::type_list<>, void> view = m_registry.view<std::shared_ptr<T>>();
            auto view = m_registry.view<std::shared_ptr<T>>();
            for (auto [entity, item]: view.each())
            {
                componentsOut.push_back(item);
            }
        }
        //! Returs all entities within manager which have components of type T
        template <typename... T>
        void getEntitiesWithComponents(std::vector<std::shared_ptr<Entity>>& entitiesOut) const
        {
            entitiesOut.clear();
            auto view = m_registry.view<std::shared_ptr<T>...>();
            auto allItems = view.each();
            for (auto item: allItems)
            {
                auto entityID = std::get<0>(item);
                if (m_entities.count(entityID) != 0)
                    entitiesOut.push_back(m_entities.at(entityID));
            }
        }

	protected:
        //! Entity manager for entt
        entt::registry m_registry;
        //! Enities of libCore
        std::map<entt::entity, std::shared_ptr<Entity>> m_entities;
	};
}