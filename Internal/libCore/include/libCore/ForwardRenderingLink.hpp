#pragma once
//! libCore
#include <libCore/RenderingLink.hpp>

//! std
#include <memory>
#include <vector>
#include <set>

namespace libCore
{
	class ShaderResources;
	class Camera;
	class Scene;
}

namespace libCore
{
	//! Given the RenderingInfo, SinglePassRendering will create, allocate, synchronize all nescesary input and output attachments,
	//! with its own single shader resources, which will be shared among all the entities and materials which want to use SinglePassRendering
	class ForwardRenderingLink: public RenderingLink
	{
	public:
		ForwardRenderingLink(std::shared_ptr<Renderer> const& renderer, std::shared_ptr<libVulkan::VulkanRenderingInfo> const& renderingInfo, std::shared_ptr<libCore::Camera> const& camera);
		~ForwardRenderingLink() = default;
		//! Buffers the Object pass resources
		void bufferResources() override;
		//! Rebinds the uniforms
		void resolutionChanged(vk::Extent2D const&) override;
		//! Getter for descriptor set
		libVulkan::VulkanDescriptorSet& getDescriptorSet() override;
		//! Returns the layout of the descriptor set
		std::shared_ptr<libVulkan::VulkanDescriptorSetLayout> getDescriptorSetLayout() const override;

		//! Allows to use different camera than the one it was constructed with
		void setCamera(std::shared_ptr<libCore::Camera> const& camera);
		//! Getter for camera used within the link
		std::shared_ptr<libCore::Camera> const& getCamera() const;

	protected:
		//! Binds Object pass resources
		void bindUniformData();
		//! Sets variables to resources, specifies buffer, buffers them and binds them
		void setUpResources();
		//! Handles the creation of desriptor set
		void createDescriptorSet();

	protected:
		//! Resources of object pass
		std::shared_ptr<ShaderResources> m_resources;
		//! Pointer to DescriptorSet
		std::weak_ptr<libVulkan::VulkanDescriptorSet> m_descriptorSet;
		//! Camera which is used as source for resources binding
		std::shared_ptr<libCore::Camera> m_camera;
	};
}