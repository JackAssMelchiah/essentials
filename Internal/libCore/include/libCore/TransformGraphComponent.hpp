#pragma once
//! std
#include <string>
#include <vector>
//! libCore
#include <glm/glm.hpp>

namespace libCore
{
	class TransformGraphComponent
	{
		friend class TransformGraphManager;
	public:
		TransformGraphComponent(std::string const& nodePath, glm::mat4 const& localMatrix);
		~TransformGraphComponent() = default;
		//! Returns count of instances the graph component has
		uint32_t getInstanceCount() const;
		//! Appends another data for instance
		void addInstance(std::string const& localNodePath, glm::mat4 const& localMatrix);
		//! Removes last instance
		void removeInstance();
		//! Local node path
		std::string getLocalNodePath(uint32_t instance) const;
		//! Managed matrix getter
		glm::mat4 getLocalMatrix(uint32_t instance);
		//! Getter for the world matrix computed by manager
		glm::mat4 getWorldMatrix(uint32_t instance);
		//! Allows to modify the local matrix
		void setLocalMatrix(glm::mat4 const& matrix, uint32_t instance);

	protected:
		using componentData = struct CD
		{
			//! Local node path
			std::vector<std::string> localPaths;
			//! Local transform which is being set by user
			std::vector<glm::mat4> localMatrices;
			//! World transform which is set by manager
			std::vector<glm::mat4> worldMatrices;

			CD(std::vector<std::string> const& localPaths, std::vector<glm::mat4> const& localMatrices, std::vector<glm::mat4> const& worldMatrices)
				: localPaths(localPaths)
				, localMatrices(localMatrices)
				, worldMatrices(worldMatrices)
			{
			}

			uint32_t getInstanceCount() const
			{
				return localPaths.size();
			}

			bool validInstance(uint32_t instance) const
			{
				return instance < localPaths.size();
			}

			void addInstance(std::string const& localNodePath, glm::mat4 const& localMatrix)
			{
				localPaths.push_back(localNodePath);
				localMatrices.push_back(localMatrix);
				worldMatrices.push_back({ glm::mat4(1.f) });
			}

			void removeInstance()
			{
				if(getInstanceCount() > 1)
				{
					localPaths.erase(localPaths.end() - 1);
					localMatrices.erase(localMatrices.end() - 1);
					worldMatrices.erase(worldMatrices.end() - 1);
				}
			}
		};

	protected:
		//! Via this manager updates the world matrix
		void setWorldMatrix(glm::mat4 const& matrix, uint32_t instance);

	protected:
		componentData m_componentData;
		std::vector<std::string> m_toErase;
	};
}