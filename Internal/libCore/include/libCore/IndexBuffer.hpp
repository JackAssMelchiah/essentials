#pragma once
//! libCore
#include <libCore/Buffer.hpp>
#include <libCore/Renderer.hpp>
//! libVulkan
#include <libVulkan/VulkanCommandPool.hpp>
#include <libVulkan/VulkanCommandBuffer.hpp>

namespace libCore
{
    class IndexBuffer: public Buffer
    {
    public:
        //! Generates single vertex buffer with maximum size specified in bytesd
        IndexBuffer(std::shared_ptr<Renderer> const& renderer, vk::IndexType type, uint32_t bytes, std::string const& name);
        ~IndexBuffer() = default;
        //! For single time data buffering, buffers content of whole vector into vbo on offset
        //! Transfering to device local memory via stage buffer -  not suitable for streaming data in
        void bufferData(std::vector<unsigned char> const& data, uint32_t offset);
        //! What kind of indices this supports
        vk::IndexType getType() const;

    protected:
        void initialize(uint32_t bytes);

    protected:
        std::weak_ptr<Renderer> m_renderer;
        //! Either short or int
        vk::IndexType m_type;
    };    
}