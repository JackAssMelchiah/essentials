#pragma once
//! libCore
#include <libCore/VertexBuffer.hpp>
#include <libCore/IndexBuffer.hpp>
#include <libCore/ShaderResources.hpp>
#include <libCore/MaterialLayout.hpp>
#include <libCore/AABB.hpp>

namespace libCore
{
	class RenderComponent
	{
	public:
		//! Creates a drawable with predefined command and descriptor pools - this already expects that they are big enought to support requirements based on materials and bufferAllocation struct - use if want to share single command and descriptor pools for multiple drawables and rest things
		RenderComponent(std::shared_ptr<Renderer> const& renderer, std::shared_ptr<MaterialLayout> materialLayout, uint32_t instances, uint32_t vertexShaderInvocationCount);
		~RenderComponent() = default;

		//! Sets vertex buffer, object will share ownership
		void setVertexBuffer(std::shared_ptr<VertexBuffer> buffer);
		//! Sets index buffer, object will share ownership
		void setIndexBuffer(std::shared_ptr<IndexBuffer> buffer);

		//! Adds an instance, returns handle to it
		ShaderResources::Instance addInstance();
		//! Removes selected instance
		void removeInstance();
		//! How many instances there are
		uint32_t getInstanceCount() const;

		//! Can use index buffer or not
		bool usesIndexBuffer() const;

		//! Records command buffer so drawable is rendered - TODO maybe this is pure virtual and subclass could be drawable
		virtual void record(libVulkan::VulkanCommandBuffer& commandBuffer);

		//! Gets which material layout object uses
		MaterialLayout::MaterialLayoutType getMaterialType() const;
		//! TODO: MAYBE DELETE THIS?
		std::shared_ptr<MaterialLayout> getMaterialLayout();

		//! Returns reference to stored object resources
		ShaderResources& getResources();
		//! Temp, since later it will be a part of entity
		void setName(std::string const& name);
		std::string const& getName() const;
		
		//! Automatically recomputes AABB from vertex buffer, note that it must exist and have data in it - expensive op TODO
		//void refreshAABB();
		//! Sets the AABB to be of min and max value
		void setAABB(std::shared_ptr<AABB> const& aabb);
		//! Getter for AABB
		std::shared_ptr<AABB> getAABB(uint32_t instance);

		//! When aabb is rendered, this color should be used
		void setAABBColor(glm::vec3 const& color, uint32_t instance);
		//! When aabb is rendered, this color should be used
		glm::vec3 getAABBColor(uint32_t instance) const;
			
	protected:
		//! Renderer - used for buffers
		std::weak_ptr<Renderer> m_renderer;

		//! Marterial which will be used to render this
		MaterialLayout::MaterialLayoutType m_materialType;
		std::shared_ptr<MaterialLayout> m_materialLayout;
		//! Material data source which will be used by Material layout
		std::shared_ptr<ShaderResources> m_objectResources;

		//! Buffers used by drawable
		std::shared_ptr<IndexBuffer> m_ibo;
		std::shared_ptr<VertexBuffer> m_vbo;

		//! For draw() method for vertexCount
		uint32_t m_vertexInvocationCount;

		//! Mandatory AABB
		std::shared_ptr<AABB> m_aabb;

		std::string m_name;
	};
}