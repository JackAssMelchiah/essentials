#pragma once

#include <glm/glm.hpp>

namespace libCore
{
	class Ray
	{
	public:
		Ray(glm::vec3 const& origin, glm::vec3 const& direction);
		Ray(Ray const& r);
		~Ray() = default;

		glm::vec3 const& origin() const;
		glm::vec3 const& direction() const;

	protected:
		glm::vec3 m_origin;
		glm::vec3 m_direction;
	};
}