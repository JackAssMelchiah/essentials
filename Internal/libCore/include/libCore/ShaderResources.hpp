#pragma once
//! std
#include <map>
#include <variant>
#include <string>
#include <cassert>
#include <vector>
#include <memory>
//! glm
#include <glm/glm.hpp>

#include <libUtils/Utils.hpp>

// test
class ShaderResourceTestSuite_InstanceWriting_Test;

namespace libVulkan
{
	class VulkanInstance;
	class VulkanDevice;
	class VulkanDescriptorSet;
}

namespace libCore
{
	class UniformBuffer;
	class TextureBuffer;
}

namespace libCore
{
	class ShaderResources
	{
		friend class ShaderResourceTestSuite_InstanceWriting_Test;
	public:
		//! Defines instance index used to access resources
		struct Instance
		{
			int32_t index;
			uint32_t count;

			//! instance handler is moved to next instance, if should overflow, returns false
			bool nextInstance()
			{
				if (index < count)
				{
					index++;
					return true;
				}
				return false;
			}

			bool toInstance(uint32_t instance)
			{
				if (index < count)
				{
					index = instance;
					return true;
				}
				return false;
			}

			bool operator == (Instance const& r) const { return index == r.index && count == r.count; }
			bool operator < (Instance const& r) const { return index < r.index; }
			int32_t operator()() const { return index; }
		};

	public:
		ShaderResources(std::shared_ptr<libVulkan::VulkanInstance> const& instance, std::shared_ptr<libVulkan::VulkanDevice> const& device);
		~ShaderResources() = default;

		//! Via this instance handle the data for first instance can be accessed and modified
		Instance getBaseInstance() const;

		//! Setter for concrete instance of resource, replaces used one if name is same. If instance does not exist, it will be created for all the resources
		template <typename T>
		void setInstanceResource(std::string const& resource, T const& value, Instance const& instance)
		{
			//1. If resource does not exist, create it
			if (!resourceExist(resource))
				createResource(resource, value);
			//2. If instance does not exist, create it and as source data take the first instance
			auto newInstance = instance;
			if (!instanceExist(instance))
				newInstance = createInstance(getBaseInstance());
			//3. Set the data to resource
			{
				//copy data to resource
				auto instanceCount = getInstanceCount();
				auto& internalResource = getInternalResource(resource, newInstance);
				assert(getSerializedSize(value) == internalResource.data.size());
				serialize(internalResource.data.data(), value);
			//4. When updaing instance resource, if its in pack, update all packs, so in buffer() it does not have to do that all
				if (resourceUsedInPack(resource))
				{
					auto& packsWithResource = getPacksForResource(resource);
					for (auto& pack : packsWithResource)
					{
						auto& resourcePack = getResourcePack(pack.bufferID);
						auto strideWithinResourcePack = resourcePack.data.size() / instanceCount;
						serialize(resourcePack.data.data() + (getIndexForInstance(newInstance) * strideWithinResourcePack + pack.firstInstanceOffset), value);
					}
				}
			}
		}
		//! Getter for concrete instance of resource
		template <typename T>
		T getInstanceResource(std::string const& resource, Instance const& instance) const
		{
			return *(reinterpret_cast<T*>(getInternalResourceCopy(resource, instance).data.data()));
		}

		//! Check if resource exists
		bool resourceExist(std::string const& resource) const;
		//! Check if instance exists
		bool instanceExist(Instance const& instance) const;
		//! Check if buffer exists
		bool bufferExists(std::string const& bufferID) const;

		//! How much instances of data are there 
		uint32_t getInstanceCount() const;

		//! All resourcess will be buffered, in order they are specified in resources param. They need to exist! Gpu buffer needs to exist too
		void bufferResources(std::vector<std::string> const& resources, std::string const& buffer);

		//! Writes descriptor set with handles of the buffers in order buffers are provided
		void useBuffersWithSet(std::vector<std::string> const& buffers, libVulkan::VulkanDescriptorSet& descriptorSet);

		//! Stores texture buffer and associates it with text buffer representation - buffer param should not already exist within. This type of resource does not support instancing.
		void setTextureBuffer(std::shared_ptr<TextureBuffer> textureBuffer, std::string const& buffer);

		//! Creates new instance, copies data from instance described by index
		Instance createInstance(Instance const& srcInstanceIndex);
		//! Removes single instance of all resources
		void removeInstance(int32_t instance);

	protected:
		struct BufferID
		{
			std::string index;
			bool operator == (BufferID const& r) const { return index == r.index; }
			bool operator < (BufferID const& r) const { return index < r.index; }
			std::string operator()() const { return index; }
		};

		struct BufferPtr
		{
			BufferID bufferID;
			uint32_t firstInstanceOffset;
		};

		struct InternalResource
		{
			std::vector<char> data;
		};

		struct InternalResourceMetadata
		{
			std::vector<BufferPtr> bufferTargets;
		};

		struct InstanceMetadata
		{
			//payload
			int32_t resourceInstanceIndex = -1;
			Instance previous;
			Instance next;

			InstanceMetadata()
				: resourceInstanceIndex(-1)
				, previous({ -1 })
				, next({ -1 })
			{}

			InstanceMetadata(int32_t payload, Instance const& prev, Instance const& next)
				: resourceInstanceIndex(payload)
				, previous(prev)
				, next(next)
			{}

			bool valid() const
			{
				return resourceInstanceIndex > -1;
			}
		};

		struct ResourcePack
		{
			std::vector<std::string> resources;
			std::vector<char> data;
		};

	protected:
		//Helper getter
		InternalResource& getInternalResource(std::string const& resource, Instance const& instance);
		InternalResource getInternalResourceCopy(std::string const& resourceName, Instance const& instance) const;
		bool resourceUsedInPack(std::string const& resource) const;
		std::vector<BufferPtr>const& getPacksForResource(std::string const& resourceName) const;

		ResourcePack& getResourcePack(BufferID const& id);

		uint32_t getIndexForInstance(Instance const& instance) const;

		bool resourcePackExists(BufferID const& buffer) const;
		bool resourcePackExists(std::vector<std::string> const& resources, BufferID const& buffer) const;

		void createBuffer(BufferID const& bufferID, uint32_t byteSize);
		void createResourcePack(std::vector<std::string> const& resources, BufferID const& bufferID);
		//! Helper for byte size
		uint32_t getResourceByteSize(std::string const& resource) const;
		UniformBuffer& getUniformBuffer(BufferID const& id);
		TextureBuffer& getTextureBuffer(BufferID const& id);

		//! Buffer must exist - if it is not, its safe to assume its uniform
		bool isTextureBuffer(BufferID const& id) const;

		Instance getFirstInstance() const;
		Instance getNextInstance(Instance instanceIndex) const;
		Instance getLastInstance() const;

		void createFirstInstance();
		//! For internal use, we want to allways have one invalid instance for easy loop
		void addEmptyInstance();
				
		//! Loops thru resources first instance and finds invalid resource, or -1
		int32_t getFirstInvalidResourceIndex() const;
		//! Will allway return something, since there is allways invalid empty instance created
		Instance getFirstInvalidInstance() const;

		//! Deletes all resource packs, and all buffers as well
		void cleanResourcePacks();

		template <typename T>
		//! Creates new resource for all instances, they will have deepcopy of same copies data of resource
		void createResource(std::string const& resource, T const& value)
		{
			assert(!resourceExist(resource));
			//1. Copy data
			auto instanceCount = getInstanceCount();
			auto resourceSize = getSerializedSize(value);
			auto newResource = InternalResource{ std::vector<char>(resourceSize) };
			serialize(newResource.data.data(), value);
			//2. Create resource - vector for each instances, and give copy of data to each resource instance
			m_resources[resource] = std::vector<InternalResource>(instanceCount, newResource);
		}

		//! Removes single resource from all instances
		void removeResource(std::string const& resourceName);

		//! Revalidates instance with payload
		void revalidateInstance(Instance const& invalidInstance, int32_t payload);

		//! Getter for size of T when its scalar or simple structure with staticly sized container
		template <typename T>
		uint64_t getSerializedSize(T const& value)
		{
			return sizeof(T);
		}

		//! Getter for size of T when its vector container -> gets size of the value * num of vals + uint64_t for storing the size within vector
		template <typename T>
		uint64_t getSerializedSize(std::vector<T> const& value)
		{
			return sizeof(uint64_t) + value.size() * sizeof(T);
		}

		template <typename T>
		//! Copies the value to dst, takes into account if the T is vector, then first value will be 8 byte sizeofVector
		void serialize(char* dst, T const& value)
		{		
			memcpy(dst, &value, getSerializedSize(value));
		}

		template <typename T>
		//! Copies the value to dst, takes into account if the T is vector, then first value will be 8 byte sizeofVector
		void serialize(char* dst, std::vector<T> const& value)
		{
			auto totalSize = getSerializedSize(value);
			auto sizeVal = uint64_t(value.size());
			// Copy vector size
			memcpy(dst, &sizeVal, sizeof(uint64_t));
			// Copy vector data
			memcpy(dst + sizeof(uint64_t), value.data(), sizeVal * sizeof(T));
		}

	protected:
		std::weak_ptr<libVulkan::VulkanInstance> m_instance;
		std::weak_ptr<libVulkan::VulkanDevice> m_device;

		//! Resources owned by Material resources
		std::map<std::string, std::vector<InternalResource>> m_resources;
		std::map<std::string, InternalResourceMetadata> m_resourceMetadata;
		//! Instance points to current index of all resources - e.q data, that belong to that instance
		std::map<Instance, InstanceMetadata> m_instancesPointer;
		//! Stored interleaved data, which can be directly memcpyied to buffer - note, that all of them are another copy of InternalResources
		std::map<BufferID, ResourcePack> m_resourcePacks;
		//! Stored buffers - used as accessors to gpu for this shader resources, binds and frequency is determined by user of ShaderResources
		std::map<BufferID, std::shared_ptr<UniformBuffer>> m_uniformBuffers;
		//! Stored Texture buffers - theese are not per instance
		std::map<BufferID, std::shared_ptr<TextureBuffer>> m_textureBuffers;

		//instance id generator
		int32_t m_instanceIDGenerator;
	};
}