#pragma once
//! Std
#include <memory>
//! glm
#include <glm/glm.hpp>
//! libVulkan
#include <libVulkan/VulkanWindow.hpp>
//! SDL - seems like fw decl isnt enough when linking from other library
#include <SDL.h>

namespace libVulkan
{
	class VulkanInstance;
}

namespace libCore
{
	class Window
	{
	public:
		Window(std::string const& name);
		~Window();

		glm::ivec2 getWindowResolution();
		void setWindowResolution(glm::ivec2 const& res);

		//! Returns all nescesary vulkan instance extensions which must be enabled for rendering to window
		std::vector<std::string> getVulkanInstanceExtensions() const;
		//! Creates and gets surface
		vk::SurfaceKHR createAndGetVulkanSurface(libVulkan::VulkanInstance& instance) const;
		//! Stores the vulkan window - used when resize to be propagated to vulkan automatically
		void setVulkanBackend(std::weak_ptr<libVulkan::VulkanWindow>& window);
		//! Getter for vulkan backend
		std::weak_ptr<libVulkan::VulkanWindow> getVulkanBackend();
		//! Moves cursor within window - dimmensions are given in range 0-1 meaning %
		void moveMouseCursor(glm::vec2 const& percentDimms);
		//! Locks the cursor, so it wont be able to move outside of window - mouse events are still generated
		void lockMouseCursor(bool on);
		//! Shows or hides the cursor
		void setMouseCursorVisibility(bool visible);
		//! Returns Raw object
		SDL_Window& operator()();

		std::string getName() const;

	protected:
		//! Raw sdl window
		SDL_Window* m_sdlWindow;
		//! Vulkan window impl
		std::weak_ptr<libVulkan::VulkanWindow> m_vulkanWindow;

		std::string m_name;
	};
}
