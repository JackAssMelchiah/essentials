#pragma once
//! libCore
#include <libCore/Ray.hpp>
//! std
#include <string>
//! glm
#include <glm/glm.hpp>

namespace libCore
{
	class Camera;
}

namespace libCore
{
	class AABB
	{
	public:
		AABB();
		AABB(glm::vec3 const& min, glm::vec3 const& max);
		AABB(AABB const& rhs);
		~AABB() = default;

		//! Potentionally expands aabb, and returns true if it did in any axis 
		void expand(AABB const& aabb);

		//! Getters
		glm::vec3 const& getMin() const;
		glm::vec3 const& getMax() const;

		//! Getter for center
		glm::vec3 center() const;

		//! Checks if BB intersects with ray, ray should be in same space as is AABB
		bool instersects(Ray const& ray, glm::vec3& firstHitOut, glm::vec3& secondHitOut) const;
		
		//! Debug output
		std::string toString() const;
			
	protected:
		glm::vec3 m_min;
		glm::vec3 m_max;
	};
}