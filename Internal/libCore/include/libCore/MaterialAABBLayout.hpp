#pragma once
//! libCore
#include <libCore/MaterialLayout.hpp>
//! glm
#include <glm/glm.hpp>

namespace libCore
{
	class MaterialAABBLayout: public MaterialLayout
	{
	public:
		MaterialAABBLayout(std::shared_ptr<libVulkan::VulkanInstance> const& instance, std::shared_ptr<libVulkan::VulkanDevice> const& device);
		~MaterialAABBLayout() = default;

		//! Getter for rendering info - what kind of FBO is needed to be able to render with this material layout
		virtual std::shared_ptr<libVulkan::VulkanRenderingInfo> const& getRenderingInfo() const;

		//! Builds pipeline
		std::shared_ptr<libVulkan::VulkanPipeline> createPipeline(std::shared_ptr<libVulkan::VulkanPipelineLayout> pipelineLayout, vk::Extent2D const& resolution) const override;

		//! Binds object data from resources with the descriptor set - should be called every time the object changes
		void bindObjectUniformData(libVulkan::VulkanDescriptorSet& modelDataDescriptorSet, ShaderResources& resources) const override;
		//! Binds object data from resources with the descriptor set - should be called every time the material changes
		void bindMaterialUniformData(libVulkan::VulkanDescriptorSet& materialDataDescriptorSet, ShaderResources& resources) const override;

		//! Rebuffers object the data stored in resources into gpu
		void bufferObjectResources(ShaderResources& resources) const override;
		//! Rebuffers material the data stored in resources into gpu
		void bufferMaterialResources(ShaderResources& resources) const override;

		//! Descriptor set layout creation for material set
		std::shared_ptr<libVulkan::VulkanDescriptorSetLayout> createMaterialDescriptorSetLayout() const override;
		//! Descriptor set layout creation for model set
		std::shared_ptr<libVulkan::VulkanDescriptorSetLayout> createObjectDescriptorSetLayout() const override;
		//! Input description for material
		std::shared_ptr<libVulkan::VulkanVertexAttributesDescription> createObjectVertexAttributesDescription() const override;

		//! Allocates object data within resources, so they can be accessed
		void allocateObjectData(ShaderResources& resources) const override;
		//! Allocates material data within resources, so they can be accessed
		void allocateMaterialData(ShaderResources& resources) const override;

		void setObjectColor(glm::vec3 const& color, ShaderResources& resources, ShaderResources::Instance const& instance) const;
		glm::vec3 getObjectColor(ShaderResources const& resources, ShaderResources::Instance const& instance) const;

		void setObjectAABBMin(glm::vec3 const& min, ShaderResources& resources, ShaderResources::Instance const& instance) const;
		glm::vec3 getObjectAABBMin(ShaderResources const& resources, ShaderResources::Instance const& instance) const;

		void setObjectAABBMax(glm::vec3 const& max, ShaderResources& resources, ShaderResources::Instance const& instance) const;
		glm::vec3 getObjectAABBMax(ShaderResources const& resources, ShaderResources::Instance const& instance) const;

	protected:
		std::shared_ptr<libVulkan::VulkanRenderingInfo> m_renderingInfo;
	};
}