#pragma once
//! libVulkan
#include <libVulkan/VulkanQueue.hpp>
//! libUtils
#include <libUtils/Time.hpp>
//! Std
#include <memory>
#include <functional>
#include <vector>

namespace libVulkan
{
	class VulkanDevice;
	class VulkanInstance;
	class VulkanCommandPool;
	class VulkanFence;
	class VulkanTimelineSemaphore;
	class VulkanDescriptorPool;
	class VulkanCommandBuffer;
	class VulkanDescriptorSet;
	class VulkanDescriptorSetLayout;
}

namespace libCore
{
	class RenderingPass;
	class ShaderResources;
}

namespace libCore
{
	//! Note about descriptors - renderer uses 4(intel's max count) descriptor sets - theese bind by frequency (from less fo most frequent) - eq,
	//! 1 - engine resources - 1x per frame
	//! 2 - render pass resources - 1x per renderPass
	//! 3 - material resources - 1x per material
	//! 4 - object resources - 1x per object draw
	//! Bindpoint then correspond to local resources in that set - e.q. set 3's bindpoint are managed strictly by material ...etc. 
	//! Ideally each should be allocated from its own pool, and object and material resources pool's should be reseted per frame and re-recorded
	
	//! Note about command buffers - there are 3 ways to interact with command buffer
	//! 1 - there is record and submit method, which will spawn nwe command buffer and set it to recording mode. Once recorded, will inmidiatelly executed and waited on. Usefull for some inmidiate initial data upload, etc.. 
	//! 2 - For normal rendering, there is 1 persistent command buffer which is recorded once, and is executed per frame. Should be used for static geometry
	//! 3 - for dynamic rendering, there is any number of command buffers, which's pool is reseted every frame. Should be used for dynamic geometry
	class Renderer
	{
		friend class RenderingPass;
	public:
		//! Whole renderer uses notation of render frequency
		enum class ResourceFrequency { Renderer, RenderPass, Material, Object};

	public:
		Renderer(std::shared_ptr<libVulkan::VulkanInstance> const& instance, std::shared_ptr<libVulkan::VulkanDevice> const& device, std::shared_ptr<libVulkan::VulkanQueue> graphicsQue);
		~Renderer();

		std::weak_ptr<libVulkan::VulkanDevice> getDevice() const;
		std::weak_ptr<libVulkan::VulkanInstance> getInstance() const;

		//! Submits the recorded command buffer to que
		void submit(libVulkan::VulkanCommandBuffer& commandBuffer);
		void submit(std::vector<std::reference_wrapper<libVulkan::VulkanCommandBuffer>> const& commandBuffers);
		//! Does single submit of CommandBuffer - will be executed and released inmidiatelly
		void recordAndSubmit(std::function<void(libVulkan::VulkanCommandBuffer& commandBuffer)> fn);

		//! Gets new dynamic comamnd buffer - will be released by end of frame
		std::weak_ptr<libVulkan::VulkanCommandBuffer> getDynamicCommandBuffer(vk::CommandBufferLevel level);
		//! Gets command buffer which is persistent - not released and the end of frame
		std::weak_ptr<libVulkan::VulkanCommandBuffer> getPersistentCommandBuffer();
		//! Explicitly releases and recreates persistent command buffer
		void releasePersistentCommandBuffer();

		//! Notifies that it is the end of frame, releases per-frame resources
		void finalizeFrame();

		//! Getter for default renderer set - can or does not need to be used
		std::weak_ptr<libVulkan::VulkanDescriptorSet> getRendererDescriptorSet();
		//! Getter for default renderer set's layout
		std::shared_ptr<libVulkan::VulkanDescriptorSetLayout> createRendererDescriptorSetLayout() const;

		//! Rebuffers the data stored in renderer's resources into gpu
		void bufferResources();
		//! Creates and returns descriptor set, from descriptor pool, each from given frequency
		std::weak_ptr<libVulkan::VulkanDescriptorSet> createDescriptorSet(std::shared_ptr<libVulkan::VulkanDescriptorSetLayout> setLayout, ResourceFrequency frequency, std::string const& name);
		//! Frees the descriptorSet
		void destroyDescriptorSet(std::weak_ptr<libVulkan::VulkanDescriptorSet> descriptorSet, ResourceFrequency frequency);
		//! BindPoint for global engine resources, bound once per frame
		uint32_t getDescriptorSetNumber(ResourceFrequency resourceFrequency) const;
		//! Rebinds the uniforms
		void resolutionChanged(vk::Extent2D const&);

		double getTime() const;
		double getFrameTime() const;

	protected:
		//! Sets up command pools, based on usage
		void initializeCommandPools();
		//! Sets up descriptor pools based on different frequencies
		void initializeDescriptorPools();

		//! Sets variables to resources, specifies buffer, buffers them and binds them
		void setUpResources();
		//! Binds renderer's uniform data structure into shader
		void bindUniformData();

		//! Initializes all the stuff used with renderpass
		void createRendererDescriptorSet();
		
		libVulkan::VulkanCommandPool& getDynamicCommandPool();
		libVulkan::VulkanCommandPool& getInmidiateCommandPool();
		libVulkan::VulkanCommandPool& getPersistentCommandPool();
		
		libVulkan::VulkanDescriptorPool& getRendererDescriptorPool();
		libVulkan::VulkanDescriptorPool& getPassDescriptorPool();
		libVulkan::VulkanDescriptorPool& getMaterialDescriptorPool();
		libVulkan::VulkanDescriptorPool& getObjectDescriptorPool();

		void createEngineDescriptorPool();
		void createPassDescriptorPool();
		void createMaterialDescriptorPool();
		void createModelDescriptorPool();

	protected:
		//! Device
		std::weak_ptr<libVulkan::VulkanDevice> m_device;
		std::weak_ptr<libVulkan::VulkanInstance> m_instance;
		
		//! Command pools
		std::array<std::shared_ptr<libVulkan::VulkanCommandPool>, 3> m_commandPools;
		//! Persistent command buffer
		std::weak_ptr<libVulkan::VulkanCommandBuffer> m_persistentCommandBuffer;

		//! Descriptor pools
		std::array<std::shared_ptr<libVulkan::VulkanDescriptorPool>, 4> m_descriptorPools;

		//! Decriptor set for renderer's data
		std::weak_ptr<libVulkan::VulkanDescriptorSet> m_rendererDescriptorSet;

		//! Sync primitives
		std::shared_ptr<libVulkan::VulkanTimelineSemaphore> m_sceneSyncSemaphore;


		//! Que non-owned for ease of access
		std::weak_ptr<libVulkan::VulkanQueue> m_graphicsQueue;

		//! Owned renderer resources
		std::shared_ptr<ShaderResources> m_resources;
		//! FrameTime timer
		libUtils::Time m_ftTimer;
		//! Total Time timer
		libUtils::Time m_totalTimer;
	};
}