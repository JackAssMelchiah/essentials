#pragma once
//! libCore
#include <libCore/MaterialLayout.hpp>
//! glm
#include <glm/glm.hpp>

namespace libCore
{
	class TextureBuffer;
	class VertexBuffer;
}

namespace libCore
{
	class MaterialPBRLayout: public MaterialLayout
	{
	public:
		MaterialPBRLayout(std::shared_ptr<libVulkan::VulkanInstance> const& instance, std::shared_ptr<libVulkan::VulkanDevice> const& device);
		~MaterialPBRLayout() = default;

		//! Getter for rendering info - what kind of FBO is needed to be able to render with this material layout
		std::shared_ptr<libVulkan::VulkanRenderingInfo> const& getRenderingInfo() const override;

		//! Builds pipeline
		std::shared_ptr<libVulkan::VulkanPipeline> createPipeline(std::shared_ptr<libVulkan::VulkanPipelineLayout> pipelineLayout, vk::Extent2D const& resolution) const override;

		//! Binds object data from resources with the descriptor set - should be called every time the object changes
		void bindObjectUniformData(libVulkan::VulkanDescriptorSet& modelDataDescriptorSet, ShaderResources& resources) const override;
		//! Binds object data from resources with the descriptor set - should be called every time the material changes
		void bindMaterialUniformData(libVulkan::VulkanDescriptorSet& materialDataDescriptorSet, ShaderResources& resources) const override;

		//! Rebuffers object the data stored in resources into gpu
		void bufferObjectResources(ShaderResources& resources) const override;
		//! Rebuffers material the data stored in resources into gpu
		void bufferMaterialResources(ShaderResources& resources) const override;

		//! Descriptor set layout creation for material set
		std::shared_ptr<libVulkan::VulkanDescriptorSetLayout> createMaterialDescriptorSetLayout() const override;
		//! Descriptor set layout creation for model set
		std::shared_ptr<libVulkan::VulkanDescriptorSetLayout> createObjectDescriptorSetLayout() const override;
		//! Input description for material
		std::shared_ptr<libVulkan::VulkanVertexAttributesDescription> createObjectVertexAttributesDescription() const override;

		//! Allocates object data within resources, so they can be accessed
		void allocateObjectData(ShaderResources& resources) const override;
		//! Allocates material data within resources, so they can be accessed
		void allocateMaterialData(ShaderResources& resources) const override;

		//! Instance helper functions
		//! Rebuffers the vertex data from vertex resources - data will be written to buffer in way material is expecting it - will be interleaved
		void updateVertexDataFromResources(VertexBuffer& buffer, std::vector<glm::vec3> const& vertices, std::vector<glm::vec3> const& colors) const;

		//! Sets the model matrix to object shader resources used by this layout for given instance
		void setObjectModelMatrix(glm::mat4 const& matrix, ShaderResources& resources, ShaderResources::Instance const& instance) const;
		glm::mat4 getObjectModelMatrix(ShaderResources& resources, ShaderResources::Instance const& instance) const;
		//! Sets the model matrix to object shader resources used by this layout for given instance
		void setObjectAlbedoColor(glm::vec3 const& albedo, ShaderResources& resources, ShaderResources::Instance const& instance) const;
		glm::vec3 getObjectAlbedoColor(ShaderResources& resources, ShaderResources::Instance const& instance) const;
		//! Sets the ambientOcclusion to object shader resources used by this layout for given instance, will be clamped in <0, 1> range
		void setObjectAmbientOcclusion(float ambientOcclusion, ShaderResources& resources, ShaderResources::Instance const& instance) const;
		float getObjectAmbientOcclusion(ShaderResources& resources, ShaderResources::Instance const& instance) const;
		//! Sets the metallicness to object shader resources used by this layout for given instance, will be clamped in <0, 1> range
		void setObjectMetallicness(float metallicness, ShaderResources& resources, ShaderResources::Instance const& instance) const;
		float getObjectMetallicness(ShaderResources& resources, ShaderResources::Instance const& instance) const;
		//! Sets the roughness to object shader resources used by this layout for given instance, will be clamped in <0, 1> range
		void setObjectRoughness(float roughness, ShaderResources& resources, ShaderResources::Instance const& instance) const;
		float getObjectRoughness(ShaderResources& resources, ShaderResources::Instance const& instance) const;

		//! Sets light position for material, material frequency
		void setMaterialLightPosition(glm::vec3 const& lightPosition, ShaderResources& resources) const;
		//! Sets camera postion for material, material frequency
		void setMaterialCameraPosition(glm::vec3 const& cameraPosition, ShaderResources& resources) const;
		//! Sets light color for material, material frequency
		void setMaterialLightColor(glm::vec3 const& lightColor, ShaderResources& resources) const;

	protected:
		std::shared_ptr<libVulkan::VulkanRenderingInfo> m_renderingInfo;
	};
}