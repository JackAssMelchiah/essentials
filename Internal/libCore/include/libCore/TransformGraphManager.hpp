#pragma once
//! glm
#include <glm/glm.hpp>
//! std
#include <map>
#include <vector>
#include <string>
#include <functional>

//! fw decl
namespace libCore
{
	class TransformGraphComponent;
	class Scene;
}

namespace libCore
{
	class TransformGraphManager
	{
	public:
		TransformGraphManager();
		~TransformGraphManager() = default;
		//! Updates all TransformGraphComponents within scene
		void processScene(libCore::Scene& scene);
		//! Returns string with debug info about the wanted system, and for all its children if withChildren is true
		std::string toString(std::string const& path, bool withChildren);
		//! Sets local transformation for path within tree
		void set(std::string const& path, glm::mat4 const& localMatrix);

	protected:
		//! Settings used for node search 
		enum class ValidSettings { VALID, INVALID, BOTH };
		//! Internal representation
		using Node = struct NODE
		{
			NODE()
				: fullPath("")
				, localMatrix(glm::mat4(1.f))
				, id(0)
				, valid(false)
				, worldMatrix(glm::mat4(1.f))
			{
			}

			NODE(std::string const& path, glm::mat4 const& localMatrix, uint64_t id, bool valid = true)
				: fullPath(path)
				, localMatrix(localMatrix)
				, id(id)
				, valid(valid)
				, worldMatrix(glm::mat4(1.f))
			{}

			bool operator < (NODE const& left)
			{
				return id < left.id;
			}

			//! Returns path of the parent
			std::string parentPath() const;
			//! Returns whether Node is root or no
			bool isRoot() const;
			//! Return local path of the Node
			std::string localPath() const;

			bool valid;
			std::string fullPath;
			glm::mat4 localMatrix;
			glm::mat4 worldMatrix;
			uint64_t id;
		};

	protected:
		//! Deletes transformation under path - all path under it are there until all were removed
		void clear(std::string const& path);

		//! Returns world matrix of given path if valid, otherwise identity will be returned
		glm::mat4 wordMatrix(std::string const& path) const;
		//! Returns local matrix of given path if valid, otherwise identity will be returned
		glm::mat4 matrix(std::string const& path) const;
		//! Computes and returns transformation matrix from sourcePath to destinationPath
		glm::mat4 matrix(std::string const& sourcePath, std::string const& destinationPath) const;

		//! Gives indication whether path exists
		bool pathExists(std::string const& path) const;


		//! Creates, adds and return node
		Node& addNode(std::string const& path, glm::mat4 const& localMatrix, bool valid = true);
		//! Populates tree with all subsystem nodes which are missing - those will be invalid
		Node& createSubSystems(std::string const& path);
		//! Returns vector filled with node's direct children 
		void children(std::string const& path, std::vector<NODE*>& children);
		//! Returns parent node
		Node* getParent(Node const& node);
		//! Updates all children's word matrix
		void update(NODE& node);
		//! Tries to clean-up all self and all children (delete nodes if they have no valid children)
		void clean(NODE& node);
		//! Getter for node, based on valid will return even invalid one
		Node* getNode(std::string const& path, ValidSettings valid);
		//! Getter for node, based on valid will return even invalid one
		Node const* getNode(std::string const& path, ValidSettings valid) const;
		//! Depth first search, which will invocate method on each node
		void DFS(Node& node, std::function<void(Node&)> evalFunc);
		//! Whether the path is the root
		bool isRoot(std::string const&) const;

	protected:
		std::map<std::string, Node> m_tree;
		uint64_t m_genId;
	};
}