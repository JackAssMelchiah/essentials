#pragma once
//! libVulkan
#include <libVulkan/VulkanObject.hpp>
//! std
#include <vector>
#include <string>
#include <span>
//! SDL_image
#include <SDL_image.h>

namespace libCore
{
	class ImageLoader
	{
	public:
		//! Initializers ImageLoader
		static bool initialize();
		//! loads image and converts it on cpu to R8G8B8A8Unorm format, does multiple copies, slow TODO: Do it directly on gpu
		static bool loadImage(std::string const& path, glm::ivec2& resolutionOut, int& pitchOut, std::vector<std::byte>& dataOut);
		//! templated load directly from memory
		template <typename T>
		static bool loadImage(std::span<T> const& bytes, glm::ivec2& resolutionOut, int& pitchOut, std::vector<std::byte>& dataOut)
		{
			dataOut.clear();
			// load image from memory
			auto stream = SDL_RWFromConstMem(bytes.data(), bytes.size());
			if (!stream)
				return false;
			
			// convert it to surface
			auto surface = IMG_Load_RW(stream, 1); //frees the stream

			if (!loadImage(surface, resolutionOut, pitchOut, dataOut))
				return false;
			return true;
		}
		//! Destroys ImageLoader
		static void quit();

	protected:
		static bool loadImage(SDL_Surface* surface, glm::ivec2& resolutionOut, int& pitchOut, std::vector<std::byte>& dataOut);
	};
}