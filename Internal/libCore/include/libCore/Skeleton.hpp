#pragma once
#include <glm/glm.hpp>
#include <glm/gtx/quaternion.hpp>
#include <vector>
#include <string>
#include <memory>

namespace libCore
{
	class Renderer;
	class Scene;
	class BasicRenderComponent;
	class TransformGraphManager;
}

namespace libCore
{
	class Skeleton
	{
	public:
		using Joint = struct J
		{
			glm::mat4 inverseBP;
			std::string name;
			uint8_t parent;
		};

	public:
		Skeleton(std::shared_ptr<Renderer> const& renderer, std::shared_ptr<Scene> const& scene, std::shared_ptr<TransformGraphManager> const& trManager, std::string const& name, std::string const& boneRepresentationOverload = "");
		~Skeleton() = default;
		void addJoint(Joint const& joint);

		void setVisible(bool on);
		bool getVisible();

	protected:
		//using JointPose = struct JP
		//{
		//	glm::quat rotation;
		//	glm::vec3 translation;
		//	float scale;
		//};

		//using SkeletalPose = struct SP
		//{
		//	Skeleton skeleton;
		//	std::vector<JointPose> poses;
		//};

	protected:
		std::vector<Joint> m_joints;
		std::weak_ptr<Scene> m_scene;
		std::shared_ptr<BasicRenderComponent> m_basicRenderComponent;
	};
}