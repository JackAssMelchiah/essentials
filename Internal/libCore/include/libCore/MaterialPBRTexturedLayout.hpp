#pragma once
//! libCore
#include <libCore/MaterialLayout.hpp>
//! glm
#include <glm/glm.hpp>

namespace libCore
{
	class TextureBuffer;
}

namespace libCore
{
	class MaterialPBRTexturedLayout: public MaterialLayout
	{
	public:
		MaterialPBRTexturedLayout(std::shared_ptr<libVulkan::VulkanInstance> const& instance, std::shared_ptr<libVulkan::VulkanDevice> const& device);
		~MaterialPBRTexturedLayout() = default;

		//! Getter for rendering info - what kind of FBO is needed to be able to render with this material layout
		std::shared_ptr<libVulkan::VulkanRenderingInfo> const& getRenderingInfo() const override;

		//! Builds pipeline
		std::shared_ptr<libVulkan::VulkanPipeline> createPipeline(std::shared_ptr<libVulkan::VulkanPipelineLayout> pipelineLayout, vk::Extent2D const& resolution) const override;

		//! Binds object data from resources with the descriptor set - should be called every time the object changes
		void bindObjectUniformData(libVulkan::VulkanDescriptorSet& modelDataDescriptorSet, ShaderResources& resources) const override;
		//! Binds object data from resources with the descriptor set - should be called every time the material changes
		void bindMaterialUniformData(libVulkan::VulkanDescriptorSet& materialDataDescriptorSet, ShaderResources& resources) const override;

		//! Rebuffers object the data stored in resources into gpu
		void bufferObjectResources(ShaderResources& resources) const override;
		//! Rebuffers material the data stored in resources into gpu
		void bufferMaterialResources(ShaderResources& resources) const override;

		//! Descriptor set layout creation for material set
		std::shared_ptr<libVulkan::VulkanDescriptorSetLayout> createMaterialDescriptorSetLayout() const override;
		//! Descriptor set layout creation for model set
		std::shared_ptr<libVulkan::VulkanDescriptorSetLayout> createObjectDescriptorSetLayout() const override;
		//! Input description for material
		std::shared_ptr<libVulkan::VulkanVertexAttributesDescription> createObjectVertexAttributesDescription() const override;

		//! Allocates object data within resources, so they can be accessed
		void allocateObjectData(ShaderResources& resources) const override;
		//! Allocates material data within resources, so they can be accessed
		void allocateMaterialData(ShaderResources& resources) const override;

		//! Sets the model matrix to object shader resources used by this layout for given instance
		void setObjectModelMatrix(glm::mat4 const& matrix, ShaderResources& resources, ShaderResources::Instance const& instance) const;
		//! Sets albedo texture
		void setObjectAlbedoTexture(std::shared_ptr<TextureBuffer> albedo, ShaderResources& resources) const;
		//! Sets normal texture
		void setObjectNormalTexture(std::shared_ptr<TextureBuffer> normal, ShaderResources& resources) const;
		//! Sets serface three channel texture defined as - (R): occlusion, (G): Metalicness, (B): Roughness
		void setObjectSurfaceTexture(std::shared_ptr<TextureBuffer> surface, ShaderResources& resources) const;

		//! Sets light position for material, material frequency
		void setMaterialLightPosition(glm::vec3 const& lightPosition, ShaderResources& resources) const;
		//! Sets camera postion for material, material frequency
		void setMaterialCameraPosition(glm::vec3 const& cameraPosition, ShaderResources& resources) const;
		//! Sets light color for material, material frequency
		void setMaterialLightColor(glm::vec3 const& lightColor, ShaderResources& resources) const;

	protected:
		std::shared_ptr<libVulkan::VulkanRenderingInfo> m_renderingInfo;
	};
}