#pragma once

#include <vulkan/vulkan.hpp>
//! std
#include <functional>
#include <map>
#include <memory>

namespace libCore
{
    class Scene;
    class RenderingLink;
}

namespace libCore
{
    class RenderChain
    {
    public:

        using ChainHandle = struct CP
        {
            CP()
                : index(0)
            {
            }

            CP(uint32_t id)
                : index(id)
            {
            }

            bool operator < (CP const& rhs) const
            {
                return index < rhs.index;
            }

            bool operator == (CP const& rhs) const
            {
                return index == rhs.index;
            }

            bool operator != (CP const& rhs) const
            {
                return index != rhs.index;
            }

        protected:
            uint32_t index;
        };

    public:

        RenderChain();
        ~RenderChain() = default;

        //! Adds new link, with vector of scenes which will use said link, will be added as last processed link
        ChainHandle addLink(std::shared_ptr<RenderingLink> const& link, std::vector<std::shared_ptr<Scene>> const& scene = {});
        //! Removes new link, with vector of scenes which will use said link
        void removeLink(ChainHandle const& link);
        //! Adds new scenes, will be added as last processed within the link
        void addScenes(ChainHandle const& handle, std::vector<std::shared_ptr<Scene>> const& scene);
        //! Removes scenes from link
        void removeScenes(ChainHandle const& handle, std::vector<std::shared_ptr<Scene>> const& scene);
        //! Callback which will get invoked once when resources are not available to initialize them, and when resize has happened
        void addOnSetup(ChainHandle const& key, std::function<void()> callbackFunc);
        //! Callback which will get invoked every time pass is about to begin
        void addOnEnter(ChainHandle const& key, std::function<void()> callbackFunc);
        //! Callback which will get invoked once when pass is done 
        void addOnLeave(ChainHandle const& key, std::function<void()> callbackFunc);

        //! Sets the scene as first processed within the link
        void setFirstScene(ChainHandle const& handle, std::shared_ptr<Scene> const& firstScene);
        //! Set the order of processing of scenes within single link
        void setOrderBetweenScenes(ChainHandle const& handle, std::shared_ptr<Scene> const& prevScene, std::shared_ptr<Scene> const& nextScene);

        //! Sets the first Link which is to be rendered
        void setFirstLink(ChainHandle const& handle);
        //! Set order of the links
        void setOrderBetweenLinks(ChainHandle const& prevLink, ChainHandle const& nextLink);

        //! Creates all nescesary resources
        void buildChain(vk::Extent2D const& resolution);

        void execute();

        
    protected:
        using Callbacks = struct CBs
        {
            std::function<void()> onSetup; //gets invoked first time the links is processed
            std::function<void()> onEnter; //gets invoked every time before the link is processed
            std::function<void()> onLeave; //gets invoked every time after the link is processed
        };

        using LinkData = struct LD
        { //Single instance of link will process all asociated scenes
        public:
            LD() //invalid 
                : link(nullptr)
                , scenes({})
                , callbacks()
                , index()
            {
            }

            LD(ChainHandle const& handle, std::shared_ptr<RenderingLink> const& link)
                : link(link)
                , scenes({})
                , callbacks()
                , index(handle)
            {
            }

            void addScenes(std::vector<std::shared_ptr<Scene>> const& addScenes)
            {
                for (auto& scene : addScenes)
                    scenes.push_back(scene);
            }

            void removeScenes(std::vector<std::shared_ptr<Scene>> const& removeScenes)
            {
                auto newScenes = std::vector<std::shared_ptr<Scene>>();
                for (const auto& element : scenes) {
                    //preserves order
                    if (std::find(removeScenes.begin(), removeScenes.end(), element) == removeScenes.end())
                    {
                        newScenes.push_back(element);
                    }
                }
                scenes = newScenes;
            }

            Callbacks& getCallbacks()
            {
                return callbacks;
            }

            void setFirstScene(std::shared_ptr<Scene> const& scene)
            {
                auto it = std::find(scenes.begin(), scenes.end(), scene);
                if (it != scenes.end()) 
                {
                    scenes.erase(it);
                    scenes.insert(scenes.begin(), scene);
                }
            }

            std::shared_ptr<RenderingLink> getLink() const
            {
                return link;
            }

            bool operator < (LD const& rhs)
            {
                return index < rhs.index;
            }

            bool operator == (LD const& rhs)
            {
                return index == rhs.index;
            }

            bool operator != (LD const& rhs)
            {
                return index != rhs.index;
            }

        protected:
            std::shared_ptr<RenderingLink> link; // link which will work with the scenes
            std::vector<std::shared_ptr<Scene>> scenes; // all processed scenes, in order!
            Callbacks callbacks; // callbacks which will get invoked
            ChainHandle index; //just info for maps and vectors for > and == operators
        };

    protected:
        //! all stored links, allows multiple same links to be invoked 
        std::map<ChainHandle, LinkData> m_passes;
        //! stored invocation order of links, the scene process order is solved on the LinkData scenes vector basis
        std::vector<ChainHandle> m_invokationOrder;

        uint32_t m_idGenerator;
    };
}