#pragma once
//! glm
#include <glm/glm.hpp>
//! std
#include <memory>
#include <vector>

namespace libInputs
{
	class InputManager;
}

namespace libCore
{
	class RenderComponent;
	class Window;
	class Camera;
	class Scene;
}

namespace libCore
{
	class Picker
	{
	public:
		Picker(std::shared_ptr<libInputs::InputManager> const& inputManager, std::shared_ptr<libCore::Window> const& window, std::shared_ptr<libCore::Camera> const& camera, std::shared_ptr<libCore::Scene> const& scene);
		~Picker() = default;
		//! Sets the camera from which the picking occurs
		void setCamera(std::shared_ptr<libCore::Camera> const& camera);
		//! Sets the scene from which to pick from
		void setScene(std::shared_ptr<libCore::Scene> const& scene);

		//! Sets default color of the AABB when unpicked or unhovered
		void setColorDefault(glm::vec3 const& color);
		//! Sets hover color of the AABB
		void setColorHover(glm::vec3 const& color);
		//! Sets select color of the AABB
		void setColorSelect(glm::vec3 const& color);

		glm::vec3 const& getColorDefault() const;
		glm::vec3 const& getColorHover() const;
		glm::vec3 const& getColorSelect() const;

		bool isSelected(std::shared_ptr<RenderComponent> const& renderComponent, uint32_t instance) const;
		bool isSelected() const;
		bool isHovered(std::shared_ptr<RenderComponent> const& renderComponent, uint32_t instance) const;
		bool isHovered() const;

		std::pair<std::shared_ptr<RenderComponent>, uint32_t> getHovered() const;
		std::pair<std::shared_ptr<RenderComponent>, uint32_t> getSelected() const;

	protected:
		struct Intersection
		{
			std::shared_ptr<libCore::RenderComponent> component;
			uint32_t instance;
			glm::vec3 point;
		};

	protected:
		void setMouseOver(std::shared_ptr<RenderComponent> const& renderComponent, uint32_t instance);
		void clearMouseOver();
		void setSelect(std::shared_ptr<RenderComponent> const& renderComponent, uint32_t instance);
		void clearSelect();
		void printPickState();

	protected:
		//! Stored input manager
		std::weak_ptr<libInputs::InputManager> m_inputManager;
		//! Stored window - no views just window
		std::weak_ptr<libCore::Window> m_window;
		//! Stored camera
		std::weak_ptr<libCore::Camera> m_camera;
		//! stored scene
		std::weak_ptr<libCore::Scene> m_scene;
		
		//! Stored intersection, refreshed each frame
		std::vector<Intersection> m_intersections;
		//! Describes hovered object
		std::shared_ptr<libCore::RenderComponent> m_hoveredComponent;
		uint32_t m_hoveredComponentInstance;
		//! Describes selected object
		std::shared_ptr<libCore::RenderComponent> m_selectedComponent;
		uint32_t m_selectedComponentInstance;
		//! Color to which to set the BB to
		glm::vec3 m_defaultColor;
		glm::vec3 m_hoverColor;
		glm::vec3 m_selectColor;

		//! logic for selecting only
		std::shared_ptr<libCore::RenderComponent> m_pressedOnComponent;
		uint32_t m_pressedOnInstance;

		//debug
		//std::shared_ptr<libCore::BasicRenderComponent> m_rayDrawable;
	};
}