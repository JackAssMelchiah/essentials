#pragma once
//! std
#include <map>
//! libCore
#include <libCore/EntityManager.hpp>
#include <libCore/TransformGraphManager.hpp>
#include <libCore/MaterialLayout.hpp>
#include <libCore/RenderComponent.hpp>

namespace libCore
{
	class Renderer;
	class MaterialBatch;
	class ShaderResources;
	class TransformGraphManager;
	class Camera;
	class AABB;
	class RenderingLink;
}

namespace libVulkan
{
	class VulkanDescriptorSet;
	class VulkanCommandBuffer;	
}

namespace libCore
{
	class Scene: public EntityManager
	{
	public:
		Scene(std::shared_ptr<Renderer> const& renderer, std::string const& name);
		~Scene() = default;
		
		//! Add an component
		template <typename T>
		void addComponentToEntity(Entity const& entity, std::shared_ptr<T> component)
		{
			EntityManager::addComponentToEntity(entity, component);

			if constexpr (std::is_same_v<T, RenderComponent>)
			{
				if (m_aabbRenderComponent != component)
				{
					auto instances = component->getInstanceCount();
					for (auto i = 0; i < instances; ++i)
					{
						m_aabbRenderComponent->addInstance();
					}
				}
			}
		}
		//! In case entity already has given component and wants it to be replaced 
		template <typename T>
		void updateComponentToEntity(Entity const& entity, std::shared_ptr<T> component)
		{
			EntityManager::updateComponentToEntity(entity, component);
			if constexpr (std::is_same_v<T, RenderComponent>)
			{
				if (m_aabbRenderComponent != component)
				{
					//take the old component count of instances
					auto old = getComponent<RenderComponent>(entity).lock();
					auto oldCount = old->getInstanceCount();
					auto newCount = component->getInstanceCount();
					if (oldCount > newCount)
					{
						auto toRemove = oldCount - newCount;
						for (auto i = 0; i < toRemove; ++i)
						{
							m_aabbRenderComponent->removeInstance();
						}
					}
					if (oldCount < newCount)
					{
						auto toAdd = newCount - oldCount;
						for (auto i = 0; i < toAdd; ++i)
						{
							m_aabbRenderComponent->addInstance();
						}
					}
				}
			}
		}

		//! Customized remove component
		void removeComponentFromEntity(Entity const& entity, std::shared_ptr<RenderComponent> component);

		//! Sets the scene name
		void setName(std::string const& name);
		std::string getName() const;

		//! Removes entity custom way
		void removeEntity(std::weak_ptr<Entity> entity) override;

		//! Getter for stored cameras
		void getCameras(std::vector<std::shared_ptr<libCore::Camera>>& camerasOut);

		//! Returns how many objects have drawable capability
		uint32_t getRenderComponentObjectCount() const;

		//! Records commandBuffer with all the objects
		void recordAll(libVulkan::VulkanCommandBuffer& commandBuffer, RenderingLink& pass);
		//! Reconds in paralel command buffers with objects, one cb per one object - should pass one per each object, if mismatch, will record what it can
		void recordSingle(libVulkan::VulkanCommandBuffer& commandBuffer, uint32_t which, RenderingLink& pass);

		//! Updates all pipelines with proper resolution
		void resolutionChanged(vk::Extent2D const& resolution);

		//! Buffers all object data stored in shaderResources
		void bufferMaterialAndObjectsData();
		//! Buffers just material data
		void bufferMaterialData();
		// Needs to be called when buffers within objects change, typically when adding or removing instance, will rebind all objects
		void bindObjectResources(MaterialLayout::MaterialLayoutType layoutType);


		//! Returns material resources
		ShaderResources& getMaterialResources();

		//! Shortcut to get all entities which have capability to be rendered, if withTransform is set, the entities will have graph component as well
		void getRenderEntities(std::vector<std::shared_ptr<libCore::Entity>>& entities, bool mustHaveGraph) const;
		
		//! Allows access to AABB
		std::shared_ptr<libCore::AABB>& getAABB();

		//! Enables rendering of AABBs within scene TODO expand it for selective aabbs 
		void setRenderAABB(bool enable);
		bool getRenderAABB() const;

		//! Sets transform to target, if it uses Transform component, will set the local matrix within transform manager
		void setTransform(std::shared_ptr<Entity> const& entity, glm::mat4 const& matrix, uint32_t instance);
		//! Returns transform of the enitity, if entity uses transform component, will get the local matrix within transform manager
		glm::mat4 getTransform(std::shared_ptr<Entity> const& entity, uint32_t instance) const;

		//! Adds an instance to all of entity's components which support instance adding
		int32_t addInstance(std::shared_ptr<Entity> const& entity);
		//! Removes an instance from all of entity's components which support instance removal
		void removeInstance(std::shared_ptr<Entity> const& entity);

		//! Updates position and dimmensions of AABB drawn instanced from all instances of RenderComponents within this scene
		void updateAABBRenderComponent();

	protected:
		//! Creates AABB render representation for this scene
		void createAABBRenderComponent();

		//! Adds the drawable represenation to drawbatch, and creates all needed resources; called when recording so the drawable component can be added anytime before recording
		void assignToBatch(std::shared_ptr<RenderComponent> const& drawable, RenderingLink& pass);
		//! Checks if drawable is prepared
		bool inBatch(std::shared_ptr<RenderComponent> const& drawable);

		//! Assigns memory of material descriptors with resources via material layout. Such binding in Vulkan needs to be done just once, and when pipeline changes
		void bindMaterialsResources();
		//! Creates descriptor set for given material
		std::weak_ptr<libVulkan::VulkanDescriptorSet> createMaterialDescriptorSet(libCore::MaterialLayout const& materialLayout) const;
		//! Creates material layout from enum :(
		std::shared_ptr<MaterialLayout> createMaterialLayout(MaterialLayout::MaterialLayoutType materialLayoutType) const;
		//! Getter for first arbitrary pipeline, useful for binding renderer and pass sets
		std::weak_ptr<libVulkan::VulkanPipeline> getArbitraryPipeline();

	protected:
		//! Renderer
		std::weak_ptr<libCore::Renderer> m_renderer;
		//! Debug info name which is also used in transform manager for scene
		std::string m_name;
		//! Spawned single entity of material per type 
		std::map<MaterialLayout::MaterialLayoutType, std::shared_ptr<MaterialBatch>> m_batches;
		//! Single shader resources for materialLayout, shared among all of them - they are rewritten every time material is bound 
		std::shared_ptr<ShaderResources> m_materialResources;
		//! Represents aabb of whole scene
		std::shared_ptr<AABB> m_aabb;
		bool m_renderAABB;

		//! Encapsulates all AABBs in scene, drawn as instances, note, its added to entity only when it needs to be rendered
		std::shared_ptr<RenderComponent> m_aabbRenderComponent;
		//! Entity handle to renderComponent - aabb renderComponent is only added when its being rendered
		std::weak_ptr<Entity> m_aabbEntity;
	};
}