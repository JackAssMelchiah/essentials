#pragma once
//! libCore
#include <libCore/ShaderResources.hpp>
#include <libVulkan/VulkanObject.hpp>
//! std
#include <memory>
#include <string>
#include <vector>

namespace libVulkan
{
	class VulkanDevice;
	class VulkanInstance;
	class VulkanPipeline;
	class VulkanRenderingInfo;
	class VulkanPipelineLayout;
	class VulkanDescriptorSet;
	class VulkanDescriptorSetLayout;
	class VulkanVertexAttributesDescription;
}

namespace libCore
{
	class ShaderResources;
}

namespace libCore
{
	class MaterialLayout
	{
	public:
		enum MaterialLayoutType {UNSHADED, UNSHADED_TEXTURED, PHONG, PHONG_NORMAL_MAP, PBR, PBR_TEXTURED, AABB, SKELETAL_ANIMATION, COUNT};

	public:
		MaterialLayout(std::shared_ptr<libVulkan::VulkanInstance> const& instance, std::shared_ptr<libVulkan::VulkanDevice> const& device, MaterialLayoutType type);
		virtual ~MaterialLayout() = 0;
		//! RTTY info
		MaterialLayoutType getType() const;

		//! Getter for rendering info - what kind of FBO is needed to be able to render with this material layout
		virtual std::shared_ptr<libVulkan::VulkanRenderingInfo> const& getRenderingInfo() const = 0;

		//! Binds object data from resources with the descriptor set - should be called every time the object changes
		virtual void bindObjectUniformData(libVulkan::VulkanDescriptorSet& modelDataDescriptorSet, ShaderResources& resources) const = 0;
		//! Binds object data from resources with the descriptor set - should be called every time the material changes
		virtual void bindMaterialUniformData(libVulkan::VulkanDescriptorSet& materialDataDescriptorSet, ShaderResources& resources) const = 0;

		//! Rebuffers object the data stored in resources into gpu
		virtual void bufferObjectResources(ShaderResources& resources) const  = 0;
		//! Rebuffers material the data stored in resources into gpu
		virtual void bufferMaterialResources(ShaderResources& resources) const = 0;

		//! Creates pipeline for material
		virtual std::shared_ptr<libVulkan::VulkanPipeline> createPipeline(std::shared_ptr<libVulkan::VulkanPipelineLayout> pipelineLayout, vk::Extent2D const& resolution) const = 0;

		//! Descriptor set layout creation for material set
		virtual std::shared_ptr<libVulkan::VulkanDescriptorSetLayout> createMaterialDescriptorSetLayout() const = 0;
		//! Descriptor set layout creation for model set
		virtual std::shared_ptr<libVulkan::VulkanDescriptorSetLayout> createObjectDescriptorSetLayout() const = 0;
		//! Input description for material
		virtual std::shared_ptr<libVulkan::VulkanVertexAttributesDescription> createObjectVertexAttributesDescription() const = 0;


		//! Allocates object data within resources, so they can be accessed
		virtual void allocateObjectData(ShaderResources& resources) const = 0;
		//! Allocates material data within resources, so they can be accessed
		virtual void allocateMaterialData(ShaderResources& resources) const = 0;
				
		bool operator== (MaterialLayout const& r) const;
		bool operator< (MaterialLayout const& r) const;

	protected:
		std::weak_ptr<libVulkan::VulkanInstance> m_instance;
		std::weak_ptr<libVulkan::VulkanDevice> m_device;
		//! To know which layout this is... it is needed
		MaterialLayoutType m_type;
	};
}