#pragma once
//! libVulkan
#include <libVulkan/VulkanObject.hpp>
//! std
#include <map>
#include <vector>

namespace libVulkan
{
	class VulkanPipeline;
	class VulkanCommandBuffer;
	class VulkanDescriptorSet;
	class VulkanDescriptorSetLayout;
}

namespace libCore
{
	class RenderComponent;
	class MaterialLayout;
	class Renderer;
	class RenderingPass;
}

namespace libCore
{
	class MaterialBatch
	{
	public:
		//! Creates material render batch, material, which is to be used, is specified here
		MaterialBatch(std::shared_ptr<Renderer> const& renderer, std::shared_ptr<libVulkan::VulkanDescriptorSetLayout> const& passDescriptorSetLayout, std::shared_ptr<MaterialLayout> const& materialLayout);
		~MaterialBatch() = default;
		bool hasObject(std::shared_ptr<RenderComponent> const& drawable) const;
		//! Adds a new object
		void addObject(std::shared_ptr<RenderComponent> drawable);
		//! Removes object from batch, returns true if its empty
		bool removeObject(std::shared_ptr<RenderComponent> drawable);
		//! Records command buffer with all models within material batch
		void recordBuffer(libVulkan::VulkanCommandBuffer& cb);
		//! Records single buffer with all models within material batch
		void recordSingleBuffer(libVulkan::VulkanCommandBuffer& cb, uint32_t which);
		//! Returns how many pipelines material batch has
		uint32_t pipelineCount() const;
		//! Returns how many objects material batch has
		uint32_t objectCount() const;
		//! Will return some of the pipelines, which are compatible with material
		std::weak_ptr<libVulkan::VulkanPipeline> getMaterialCompatiblePipeline() const;
		//! Getter for material's descriptor set
		std::weak_ptr<libVulkan::VulkanDescriptorSet> getMaterialDescriptorSet();
		//! Getter for material layout
		MaterialLayout& getMaterialLayout();
		//! Updates pipelines's resolution and recreates them
		void resolutionChanged(vk::Extent2D const& resolution);
		//! Buffers all object data stored in shaderResources
		void bufferObjectsData();
		//! Assigns memory of object descriptors with resources via material layout. Such binding in Vulkan needs to be done just once, and when pipeline changes or when the underlying buffer changes
		void bindObjectResources();

	protected:
		struct RenderComponentKey 
		{
			int32_t index;
			bool operator == (RenderComponentKey const& r) const { return index == r.index; }
			bool operator < (RenderComponentKey const& r) const { return index < r.index; }
			int32_t operator()() const { return index; }
		};

		struct PipelineKey
		{
			int32_t index;
			bool operator == (PipelineKey const& r) const { return index == r.index; }
			bool operator < (PipelineKey const& r) const { return index < r.index; }
			int32_t operator()() const { return index; }
		};

		struct Object
		{
			RenderComponentKey key;
			std::shared_ptr<RenderComponent> drawable;
			std::weak_ptr<libVulkan::VulkanDescriptorSet> objectDescriptorSet;

			Object();
			Object(RenderComponentKey key, std::shared_ptr<RenderComponent> drawable, std::weak_ptr<libVulkan::VulkanDescriptorSet> objectDescriptorSet);
			bool contains(std::shared_ptr<RenderComponent> const& r) const;
			bool valid() const;
			void destroy(std::shared_ptr<Renderer> const& renderer);
		};

	protected:
		//! Creates pipeline for combination of material and drawable, and creates descriptor for drawable
		PipelineKey createAndAddPipeline(RenderComponent const& drawable, MaterialLayout const& materialLayout);
		//! If pipeline which is compatible with givendrawable exists, will return index to it. Otherwise -1 - backwards compatibility
		PipelineKey findCompatiblePipelineWithRenderComponent(MaterialLayout const& drawable) const;
		//! Creates new descripttor set which will be compatible with layout given by drawable
		std::weak_ptr<libVulkan::VulkanDescriptorSet> createDescriptorSetForRenderComponent(RenderComponent const& drawable, Renderer& renderer);
		//! Inserts object into batch
		void insertObject(std::shared_ptr<RenderComponent> drawable, PipelineKey const& pipelineKey, std::weak_ptr<libVulkan::VulkanDescriptorSet> modelDataDescriptorSet);
		//! Helper method, also recreates set, should it be invalid
		std::weak_ptr<libVulkan::VulkanDescriptorSet> getObjectDescriptorSet(Object& object);

	protected:
		//! renderer
		std::weak_ptr<Renderer> m_renderer;
		//! pass set layout
		std::shared_ptr<libVulkan::VulkanDescriptorSetLayout> m_passDescriptorSetLayout;

		//! The material type, describes how and which data from objects are used, as well as which data are used for material
		std::shared_ptr<MaterialLayout> m_materialLayout;
		//! Material's descriptor set, which is used exclusively with material data
		std::weak_ptr<libVulkan::VulkanDescriptorSet> m_materialDescriptorSet;

		//! stored drawables, each set is rendered with specific pipeline (given the requirements in model's setLayout) 
		std::map<PipelineKey, std::vector<Object>> m_pipelinesWithObjects;
		//! all created pipelines
		std::vector<std::shared_ptr<libVulkan::VulkanPipeline>> m_pipelines;
	};
}