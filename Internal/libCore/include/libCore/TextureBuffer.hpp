#pragma once
//! std
#include <span>
//! libVulkan
#include <libVulkan/VulkanCommandPool.hpp>
#include <libVulkan/VulkanCommandBuffer.hpp>
#include <libVulkan/VulkanQueue.hpp>
#include <libVulkan/VulkanImage.hpp>
#include <libVulkan/VulkanDescriptorPool.hpp>
#include <libVulkan/VulkanImageSampler.hpp>
#include <libVulkan/VulkanBarrier.hpp>
//! libCore
#include <libCore/Renderer.hpp>

namespace libCore
{
    //! Consists of vertex buffer with memory on device_local, and staging buffer
    //! - should be used for resources which are copied to GPU once and then re-read multiple times
    class TextureBuffer
    {
    public:
        //! Desctibes allocation for single image
        struct ImageInfo
        {
            //! Image resolution
            glm::ivec2 resolution;
            //! How many bytes it has
            uint32_t byteSize;
            //! What kind of format it uses
            vk::Format format;

            ImageInfo(glm::ivec2 const& resolution, vk::Format format, uint32_t imageByteSize)
                : resolution(resolution)
                , format(format)
                , byteSize(imageByteSize)
            {}

            ImageInfo() = default;
        };

    public:
        //! Creates texture buffer which holds one to N textures, which will be all send to GPU
        TextureBuffer(std::shared_ptr<libCore::Renderer> const& renderer, std::vector<ImageInfo> const& images, bool sampledImages, std::string const& name);
        ~TextureBuffer() = default;
        //! Getter for texture infos
        std::vector<ImageInfo> const& getTextureInfos() const;
        //! Useful for descriptor write
        void getDescriptorImageInfos(std::vector<vk::DescriptorImageInfo>& infos);

        //TODO: for bufferdata use template trait instead of 2 templates...

        //! For single time data buffering, buffers content of whole image data into GPU memory and ties buffered data together with image at imageIndex
        //! Transfering to device local memory via stage buffer - not suitable for streaming data in
        template <typename T>
        void bufferData(std::vector<T>const& data, uint32_t imageIndex)
        {
            auto renderer = m_renderer.lock();
            auto instance = renderer->getInstance().lock();
            auto device = renderer->getDevice().lock();

            // compute offset for assert purposess
            auto offset = vk::DeviceSize(0);
            for (int currentImageIndex = 0; currentImageIndex < m_imageInfos.size(); ++currentImageIndex)
            {
                if (currentImageIndex == imageIndex)
                    break;
                offset += m_imageInfos.at(currentImageIndex).byteSize;
            }

            auto bytes = uint32_t(sizeof(T) * data.size());
            auto& currentImage = *m_images.at(imageIndex).first;

            assert(bytes == m_imageInfos.at(imageIndex).byteSize);
            assert(bytes + offset <= m_size);

            //1. creates staging buffer            
            // {eHostVisible|eHostCoherent} - CPU writes, copies to GPU, or CPU Writes, GPU reads directly
            auto stageBuffer = std::make_shared<libVulkan::VulkanBuffer>(instance, device, vk::BufferUsageFlagBits::eTransferSrc, vk::MemoryPropertyFlagBits::eHostVisible | vk::MemoryPropertyFlagBits::eHostCoherent, bytes, "stage buffer of" + m_name);
            //2. map that buffer to be writtable
            stageBuffer->map();
            //3. copy data to it
            stageBuffer->bufferData(data);
            //4. unmap it since this is not persistent buffer and after copyQue will be deleted
            stageBuffer->unmap();
            //5. transition image layout to transfer optimal so it can be copied 
            renderer->recordAndSubmit([&](libVulkan::VulkanCommandBuffer& commandBuffer)
                {
                    //do dependency chain - anything before gets inovked, transfer commands after this barrier will wait, and then on second barrier all transfer before will execute and then transition will take place
                    // - transfer stage should wait for image to get into dst optial layout
                    libVulkan::VulkanBarrier::recordBarriers(commandBuffer, { currentImage.generateBarrier(vk::ImageLayout::eGeneral, vk::ImageLayout::eTransferDstOptimal, vk::AccessFlagBits2::eNone, vk::PipelineStageFlagBits2::eTopOfPipe, vk::AccessFlagBits2::eTransferWrite, vk::PipelineStageFlagBits2::eTransfer) });
                    //6. copy data within the staging buffer to texture
                    commandBuffer.copyBufferToImage(*stageBuffer, 0, currentImage);
                    //7. since this is texture from which its gonna be sampled from within shader, another layout transition is nescessary
                    libVulkan::VulkanBarrier::recordBarriers(commandBuffer, { currentImage.generateBarrier(vk::ImageLayout::eTransferDstOptimal, vk::ImageLayout::eGeneral, vk::AccessFlagBits2::eTransferWrite | vk::AccessFlagBits2::eTransferRead, vk::PipelineStageFlagBits2::eTransfer, vk::AccessFlagBits2::eNone, vk::PipelineStageFlagBits2::eBottomOfPipe) });
                }
            );
        }

        //! For single time data buffering, buffers content of whole image data into GPU memory and ties buffered data together with image at imageIndex
        //! Transfering to device local memory via stage buffer - not suitable for streaming data in
        template <typename T>
        void bufferData(std::span<T>const& data, uint32_t imageIndex)
        {
            auto renderer = m_renderer.lock();
            auto instance = renderer->getInstance().lock();
            auto device = renderer->getDevice().lock();

            // compute offset for assert purposess
            auto offset = vk::DeviceSize(0);
            for (int currentImageIndex = 0; currentImageIndex < m_imageInfos.size(); ++currentImageIndex)
            {
                if (currentImageIndex == imageIndex)
                    break;
                offset += m_imageInfos.at(currentImageIndex).byteSize;
            }

            auto bytes = sizeof(T) * data.size();
            auto& currentImage = *m_images.at(imageIndex).first;

            //assert(bytes == m_imageInfos.at(imageIndex).byteSize);
            assert(bytes + offset <= m_size);

            //1. creates staging buffer            
            // {eHostVisible|eHostCoherent} - CPU writes, copies to GPU, or CPU Writes, GPU reads directly
            auto stageBuffer = std::make_shared<libVulkan::VulkanBuffer>(instance, device, vk::BufferUsageFlagBits::eTransferSrc, vk::MemoryPropertyFlagBits::eHostVisible | vk::MemoryPropertyFlagBits::eHostCoherent, bytes);
            //2. map that buffer to be writtable
            stageBuffer->map();
            //3. copy data to it
            stageBuffer->bufferData(data);
            //4. unmap it since this is not persistent buffer and after copyQue will be deleted
            stageBuffer->unmap();
            //5. transition image layout to transfer optimal so it can be copied 
            auto renderer = m_renderer.lock();
            renderer->recordAndSubmit([=](libVulkan::VulkanCommandBuffer& commandBuffer)->void
                {
                    libVulkan::VulkanBarrier::imageTransitioned(commandBuffer, currentImage, vk::ImageLayout::eTransferDstOptimal);
                    //6. copy data within the staging buffer to texture
                    commandBuffer.copyBufferToImage(*stageBuffer, 0, currentImage);
                    //7. since this is texture from which its gonna be sampled from within shader, another layout transition is nescessary
                    libVulkan::VulkanBarrier::imageTransitioned(commandBuffer, currentImage, vk::ImageLayout::eShaderReadOnlyOptimal);
                }
            );
        }

    protected:
        //! Initializes underlying image vulkan structure
        void initializeImage(ImageInfo const& imageInfo, bool sampledImage);

    protected:
        std::weak_ptr<libCore::Renderer> m_renderer;

        //! Describes informations about images
        std::vector<ImageInfo> m_imageInfos;
        //! Cumulative size of the all images together - only for validation
        uint32_t m_size;
        
        //! Whether all images use samplers or no
        bool m_sampled;

        //! Refered images with samplers 
        std::vector<std::pair<std::shared_ptr<libVulkan::VulkanImage>, std::shared_ptr<libVulkan::VulkanImageSampler>>> m_images;

        std::string m_name;
    };
}