#pragma once
//!std
#include <span>
#include <string>
#include <tuple>
//! tinygltf
#include <tinygltf/tiny_gltf.h>
//! libCore
#include <libCore/TextureBuffer.hpp>

namespace libCore
{
	class Scene;
	class Renderer;
	class MaterialLayout;
	class TransformGraphManager;
	class TransformGraphComponent;
	class TextureBuffer;
	class RenderComponent;
	class VertexBuffer;
	class IndexBuffer;
	class AABB;
	class Camera;
	class Skeleton;
}

namespace libCore
{
	class Loader
	{
	public:
		enum class LoadSettings { BINARY, ASCII, AUTO };
		enum class Support { NONE, PARTIAL, FULL };
		//! Order is important, should go from least amount of data req to the most
		enum class Material { UNSHADED, UNSHADED_TEXTURED, PBR, PHONG, NORMAL_PHONG, PBR_TEXTURED, SKELETAL_ANIMATION, COUNT};

	public:
		Loader(std::shared_ptr<Renderer> const& renderer, std::weak_ptr<TransformGraphManager> transformationManager);
		~Loader();

		//! Loads given gltf
		bool load(std::string const& path, Material prefferedMaterial = Material::COUNT, LoadSettings loadSettings = LoadSettings::AUTO);
		//! Frees up loaded gltf
		void free();
		//! Checks whether now data are loaded, and can be retrieved
		bool hasLoadedData() const;
		//! Gets loaded scene
		std::shared_ptr<Scene> getScene();
		//! For loaded scene, returns all used materials
		std::vector<Loader::Material> const& getUsedMaterials();
		
	protected:
		struct VertexAttributeDescription
		{
			//view - other stuff which is needed
			tinygltf::BufferView view;
			//byteSize of attribute per vertexShader invocation, not the whole array
			uint32_t attributeByteSize;
			//accessor offset
			uint32_t accessorByteOffset;
			// in case the attribute is custom, is instead of bufferview stored directly here
			std::vector<unsigned char> customAttribute;
		};

	protected:
		//! Stores the material in vector, only if already not stored
		void markUsedMaterial(Material material);
		//! Creates specific texture buffer for primitive, buffers the texture, and returns the buffer
		std::shared_ptr<TextureBuffer> loadAndBufferTexture(tinygltf::Model const& tinyModel, tinygltf::Image const& image, std::string const& nodeName);
		std::shared_ptr<TextureBuffer> loadAndBufferAlbedoTexture(tinygltf::Model const& tinyModel, tinygltf::Primitive const& primitive, std::string const& nodeName);
		std::shared_ptr<TextureBuffer> loadAndBufferNormalTexture(tinygltf::Model const& tinyModel, tinygltf::Primitive const& primitive, std::string const& nodeName);
		std::shared_ptr<TextureBuffer> loadAndBufferOcclusionTexture(tinygltf::Model const& tinyModel, tinygltf::Primitive const& primitive, std::string const& nodeName);
		std::shared_ptr<TextureBuffer> loadAndBufferMetallicRoughnessTexture(tinygltf::Model const& tinyModel, tinygltf::Primitive const& primitive, std::string const& nodeName);
		//! Starts parsing the input file
		void parse(tinygltf::Model const& tinyModel, libCore::Scene& scene, Material prefferedMaterial);
		//! Walks thru the node tree, and for each node applies function fn. Can specify rootNodeIDWithParent which will walk the tree from that node down
		void walkTree(tinygltf::Model const& tinyModel, tinygltf::Scene const& tinyScene, std::function<void(tinygltf::Node const&, int nodeID, int parentID)> const& fn, std::pair<int, int> rootNodeIDWithParent = {-1,-1});
		//! Processes single node in scene - return -1 or index to processed mesh
		void processNode(tinygltf::Model const& tinyModel, tinygltf::Scene const& tinyScene, tinygltf::Node const& tinyNode, std::string const& nodePath, libCore::Scene& scene, Material prefferedMaterial);
		//! Parses and creates camera
		std::shared_ptr<libCore::Camera> parseCamera(tinygltf::Camera const& tinyCamera, std::string const& transformId);
		//! Parses and creates model
		std::shared_ptr<RenderComponent> parseModel(tinygltf::Model const& tinyModel, tinygltf::Mesh const& tinyMesh, libCore::Scene& scene, Material prefferedMaterial, std::string const& nodeName);
		//! Parses transformation matrix
		glm::mat4 parseMatrix(tinygltf::Node const& node);

		//! Debug print of what the loaded gltf contains
		void debug(tinygltf::Model const& tinyModel, std::string const& fileName) const;
		std::string printImage(tinygltf::Image const& image) const;
		std::string printMaterial(tinygltf::Material const& material) const;
		std::string printMatchedMaterial(tinygltf::Model const& tinyModel, tinygltf::Primitive const& primitive, tinygltf::Material const& material) const;
		
		//! Loads drawable from tinygltf's mesh
		std::tuple<std::shared_ptr<RenderComponent>, std::shared_ptr<MaterialLayout>> createRenderComponent(Material material, tinygltf::Model const& tinyModel, tinygltf::Primitive const& primitive) const;
		//! Buffers vertex and index atributes for drawable
		void bufferAttributes(libCore::RenderComponent& drawable, Material material, tinygltf::Model const& tinyModel, tinygltf::Primitive const& primitive, std::string const& nodeName);
		//! Initializes all needed uniforms based on material
		void initializeUniforms(libCore::RenderComponent& drawable, Material material, tinygltf::Model const& tinyModel, tinygltf::Primitive const& primitive, std::string const& nodeName);
		
		//! How many vertex shader invocations for model there will be
		uint32_t createVertexInvocationCount(tinygltf::Model const& tinyModel, tinygltf::Primitive const& primitive) const;

		//! VertexAtributes helper methods
		vk::Format getAttributeFormat(std::pair<std::string const, int> const& attribute) const;
		uint32_t getComponentSize(int componentType) const;
		uint32_t getTypeSize(int type) const;
		uint32_t getAtributeByteSize(std::pair<std::string const, int> const& attribute, tinygltf::Model const& tinyModel) const;
		uint32_t getAtributeCount(std::pair<std::string const, int> const& attribute, tinygltf::Model const& tinyModel) const;
		uint32_t getIndexBufferCount(tinygltf::Model const& tinyModel, tinygltf::Primitive const& primitive) const;
		//! What kind of index type indices have
		vk::IndexType getIndexType(tinygltf::Model const& tinyModel, tinygltf::Primitive const& primitive) const;
		
		uint32_t getVertexCount(tinygltf::Model const& tinyModel, tinygltf::Primitive const& primitive) const;

		uint32_t getVBOSize(Material const& material, tinygltf::Model const& tinyModel, tinygltf::Primitive const& primitive) const;
		uint32_t getIBOSize(tinygltf::Model const& tinyModel, tinygltf::Primitive const& primitive) const;
		void getTBOSize(Material const& material, tinygltf::Model const& tinyModel, tinygltf::Primitive const& primitive, std::vector<TextureBuffer::ImageInfo>& sizesOut) const;
		uint32_t getVertexStride(Material material) const;

		std::pair<std::string, int> findVertexAttribute(tinygltf::Primitive const& primitive) const;
		std::pair<std::string, int> findColorAttribute(tinygltf::Primitive const& primitive) const;
		std::pair<std::string, int> findNormalAttribute(tinygltf::Primitive const& primitive) const;
		std::pair<std::string, int> findUvAttribute(tinygltf::Primitive const& primitive) const;
		std::pair<std::string, int> findTangentAttribute(tinygltf::Primitive const& primitive) const;
		std::pair<std::string, int> findJointAttribute(tinygltf::Primitive const& primitive) const;
		std::pair<std::string, int> findWeightAttribute(tinygltf::Primitive const& primitive) const;

		std::pair<std::string, int> findAttributeSet(uint32_t setIndex, std::string const& attributeName, tinygltf::Primitive const& primitive) const;

		void generateColors(tinygltf::Model const& tinyModel, tinygltf::Primitive const& primitive, VertexAttributeDescription& descriptionOut) const;
		void generateNormals(tinygltf::Model const& tinyModel, tinygltf::Primitive const& primitive, VertexAttributeDescription& descriptionOut) const;
		void generateTangents(tinygltf::Model const& tinyModel, tinygltf::Primitive const& primitive, VertexAttributeDescription& descriptionOut) const;

		bool hasAttribute(std::pair<std::string, int> const& attribute, std::string const& searchedAttrib) const;
		bool hasIndices(tinygltf::Primitive const& primitive) const;
		bool hasVertices(tinygltf::Primitive const& primitive) const;
		bool hasNormals(tinygltf::Primitive const& primitive) const;
		bool hasUvs(tinygltf::Primitive const& primitive) const;
		bool hasTangents(tinygltf::Primitive const& primitive) const;
		bool hasColors(tinygltf::Primitive const& primitive) const;
		bool hasJoints(tinygltf::Primitive const& primitive) const;
		bool hasWeights(tinygltf::Primitive const& primitive) const;
		VertexAttributeDescription getVertexView(tinygltf::Model const& tinyModel, tinygltf::Primitive const& primitive) const;
		VertexAttributeDescription getNormalView(tinygltf::Model const& tinyModel, tinygltf::Primitive const& primitive) const;
		VertexAttributeDescription getUvView(tinygltf::Model const& tinyModel, tinygltf::Primitive const& primitive) const;
		VertexAttributeDescription getTangentView(tinygltf::Model const& tinyModel, tinygltf::Primitive const& primitive) const;
		VertexAttributeDescription getColorView(tinygltf::Model const& tinyModel, tinygltf::Primitive const& primitive) const;
		VertexAttributeDescription getJointView(tinygltf::Model const& tinyModel, tinygltf::Primitive const& primitive) const;
		VertexAttributeDescription getWeightView(tinygltf::Model const& tinyModel, tinygltf::Primitive const& primitive) const;

		std::shared_ptr<libCore::AABB> getMinMaxBBValue(tinygltf::Model const& tinyModel, tinygltf::Primitive const& primitive) const;

		void interleaveData(uint32_t vboSize, std::vector<VertexAttributeDescription> const& attributes, tinygltf::Model const& tinyModel, tinygltf::Primitive const& primitive, std::vector<unsigned char>& dataOut ) const;

		void gatherVertexAttributesForMaterial(Material materialType, tinygltf::Model const& tinyModel, tinygltf::Primitive const& primitive, std::vector<VertexAttributeDescription>& descriptionsOut) const;

		void bufferVBO(VertexBuffer& buffer, Material material, tinygltf::Model const& tinyModel, tinygltf::Primitive const& primitive);
		void bufferTBO(TextureBuffer& buffer, Material material, tinygltf::Model const& tinyModel, tinygltf::Primitive const& primitive);
		void bufferIBO(IndexBuffer& buffer, tinygltf::Model const& tinyModel, tinygltf::Primitive const& primitive);

		glm::vec4 getAlbedoColorValue(tinygltf::Material const& material) const;
		glm::vec3 getEmissivityValue(tinygltf::Material const& material) const;
		float getRoughnessValue(tinygltf::Material const& material) const;
		float getMetallicnessValue(tinygltf::Material const& material) const;

		//! texture helper loader methods
		bool hasMaterial(tinygltf::Model const& tinyModel, tinygltf::Primitive const& primitive) const;
		tinygltf::Material const& getMaterial(tinygltf::Model const& tinyModel, tinygltf::Primitive const& primitive) const;
		bool hasAlbedoTexture(tinygltf::Model const& tinyModel, tinygltf::Primitive const& primitive) const;
		bool hasNormalTexture(tinygltf::Model const& tinyModel, tinygltf::Primitive const& primitive) const;
		bool hasOcclusionTexture(tinygltf::Model const& tinyModel, tinygltf::Primitive const& primitive) const;
		bool hasEmissiveTexture(tinygltf::Model const& tinyModel, tinygltf::Primitive const& primitive) const;
		bool hasMetalicRoughnessTexture(tinygltf::Model const& tinyModel, tinygltf::Primitive const& primitive) const;
		tinygltf::Image const& getAlbedoImage(tinygltf::Model const& tinyModel, tinygltf::Primitive const& primitive) const;
		tinygltf::Image const& getNormalImage(tinygltf::Model const& tinyModel, tinygltf::Primitive const& primitive) const;
		tinygltf::Image const& getOcclusionImage(tinygltf::Model const& tinyModel, tinygltf::Primitive const& primitive) const;
		tinygltf::Image const& getEmissiveImage(tinygltf::Model const& tinyModel, tinygltf::Primitive const& primitive) const;
		tinygltf::Image const& getMetalicRoughnessImage(tinygltf::Model const& tinyModel, tinygltf::Primitive const& primitive) const;
		
		void generateTextureInfo(tinygltf::Image const& image, libCore::TextureBuffer::ImageInfo& infoOut) const;
		vk::Format getFormat(tinygltf::Image const& image) const;
		bool textureFromBuffer(tinygltf::Image const& image) const;
		uint32_t getImageByteSize(tinygltf::Image const& image) const;
		void getImageData(tinygltf::Model const& tinyModel, tinygltf::Image const& image, std::span<unsigned char>& imageOut) const;
		//! Buffers an arbitrary image into buffer
		uint32_t bufferTexture(TextureBuffer& buffer, tinygltf::Model const& tinyModel, tinygltf::Image const& image, uint32_t imageIndex);
		
		//! Material stuff
		Material findMaterialForModel(tinygltf::Model const& tinyModel, tinygltf::Primitive const& primitive) const;
		//! Gives indication whether this model fully or partially supports given material - partial support means the missing data can be computed
		Support supportsMaterial(Material materialType, tinygltf::Model const& tinyModel, tinygltf::Primitive const& primitive) const;
		//! Builds material from enum
		std::shared_ptr<MaterialLayout> createMaterialLayout(Material materialType) const;

		std::shared_ptr<Skeleton> parseSkin(tinygltf::Model const& tinyModel, tinygltf::Scene const& tinyScene, tinygltf::Skin const& skin);
		void loadBindMatrices(tinygltf::Model const& tinyModel, int accessor, std::vector<glm::mat4>& inverseBinds);
		std::shared_ptr<Skeleton> parseJoints(tinygltf::Model const& tinyModel, tinygltf::Scene const& tinyScene, tinygltf::Skin const& skin, std::vector<glm::mat4> const& inverseBinds);
	
		uint32_t getJointAttribSize(tinygltf::Model const& tinyModel, tinygltf::Primitive const& primitive) const;
		void getExpandedJointsView(tinygltf::Model const& tinyModel, tinygltf::Primitive const& primitive, VertexAttributeDescription& descriptionOut) const;

	protected:
		//! Renderer thru which loaded data will be buffered
		std::weak_ptr<Renderer> m_renderer;
		//! Scene transformations and it's "layout" will be recorded by manager
		std::weak_ptr<TransformGraphManager> m_transformGraphManager;

		//! Whole gltf loader
		std::shared_ptr<tinygltf::TinyGLTF> m_loader;

		//! Loaded scene
		std::shared_ptr<Scene> m_scene;

		//! Stored materials which were loaded - so caller knows which materials it needs to buffer
		std::vector<Material> m_loadedMaterials;

		//! Stored path to file
		std::string m_path;
	};
}