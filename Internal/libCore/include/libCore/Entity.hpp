#pragma once
//! std
#include <string>
#include <memory>
//! glm
#include <glm/glm.hpp>
//! entt
#include <entt.hpp>

namespace libCore
{
	class Entity
	{
	public:
		//! entt::entity - is just 32 bit int, components are added to registry
		Entity(entt::entity entity, std::string const& displayName);
		~Entity() = default;

		//! Display name 
		void setDisplayName(std::string const& name);
		std::string getDisplayName() const;

		//! Returns entity id
		entt::entity operator()() const;
	
	protected:
		//! Name which can be presented to user
		std::string m_displayName;
		//! Entity id - basically uint
		entt::entity m_entity;
	};
}