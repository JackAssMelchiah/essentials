#pragma once
//! Vulkan
#include <vulkan/vulkan.hpp>

//! std
#include <memory>
#include <vector>

namespace libCore
{
	class Renderer;
}

namespace libVulkan
{
	class VulkanDevice;
	class VulkanDescriptorSet;
	class VulkanDescriptorSetLayout;
	class VulkanRenderingInfo;
	class VulkanImageAttachment;
}

namespace libCore
{
	//! Given the RenderingInfo, SinglePassRendering will create, allocate, synchronize all nescesary input and output attachments,
	//! with its own single shader resources, which will be shared among all the entities and materials which want to use SinglePassRendering
	class RenderingLink
	{
	public:
		RenderingLink(std::shared_ptr<Renderer> const& renderer, std::shared_ptr<libVulkan::VulkanRenderingInfo> const& renderingInfo);
		~RenderingLink() = default;

		//! Rebinds the uniforms
		virtual void resolutionChanged(vk::Extent2D const&) = 0;
		//! Buffers the pass resources
		virtual void bufferResources() = 0;
		//! Getter for descriptor set
		virtual libVulkan::VulkanDescriptorSet& getDescriptorSet() = 0;
		//! Returns the layout of the descriptor set
		virtual std::shared_ptr<libVulkan::VulkanDescriptorSetLayout> getDescriptorSetLayout() const = 0;


		//! Checks whether the rendering info can be used with this object pass
		bool compatible(libVulkan::VulkanRenderingInfo const& renderingInfo);
		//! Returns the rendering info for which this pass was created
		std::shared_ptr<libVulkan::VulkanRenderingInfo> const& getRenderingInfo() const;

		//! TODO: SHOULD not be here
		std::weak_ptr<libVulkan::VulkanImageAttachment> getOutputAttachment();
		std::weak_ptr<libVulkan::VulkanImageAttachment> getOutputDepthStencilAttachment();
		void createAttachments(vk::Extent2D const& resolution);


	protected:
		//! Renderer
		std::weak_ptr<Renderer> m_renderer;

		//! Info about vulkan attachments for the pass
		std::shared_ptr<libVulkan::VulkanRenderingInfo> m_renderingInfo;

		std::vector<std::shared_ptr<libVulkan::VulkanImageAttachment>> m_inputAttachments;
		std::vector<std::shared_ptr<libVulkan::VulkanImageAttachment>> m_outputAttachments;
		std::shared_ptr<libVulkan::VulkanImageAttachment> m_depthStencilAttachment;
	};
}