#pragma once
//! libCore
#include <libCore/Ray.hpp>
//! glm
#include <glm/glm.hpp>

namespace libCore
{
	class AABB;
}

namespace libCore
{
	//! For now since there are no mechanism of some events, etc... it is implemented explicitly
	class Camera
	{
		// view matrix format goes as follows:
			/*| rx upx -fx eyex |
			  | ry upy -fy eyey |
			  | rz upz -fz eyez |
			  | 0  0   0    1   |*/
			  // r,u,f are normalized
			  // r - right vector of the camera, defines rightward direction relative to camera's orientation, in camera space
			  // up - up vector of the camera, defines upward direction relative to camera's orientation, in camera space
			  // f - negative forward vector of the camera - defines the direction in which camera is looking (view Direction) - this is the same as the world space camera direction.
			  // Negated because traditionally camera's forward direction is considered to be along negative z-axis
			  // eye is position of the camera, but in camera space

	public:
		//! Perspective camera
		Camera(double fov, double aspect, glm::vec2 const& nearFar);
		//! Ortho camera
		Camera(glm::vec2 const& leftRight, glm::vec2 const& topBottom, glm::vec2 const& nearFar);
		virtual ~Camera() = default;


		glm::vec3 NDCtoWorldCoords(glm::vec2 const& ndc) const;
		glm::vec3 NDCtoWorldDir(glm::vec2 const& ndc) const;

		//! Generates ray
		Ray generateWorldRay(glm::vec2 const& viewSpaceCoords) const;
					
		//! Computes orthogonal projection, parameters should be {aspectRatio, -aspectRatio}, {-1.f, 1.f}, {-1.f, 1.f}
		void setOrthogonalProjection(glm::vec2 const& leftRight, glm::vec2 const& topBottom, glm::vec2 const& nearFar);
		//! Computes perspective projection
		void setPerspectiveProjection(float FoV, float aspectRatio, glm::vec2 const& nearFar);
		//! Getter
		glm::mat4 getProjectionMatrix() const;
		//! Sets viewMatrix
		void setViewMatrix(glm::mat4 viewMatrix);
		//! Getter
		glm::mat4 getViewMatrix() const;
		glm::mat4& getViewMatrix();

		//! Returns world space camera position
		glm::vec3 getPosition() const;
		//! Returs view direction
		glm::vec3 getForwardDirection() const;

		//! Sets the camera to view the AABB pefectly
		void view(libCore::AABB const&);

		//! Returs transformation matrix which when applied will transform model to be exact fit to screen size
		glm::mat4 getFitTranform(libCore::AABB const&) const;

	protected:
		//! Stored matrices, so there is no need to recompute it all the time
		glm::mat4 m_projectionMatrix;
		glm::mat4 m_view;
		//! Near far plane
		glm::vec2 m_nearFar;
	};
}
