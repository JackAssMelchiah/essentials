#pragma once
//! libCore
#include <libCore/Handler.hpp>
//! libUtils
#include <libUtils/Time.hpp>
//! glm
#include <glm/glm.hpp>
//! std
#include <vector>
#include <map>

namespace libCore
{
	class FirstPersonHandler: public Handler
	{
	public:
		//! Custom key action which handler recognizes, and which can be customized with key
		enum class KeyAction { FORWARD, BACKWARD, UP, DOWN, LEFT, RIGHT, SELECT, DESELECT };
		//! Customizable key array
		using KeyMappings = std::map<KeyAction, SDL_Keycode>;

	public:
		FirstPersonHandler(glm::vec3 const& position, glm::vec3 const& direction = glm::vec3(0.f, 0.f, -1.f));
		~FirstPersonHandler() = default;
		
		void handle(Camera& camera) override;
		//! Customizes inputs for actions
		bool customizeKeyMappings(KeyMappings const& keys);

		void resize(glm::ivec2 const& viewport) override;
				
		//! Sets postition of camera and direction which its facing -- this is useful when need to change camera position completely
		void set(glm::vec3 position, glm::vec3 viewDirection) override;

	protected:
		struct KeyActionMetadata
		{
			KeyAction action;
			libUtils::Time timer;
		};

		struct MouseActionMetadata
		{
			libUtils::Time timer;
		};

	protected:
		void processKeyboardKey(SDL_KeyboardEvent const& keyboardEvent, libInputs::Keyboard const& keyboard, libInputs::Mouse const& mouse) override;
		void processMouseMotion(SDL_MouseMotionEvent const& mouseMotion, libInputs::Keyboard const& keyboard, libInputs::Mouse const& mouse) override;
		void processMouseButton(SDL_MouseButtonEvent const& mouseButton, libInputs::Keyboard const& keyboard, libInputs::Mouse const& mouse) override;
		void processMouseWheel(SDL_MouseWheelEvent const& mouseWheel, libInputs::Keyboard const& keyboard, libInputs::Mouse const& mouse) override;
		void processGamepadAxisMotion(SDL_ControllerAxisEvent const& controllerAxis, libInputs::Gamepad const& gamepad) override;
		void processGamepadButton(SDL_ControllerButtonEvent const& controllerButton, libInputs::Gamepad const& gamepad) override;
		void processFocusChanged(bool focusLost) override;

		void handleAction(KeyActionMetadata const& action);


		//! Moves camera up/down, expects allready framerate independent and acceleration modified value for distance
		void moveUpDown(float distance);
		//! Camera strafes left(-) or right(+), expects allready framerate independent and acceleration modified value for distance
		void strafeLeftRight(float distance);
		//! Camera moves forward(+) or backward(-), expects allready framerate independent and acceleration modified value for distance
		void moveForwardBackward(float distance);
		//! Rotates camera along X axis in certain angle in degrees, expects allready framerate independent and acceleration modified value for angle
		void changeCameraPitch(float angle);
		//! Rotates camera along Y axis(Z blender's) in certain angle in degrees, expects allready framerate independent and acceleration modified value for angle
		void changeCameraYaw(float angle);

		void updateDirection();

	protected:
		//! Actions which handlers use, and are asociated with keys 
		std::map<SDL_Keycode, KeyActionMetadata> m_keyActions;
	
		glm::vec3 m_direction;
		glm::vec3 m_position;

		float m_yaw;
		float m_pitch;

		bool m_handleMouseMovement;
	};
}