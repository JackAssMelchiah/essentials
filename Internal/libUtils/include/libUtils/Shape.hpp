#pragma once
//! std
#include <vector>
#include <array>
//! glm
#include <glm/glm.hpp>

namespace libUtils
{
	class Shape
	{
	public:
		struct Vertex 
		{
			glm::vec3 vertex;
			glm::vec3 normal;
			glm::vec2 uv;
		};

		struct ColoredVertex
		{
			glm::vec3 vertex;
			glm::vec3 color;
		};

	public:
		//! Generates Axies mesh and colors
		static void axies(std::vector<glm::vec3>& vertices, std::vector<glm::vec3>& colors);
		//! Generates cube mesh and colors
		static void cube(std::vector<glm::vec3>& vertices, std::vector<glm::vec3>& colors);
		//! Generates cube mesh with packed struct
		static void cube(std::vector<Vertex>& vertices);
		//! Generates cube mesh with packed struct
		static void cube(std::vector<ColoredVertex>& vertices);
		//! Generates grid within <-1,1>, consisting from desired number of points
		static void grid(std::vector<glm::vec3>& vertices, unsigned int rowPointsCount);
		//! Generates sample chess board for given resolution with format r8g8b8a8Unorm - note that it should be atleast 8x8
		static void generateSampleImage(glm::ivec2 const& imageResolution, glm::ivec2 const& quadCount, std::array<std::byte, 4> const& darkColor, std::array<std::byte, 4> const& lightColor, std::vector<std::byte>& image);

	protected:
		//! Returns cube as vertices, given by with heigh depth params, as well as given by origin, note that [-1-,1,-1] is left corner, [-1-,1,-1] is right corner
		static void parametricCube(std::vector<glm::vec3>& vertices, float width, float height, float depth, glm::vec3 unitOrigin);
		static void parametricCube(std::vector<Vertex>& data, float width, float height, float depth, glm::vec3 unitOrigin);
		static void parametricCube(std::vector<ColoredVertex>& data, float width, float height, float depth, glm::vec3 unitOrigin);
	};
}