#pragma once
#include <cstdint>

namespace libUtils
{
	class PCG32 
	{
	public:
		// Constructor to initialize the generator with a seed
		explicit PCG32(uint64_t seed);

		// Generate a random 32-bit number
		uint32_t random();

		// Generate a random number in a specific range
		uint32_t randomInRange(uint32_t min, uint32_t max);

		// Generate a random float in a specific range
		float randomInRange(float min, float max);

		// Reset the generator with a new seed value
		void reseed(uint64_t seed);

	private:
		uint64_t m_state; // Current state of the generator
		uint64_t m_inc;   // Increment value
	};
}