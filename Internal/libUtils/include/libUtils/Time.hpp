#pragma once
#include <vector>
#include <chrono>
#include <string>

namespace libUtils
{
	class Time
	{
	public:
		Time();
		~Time() = default;
		//! Starts timer, only if it is stopped
		void start();
		//! When running will pause, and save the internal clock
		void pause();
		void resume();
		void stop();
		bool paused() const;
		bool stopped() const;
		//! Returns measured time in miliseconds
		double getTime() const;
		//! Returns string with current system time
		static std::string getSystemTime();
		//! Returns string with timer's time - getTime() but formatted
		std::string getTimeString() const;
		
	protected:
		enum class ClockState { RUNNING, PAUSED, STOPPED };

	protected:
		//! Returns actual time in miliseconds from two time points
		double getMiliseconds() const;
		//! Converts timepoint to readable string
		static std::string toString(std::chrono::time_point<std::chrono::system_clock> const& timepoint);

	protected:
		ClockState m_state;
		double m_pausedMiliseconds;
		std::chrono::system_clock::time_point m_startTimePoint;
	};
}