#pragma once
//! std
#include <vector>
#include <string>
#include <algorithm>
#include <fstream>
#include <filesystem>
#include <span>
//! glm
#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>
//! libUtils
#include <libUtils/Shape.hpp>

namespace libUtils 
{
	//! Checks if second vector encapsulates with content the first one
	template<typename T>
	bool v2EncapsulatesV1(std::vector<T> const& v1, std::vector<T> const& v2)
	{
		std::vector<bool> matches(v1.size(), false);
		for (int i = 0; i < v1.size(); ++i)
		{
			for (int j = 0; j < v2.size(); ++j)
			{
				if (v1.at(i) == v2.at(j))
				{
					matches[i] = true;
					break;
				}
			}
		}
		for (auto& match : matches)
		{
			if (!match) return false;
		}
		return true;
	}

	//! Shifts vector left by specified amount units
	template <typename T> 
	static void shiftLeft(std::vector<T>& vector, int by)
	{
		std::rotate(vector.begin(), vector.begin() + by, vector.end());
		for (int i = vector.size() - 1; i > vector.size() - 1 - by; --i)
		{
			vector.at(i) = (T)0;
		}
	}

	//! Shifts vector right by specified amount units
	template <typename T> 
	static void shiftRight(std::vector<T>& vector, int by)
	{
		std::rotate(vector.begin(), vector.begin() + by, vector.end());
		for (int i = 0; i < by - 1; ++i) 
		{
			vector.at(i) = (T)0;
		}
	}
	
	//! Reads file
	template <typename T>
	bool readFile(std::string const& path, std::vector<T>& data)
	{
		auto fileStream = std::fstream(path, std::ios_base::in | std::ios_base::binary);
		if (!fileStream.is_open())
			return false;

		auto fileSize = std::filesystem::file_size({ path });
		data.resize(fileSize);
		fileStream.read(reinterpret_cast<char*>(data.data()), fileSize * sizeof(T));
		fileStream.close();
		return true;
	}

	//! Filesystem functions
	void listDir(std::string const& path, std::vector<std::string>& result);
	bool exists(std::string const& path);
	bool isFile(std::string const& path);
	bool isFolder(std::string const& path);
	bool isSymlink(std::string const& path);
	std::string absoulutePath(std::string const& path);

	//! Conversion functions
	std::string toString(float);
	std::string toString(double);
	std::string toString(uint32_t);
	std::string toString(int32_t);
	std::string toString(long);
	std::string toString(std::vector<float> const& vector);
	std::string toString(glm::vec4 const&);
	std::string toString(glm::vec3 const&);
	std::string toString(glm::vec2 const&);
	std::string toString(glm::mat4 const&);

	//! Interpolation methods
	float interpolate(float A, float B, float t);
	glm::vec3 interpolate(glm::vec3 const& A, glm::vec3 const& B, float t);
	glm::quat interpolate(glm::quat const& A, glm::quat const& B, float t);

	//! Prints out formated string in different color with time info
	void printOutErr(std::string const&);
	void printOutWar(std::string const&);
	void printOut(std::string const&);
	void printOutGL(std::string const&);
	
	//! Splits std string 
	void splitString(std::string const& target, std::string const& split_by, std::vector<std::string>& ret);
	//! Splits std string 
	void replaceWithinString(std::string const& target, std::string const& toReplace, std::string const& replaceWith, std::string& ret);
	//! Converts range into new range
	float convertRange(float curr_val, float old_min, float old_max, float new_min, float new_max);
	//! Converts range into new range
	void doIndexing(std::vector<glm::vec3> const& vertices, std::vector<glm::vec2> const& uvs, std::vector<glm::vec3> const& normals, std::vector<int> const& indices, std::vector<glm::vec3>& indexed_vert, std::vector<glm::vec2>& indexed_uvs, std::vector<glm::vec3>& indexed_normals);
	//! Alignes input number to be divisible without rest to alignment
	template <typename T>
	T align(T const& input, T const& alignment)
	{
		return input + ((alignment - (input % alignment)) % alignment);
	}
	//! Generates string from id
	std::string stringID(uint64_t id);
}