#include <libUtils/PCG32.hpp>

namespace libUtils
{
    PCG32::PCG32(uint64_t seed)
    {
        reseed(seed);
    }

    // Generate a random 32-bit number
    uint32_t PCG32::random()
    {
        uint64_t oldstate = m_state;
        // Advance internal state
        m_state = oldstate * 6364136223846793005ULL + (m_inc | 1);
        // Calculate output function (XSH RR), uses old state for max ILP
        uint32_t xorshifted = ((oldstate >> 18u) ^ oldstate) >> 27u;
        uint32_t rot = oldstate >> 59u;
        return (xorshifted >> rot) | (xorshifted << ((-rot) & 31));
    }

    // Generate a random number in a specific range
    uint32_t PCG32::randomInRange(uint32_t min, uint32_t max)
    {
        return min + random() % (max - min + 1);
    }

    // Generate a random float in a specific range
    float PCG32::randomInRange(float min, float max)
    {
        // Generate a random integer in the range [0, UINT32_MAX]
        uint32_t randInt = random();
        // Scale it to the range [0, 1)
        float randFloat = static_cast<float>(randInt) / static_cast<float>(UINT32_MAX);
        // Scale and shift to the desired range [min, max)
        return min + randFloat * (max - min);
    }

    void PCG32::reseed(uint64_t seed)
    {
        // Initialize state and increment
        m_state = seed;
        m_inc = (seed << 1u) | 1u; // Ensure increment is odd
        random();
    }
}