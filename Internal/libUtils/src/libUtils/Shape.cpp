#include <libUtils/Shape.hpp>

namespace libUtils
{
	void Shape::axies(std::vector<glm::vec3>& vertices, std::vector<glm::vec3>& colors) 
	{
		//! Creates 3 cubes, then rotates them
		vertices.clear();
		colors.clear();

		Shape::parametricCube(vertices, 1.f, .1f, .1f, glm::vec3(-1.f, -1.f, 0.f));
		Shape::parametricCube(vertices, .1f, 1.f, .1f, glm::vec3(-1.f, -1.f, 0.f));
		Shape::parametricCube(vertices, .1f, .1f, 1.f, glm::vec3(-1.f, -1.f, -1.f));

		for (int i = 0; i < vertices.size() / 3; ++i)
			colors.push_back(glm::vec3(1.f, 0.f, 0.f));
		
		for (int i = 0; i < vertices.size() / 3; ++i)
			colors.push_back(glm::vec3(0.f, 0.f, 1.f));
		
		for (int i = 0; i < vertices.size() / 3; ++i)
			colors.push_back(glm::vec3(0.f, 1.f, 0.f));
	}

	void Shape::cube(std::vector<glm::vec3>& vertices, std::vector<glm::vec3>& colors) 
	{
		vertices.clear();
		colors.clear();
		Shape::parametricCube(vertices, 1, 1, 1, glm::vec3(0.f, 0.f, 0.f));

		auto faceColors = std::vector<glm::vec3>
		{
			glm::vec3{0.7f, 0.2f, 0.f}, glm::vec3{0.2f, 0.5f, 0.f}, glm::vec3{0.f, 0.5f, 0.6f},
			glm::vec3{0.8f, 0.3f, 0.7f}, glm::vec3{0.3f, 0.7f, 0.1f}, glm::vec3{0.4f, 0.2f, 0.1f}
		};

		for (int face = 0; face < 6; ++face)
		{
			for (int faceVert = 0; faceVert < 6; ++faceVert)
			{
				colors.push_back(faceColors.at(face));
			}
		}
	}

	void Shape::cube(std::vector<Vertex>& vertices)
	{
		vertices.clear();
		Shape::parametricCube(vertices, 1, 1, 1, glm::vec3(0.f, 0.f, 0.f));
	}

	void Shape::cube(std::vector<ColoredVertex>& vertices)
	{
		vertices.clear();
		Shape::parametricCube(vertices, 1, 1, 1, glm::vec3(0.f, 0.f, 0.f));
	}

	void Shape::parametricCube(std::vector<glm::vec3>& vertices, float width, float height, float depth, glm::vec3 unitOrigin)
	{
		glm::vec3 origin = glm::clamp(unitOrigin, glm::vec3(-1.f), glm::vec3(1.f)) * glm::vec3(width, height, depth) / 2.f;

		glm::vec3 minVal(-width / 2.f, -height / 2.f, -depth / 2.f);
		glm::vec3 maxVal(width / 2.f, height / 2.f, depth / 2.f);

		//front face
		vertices.push_back(glm::vec3(minVal.x, minVal.y, minVal.z));
		vertices.push_back(glm::vec3(minVal.x, maxVal.y, minVal.z));
		vertices.push_back(glm::vec3(maxVal.x, minVal.y, minVal.z));

		vertices.push_back(glm::vec3(maxVal.x, minVal.y, minVal.z));
		vertices.push_back(glm::vec3(minVal.x, maxVal.y, minVal.z));
		vertices.push_back(glm::vec3(maxVal.x, maxVal.y, minVal.z));

		//back face
		vertices.push_back(glm::vec3(minVal.x, minVal.y, maxVal.z));
		vertices.push_back(glm::vec3(minVal.x, maxVal.y, maxVal.z));
		vertices.push_back(glm::vec3(maxVal.x, minVal.y, maxVal.z));

		vertices.push_back(glm::vec3(maxVal.x, minVal.y, maxVal.z));
		vertices.push_back(glm::vec3(minVal.x, maxVal.y, maxVal.z));
		vertices.push_back(glm::vec3(maxVal.x, maxVal.y, maxVal.z));

		//left face
		vertices.push_back(glm::vec3(minVal.x, minVal.y, minVal.z));
		vertices.push_back(glm::vec3(minVal.x, maxVal.y, minVal.z));
		vertices.push_back(glm::vec3(minVal.x, minVal.y, maxVal.z));

		vertices.push_back(glm::vec3(minVal.x, minVal.y, maxVal.z));
		vertices.push_back(glm::vec3(minVal.x, maxVal.y, minVal.z));
		vertices.push_back(glm::vec3(minVal.x, maxVal.y, maxVal.z));

		//right face
		vertices.push_back(glm::vec3(maxVal.x, minVal.y, maxVal.z));
		vertices.push_back(glm::vec3(maxVal.x, maxVal.y, minVal.z));
		vertices.push_back(glm::vec3(maxVal.x, minVal.y, minVal.z));

		vertices.push_back(glm::vec3(maxVal.x, minVal.y, maxVal.z));
		vertices.push_back(glm::vec3(maxVal.x, maxVal.y, maxVal.z));
		vertices.push_back(glm::vec3(maxVal.x, maxVal.y, minVal.z));

		//top face
		vertices.push_back(glm::vec3(minVal.x, maxVal.y, minVal.z));
		vertices.push_back(glm::vec3(minVal.x, maxVal.y, maxVal.z));
		vertices.push_back(glm::vec3(maxVal.x, maxVal.y, maxVal.z));

		vertices.push_back(glm::vec3(maxVal.x, maxVal.y, maxVal.z));
		vertices.push_back(glm::vec3(maxVal.x, maxVal.y, minVal.z));
		vertices.push_back(glm::vec3(minVal.x, maxVal.y, minVal.z));

		//bottom face
		vertices.push_back(glm::vec3(minVal.x, minVal.y, minVal.z));
		vertices.push_back(glm::vec3(minVal.x, minVal.y, maxVal.z));
		vertices.push_back(glm::vec3(maxVal.x, minVal.y, maxVal.z));

		vertices.push_back(glm::vec3(maxVal.x, minVal.y, maxVal.z));
		vertices.push_back(glm::vec3(maxVal.x, minVal.y, minVal.z));
		vertices.push_back(glm::vec3(minVal.x, minVal.y, minVal.z));

		for (int i = 0; i < vertices.size(); ++i) 
			vertices[i] += origin;
	}

	void Shape::parametricCube(std::vector<Vertex>& data, float width, float height, float depth, glm::vec3 unitOrigin)
	{
		glm::vec3 origin = glm::clamp(unitOrigin, glm::vec3(-1.f), glm::vec3(1.f)) * glm::vec3(width, height, depth) / 2.f;

		glm::vec3 minVal(-width / 2.f, -height / 2.f, -depth / 2.f);
		glm::vec3 maxVal(width / 2.f, height / 2.f, depth / 2.f);

		//TODO: compute normals

		//front face
		data.push_back({ glm::vec3(minVal.x, minVal.y, minVal.z), glm::vec3(0.f, 0.f, 1.f), glm::vec2(0.f, 0.f)});
		data.push_back({ glm::vec3(minVal.x, maxVal.y, minVal.z), glm::vec3(0.f, 0.f, 1.f), glm::vec2(0.f, 1.f)});
		data.push_back({ glm::vec3(maxVal.x, minVal.y, minVal.z), glm::vec3(0.f, 0.f, 1.f), glm::vec2(1.f, 0.f)});

		data.push_back({ glm::vec3(maxVal.x, minVal.y, minVal.z), glm::vec3(0.f, 0.f, 1.f), glm::vec2(1.f, 0.f)});
		data.push_back({ glm::vec3(minVal.x, maxVal.y, minVal.z), glm::vec3(0.f, 0.f, 1.f), glm::vec2(0.f, 1.f)});
		data.push_back({ glm::vec3(maxVal.x, maxVal.y, minVal.z), glm::vec3(0.f, 0.f, 1.f), glm::vec2(1.f, 1.f)});

		//back face
		data.push_back({ glm::vec3(minVal.x, minVal.y, maxVal.z), glm::vec3(0.f, 0.f, 1.f), glm::vec2(0.f, 0.f) });
		data.push_back({ glm::vec3(minVal.x, maxVal.y, maxVal.z), glm::vec3(0.f, 0.f, 1.f), glm::vec2(0.f, 1.f) });
		data.push_back({ glm::vec3(maxVal.x, minVal.y, maxVal.z), glm::vec3(0.f, 0.f, 1.f), glm::vec2(1.f, 0.f) });

		data.push_back({ glm::vec3(maxVal.x, minVal.y, maxVal.z), glm::vec3(0.f, 0.f, 1.f), glm::vec2(1.f, 0.f) });
		data.push_back({ glm::vec3(minVal.x, maxVal.y, maxVal.z), glm::vec3(0.f, 0.f, 1.f), glm::vec2(0.f, 1.f) });
		data.push_back({ glm::vec3(maxVal.x, maxVal.y, maxVal.z), glm::vec3(0.f, 0.f, 1.f), glm::vec2(1.f, 1.f) });

		//left face											
		data.push_back({ glm::vec3(minVal.x, minVal.y, minVal.z), glm::vec3(0.f, 0.f, 1.f), glm::vec2(0.f, 0.f) });
		data.push_back({ glm::vec3(minVal.x, maxVal.y, minVal.z), glm::vec3(0.f, 0.f, 1.f), glm::vec2(0.f, 1.f) });
		data.push_back({ glm::vec3(minVal.x, minVal.y, maxVal.z), glm::vec3(0.f, 0.f, 1.f), glm::vec2(1.f, 0.f) });

		data.push_back({ glm::vec3(minVal.x, minVal.y, maxVal.z), glm::vec3(0.f, 0.f, 1.f), glm::vec2(1.f, 0.f) });
		data.push_back({ glm::vec3(minVal.x, maxVal.y, minVal.z), glm::vec3(0.f, 0.f, 1.f), glm::vec2(0.f, 1.f) });
		data.push_back({ glm::vec3(minVal.x, maxVal.y, maxVal.z), glm::vec3(0.f, 0.f, 1.f), glm::vec2(1.f, 1.f) });

		//right face											
		data.push_back({ glm::vec3(maxVal.x, minVal.y, maxVal.z), glm::vec3(0.f, 0.f, 1.f), glm::vec2(0.f, 0.f) });
		data.push_back({ glm::vec3(maxVal.x, maxVal.y, minVal.z), glm::vec3(0.f, 0.f, 1.f), glm::vec2(1.f, 1.f) });
		data.push_back({ glm::vec3(maxVal.x, minVal.y, minVal.z), glm::vec3(0.f, 0.f, 1.f), glm::vec2(1.f, 0.f) });

		data.push_back({ glm::vec3(maxVal.x, minVal.y, maxVal.z), glm::vec3(0.f, 0.f, 1.f), glm::vec2(0.f, 0.f) });
		data.push_back({ glm::vec3(maxVal.x, maxVal.y, maxVal.z), glm::vec3(0.f, 0.f, 1.f), glm::vec2(0.f, 1.f) });
		data.push_back({ glm::vec3(maxVal.x, maxVal.y, minVal.z), glm::vec3(0.f, 0.f, 1.f), glm::vec2(1.f, 1.f) });

		//top face											
		data.push_back({ glm::vec3(minVal.x, maxVal.y, minVal.z), glm::vec3(0.f, 0.f, 1.f), glm::vec2(0.f, 1.f) });
		data.push_back({ glm::vec3(minVal.x, maxVal.y, maxVal.z), glm::vec3(0.f, 0.f, 1.f), glm::vec2(0.f, 0.f) });
		data.push_back({ glm::vec3(maxVal.x, maxVal.y, maxVal.z), glm::vec3(0.f, 0.f, 1.f), glm::vec2(1.f, 0.f) });

		data.push_back({ glm::vec3(maxVal.x, maxVal.y, maxVal.z), glm::vec3(0.f, 0.f, 1.f), glm::vec2(1.f, 0.f) });
		data.push_back({ glm::vec3(maxVal.x, maxVal.y, minVal.z), glm::vec3(0.f, 0.f, 1.f), glm::vec2(1.f, 1.f) });
		data.push_back({ glm::vec3(minVal.x, maxVal.y, minVal.z), glm::vec3(0.f, 0.f, 1.f), glm::vec2(0.f, 1.f) });

		//bottom face											
		data.push_back({ glm::vec3(minVal.x, minVal.y, minVal.z), glm::vec3(0.f, 0.f, 1.f), glm::vec2(0.f, 1.f) });
		data.push_back({ glm::vec3(minVal.x, minVal.y, maxVal.z), glm::vec3(0.f, 0.f, 1.f), glm::vec2(0.f, 0.f) });
		data.push_back({ glm::vec3(maxVal.x, minVal.y, maxVal.z), glm::vec3(0.f, 0.f, 1.f), glm::vec2(1.f, 0.f) });

		data.push_back({ glm::vec3(maxVal.x, minVal.y, maxVal.z), glm::vec3(0.f, 0.f, 1.f), glm::vec2(1.f, 0.f) });
		data.push_back({ glm::vec3(maxVal.x, minVal.y, minVal.z), glm::vec3(0.f, 0.f, 1.f), glm::vec2(1.f, 1.f) });
		data.push_back({ glm::vec3(minVal.x, minVal.y, minVal.z), glm::vec3(0.f, 0.f, 1.f), glm::vec2(0.f, 1.f) });

		for (int i = 0; i < data.size(); ++i)
			data[i].vertex += origin;
	}

	void Shape::parametricCube(std::vector<ColoredVertex>& data, float width, float height, float depth, glm::vec3 unitOrigin)
	{
		glm::vec3 origin = glm::clamp(unitOrigin, glm::vec3(-1.f), glm::vec3(1.f)) * glm::vec3(width, height, depth) / 2.f;

		glm::vec3 minVal(-width / 2.f, -height / 2.f, -depth / 2.f);
		glm::vec3 maxVal(width / 2.f, height / 2.f, depth / 2.f);

		auto faceColors = std::vector<glm::vec3>
		{
			glm::vec3{0.5f, 0.0f, 0.0f}, glm::vec3{1.0f, 0.0f, 0.0f}, glm::vec3{0.f, 0.5f, 0.0f},
			glm::vec3{0.0f, 1.0f, 0.0f}, glm::vec3{0.0f, 0.0f, 0.5f}, glm::vec3{0.0f, 0.0f, 1.0f}
		};

		//front face
		data.push_back({ glm::vec3(minVal.x, minVal.y, minVal.z), faceColors.at(0) });
		data.push_back({ glm::vec3(minVal.x, maxVal.y, minVal.z), faceColors.at(0) });
		data.push_back({ glm::vec3(maxVal.x, minVal.y, minVal.z), faceColors.at(0) });

		data.push_back({ glm::vec3(maxVal.x, minVal.y, minVal.z), faceColors.at(0) });
		data.push_back({ glm::vec3(minVal.x, maxVal.y, minVal.z), faceColors.at(0) });
		data.push_back({ glm::vec3(maxVal.x, maxVal.y, minVal.z), faceColors.at(0) });

		//back face
		data.push_back({ glm::vec3(minVal.x, minVal.y, maxVal.z), faceColors.at(1) });
		data.push_back({ glm::vec3(minVal.x, maxVal.y, maxVal.z), faceColors.at(1) });
		data.push_back({ glm::vec3(maxVal.x, minVal.y, maxVal.z), faceColors.at(1) });

		data.push_back({ glm::vec3(maxVal.x, minVal.y, maxVal.z), faceColors.at(1) });
		data.push_back({ glm::vec3(minVal.x, maxVal.y, maxVal.z), faceColors.at(1) });
		data.push_back({ glm::vec3(maxVal.x, maxVal.y, maxVal.z), faceColors.at(1) });

		//left face											
		data.push_back({ glm::vec3(minVal.x, minVal.y, minVal.z), faceColors.at(2) });
		data.push_back({ glm::vec3(minVal.x, maxVal.y, minVal.z), faceColors.at(2) });
		data.push_back({ glm::vec3(minVal.x, minVal.y, maxVal.z), faceColors.at(2) });

		data.push_back({ glm::vec3(minVal.x, minVal.y, maxVal.z), faceColors.at(2) });
		data.push_back({ glm::vec3(minVal.x, maxVal.y, minVal.z), faceColors.at(2) });
		data.push_back({ glm::vec3(minVal.x, maxVal.y, maxVal.z), faceColors.at(2) });

		//right face											
		data.push_back({ glm::vec3(maxVal.x, minVal.y, maxVal.z), faceColors.at(3) });
		data.push_back({ glm::vec3(maxVal.x, maxVal.y, minVal.z), faceColors.at(3) });
		data.push_back({ glm::vec3(maxVal.x, minVal.y, minVal.z), faceColors.at(3) });

		data.push_back({ glm::vec3(maxVal.x, minVal.y, maxVal.z), faceColors.at(3) });
		data.push_back({ glm::vec3(maxVal.x, maxVal.y, maxVal.z), faceColors.at(3) });
		data.push_back({ glm::vec3(maxVal.x, maxVal.y, minVal.z), faceColors.at(3) });

		//top face											
		data.push_back({ glm::vec3(minVal.x, maxVal.y, minVal.z), faceColors.at(4) });
		data.push_back({ glm::vec3(minVal.x, maxVal.y, maxVal.z), faceColors.at(4) });
		data.push_back({ glm::vec3(maxVal.x, maxVal.y, maxVal.z), faceColors.at(4) });

		data.push_back({ glm::vec3(maxVal.x, maxVal.y, maxVal.z), faceColors.at(4) });
		data.push_back({ glm::vec3(maxVal.x, maxVal.y, minVal.z), faceColors.at(4) });
		data.push_back({ glm::vec3(minVal.x, maxVal.y, minVal.z), faceColors.at(4) });

		//bottom face											
		data.push_back({ glm::vec3(minVal.x, minVal.y, minVal.z), faceColors.at(5) });
		data.push_back({ glm::vec3(minVal.x, minVal.y, maxVal.z), faceColors.at(5) });
		data.push_back({ glm::vec3(maxVal.x, minVal.y, maxVal.z), faceColors.at(5) });

		data.push_back({ glm::vec3(maxVal.x, minVal.y, maxVal.z), faceColors.at(5) });
		data.push_back({ glm::vec3(maxVal.x, minVal.y, minVal.z), faceColors.at(5) });
		data.push_back({ glm::vec3(minVal.x, minVal.y, minVal.z), faceColors.at(5) });

		for (int i = 0; i < data.size(); ++i)
			data[i].vertex += origin;
	}

	void Shape::grid(std::vector<glm::vec3>& vertices, unsigned int rowPointsCount)
	{
		vertices.clear();
		float delta = 2.f / rowPointsCount;
		glm::vec3 center;
		glm::vec3 TL(-delta / 2.f, 0.f, -delta / 2.f);
		glm::vec3 TR(+delta / 2.f, 0.f, -delta / 2.f);
		glm::vec3 BL(-delta / 2.f, 0.f, +delta / 2.f);
		glm::vec3 BR(+delta / 2.f, 0.f, +delta / 2.f);

		for (int i = 0; i < rowPointsCount; ++i) 
		{
			for (int j = 0; j < rowPointsCount; ++j) 
			{
				center = glm::vec3((i * delta + delta / 2.f) - 1.f, 0.f, (j * delta + delta / 2.f) - 1.f);

				vertices.push_back(center + TL);
				vertices.push_back(center + BL);
				vertices.push_back(center + TR);

				vertices.push_back(center + TR);
				vertices.push_back(center + BL);
				vertices.push_back(center + BR);
			}
		}
	}

	void Shape::generateSampleImage(glm::ivec2 const& imageResolution, glm::ivec2 const& quadCount, std::array<std::byte, 4> const& darkColor, std::array<std::byte, 4> const& lightColor, std::vector<std::byte>& image)
	{
		assert(imageResolution.x >= quadCount.x && imageResolution.y >= quadCount.y && quadCount.x > 0 && quadCount.y > 0);
		auto totalPixels = imageResolution.x * imageResolution.y;
		auto dark = false;

		for (int px = 0; px < totalPixels; ++px)
		{
			//change color every n-th pixel
			if (px % quadCount.x == 0)
			{
				dark = !dark;
			}

			if (dark)
			{
				image.push_back(darkColor.at(0));
				image.push_back(darkColor.at(1));
				image.push_back(darkColor.at(2));
				image.push_back(darkColor.at(3));
			}
			else 
			{
				image.push_back(lightColor.at(0));
				image.push_back(lightColor.at(1));
				image.push_back(lightColor.at(2));
				image.push_back(lightColor.at(3));
			}

			// also, if all quads in one dimms have been generated, then shift color again
			if (px % (imageResolution.x * quadCount.y) == 0)
			{
				dark = !dark;
			}
		}
	}
}