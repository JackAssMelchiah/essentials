//! libUtils
#include <libUtils/Utils.hpp>
#include <libUtils/Time.hpp>
//! std
#include <iostream>

#ifdef _MSC_VER
#include <windows.h>
#endif


namespace libUtils
{
	void listDir(std::string const& path, std::vector<std::string>& result)
	{
		result.clear();
		auto p = std::filesystem::path(path);
		assert(std::filesystem::exists(p));

		for (auto& item : std::filesystem::directory_iterator(p))
		{
			result.push_back(item.path().string());
		}
	}

	bool exists(std::string const& path)
	{
		return std::filesystem::exists(path);
	}

	bool isFile(std::string const& path)
	{
		assert(std::filesystem::exists(path));
		return (!isFolder(path)) && (!isSymlink(path)) && std::filesystem::exists(std::filesystem::path(path));
	}

	bool isFolder(std::string const& path)
	{
		assert(std::filesystem::exists(path));
		return std::filesystem::is_directory(std::filesystem::path(path));
	}

	bool isSymlink(std::string const& path)
	{
		assert(std::filesystem::exists(path));
		return std::filesystem::is_symlink(std::filesystem::path(path));
	}

	std::string absoulutePath(std::string const& path)
	{
		assert(std::filesystem::exists(path));
		return std::filesystem::absolute(std::filesystem::path(path)).string();
	}

	std::string toString(float val)
	{
		return std::to_string(val);
	}

	std::string toString(double val)
	{
		return std::to_string(val);
	}
	
	std::string toString(uint32_t val)
	{
		return std::to_string(val);
	}

	std::string toString(int32_t val)
	{
		return std::to_string(val);
	}

	std::string toString(long val)
	{
		return std::to_string(val);
	}

	std::string toStr(std::vector<float> const& vector) {

		std::string retVal = "[";
		for (int i = 0; i < vector.size(); ++i) {
			retVal += std::to_string(vector[i]) + ",";
		}
		retVal += "]";
		return retVal;
	}

	std::string toString(glm::vec4 const& v)
	{
		auto s = "[" + std::to_string(v.x) + ", " + std::to_string(v.y) + ", " + std::to_string(v.z) + ", " + std::to_string(v.w) + "]";
		return s;
	}

	std::string toString(glm::vec3 const& v)
	{
		auto s = "[" + std::to_string(v.x) + ", " + std::to_string(v.y) + ", " + std::to_string(v.z) + "]";
		return s;
	}

	std::string toString(glm::vec2 const& v)
	{
		auto s = "[" + std::to_string(v.x) + ", " + std::to_string(v.y) + "]";
		return s;
	}

	std::string toString(glm::mat4 const& m)
	{
		auto s = std::string();
		for (int i = 0; i < 4; ++i)
		{
			s += toString(m[i]);
			if (i < 3)
				s += "\n";
		}
		return s;
	}

	glm::vec3 interpolate(glm::vec3 const& A, glm::vec3 const& B, float t)
	{
		return B * t + A * (1.f - t);
	}

	glm::quat interpolate(glm::quat const& A, glm::quat const& B, float t)
	{
		return glm::slerp(A, B, t);
	}

	float interpolate(float A, float B, float t)
	{
		return (B - A) * t + A;
	}

	void printOutErr(std::string const& msg)
	{
	#ifdef WANT_MVS_OUTPUT
		auto str = "(" + Time::getSystemTime() + ")[ERROR]:" + msg + "\n";
		OutputDebugString(str.c_str());
	#else
		#ifdef PLATFORM_WINDOWS
				std::cout << "(" << Time::getSystemTime() << ")[ERROR]:" << msg << std::endl << std::flush;
		#else
				std::cout << "\033[1;31m(" << Time::getSystemTime() << ")[ERROR]:\033[0m" << msg << std::endl << std::flush;
		#endif
	#endif
	}

	void printOutWar(std::string const& msg)
	{
	#ifdef WANT_MVS_OUTPUT
		auto str = "(" + Time::getSystemTime() + ")[WARNING]:" + msg + "\n";
		OutputDebugString(str.c_str());
	#else
		#ifdef PLATFORM_WINDOWS
			std::cout << "(" << Time::getSystemTime() << ")[WARNING]:" << msg << std::endl << std::flush;
		#else
			std::cout << "\033[1;33m(" << Time::getSystemTime() << ")[WARNING]:\033[0m" << msg << std::endl << std::flush;
		#endif
	#endif
	}

	void printOut(std::string const& msg)
	{
	#ifdef WANT_MVS_OUTPUT
		auto str = "(" + Time::getSystemTime() + ")[INFO]:" + msg + "\n";
		OutputDebugString(str.c_str());
	#else
		#ifdef PLATFORM_WINDOWS
			std::cout << "(" << Time::getSystemTime() << ")[INFO]:" << msg << std::endl << std::flush;
		#else
			std::cout << "\033[1;32m(" << Time::getSystemTime() << ")[INFO]:\033[0m" << msg << std::endl << std::flush;
		#endif
	#endif
	}

	void printOutGL(std::string const& msg)
	{
	#ifdef WANT_MVS_OUTPUT
		auto str = "(" + Time::getSystemTime() + ")[GRAPHIC_API]:" + msg + "\n";
		OutputDebugString(str.c_str());
	#else
		#ifdef PLATFORM_WINDOWS
			std::cout << "(" << Time::getSystemTime() << ")[GRAPHIC_API]:" << msg << std::endl << std::flush;
		#else
			std::cout << "\033[1;35m(" << Time::getSystemTime() << ")[GRAPHIC_API]:\033[0m" << msg << std::endl << std::flush;
		#endif
	#endif
	}

	void splitString(std::string const& target, std::string const& split_by, std::vector<std::string>& ret)
	{
		ret.clear();
		if (split_by.empty() || target.empty()) {
			return;
		}

		auto begginingIndex = size_t(0);
		std::vector<int> delimeterIndexes;
		//find delimeter indexes within the string
		while (begginingIndex < target.size()) {
			begginingIndex = target.find(split_by, begginingIndex);
			if (begginingIndex == std::string::npos)
				break;
			delimeterIndexes.push_back(int(begginingIndex));
			begginingIndex += split_by.size();
		}

		// no delimeter was found ..
		if (delimeterIndexes.empty()) {
			return;
		}

		//handle beginning substing
		{
			auto substring = target.substr(0, delimeterIndexes.front());
			if (!substring.empty()) {
				ret.push_back(substring);
			}
		}
		//handle all middle substing
		{
			for (int i = 1; i < delimeterIndexes.size(); ++i) {
				auto substring = target.substr(delimeterIndexes.at(i - 1) + split_by.size(), delimeterIndexes.at(i) - delimeterIndexes.at(i - 1) - split_by.size());
				if (!substring.empty()) {
					ret.push_back(substring);
				}
			}
		}
		//handle end substing
		{
			auto substring = target.substr(delimeterIndexes.back() + split_by.size(), target.size() - delimeterIndexes.back());
			if (!substring.empty()) {
				ret.push_back(substring);
			}
		}
	}

	void replaceWithinString(std::string const& target, std::string const& toReplace, std::string const& replaceWith, std::string& ret)
	{
		auto finalStr = std::string();
		auto retvals = std::vector<std::string>();
		splitString(target, toReplace, retvals);
		if (retvals.empty())
		{
			ret = target;
			return;
		}

		for (auto& retval : retvals)
		{
			finalStr += retval + replaceWith;
		}
		ret = finalStr.substr(0, finalStr.size() - replaceWith.size());
	}

	float convertRange(float curr_val, float old_min, float old_max, float new_min, float new_max) 
	{
		return (((curr_val - old_min) * (new_max - new_min)) / (old_max - old_min)) + new_min;
	}

	void doIndexing(std::vector<glm::vec3> const& vertices, std::vector<glm::vec2> const& uvs, std::vector<glm::vec3>const& normals, std::vector<int> const& indices, std::vector<glm::vec3>& indexed_vert, std::vector<glm::vec2>& indexed_uvs, std::vector<glm::vec3>& indexed_normals)
	{
		for (unsigned long i = 0; i < indices.size(); ++i)
		{
			long index = indices[i];

			glm::vec3 indexed_vertex = vertices[index];
			glm::vec2 indexed_uv = uvs[index];
			glm::vec3 indexed_normal = normals[index];

			indexed_vert.push_back(indexed_vertex);
			indexed_uvs.push_back(indexed_uv);
			indexed_normals.push_back(indexed_normal);
		}
	}

	std::string stringID(uint64_t id)
	{
		return std::to_string(id);
	}
}