//! libUtils
#include <libUtils/Time.hpp>

#include <ctime>   // localtime
#include <sstream> // stringstream
#include <iomanip> // put_time
#include <string>  // string


namespace libUtils
{
	Time::Time()
		: m_state(ClockState::STOPPED)
		, m_pausedMiliseconds(0.)
	{
	}

	void Time::start()
	{
		if (m_state != ClockState::STOPPED) 
			return;
		
		m_pausedMiliseconds = .0;
		m_state = ClockState::RUNNING;
		m_startTimePoint = std::chrono::system_clock::now();
	}

	void Time::pause() 
	{
		if (m_state != ClockState::RUNNING)
			return;
		m_state = ClockState::PAUSED;
		m_pausedMiliseconds = getMiliseconds();
	}

	void Time::resume()
	{
		if (m_state != ClockState::PAUSED)
			return;
		
		m_state = ClockState::RUNNING;
		//time is saved, now overide starting timepoint
		m_startTimePoint = std::chrono::system_clock::now();
	}

	void Time::stop() 
	{
		if (m_state == ClockState::STOPPED)
			return;

		m_state = ClockState::STOPPED;
		m_pausedMiliseconds = .0;
	}

	bool Time::paused() const
	{
		return m_state == ClockState::PAUSED;
	}

	bool Time::stopped() const
	{
		return m_state == ClockState::STOPPED;
	}

	double Time::getTime() const
	{
		if (m_state == ClockState::STOPPED)
			return 0;

		//if timer is paused, then return only the paused time
		if (m_state == ClockState::PAUSED)
			return m_pausedMiliseconds;
		
		return getMiliseconds();
	}

	std::string Time::getSystemTime()
	{
		return toString(std::chrono::system_clock::now());
	}

	std::string Time::toString(std::chrono::time_point<std::chrono::system_clock> const& timepoint)
	{
		std::stringstream ss;
		auto time = std::chrono::system_clock::to_time_t(timepoint);
		ss << std::put_time(std::localtime(&time), "%X"); //"%Y-%m-%d %X"
		return ss.str();
	}

	double Time::getMiliseconds() const
	{
		auto now = std::chrono::system_clock::now();
		auto duration = std::chrono::duration<double, std::milli>(now - m_startTimePoint);
		return duration.count() + m_pausedMiliseconds;
	}

	std::string Time::getTimeString() const
	{
		auto time = getTime();
		auto tp = std::chrono::system_clock::now() - m_startTimePoint;
		return toString(std::chrono::time_point<std::chrono::system_clock>(tp));
	}
}